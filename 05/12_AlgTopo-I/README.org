#+TITLE: Algebraic Topology I - README
#+AUTHOR: Han-Miru Kim
#+PROPERTY: header-args :tangle ./header.tex

* About
This file contains general information about the lecture notes aswell as the configuration for my Algebraic Topology notes.

It is an Emacs Org Mode file which is edited and then /tangled/ with the ~header.tex~ file, which I do not touch manually.

The benefit of this is that comments, which would have been written in plaintext inside ~header.tex~ can now be written using Org Mode Formatting, allowing for literate configuration.

For example, I can use headings, use formatted text, insert tables, source code blocks or even images, which would not be possible in LaTeX comments.

I structure my notes as follows:
Instead of a ~main.tex~ file, I have a ~main.org~ file which I then export to latex via ~M-x org-export-dispatch l o~.

Since I have just started using Emacs, my configuration does not quite allow me to take notes in real-time.
This means I still take my notes in Vim by writing ~.tex~ files and putting them in the ~./files~ directory, from where they will be sourced and can be compiled using my existing vim-tex setup.


** How to use
After changing the configuration, place the point at the top of this document (~g g~ in evil-normal mode) and do ~C-c C-c~ to refresh. Then execute ~M-x org-babel-tangle~, which will update ~header.tex~.
It should output ~Tangled n code blocks from README.org~

To use source code blocks without having them exported to ~header.tex~, use the ~:tangle no~ option.

** TODO
- I plan on trying out an Emacs-only solution which will let me export this document in ~.html~ format and upload it on my webpage.

- As of now, I use different configuration files for every lecture. This means that there is alot of code redundancy as the configuration only differs by about 5% (if at all) between different lecture notes.
But since the README.org files are only a couple kilobytes in size each, I should be fine.



* Package Imports
** Font Settings

#+BEGIN_SRC latex
  \usepackage[utf8]{inputenc}
  \usepackage{lmodern} % Latin Modern
  \usepackage{fontenc}[t1]
  \usepackage{microtype}
#+END_SRC

** Content

#+BEGIN_SRC latex
  \usepackage{caption}
  \usepackage{enumerate}
  \usepackage{hyperref}
  %\usepackage{wrapfig}
  \usepackage{array}   
  \newcolumntype{L}{>{$}l<{$}} % math mode l
  \usepackage[framemethod=TikZ]{mdframed}
  \usepackage[customcolors,norndcorners]{hf-tikz}
#+end_src

** Page Settings

#+BEGIN_SRC latex
  \usepackage{geometry}
  \usepackage{fancyhdr}
          \pagestyle{fancy}
          \fancyhf{}
          \hoffset = -0.45 in
          \voffset = -0.3 in
          \textwidth = 500pt
          \textheight = 650pt
          \setlength{\headheight}{25.6pt}
          \setlength{\headwidth}{500pt}
          \setlength{\parindent}{0pt}
          \marginparwidth = 0pt
          \fancyhead[L]{\rightmark}
          \fancyhead[R]{\today}
          \fancyfoot[L]{Han-Miru Kim}
          \fancyfoot[C]{\href{mailto:kimha@student.ethz.ch}{kimha@student.ethz.ch}}
          \fancyfoot[R]{\thepage}
#+END_SRC

** Math
The ~amsmath~ and ~amssymb~ form a basic collection of mathematic packages.
Insert more explanations here.
#+BEGIN_SRC latex
  \usepackage{amsmath}
  \usepackage{amssymb} 
  \usepackage{amsthm} 
  \usepackage{dsfont}
  \usepackage{amsfonts}
  \usepackage{mathdots}
  \usepackage{mathrsfs}
  \usepackage{bm} % bold math
  \usepackage{empheq} % colored equations
  \usepackage[most]{tcolorbox}
  \usepackage{mathtools}
  \usepackage{faktor} % Quotients
  \usepackage{xcolor}
  \usepackage{stmaryrd}
  \let\tempphi\phi
  \let\phi\varphi % curly
  \let\varphi\phi % ugly
#+END_SRC
The last three lines swap the commands ~\phi~ and ~\varphi~.
I do this becuase curly one (now ~\phi~) looks much than the default.

** Graphics
#+BEGIN_SRC latex
  \usepackage{graphicx}
  \usepackage{tikz}
      \usetikzlibrary{cd}
#+END_SRC

** Formatting
Packages for sectioning and positioning.
#+BEGIN_SRC latex
  \usepackage{sectsty}
      \sectionfont{\LARGE}
      \subsectionfont{\Large}
      \subsubsectionfont{\large}
      \paragraphfont{\large}
  %\usepackage{adjustbox}  %\adjustbox{scale=2,center}{}
  %\usepackage{paracol}
  \usepackage{float}

  #+END_SRC

* Macros
** Symbols
~\DeclareMathOperator~ defines commands that print out with the standard text-font over the italic math-fonts used for variables etc.

#+BEGIN_SRC latex
  \newcommand{\N}{\mathbb{N}}
  \newcommand{\Z}{\mathbb{Z}}
  \newcommand{\Q}{\mathbb{Q}}
  \newcommand{\R}{\mathbb{R}}
  \newcommand{\IS}{\mathbb{S}}
  \newcommand{\ID}{\mathbb{D}}
  \newcommand{\IT}{\mathbb{T}}
  \newcommand{\C}{\mathbb{C}}
  \newcommand{\K}{\mathbb{K}}
  \newcommand{\F}{\mathbb{F}}
  \newcommand{\IP}{\mathbb{P}}

  \DeclareMathOperator{\charac}{char}
  \DeclareMathOperator{\degree}{deg}  

  \newcommand{\del}{\partial}
  \newcommand{\es}{\text{\o}}
  #+END_SRC

** Delimiters

#+BEGIN_SRC latex
  \DeclarePairedDelimiter\abs{\vert}{\vert}
  \DeclarePairedDelimiter\ceil{\lceil}{\rceil}
  \DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
  \DeclarePairedDelimiter\Norm{\vert\vert}{\vert\vert}
  \DeclarePairedDelimiter\scal{\langle}{\rangle}
  \DeclarePairedDelimiter\dbrack{\llbracket}{\rrbracket}

#+END_SRC

** Math Operators

#+BEGIN_SRC latex
  %   Algebra
  \newcommand{\iso}{\cong}
  \newcommand{\mono}{\rightarrowtail}
  \newcommand{\epi}{\twoheadrightarrow}

  \DeclareMathOperator{\Bil}{Bil}
  \DeclareMathOperator{\ev}{ev}
  \DeclareMathOperator{\Hom}{Hom}
  \DeclareMathOperator{\kernel}{ker}
  \DeclareMathOperator{\coker}{coker}
  \DeclareMathOperator{\image}{im}
  \DeclareMathOperator{\spn}{span}
  \DeclareMathOperator{\rank}{rank}
  \DeclareMathOperator{\rang}{rang}
  \DeclareMathOperator{\id}{id}
  \DeclareMathOperator{\End}{End}
  \DeclareMathOperator{\Eig}{Eig}
  \DeclareMathOperator{\Hau}{Hau}
  \DeclareMathOperator{\trace}{tr}
  \DeclareMathOperator{\rot}{rot} 
  \DeclareMathOperator{\ad}{ad}
  \DeclareMathOperator{\Mat}{Mat}
  \DeclareMathOperator{\GL}{GL}
  \DeclareMathOperator{\lcd}{lcd}
  \DeclareMathOperator{\ggT}{ggT}
  \DeclareMathOperator{\Aut}{Aut}
  \DeclareMathOperator{\sgn}{sgn}

  \DeclareMathOperator{\Sd}{Sd}

  %   Cat
  \newcommand*{\Top}{\text{\sffamily Top}}
  \newcommand*{\Ab}{\text{\sffamily Ab}}
  \newcommand*{\hTop}{\text{\sffamily hTop}}
  \newcommand*{\Grp}{\text{\sffamily Grp}}
  \newcommand*{\Set}{\text{\sffamily Set}}
  \newcommand*{\Ring}{\text{\sffamily Ring}}
  \newcommand*{\Comp}{\text{\sffamily Comp}}
  \newcommand*{\hComp}{\text{\sffamily hComp}}
  %\newcommand{\coprod}{\amalg}

  % Analysis
  \DeclareMathOperator{\grad}{grad}
  \DeclareMathOperator{\graph}{graph}
  \DeclareMathOperator{\supp}{supp}


  % Formatting 
  \newsavebox\MBox
  \newcommand\cunderline[2][red]{ % Colored underline
    {\sbox\MBox{$#2$}%
    \rlap{\usebox\MBox}\color{#1}\rule[-1.2\dp\MBox]{\wd\MBox}{0.5pt}}
  } 

#+END_SRC

* Environments

** Default Environemnts
Changing the behaviour of the ~enumerate~ environment to use letters over roman numerals.

#+BEGIN_SRC latex
  \renewcommand{\labelenumi}{(\alph{enumi})}
  #+END_SRC

** Theorem
Define the environments using the ~amsthm~ package.
They are easy to set up and respect the section numbering, but I prefer the fancy coloured boxes using ~mdframed~.
#+BEGIN_SRC latex
  %\newtheorem{thm}{Theorem}[section]
  %\newtheorem{cor}{Corollary}[thm]
  %\newtheorem{lem}[thm]{Lemma}
  %\newtheorem{prop}{Proposition}[thm]

  %\theoremstyle{definition}
  %\newtheorem{dfn}[thm]{Definition}
  %\newtheorem{rem}[thm]{Remark}
  %\newtheorem{note}[thm]{Note}
  %\newtheorem{ex}[thm]{Example}

  #+END_SRC

** Math boxes
From https://tex.stackexchange.com/questions/20575/attractive-boxed-equations:
Requires the the ~empheq~ and ~tcolorbox~ packages.
Makes the cool blue equation boxes.
#+BEGIN_SRC latex
  \newtcbox{\bluebase}[1][]{%
        nobeforeafter, math upper, tcbox raise base,
        enhanced, colframe=black,
        colback=blue!20, boxrule=0.5pt,
        sharp corners,
        #1
  }
#+END_SRC

You can then write the equations as follows

#+BEGIN_SRC latex :tangle no
\begin{empheq}[box=\bluebase]{align*}
  \nabla \cdot E = \frac{\rho}{\epsilon_0}
\end{empheq}
#+END_SRC

** Fancy Boxes
*** Setup
Taken from this [texblog post](https://texblog.org/2015/09/30/fancy-boxes-for-theorem-lemma-and-proof-with-mdframed/)

Setup numbering system and define a function that creates environment.
~\newenvironment~ takes one optional and three necessary arguments.
- The environment name (that which is put inside ~\begin{###}~)
- The Title name which is printed on the page
- The colour of the box. I use colors in the format ~blue!50!green!50~ etc.

#+BEGIN_SRC latex
  \newcounter{theo}[section]
  \renewcommand{\thetheo}{\arabic{section}.\arabic{theo}}

  \newcommand{\defboxenv}[3]{
    \newenvironment{#1}[1][]{
      \refstepcounter{theo}
      \ifstrempty{##1} { % no Title
        \mdfsetup{
          frametitle={
            \tikz[baseline=(current bounding box.east), outer sep=0pt]
            \node[anchor=east,rectangle,fill=#3]
            {\strut #2~\thetheo};
          }
        }
      }{ % else: with Title
        \mdfsetup{
          frametitle={
            \tikz[baseline=(current bounding box.east),outer sep=0pt]
            \node[anchor=east,rectangle,fill=#3]
            {\strut #2~\thetheo:~##1};
          }
        }
      } % either case
      \mdfsetup{
        linecolor=#3,
        innertopmargin=0pt,
        linewidth=2pt,
        topline=true,
        frametitleaboveskip=\dimexpr-\ht\strutbox\relax,
      }
      \begin{mdframed}[]\relax
      }{ % post env command
      \end{mdframed}
    }
  }
#+END_SRC

*** Environments
#+BEGIN_SRC latex
  \defboxenv{dfn}{Definition}{orange!50}
  \defboxenv{edfn}{(Definition)}{orange!20}
  \defboxenv{cor}{Corollary}{blue!20!green!20!white}
  \defboxenv{thm}{Theorem}{blue!40}
  \defboxenv{lem}{Lemma}{blue!20}
  \defboxenv{xmp}{Example}{black!20}
  \defboxenv{prop}{Proposition}{blue!30}
  \defboxenv{exr}{Exercise}{black!50}
  \defboxenv{rem}{Remark}{red!20}
#+END_SRC
