\section{The homotopy axiom}

Recall that the fundamental group $\pi_1. \Top_{\ast}\to \Grp$ had homotopy-preserving properties that let us interpret it as a functor $\pi_1: \hTop \to \Grp$.

We wish to show that $H_n:\Top \to \Ab$ has some homotopy-preserving properties aswell, which will let us interpret $H_n$ as a functor $\hTop \to \Ab$.

In particular we want to show the \textbf{homotopy axiom}, which states that for maps $f,g: X \to Y$
\begin{align*}
  [f] = [g] \implies H_n(f) = H_n(g) \quad \forall n \geq 0
\end{align*}
The terminology will be explained when we cover the \emph{Eilenberg-Steenrod axioms}.

%In the case of the fundamental group, two maps are \emph{homotopic}, if there exists a map $h: I^{2} \to X$ such that

\begin{prop}[Dimension axiom]
  Let $X$ be a one-point space $\{\ast\}$. 
  Then $H_n(X) = 0$ for all $n \geq 0$.
\end{prop}
\begin{proof}
There is only the constant $n$-simplex $\sigma_n: \Delta^{n} \to \{\ast\}$, meaining $C_n(X) = \scal{\sigma_n} \iso \Z$ for all $n \geq 0$. 
Moreover, $\del_0(\sigma_0) = 0$ and $\del_1(\sigma_1) = 0$ but $\del_2(\sigma_2) = \sigma_1$.
More generally, for all $n > 0$, we have
\begin{align*}
  \del_n(\sigma_n) = \left\{\begin{array}{ll}
    0 & \text{ if $n$ is odd} \\
    \sigma_{n-1}  &  \text{ if $n$ is even}
  \end{array} \right.
\end{align*}
Therefore
\begin{align*}
  H_n(X) = \faktor{\kernel \del_n}{\image \del_{n+1}} \iso
  \left\{\begin{array}{ll}
    \faktor{\Z}{\Z} = 0 & \text{ if $n$ is odd}\\
    \faktor{0}{0} =0 & \text{ if $n$ is even}
  \end{array} \right.
\end{align*}
\end{proof}


\begin{prop}[]
Let $X$ be a topological space. 
Let $\{X_{\lambda} \big\vert \lambda \in \Lambda\}$ denote the path components of $X$. 
Then for every $n \geq 0$ one has
\begin{align*}
  H_n(X)  \iso \bigoplus_{\lambda \in \Lambda} H_n(X_ljk  )
\end{align*}
\end{prop}
It therefore suffices to compute $H_n(X)$ only on the path-connected spaces $X$.

However, computing $H_n(X)$ for $n > 0$ is generally difficult, but it is always possible to compute $H_0(X)$.


\begin{prop}[]
If $X$ is a non-empty path connected space, then $H_0(X) = \Z$.
A generator is given by $\scal{x}$ for any $x \in X$ and if $x,y \in X$, then $\scal{x} = \scal{y}$.

Moreover if $\scal{c}$ is any generator, then $\scal{c} = \scal{x}$ for some $x \in X$.
\end{prop}
\begin{proof}
  As $\del_0: C_0(X) \to 0$, $Z_0(X) = C_0(X)$.
  As $0$-cycles can be identified with their image, a general $0$-chain $c \in C_0(X)$ is of the form
  \begin{align*}
    c = \sum_{x \in X}m_x x, \quad m_x \in Z
  \end{align*}
  where all but finitely of $m_x$ are zero.
  We claim that
  \begin{align*}
    B_0(X) = \left\{
      \sum_{x \in X}m_x x \big\vert \sum_{x \in X}m_x = 0
    \right\}
  \end{align*}
  For ``$\supseteq$'', assume $\sum_{i = 1}^{n}m_ix_i = 0$.
  To pick a $1$-chain $a \in C_1(X)$ such that $\del(a) = c$, chose any point $p \in X$.
  A $\Delta^{1}$ is isomorphic to the interval $I$, we can identify $1$-simplices with paths.
  As $X$ is path connected, we consider a path $\sigma_ii$ (i.e. a $1$-simplex) starting at $p$ and ending at $x_i$.
  Then $\del \sigma_i = x_i - p \in C_0(X)$.

  Set $a := \sum_{i=1}^{n}m_i \sigma_i$. Then
  \begin{align*}
    \del a = \del \left(
      \sum_{i=1}^{n}m_i \sigma_i
    \right)
    =
    \sum_{i=1}^{n}m_i \del \sigma_i
    =
    \sum_{i=1}^{n}m_i x_i - \underbrace{\sum_{i=1}^{n}m_i}_{=0} p
    = c
  \end{align*}
  Now for ``$\subseteq$'' in the claim, let $d \in B_0(X)$.
  So there exists a $1$-chain $b \in C_1(X)$ such that $\del b = d$.
  Since the general form of a $1$-chain is
  \begin{align*}
    b = \sum_{j=1}^{k}l_j \tau_j, \quad \text{where} \quad \tau_j: \Delta^{1} \to X, \quad l_i \in \Z
  \end{align*}
  we have
  \begin{align*}
    d = \del b = \sum_{j=1}^{k}l_j (\tau_j(e_1) - \tau_j(e_0))
  \end{align*}
  so every coefficient $l_j$ appears twice with opposite sign satisfying the condition in the RHS of the claim.


  Therefore we have a short exact sequence
  \begin{align*}
    0 \to B_0(X) \hookrightarrow Z_0(X) \stackrel{\phi}{\to} \Z \to 0
  \end{align*}
  where $\phi$ is the map extracting the coefficients
  \begin{align*}
    \phi: Z_0(X) = C_0(X) \to \Z, \quad \sum_{x \in X}x \mapsto  \sum_{x \in X}m_x
  \end{align*}
  thus $H_0(X) \iso \image \phi = \Z$.


  The rest of the proof is trivial.
\end{proof}


\begin{cor}[]
Let $X,Y$ be path connected spaces and $f: X \to Y$ continuous.
Then $H_0(f) : H_0(X) \to H_0(Y)$ maps generators of $H_0(X)$ to generators of $H_0(Y)$.
\end{cor}

\begin{prop}[]\label{prop:homology-interval}
Let $X$ be a topological space and define includsions $\iota,j: X \to X \times I$ with
\begin{align*}
  \iota(x) = (x,0), \quad j(x,) = (x,1)
\end{align*}
Then $H_n(i) = H_n(j)$.
\end{prop}
There is a very nice proof using the \emph{acyclic models theorem} which is presented at the end of the course.
We give a more elementary proof below.


\begin{lem}[]\label{lem:homology-p}
Let $f,g: X \to Y$ continuous. Assume for each $n \geq -1$ there is a homomorphism
\begin{align*}
  P: C_n(X) \to C_{n+1}(Y)
\end{align*}
with
\begin{align*}
  f_{\#} - g_{\#} = \del P + P \del
\end{align*}

Then $H_n(f) = H_n(g)$ for all $n \geq 0$
\end{lem}
The maps $P_n$ look like this
\begin{center}
\begin{tikzcd}[] 
  \ldots
  \arrow[]{r}{}
  &
  C_{n+1}(X)
  \arrow[]{r}{\del}
  &
  C_n(X)
  \arrow[]{r}{\del}
  \arrow[]{dl}{P}
  \arrow[]{d}{f_{\#}-g_{\#}}
  &
  C_{n-1}(X)
  \arrow[]{r}{}
  \arrow[]{dl}{P}
  &
  \ldots
  \\
  \ldots
  \arrow[]{r}{}
  &
  C_{n+1}(Y)
  \arrow[]{r}{\del}
  &
  C_n(Y)
  \arrow[]{r}{\del}
  &
  C_{n-1}(Y)
  \arrow[]{r}{}
  &
  \ldots
\end{tikzcd}
\end{center}
Beware that this diagram does not necessarily commute!

\begin{proof}
  Let $c \in \Z_n(X)$. 
  To say that $H_n(f) = H_n(g):H_n(X) \to H_n(Y) = \faktor{Z_n(Y)}{B_n(Y)}$, means we need to find an element $d \in C_{n+1}(Y)$ such that $\del d = f_{\#}(c) - g_{\#}(c)$.
  
  \begin{align*}
    (f_{\#} - g_{\#})c = (\del P + P \del)c = \del P c \in B_n(Y)
  \end{align*}
  thus we can set $d = Pc \in C_{n+1}(Y)$ and the result follows.
\end{proof}


The first step in the proof of Proposition~\ref{prop:homology-interval}is the following observation:

The square $\Delta^{1} \times I$ is the union of two triangles $\Delta^{2}$.
The prism $\Delta^{2} \times I$ is the union of three tetraeders $\Delta^{3}$.
More generally:

\begin{lem}[]
$\Delta^{n} \times I$ is the union of $n+1$ copies of $\Delta^{n+1}$.
\end{lem}
\begin{proof}
Missing
\end{proof}

\begin{proof}[Proof Proposition]
Missing
\end{proof}


\begin{thm}[The homotopy axiom]
  Let $f,g: X \to Y$ homotopic.
  Then $H_n(f) = H_n(g)$ for all $n \geq 0$. Thus, for each $n \geq 0$, $H_n: \hTop \to \Ab$ is a functor.
\end{thm}
\begin{proof}
Let $F: f \sim g$ be a homotopy. Using the maps $\iota,j$ from Proposition \ref{prop:homology-interval} we have
\begin{align*}
  f = F \circ i, \quad g = F \circ j
\end{align*}
By the functor properties of $H_n$:
\begin{align*}
  H_n(f) = H_n(F \circ \iota) = H_n(F) \circ H_n(\iota) = H_n(F) \circ H_n(j) = H_n(g)
\end{align*}
\end{proof}

\begin{cor}[]
If $X$ and $Y$ have the same homotopy type, then $H_n(X) \iso H_n(Y)$ for all $n \geq 0$, where the isomorphism is induced by any homotopy equivalence.
\end{cor}

\begin{cor}[]
If $X$ is contractible, then $H_n(X) = 0$ for all $n > 0$.
\end{cor}
