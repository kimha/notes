\section{The degree}

Recall when we defined the degree of a loop $u: (I,\del I) \to (S^{1},1)$ by its winding number.

This construction can be though of as a map 
\begin{align*}
  \pi_1(S^{1},1) \iso \underbrace{\Hom_{\hTop_{\ast}}((S^{1},1),(S^{1},1))}_{= [(S^{1},1),(S^{1},1)]} \to \Z
\end{align*}
We can generalize this to continuous maps from an $n$-sphere to itself.
Since we want to work with homology, we want to get rid of the basepoints.


\begin{prop}[]
Let $X$ be path connected and let $\xi(X,p) \to [S^{1},X]$ be the function that sends the path class $[u]$ to the free homotopy class of the map $\hat{u}:S^{1}$ to $X$ given by
\begin{align*}
  \hat{u}(e^{2 \pi i s}) := u(s), \quad s \in I
\end{align*}
This function is surjective and if $\pi_1(X,p)$ is abelian, then $\xi$ is an isomorphism and hence $\pi_1(X,p) \iso [S^{1},X]$.
\end{prop}

Note that any group homomorphism $\phi: \Z \to \Z$ can be described by a multiplication by some integer, i.e. $\phi = x \mapsto mx$ for some $m \in \Z$.

\begin{dfn}[]
Let $n \geq 1$ and $f: S^{n} \to S^{n}$, continuous.
Then $H_n(f): H_n(S^{n}) \to H_n(S^{n})$ is a group homomorphism.
As both groups are isomorphic to $\Z$, they are multiplication by some integer.
This integer is called the \textbf{degree} of $f$ and denoted $\degree f$.
Thus
\begin{align*}
  H_n(f) \scal{c} = \degree f \scal{c}, \quad \forall c \in C_n(S^{n})
\end{align*}
\end{dfn}
The homotopy axiom implies that $\degree: [S^{n},S^{n}] \to \Z$ is well-defined, since if $f \sim g$ are homotopic, then $H_n(f) = H_n(g) \implies \degree f = \degree g$.

One can show that this definition of degree agrees with the old definition of degree of a loop in $S^{1}$.

This follows from the commutativity of the diagram of the Hurewicz map $h: \pi(S^{1},p) \to H_1(S^{1})$.
As $\pi_1(S^{1},p)$ is abelian and $H_1(S^{1})$ is its abelianisation, $h$ is an isomorphism.

Similar to the degree of loops, the new degree function satisfies some nice properties
\begin{prop}[]
Let $n \geq 1$ and $f,g:S^{n} \to S^{n}$ continuous.
Then
\begin{itemize}
  \item $\degree g \circ f = \degree g \degree f$
  \item $\degree \id = 1$
  \item If $f$ is constant, then $\degree f = 0$
  \item $f \sim g \implies \degree f = \degree g$
  \item If $f$ is a homotopy equivalence, then $\degree f = \pm 1$.
\end{itemize}
\end{prop}
All properties follow from the fact that $H_n$ is a functor.

The third property comes form the fact that that a constant function can be factored as a composition $S^{n} \to \{\ast\} \to S^{n}$.


The following result is far less obvious
\begin{prop}[]
Let $n \geq 1$ and let $A \in O(n+1)$ be an orthogonal transformation. 
Set $f := A|_{S^{n}}$. Then $\degree f = \det A$.
\end{prop}
