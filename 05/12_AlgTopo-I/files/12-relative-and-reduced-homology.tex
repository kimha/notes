\section{Relative homology and reduced homology}
We extend $H_n$ to a functor $\Top^{2} \to \Ab$.

The following is trivial to prove.
\begin{lem}[]
  Let $X' \subseteq X$ with inclusion mapping $\iota$.
  Then for every $n \geq 0$ the map $\iota_{\#}C_n(X') \to C_n(X)$ is an injection.
\end{lem}
This means we can think of $C_{\bullet}(X')$ as a subcomplex of $C_{\bullet}(X)$, so we have a short exact sequence of complexes
\begin{align*}
  0 \to C_{\bullet}(X') \to C_{\bullet}(X) \faktor{C_{\bullet}(X)}{C_{\bullet}(X')} \to 0
\end{align*}


\begin{dfn}[]
Let $X' \subseteq X$ be a subspace. 
We define the \textbf{relative homology groups} $H_n(X,X')$ of the pair $(X,X')$ to be the homology of the complex $\faktor{C_{\bullet}(X)}{C_{\bullet}(X')}$.
\end{dfn}

The next result follows from the long exact sequence theorem and its naturality (from Chapter 11).

\begin{prop}[The exact sequence axiom]
  Let $X'$ be a subspace of $X$. Then there is a long exact sequence
  \begin{align*}
    \ldots \to H_n(X') \to H_n(X) \to H_n(X,X') \stackrel{\delta}{\to} H_{n-1}(X') \to \ldots
  \end{align*}
  Moreover, if $f: (X,X') \to (Y,Y')$ is a map of pairs then there is a commutative diagram
  \begin{center}
  \begin{tikzcd}[]
    \ldots \arrow[]{r}{}
    & H_n(X') \arrow[]{r}{} \arrow[]{d}{}
    & H_n(X) \arrow[]{r}{} \arrow[]{d}{}
    & H_n(X,X') \arrow[]{r}{} \arrow[]{d}{}
    & H_{n-1}(X') \arrow[]{r}{} \arrow[]{d}{}
    & \ldots
    \\
    \ldots \arrow[]{r}{}
    & H_n(Y') \arrow[]{r}{} 
    & H_n(Y) \arrow[]{r}{} 
    & H_n(Y,Y') \arrow[]{r}{} 
    & H_{n-1}(Y') \arrow[]{r}{} 
    & \ldots
  \end{tikzcd}
  \end{center}
  where all the vertical maps are induced by $f$.
\end{prop}

\begin{rem}[]
  This construction allows us to see homology as a functor $H_n: \Top^{2} \to \Ab$ as the composition of the chain complex functor
  \begin{align*}
    \Top^{2} \to \Comp, \quad (X,X') \mapsto \faktor{C_{\bullet}(X)}{C_{\bullet}(X')}\\
    f:(X,X') \to (Y,Y') \mapsto f_{\#}: \faktor{C_{\bullet}(X)}{C_{\bullet}(X')} \to \faktor{(C_{\bullet}(Y)}{C_{\bullet}(Y')}
  \end{align*}
  with the usual homology functor $H_n: \Comp \to \Ab$.
\end{rem}


There is also another (as we will see shortly, equivalent) way to define this functor. Namely in a similar way to how we defined the homology functor time first time $\Top \to \Ab$.

\begin{dfn}[]
  Define the group of \textbf{relative $n$-cycles mod $X'$} to be
  \begin{align*}
    Z_n(X,X') = \left\{
      c \in C_n(X) \big\vert \del c \in C_{n-1}(X')
    \right\}
  \end{align*}
  and the group of \textbf{relative $n$-boundaries mod $X'$}
  \begin{align*}
    B_n(X,X') 
    &= \left\{
      c \in C_n(X) \big\vert c - c' \in B_n(X) \text{ for some }c' \in C_n(X')
    \right\}
    \\
    &= B_n(X) + C_n(X')
  \end{align*}
  then $B_n(X,X') \subseteq Z_n(X,X')$
\end{dfn}

\begin{prop}[]
For alln $n \geq 0$
\begin{align*}
  H_n(X,X') \iso \faktor{Z_n(X,X')}{B_n(X,X')}
\end{align*}
\end{prop}
\begin{proof}
By definition, the boundary operator $\overline{\del}$ of the quotient complex $C_{\bullet}(X)/C_{\bullet}(X')$ is given by
\begin{align*}
  \overline{\del}: c + C_n(X') \mapsto \del c + C_{n-1}(X'), \quad c \in C_n(X), n \geq 0
\end{align*}
thus
\begin{align*}
  \kernel \overline{\del} = \left\{
    c + C_n(X') \big\vert \del c \in C_{n-1}(X')
  \right\}
  = \faktor{Z_n(X,X')}{C_n(X')}
\end{align*}
and
\begin{align*}
  \image \overline{\del}
  =
  \left\{
    c + C_n(X') \big\vert c \in B_n(X)
  \right\}
  = \faktor{B_n(X,X')}{C_n(X')}
\end{align*}
By the third isomorphism theorem, the resuult follows.
\end{proof}



\begin{prop}[]
Let $X$ be path connected, and $X' \subseteq X$ non-empty. Then $H_0(X,X') = 0$.
\end{prop}
\begin{proof}
  We use the previous proposition and show that $\faktor{Z_0(X,X')}{B_0(X,X')} = 0$.

  Since $\forall c \in C_0(X), \del c = 0 \in C_{-1}(X')$, we have $Z_0(X,X') = C_0(X)$.

  Recall that
  \begin{align*}
    B_0(X,X') = \{c \in C_0(X) \big\vert c - c' \in B_0(X) \text{ for some } c' \in C_0(X')\}
  \end{align*}

  Let $c = \sum_{x}m_x x \in C_0(X)$ and let $p \in X'$.
  For $x \in X$, chose a path $\sigma_x: \Delta^{1} \to X$ starting $p$ and ending in $x$.
  Then take the $1$-chain
  \begin{align*}
    a = \sum_{x}m_x \sigma_x \in C_1(X) \implies \del a = c - \underbrace{\sum_{x}m_x p}_{=: c'} \quad \text{with} \quad  c' \in C_0(X')
  \end{align*}
  this shows $c \in B_0(X,X')$ and thus
  $B_0(X,X') = C_0(X) = Z_0(X,X')$ giving us $H_0(X,X') = 0$.
\end{proof}

This generelizes to non-path-connected spaces, where the subspace $X'$ contains points in all of the path components.
Moreover, we can find out how many path components $X'$ does \emph{not} touch.

\begin{prop}[]
Let $\{X_{\lambda} \big\vert \lambda \in \Lambda\}$ denote the path components of $X$ and let $X' \subseteq X$ be a subspace.
For each $n \geq 0$, one has
\begin{align*}
  H_n(X,X') \iso \bigoplus_{\lambda \in \Lambda}H_n(X_{\lambda},X_{\lambda} \cap X')
\end{align*}
\end{prop}
\begin{proof}
This is immediate from Problem D.4 and E.6
\end{proof}

A special case of this statement is the following:

\begin{cor}[Additivity axiom]
  Let $(X_{\lambda},X'_{\lambda})_{\lambda \in \Lambda}$ be a family of pairs of spaces. Denote by
  \begin{align*}
    \iota_{\lambda}: (X_{\lambda},X_{\lambda}') \hookrightarrow
    \left(
      \bigsqcup_{\lambda \in \Lambda} X_{\lambda}, \bigsqcup_{\lambda \in \Lambda} X_{\lambda}'
    \right)
  \end{align*}
  the inclusion mappings. 
  Then for all $n \geq 0$, the map
  \begin{align*}
    \sum_{\lambda \in \Lambda}H_n(\iota_{\lambda}):
    \bigoplus_{\lambda \in \Lambda}H_n(X_{\lambda},X_{\lambda}')
    \to 
    H_n \left(
      \bigsqcup_{\lambda \in \Lambda} X_{\lambda}, \bigsqcup_{\lambda \in \Lambda} X_{\lambda}'
    \right)
  \end{align*}
  is an isomorphism.
\end{cor}

\begin{cor}[]
Let $\{X_{\lambda} \big\vert \lambda \in \Lambda\}$ denote the path components of $X$ and $X' \subseteq X$ be a subspace.
The group $H_0(X,X')$ is free abelian with
\begin{align*}
  \text{rank} H_0(X,X') = \text{card}\left\{
    \lambda \in \Lambda \big\vert X_{\lambda} \cap X' = \emptyset
  \right\}
\end{align*}
\end{cor}
\begin{proof}
If $X'$ intersects the path component $X_{\lambda}$, then we already saw that $H_0(X,X') = 0$.

But $X'$ doesn't intersect $X_{\lambda}$, then we are simply computing the the homology group of a path connected space, which is just $H_0(X_{\lambda},X_{\lambda} \cap X') = H_0(X_{\lambda}) = \Z$.
\end{proof}

\begin{cor}[]
Let $(X,p) \in \Top^{\ast}$. Then $H_0(X,p)$ is a free abelian group of rank $r$, where $X$ has $r+1$ path components.
\end{cor}
So the zero-th relative homology groups are easy to compute.
What about the higher ones?
We can show that the higher homotopy groups do not care about single points.
\begin{prop}[]
Let $(X,p) \in \Top^{\ast}$. Then for all $n \geq 1$
\begin{align*}
  H_n(X,p) \iso H_n(X)
\end{align*}
\end{prop}
\begin{proof}
By the exact sequence axiom, there is a long exact sequence
\begin{align*}
  \ldots \to H_n(X') \to H_n(X) \to H_n(X,X') \stackrel{\delta}{\to} H_{n-1}(X') \to  \ldots
\end{align*}
with $X' = \{p\}$.
Let's first consider the case $n \geq 2$.
By the dimension axiom, $H_n(p) = H_{n-1}(p) = 0$, so we are looking at the sequence
\begin{align*}
  0 \to H_n(X) \to  H_n(X,p) \to 0
\end{align*}
which gives the desired isomorphism. 
For $n = 1$, we label the maps as follows 
\begin{align*}
  \underbrace{H_1(p)}_{= 0}\to H_1(X) \stackrel{f}{\to}  H_1(X,p) \stackrel{g}{\to} \underbrace{H_0(p)}_{= \Z} \stackrel{h}{\to} H_0(X) \stackrel{i}{\to} H_0(X,p)
\end{align*}
Here, $f$ is injective and by the fourth point in Example \ref{xmp:exact-sequences}, it is also surjective if and only if $h$ is injective.
Since $H_0(X)$ is free abelian, either $h$ is the zero map or injective.

If $h$ were the zero map, then by exactness $i$ would be injective.

So to complete the proof, we only need to provide a non-zero element in the kernel of $i$.
Such an element is given by the class of the singular $0$-chain $p$ in $H_0(X)$.
\end{proof}

This means that for $n \geq 1$, we can regard $H_n$ as a functor on $\Top_{\ast}$.


\begin{dfn}[]
Let $0 \to A \stackrel{f}{\to} B \stackrel{g}{\to}C \to  0$ be a short exact sequence of abelian groups.
We say that the sequence \textbf{splits} if there exists a map $h: C \to B$ such that $gh = \id_C$. We call $h$ a \textbf{splitting map} of the sequence.
\end{dfn}
The splitting map is not unique.
On problem sheet F, we see that an equivalent definition is asking that $f$ admits a left inverse.

\begin{prop}[]
Let $0 \to A \stackrel{f}{\to} B \stackrel{g}{\to}C \to  0$ be a short exact sequence of abelian groups.

Then the sequence splits if and only if there exists a map $k: B \to  A$ such that $kf = \id_A$.
\end{prop}

\begin{prop}[]
Let $0 \to A \stackrel{f}{\to} B \stackrel{g}{\to}C \to  0$ be a split short exact sequence.
Then $B \iso A \oplus C$.
\end{prop}
\begin{proof}
  Let $h: C \to  B$ such that $gh = \id_C$. We show that $B = \image f \oplus \image h$.

  Let $b \in B$. Then $g(b) \in C$ and
  \begin{align*}
    g(b - hg(b)) = gb - ghg(b) = 0 \implies b - hg(b) \in \kernel g = \image f
  \end{align*}
  So there is an $a \in A$ with $f(a) = b - hg(b)$.
  Thus $b = f(a) + hg(b)$, so $B = \image f + \image h g$.

  It remains to show that $\image f \cap \image h = \{0\}$.
  If $f(a) = x = h(c)$, then $g(x) = gf(a) = 0$, and $g(x) = gh(c) = c$, thus $x = h(c) = 0$.
\end{proof}

The splitting map is not natural, as the isomorphism depends on the splitting map $h$.

\begin{dfn}[]
Let $X$ be a non-empty topological space and $\{\ast\}$ be a singleton space with $j: X \to \{\ast\}$ the unique morphism.

For any map $i: \{\ast\} \to  X$, we have $j \circ i = \id_{\{\ast\}}$.

Thus the induced map $H_n(j): H_n(X) \to H_n(\ast)$ is always surjective.

We define $\tilde{H}_0(X) := \kernel H_0(j)$ and call it the \textbf{zeroth reduced homology group}.
This gives us a short exact sequence
\begin{align*}
  0 \to \tilde{H}_0(X) \to H_0(X) \stackrel{H_0(j)}{\to}H_0(\ast) \to 0
\end{align*}
since this sequence splits (via $H_0(i)$), we have
\begin{align*}
  H_0(X) \iso \tilde{H}_0(X) \oplus \Z
\end{align*}
\end{dfn}


\begin{rem}[]
Under the identification $H_0(\ast) \iso \Z$ given by
\begin{align*}
  m \scal{\ast} \to m, \quad m \in \Z
\end{align*}
the map $H_0(j) H_0(X) \to H_0(\ast)$ is the one induced by the map
\begin{align*}
  \tilde{j}: C_0(X) \to \Z, \quad
  \sum_{x}m_x x \mapsto  \sum_{x}m_x
\end{align*}
\end{rem}
Now we extend $\tilde{H}_n$ for all $n$ by simply setting $\tilde{H}_n(X) := H_n(X)$ for $n \geq 1$.

We call $\tilde{H}_{\bullet}(X)$ the \textbf{reduced homology groups} of $X$.


There is another way to define the reduced homology and that is to consider the augmented chain complex
\begin{align*}
  \ldots \to C_1(X) \stackrel{\tilde{\del}_1}{\to} C_0(X) \stackrel{\tilde{\del}_0 = \tilde{j}}{\to}\Z \to 0
\end{align*}
and define $\tilde{H}_n(X) = \faktor{\kernel \tilde{\del}_{n}}{\image \tilde{\del}_{n+1}}$

This lets us get a ``better'' version of the dimension axiom to let the singleton space have \emph{all} its homology equal to zero.

\begin{cor}[]
  If $X$ is a non-empty contractible space, then $\tilde{H}_n(X) = 0$ for all $n \geq 0$.
\end{cor}

On Problem sheet F, we show that an analogue to the long exact sequence axiom also works for the reduced homology.
\begin{prop}[]
Let $\emptyset \neq X' \subseteq X$. Then there is a long exact sequence
\begin{align*}
  \ldots \to \tilde{H}_n(X') \to \tilde{H}_n(X) \to H_n(X,X') \to \tilde{H}_{n-1}(X') \to \ldots
\end{align*}
which ends with $\tilde{H}_n(X') \to \tilde{H}_0(X) \to H_n(X,X') \to 0$.
\end{prop}

\begin{cor}[]
If $p \in X$, then $\tilde{H}_n(X) \iso H_n(X,p)$ for all $n \geq 0$.
\end{cor}

We will only use reduced homology later, which let us compute the homology of $S^{n}$. 
We will also seee that if a pair $(X,X') \in \Top^{2}$ is sufficiently ``nice'' , then
\begin{align*}
  H_n(X,X') \iso \tilde{H}_n(X/X')
\end{align*}
where $X/X'$ is the quotient space by collapsing $X'$ to a point.

We now conclude with the homotopy version of $\Top^{2}$.

\begin{dfn}[]
If $f,g: (X,X') \to (Y,Y')$ are morphisms in $\Top^{2}$, we say $f \sim g \mod\ X'$ if there exists a continuous map $F: (X \times I, X' \times I) \to (Y,Y')$ with $F(x,0) = f(x)$ and $F(x,1) = g(x)$ for all $x \in X$.
\end{dfn}
Note that this is \emph{not} the same as saying $f \sim g$ rel $X'$, since the definition does not require that $f|_{X'} = g|_{X'}$ and that $F(x',t)$ is independent of $t$ for all $x' \in X'$.

This relation defines a congruence on $\Top^{2}$ and thus yields a new category $\hTop^{2}$.


\begin{thm}[Homotopy axiom for pairs]
  If $f,g: (X,X') \to (Y,Y')$ are morphisms in $\Top^{2}$ with $f \sim g \mod\ X'$, then for all $n \geq 0$:
  \begin{align*}
    H_n(f) = H_n(g): H_n(X,X') \to H_n(Y,Y')
  \end{align*}
  Thus, $H_n: \hTop^{2} \to \Ab$ is a functor.
\end{thm}
The proof is analogous to the proof of the homotopy axiom for $H_n: \hTop \to \Ab$.
