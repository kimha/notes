\section{Jordan Brouwer Separation Theorem}


\begin{dfn}[]
Let $X$ be a topological space.
We say that
\begin{itemize}
  \item $X$ is a $\bm{T_1}$ space if points are closed in $X$
  \item $X$ is \textbf{weakly Hausdorff space}, if every continuous map $f: K \to  X$ from a compact Hausdorff space, $f(K)$ is closed in $X$.
\end{itemize}
\end{dfn}

\begin{lem}[]
\begin{align*}
  \{\text{Hausdorff spaces}\} \subsetneq \{\text{weakly Hausdorff spaces}\} \subsetneq \{T_1 \text{\ spaces}\}
\end{align*}
\end{lem}
\begin{proof}
\textbf{The first inclusion:} Let $X$ be Hausdorff, $K$ compact (Hausdorff) and $f: K \to X$ continuous.

Let $z \in {f(K)}^{c}$. We show that there exists an open neighbourhoood of $z$ not intersecting $f(K)$.
Since $X$ is Hausdorff, for all $y \in f(K)$, there exist disjoint open neighborhoods $U_y, V_z$ of $y$ and $z$.
By continuity of $f$, $f^{-1}(U_y)$ is open in $K$. 
Moreover the collection ${(f^{-1}(U_y))}_{y \in f(K)}$ is an open covering of $K$, so by compactness, there are finitely many $f^{-1}(U_1),\ldots,f^{-1}(U_n)$ covering $K$.
then $U_1,\ldots,U_n$ cover $f(K)$, so we see that
\begin{align*}
  z \in V := \bigcap_{i=1}^{n}V_n, \quad \text{and} \quad V \cap f(K) = \emptyset
\end{align*}
which shows that $f(K)$ is closed.

\textbf{The second inclusion:} Let $X$ be weakly Hausdorff and $x \in X$.
Take $K = \{\ast\}$ the one-point space which is compact Hausdorff and define $f: K \to X$ by $f(\ast) = x$.
Then $f(K) =x$ is closed, so $X$ is $T_1$.

\textbf{$T_1$, but not weakly Hausdorff:}
Let $X$ be the line with two origins, i.e.
\begin{align*}
  X = \faktor{\R \sqcup \R}{\sim}, \quad \text{where} \quad (x,1) \sim (x,2) \iff x \neq 0
\end{align*}
Denote the origins by $0_1 = [(x,1)], 0_2 = [(x,2)]$.

Moreover, $\{0_1\}$ (and by symmetry $\{0_2\}$) is closed, because the pre-image of its complement is $(\R,1) \sqcup (\R \setminus \{0\},2)$ and thus open.
It follows that $X$ is $T_1$.

Consider the embedding of the compact Hausdorff space $K = [-1,1]$ into $X$ containing one origin.
Since the other origin has no neighbourhood that doesn't intersect the image, $f(K)$ is not closed.

\textbf{Weakly Hausdorff, but not Hausdorff:}

Let $X$ be an uncountable set with the cocountable topology, where the open sets are those with countable complement.
(The proof also works with the cofinite topology on infinite sets.)

Let $U,V$ be non-empty open sets. Then $U^{c}$ is countable and $V$ is uncountable, so we cannot have $V \subseteq U^{c}$, so it must intersect $U$.
So $X$ is not Hausdorff.


Now let $K$ be compact Hausdorff and $f: K \to X$ continuous.
Since $K$ is compact, $f(K)$ must also be compact.
But the compact sets are exactly the countable ones.
\end{proof}

\begin{prop}[]
Given a family $i_n: X_n \to X_{n+1}$ for $n \in \N$ of closed inclusions.
Assume that for each $n$, the space $X_n$ is $T_1$.
Then
\begin{center}
The filtered colimti of $C_{\bullet}(X_n)$ is the $C_{\bullet}(-)$ of the filtered colimit.
\end{center}
\end{prop}

\begin{thm}[Invariance of Domain]
Suppose $U,U'$ are subsets of $S^{n}$ and $f: U \to U'$ is a homeomorphism.
If $U$ is open, so is $U'$.
\end{thm}
\begin{proof}
  Let $p \in f(U) = U'$. We show that $p$ is an interior point of $U'$.

  Let $x = f^{-1}(p) \in U$. As $U$ is open, let $B_{\epsilon}(x) \subseteq U$ be an open Ball such that $\overline{B_{\epsilon}(x)} \subseteq U$.
  Consider $S := f(\del \overline{B_{\epsilon}(x)}) \subseteq S^{n}$ an embedded $(n-1)$ sphere.

  By the Jordan-Brouwer Separation theorem, $S^{n} \setminus S$ has two components $X$ and $Y$ and $S$ is the boundary of both of them.

\end{proof}


The name ``invariance of domain'' comes from its corollary:
\begin{cor}[]
If $\R^{n}$ contains a subspace isomorphic to $\R^{m}$, then $m \leq n$.
\end{cor}
\begin{proof}
We apply the invariance of domain to the one-point compactification of $\R^{n}$, which is $S^{n}$.

Suppose by contraposition that there exists an open subset $U \subseteq \R^{n}$ and a homeomoprhism $f: \R^{m} \to  U$ for $m > n$.

As the inclusion
\begin{align*}
  i: \R^{n} \to \R^{m}, \quad (x_{1}, \ldots, x_{n}) \mapsto (x_{1}, \ldots, x_{n},0,\ldots,0)
\end{align*}
is continuous and injective. 
Then the composition $f \circ i: \R^{n} \to \R^{n}$ is also continuous and injective.
But since $\image i$ is contained in the hyperplane $\R^{n} \otimes 0 \subseteq \R^{m}$, $\image f \circ i$ cannot be open, as $f$ is continuous.

Viewing $U$ as an open subset of $S^{n}$, we can apply the invariance of domain theorem to the isomorphism $f \circ i$, and get that $\image f \circ i$ should be open in $S^{n}$, contradicting the earlier result.
\end{proof}


