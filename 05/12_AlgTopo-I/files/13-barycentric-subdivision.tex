\section{Barycentric subdivision}


Consider the unit ball $D^{2} \in \R^{2}$.
If we have a singular $1$-simplex $\sigma: \Delta^{1} \to D$ (i.e.\ a path) with endpoints $x,y$ and we chose a third point $p \in D^{2}$, we can form a triangle with edges $x,y,p$ and use it to define a singular $2$-simplex $Q(p,\sigma): \Delta^{2} \to D$.


This easily generalizes to any singular $n$-simplex with convex euclidean domain.

\begin{dfn}[]
Let $D$ be a bounded convex subset of some Euclidean space.
Fix $p \in D$ and suppose $\sigma: \Delta^{n} \to D$ is a singular $n$-simplex.
We define the \textbf{cone over $\sigma$ with vertex $p$} to be the singular $(n+1)$-simplex $Q(p,\sigma): \Delta^{n+1} \to D$ defined by
\begin{align*}
  Q(p,\sigma)(s_{1}, \ldots, s_{n+1}) :=
  \left\{\begin{array}{ll}
    p & s_0 = 1\\
    s_0 p + (1 -s_0)\sigma(\tfrac{s_1}{1-s_0},\ldots,\tfrac{s_{n+1}}{1 - s_0}) & s_0 \neq 1
  \end{array} \right.
\end{align*}
\end{dfn}

So given a choice $p \in D$, we can extend this linearly to obtain a map $C_n(D) \to C_{n+1}(D)$, which we write as $c \mapsto Q(p,c)$.

This construction behaves nicely with the boundary operator:
In the example above, if we apply the boundary operator to $Q(p,\sigma)$, 
we obtain the edge $\sigma$ aswell as the edges connecting $p$ to $x$ and the one connecting $p$ to $y$ (with a $\pm$ sign) when viewing it as an element of $C_1(D)$.
These additional edges can be viewed as $Q(p,x)$ and $Q(p,y)$ (with alternating sign), respectively.

\begin{prop}[]
Let $c = \sum_{m_i}\sigma_i \in C_n(D)$, then
\begin{align*}
  \del Q(p,c) = \left\{\begin{array}{ll}
    c - Q(p,\del c) & n \geq 1\\
    c - (\sum m_i) p & n = 0
  \end{array} \right.
\end{align*}
\end{prop}
\begin{proof}
Let's first check the case $n \geq 1$.
Let $\epsilon_i^{n+1}: \Delta^{n} \to  \Delta^{n+1}$ be the ``inclusion'' of the $i$-th face into $\Delta^{n+1}$.

If $i = 0$, we have
\begin{align*}
  Q(p,\sigma) \circ \epsilon_i^{0}(s_{0}, \ldots, s_{n}) = Q(p,\sigma) (0,s_{0}, \ldots s_{n}) = \sigma(s_{0}, \ldots, s_{n})
\end{align*}
and if $1 \leq i \leq n+1$, then
\begin{align*}
  Q(p,\sigma) \circ \epsilon_i^{n+1}(s_{0}, \ldots, s_{n}) = Q(p,\sigma) (s_{0}, \ldots, s_{i-1},0,s_{i+1}, \ldots s_{n})
\end{align*}
So if $s_0 = 1$, this reduces to $Q(p,\sigma)(1,0,\ldots,0) = p$.
And if $s_0 \neq 1$, we have
\begin{align*}
  Q(p,\sigma) \circ \epsilon_i^{n+1}(s_{0}, \ldots, s_{n}) = \cdots = Q(p,\sigma \circ \epsilon_{i-1}^{n})(s_0,\ldots,s_n)
\end{align*}

Thus, for $n \geq 1$, it follows
\begin{align*}
  Q(p,\sigma) \circ \epsilon_0^{n+1} = \sigma
\end{align*}
and for $i \geq 1$:
\begin{align*}
  Q(p,\sigma) \circ \epsilon_i^{n+1} = Q(p,\sigma \circ \epsilon_{i-1}^{n})
\end{align*}
Taking alternating sums, we get
\begin{align*}
  \del Q(p,\sigma) 
  &= \sum_{i=0}^{n+1} {(-1)}^{i}Q(p,\sigma) \circ e_i^{n+1}\\
  &= \sigma + \sum_{i=1}^{n+1} {(-1)}^{i}Q(p,\sigma) \circ e_i^{n+1}\\
  &= \ldots \\
  &= \sigma - Q(p,\del \sigma)
\end{align*}

For $n = 0$, observe that if $x$ is a point in $X$, then $Q(p,x)$ is a $1$-sipmlex $\sigma$ with endpoint $p$ and $x$. Thus $\del \sigma = x - p$.
\end{proof}


\begin{cor}[]
Let $D$ be a bounded convex subset of some Euclidean space. Then the reduced homology groups vanish: $\tilde{H}_n(D) = 0$ for all $n \geq 0$.
\end{cor}
\begin{proof}
The operator $c \mapsto  Q(p,c)$ is a contracting homotopy for the chain complex $\tilde{C}_{\bullet}(D)$.
Thus, by Corollary~\ref{cor:contracting-homotopy}, $\tilde{H}_n(D) = 0$ for all $n \geq 0$.
\end{proof}


This proof did not use the homotopy axiom.
We use this fact later when giving an alternative proof of the homotopy axiom using the Acyclic Models Theorem.


When we have a $1$-simplex $\sigma: \Delta^{1} \to X$, the corresponding line may be curved (when viewing $X \subseteq \R^{n}$) or the path may go slowly in the start and speed up in the end.

It is useful to restrict ourselves to $n$-simplices whose image is not curved in any way and have constant ``speed'', i.e.\ are affinely linear.


\begin{dfn}[]
Let $D$ be convex. A singular $n$-simplex $\sigma: \Delta^{n} \to D$ is said to be \textbf{affine}, if
\begin{align*}
  \sigma\left(
    \sum_{i=0}^{n}s_i e_i
  \right) = \sum_{i=0}^{n} s_i \sigma(e_i), \quad \forall (s_{0}, \ldots, s_{n}) \in \Delta^{n}
\end{align*}
\end{dfn}
If $\sigma$ is affine, then so is $\del \sigma$.
Thus the space off affine singular $n$-simplices defines a subcomplex of $C_{\bullet}(D)$.
We write $C_{\bullet}^{\text{affine}}(D)$.

Note that if $\sigma$ is affine, then so is $Q(p,\sigma)$ for any $p \in D$.

To motivate the next concept, consider an affine $1$-simplex.
We can then split the image in its center obtain two $1$-simplices that make each half of the original simplex.

Next, for a $2$-simplex we take its center $b$ and take lines from the corner to the center until they reach the opposite side.
We then can divide the original triangle into $6$ triangles.

This construction can also be made ``inductively''.
For a triangle with corners $A,B,C$ with opposite edges $a,b,c$, and barycenter $M$,
we can obtain the two triangles obtained from subdividing the triangle $ABM$ as follows:
Let $M_c$ be the middle of the edge $c$.
For $F(c)$ the sum of the two line segments $AM_c,M_c,B$, take $Q(b,F(c))$.

So if we call $F$ the subdivision operator, and let $\sigma$ be the triangle $ABC$, we can write
\begin{align*}
  F(\sigma) = Q(M,F(\del \sigma))
\end{align*}

This generalizes to any $n$-simplex.
Let us denote by
\begin{align*}
  b_n := \frac{1}{n+1}\left(
    e_{0} + \cdots + e_{n}
  \right)
\end{align*}
the \textbf{barycentre} of the standard simplex $\Delta^{n}$.


\begin{dfn}[]
Let $D$ be convex. Define the \textbf{convex barycentric subdivision} operator
\begin{align*}
  \text{Sd}_n^{\text{cv}}: C_n^{\text{affine}}(D) \to C_n^{\text{affine}}(D)
\end{align*}
inductively. For any affine singular $n$-simplex $\sigma: \Delta^{n} \to D$, set
\begin{align*}
  \text{Sd}_n^{\text{cv}}(\sigma) :=
  \left\{\begin{array}{ll}
    \sigma & n = 0\\
    Q(\sigma(b_n), \text{Sd}_{n-1}^{cv}(\del \sigma)) & n \geq 1
  \end{array} \right.
\end{align*}
\end{dfn}

In an arbitrary topological space, this construction doesn't work as $X$ (or $\sigma$) have all the necessary properties.
However, if we are given a $n$-simplex $\sigma: \Delta^{n} \to X$, we can apply the barycentric subdivision to $\Delta^{n}$ and then obtain our sum of simplices that way.

Let $l_n: \Delta^{n} \to \Delta^{n}$ be the identity map, thought of as a singular $n$-simplex \emph{in} $\Delta^{n}$.

\begin{dfn}[]
Let $X$ be a topological space. 
Define the \textbf{barycentric subdivision} $\Sd_n: C_n(X) \to C_n(X)$ by
\begin{align*}
  \Sd_n(\sigma) := \sigma_{\#}\left(
    \Sd_n^{\text{cv}}(l_n)
  \right)
\end{align*}
where $\sigma_{\#}: C_n(\Delta^{n}) \to C_n(X)$.
\end{dfn}
This is well-defined because $l_n$ is affine and $C_n^{\text{affine}}(\Delta^{n}) \subseteq C_n(\Delta^{n})$.

\begin{lem}[]
If $D$ is a convex bounded subset of some euclidean space, then for all affine simplices $\sigma: \Delta^{n} \to D$
\begin{align*}
  \Sd_n(\sigma) = \Sd_n^{\text{cv}}(\sigma)
\end{align*}
\end{lem}
The proof is on Problem Sheet G.

\begin{prop}[]
The barycentric subdivition is a chain map.
Moreover, if $f: X \to Y$ is continuous, then the following diagram commutes for all $n\geq 0$:
\begin{center}
\begin{tikzcd}[] 
  C_n (X)
  \arrow[]{r}{f_{\#}}
  \arrow[]{d}{\Sd_n}
  &
  C_n (Y)
  \arrow[]{d}{\Sd_n}
  \\
  C_n (X)
  \arrow[]{r}{f_{\#}}
  &
  C_n (Y)
\end{tikzcd}
\end{center}
where $f_{\#} = (\sigma \mapsto f \circ \sigma)$
\end{prop}

The fact that the diagram commutes means that $\Sd_n$ is a natural transformation $\Sd_n: C_{n}(-) \to C_n(-)$ (we define later what this is).
We often refer to the commutation relation as \emph{naturality of the square}.

\begin{proof}
We show first that the square commmutes:
\begin{align*}
  f_{\#}\Sd_n(\sigma) = f_{\#} \sigma_{\#} \Sd_n^{\text{cv}}(l_n) = {(f \circ \sigma)}_{\#} \Sd_n^{\text{cv}}(l_n) = \Sd_n(f \circ \sigma) = \Sd_n(f_{\#} \sigma)
\end{align*}

We first prove the result in the case that $X$ is a bounded convex subset of some euclidean space.
As we saw before, we then have $\Sd_n = \Sd_n^{\text{cv}}$.

We prove this by induction on $n$.
The case $n = 0$ is obvious, as $\Sd_0$ is just the identity.
For the inductive step, we have
\begin{align*}
  \del \Sd_n^{\text{cv}}(\sigma)
  &= \del Q(\sigma(b_n), \Sd_{n-1}^{cv}(\del \sigma))\\
  &= \Sd_{n-1}^{\text{cv}}(\del \sigma) - Q(\sigma(b_{n}),\del \Sd_{n-1}^{\text{cv}}(\del \sigma))\\
  &= \Sd_{n-1}^{\text{cv}}(\del \sigma)
\end{align*}

\end{proof}
