\section{Singular Homology}

A group homomorphism from a free abelian group is determined by its basis.

\begin{dfn}[$n$-simplex]
  An ordered tuple $(z_0,\ldots,z_n)$ in $\R^{m}$ is said to be \textbf{affinely independent}, if $\{z_1-z_0,\ldots,z_n-z_0\}$ is linearly independent.

  Given such an affinely independent tuple the $n$-simplex spanned by $(z_{0}, \ldots, z_{n})$ is the set
\begin{align*}
  [z_0,z_1,\ldots,z_n] := \left\{
    x \in \R^{m} \big\vert x = \sum_{i=0}^{n}s_i z_I, 0\leq s_i \leq 1, \sum_{i=0}^{n}s_i = 1
  \right\}
\end{align*}
\end{dfn}

\begin{dfn}[]
  The face opposite to $z_i$ is
  \begin{align*}
    [z_0,\ldots,\hat{z}_i,\ldots,z_n] := \left\{
      x \in [z_{0}, \ldots, z_{n}] \big\vert s_i = 0
    \right\}
  \end{align*}
  The boundary of $[z_{0}, \ldots, z_{n}]$ is the union of all its faces.

  The standard $n$-simplex is $\Delta^{n}\subseteq \R^{n+1}$ spanned by the canonical basis vectors.

  A singular $n$-simplex is a continuous map $\sigma: \Delta^{n} \to X$

  The restriction of $\sigma$ to its $i$-th face induces a $n-1$ simplex $\sigma \circ \epsilon_i: \Delta^{n-1} \to X$.

  The alternating sum of the restriction maps is the \textbf{boundary} of the simplex
  \begin{align*}
    \del \sigma := \sum_{i=0}^{n}(-1)^{i} \sigma \circ \epsilon_i
  \end{align*}
\end{dfn}
As a motivation for the alternating sign, we can now (with some abuse of notation) write the fundamental theorem of calculus as
\begin{align*}
  \int_{I} \del F dx = F_{|\del I} = F(b) - F(a)
\end{align*}


\begin{dfn}[Singular $n$-chain]
  $C_n(X)$ is the free abelian group with basis the set of $n$-simplices in $X$.
  Its elements are called singular $n$-chains.

  We have the \textbf{singular boundary operators} which are induced by the map $\sigma \mapsto \del \sigma$.

  It is useful to define $C_{-1}(X) = 0$.
\end{dfn}

\begin{prop}[]
$\del^{2} = 0$.
\end{prop}
This follows from the alternating definition of the boundary.


\begin{dfn}[$n$-th singular homology group]
  Singular $n$-chains that are in the kernel of $\del_n$ are called \textbf{singular $n$-cycles}, $Z_n(X) := \kernel \del_n \subseteq C_n(X)$
  
  Those who are in the image of $\del_{n+1}$ are called \textbf{singular $n$-boundaries}, $B_n(X) := \image \del_{n+1} \subset C_n(X)$


  The \textbf{$n$-th singular homology group} is
  \begin{align*}
    H_n(x) := \faktor{Z_n(X)}{B_n(X)}
  \end{align*}
  Which is an abelian subgroup of $C_n(X)$


  For some singular $n$-cycle $c$, denote by $\scal{c}$ the coset $c + \kernel \del_n$ called the \textbf{homology class} of $c$.
\end{dfn}

If $f: X \to Y$ is continuous, this defines a map
\begin{align*}
  f_{\#}: C_n(X) \to C_n(Y), \quad \sigma \mapsto f \circ \sigma 
\end{align*}

Moreover, $f_{\#}$ commutes with $\del$ and induces a map
\begin{align*}
  H_n(f): H_n(X) \to H_n(Y), \quad \scal{c} \mapsto \scal{f_{\#}(c)}
\end{align*}

\begin{cor}[]
For each $n \geq 0$ $H_n: \Top \to \Ab$ is a functor.
\end{cor}

\begin{cor}[]
If $X,Y$ are homeomorphic, then $H_n(X) \iso H_n(Y)$ for all $n \geq 0$
\end{cor}
Follows directly from functor properties.

