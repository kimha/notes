\section{The Snake Lemma and its friends}

\begin{prop}[The Snake Lemma]
  Suppose we are given a commutative diagram of abelian groups, where the rows are exact:
  \begin{center}
  \begin{tikzcd}[ ] 
    &
    A \arrow[]{r}{i}\arrow[swap]{d}{f}
    &
    B \arrow[]{r}{j} \arrow[]{d}{g}
    &
    C \arrow[]{r}{} \arrow[]{d}{h}
    &
    0\\
    0 \arrow[]{r}{}
    &
    A' \arrow[]{r}{i'}
    &
    B' \arrow[]{r}{j'}
    &
    C'
  \end{tikzcd}
  \end{center}
  Then there is a well-defined homomorphism
  \begin{align*}
    \delta: \kernel h \to \coker 
  \end{align*}
  such that there is an exact sequence
  \begin{align*}
    \kernel f \to \kernel g \to  \kernel h \stackrel{\delta}{\to} \coker f \to \coker g \to \coker h
  \end{align*}
  Excplicitly, the map $\delta$ is given by
  \begin{align*}
    \delta(c) = ((i')^{-1} \circ g \circ j^{-1})(c) + \image f
  \end{align*}
  Where $i'^{-1}$ and $j^{-1}$ denote any choice of preimage.
  So the composition is independent of the cohice.
\end{prop}
  The proof may look quite long but it is indeed very easy easy as at every step we do ``the only thing possible''. 
  This type of proof is usually referred to as \emph{diagram chasing} and I would compare it with playing Sudoku.
\begin{proof}

  First, we extend the diagram to include the kernels and cokernels.

  \begin{center}
  \begin{tikzcd}[]
    & 0 \arrow[]{d}{}
    & 0 \arrow[]{d}{}
    & 0 \arrow[]{d}{}
    \\
    & \kernel f \arrow[hook]{d}{} \arrow[]{r}{k}
    & \kernel g \arrow[hook]{d}{} \arrow[]{r}{l}
    & \kernel h \arrow[hook]{d}{}
    \\
    & A \arrow[]{r}{i} \arrow[]{d}{f}
    & B \arrow[]{r}{j} \arrow[]{d}{g}
    & C \arrow[]{r}{}  \arrow[]{d}{h}
    & 0
    \\
    0 \arrow[]{r}{}
    & A' \arrow[]{r}{i'} \arrow[]{d}{}
    & B' \arrow[]{r}{j'} \arrow[]{d}{}
    & C' \arrow[]{d}{}
    \\
    & \coker f \arrow[]{r}{p} \arrow[]{d}{}
    & \coker g \arrow[]{r}{q} \arrow[]{d}{}
    & \coker h \arrow[]{d}{} 
    \\
    & 0
    & 0
    & 0
  \end{tikzcd}
  \end{center}
  Here $k,l$ are just the restrictions of $i,j$ and the maps $p,q$ are induced by $i'$ and $j'$ namely
  \begin{align*}
    p(a' + \image f) := i'(a') + \image g\\
    q(b' + \image g) := j'(b') + \image h
  \end{align*}
  This is well-defined because by commutativity of the diagram, we have
  \begin{align*}
    a' - \tilde{a}' \in \image f \implies
    i'(a') - i'(\tilde{a}') = i'(a' - \tilde{a}') = (i' \circ f)(a) = (g \circ i)(a) \in \image g
  \end{align*}
  for some $a \in A$.

  We now prove that the top and bottom rows are exact, i.e.
  \begin{align*}
    \image k = \kernel l \quad \text{and} \quad \image p = \kernel q
  \end{align*}
  \textbf{Exactness at $\bm{\kernel g}$:}
  Let $a \in \kernel f$. Then $(g \circ i)(a) = (i' \circ f)(a) = 0$, so $k(a) = i(a) \in \kernel g$.
  Since $A \stackrel{i}{\to} B \stackrel{j}{\to} C$ is exact, 
  we have $(l \circ k)(a) = (j \circ i)(a) = 0$,
  so $\image k \subseteq \kernel l$.
  For ``$\supseteq$'' let $b \in \kernel l \subseteq \kernel g$. 
  By exactness of $A \to  B \to C$ it follows that $j(b) = 0 \implies b = i(a)$ for some $a \in A$.
  Then, $(i' \circ f)(a) = (g \circ i)(a) = g(b) = 0$.
  Since $i'$ is injective, it follows that $f(a) = 0$ so $a \in \kernel f$ meaning we have $b = k(a)$.
  Thus $\kernel l \subseteq \image k$ and exactness at $\kernel g$ follows.

  \textbf{Exactness at $\bm{\coker g}$:} 
  First note that from exactness of $A' \to B' \to C'$ it follows that $(j' \circ i') = 0$, so for all $a' + \image f \in \coker f$:
  \begin{align*}
    (q \circ p)(a' + \image f) 
    = 
    q(i'(a) + \image g)
    =
    (j' \circ i')(a') + \image h = 0 + \image h = 0 \in \coker h
  \end{align*}
  Thus $\image p \subseteq \kernel q$.
  For ``$\supseteq$'', let $b' + \image g \in \kernel q$.
  This means $j'(b') \in \image h$, so there exists a $c \in C$ such that $h(c) = j'(b')$.
  Since $j$ is surective, there exists a $b \in B$ with $j(b) = c$.
  Therefore
  \begin{align*}
    j'(b' - g(b))  
    = j'(b') - (j' \circ g)(b)
    =
    h(c) - (h \circ j)(b)
    =
    h(c) - h(c) = 0 \in C'
  \end{align*}
  so $b' - g(b) \in \kernel j' = \image i'$ by exactness at $B'$.

  So there is some $a' \in A'$ with $i'(a') = b' - g(b)$. Therefore
  \begin{align*}
    p(a' + \image f) = i'(a') + \image g = (b' - g(b)) + \image g = b' + \image g
  \end{align*}
  This shows $\kernel q \subseteq \image p$, proving exactness at $\coker g$.

  \textbf{Defining the map $\bm{\delta: \kernel h \to \coker f}$: }
  \begin{center}
  \begin{tikzcd}[] 
    \kernel f \arrow[]{r}{k}
    & \kernel g \arrow[]{r}{l}
    & \kernel h \arrow[out=0,in=180,swap,looseness=2]{dll}{\delta}
    \\
    \coker f \arrow[]{r}{p}
    & \coker g \arrow[]{r}{q}
    & \coker h
  \end{tikzcd}
  \end{center}
  Let $c \in \kernel h$. 
  Since $j$ is surjective, there is some (possible not unique) $b \in B$ with $j(b) = c$.
  Then $g(b) \in \kernel j'$ as $(j' \circ g)(b) = (h \circ j)(b) = 0$.
  By exactness at $B'$ it follows $g(b) \in \kernel j' = \image i'$, so let $a' \in A$ such that $i'(a') = g(b)$.
  Since $i'$ is injective, $a'$ is unique for that $b$.
  We define $\delta(c) := a' + \image f \in \coker f$.
  
  \textbf{Uniqueness of $\bm{\delta(c)}$}:  We need to show that $\delta(c)$ does not depend on the choice of $b \in j^{-1}(c)$.
  Let $b_1,b_2 \in B$ with $j(b_1) = j(b_2) = c$ and let $a_1',a_2' \in A$ such that $i'(a_1') = g(b_1), i'(a_2') = g(b_2)$.
  We claim that both produce the same coset, i.e. $a_1' + \image f = a_2' + \image f$.
  
  Since $j(b_1) = j(b_2)$ we have $b_1 - b_2 \in \kernel j = \image i$ (by exactness at $B$), so there exists an $a \in A$ such that $i(a) = b_1 - b_2$.
  Then
  \begin{align*}
    (i' \circ f)(a) = (g \circ i)(a) = g(b_1 - b_2) = g(b_1) - g(b_2) = i'(a_1') - i'(a_2') =  i'(a_1' - a_2')
  \end{align*}
  Since $i'$ is injective it follows $a_1' - a_2' = f(a) \in \image f$.

  Therefore, $\delta(c) = ((i')^{-1} \circ g \circ j^{-1})(c) + \image f$ is well defined.
  It's clear that it is a homomorphism, since we only used homomorphisms to construct $\delta$.


  Lastly, we have to check for exactness at $(\kernel g \stackrel{l}{\to} \kernel h \stackrel{\delta}{\to} \coker f)$ and at $(\kernel h \stackrel{\delta}{\to} \coker f \stackrel{p}{\to} \coker g)$.

  \textbf{Exactness at $\bm{\kernel h}$:}
  To show $\image l \subseteq \kernel \delta$, let $b \in \kernel g$ and set $c = l(b) \in \kernel h$.
  Then the unique $a' \in A'$ in the construction of $\delta(c)$ from earlier satisfies $i'(a') = g(b) = 0$, so 
  $\delta(c) = 0 + \image f = 0 \in \coker f$.
  For ``$\supseteq$'', let $c \in \kernel \delta \subseteq C$. 
  Since $j$ is surjective, $c = j(b)$ for some $b \in B$.
  Also, the $a'$ in $\delta(c) = a' + \image f$ satisfies $a' \in \image f$.
  So let $a\in f^{-1}(a')$.
  Then $(g \circ i )(a) = (i' \circ f)(a) = i'(a') = g(b)$.
  Thus $b - i(a) \in \kernel g$ and
  $j(b - i(a)) = j(b) - (j \circ i)(a) = c$ by exactness of $A \stackrel{i}{\to} B \stackrel{j}{\to}C$.

  This means $l(b - i(a)) = c$, so $c \in \image l$ proving exactness at $\kernel h$.

  \textbf{Exactness at $\bm{\coker f}$:} 
  First note that $(p \circ \delta)(c) = p(a' + \image f) = i'(a') + \image g$, but since $i'(a') = g(b)$ this coset is zero, where $b$ as before satisfies $j(b) = c$ (by surjectivity of $j$).
  So we have $\image \delta \subseteq \kernel p$.

  To show ``$\supseteq$'', let $a' + \image f \in \kernel p$.
  This means $i'(a') \in \image g$, so there exists a $b \in B$ with $g(b) = i'(a')$. Setting $c = j(b)$, it follows from exactness at $B'$ that.
  \begin{align*}
    h(c) = (h \circ j)(b) = (j' \circ g)(b) = (j' \circ i')(a') = 0
  \end{align*}
  By construction, we then have $\delta(c) = a' + \image f$.

  This completes the proof.
\end{proof}

Next up, we see the snake lemma in action.

\begin{thm}[The long exact sequence in homology]
  Let the following be a short exact sequence of chain complexes.
  \begin{align*}
    0 \to C_{\bullet} \stackrel{f}{\to} C_{\bullet}' \stackrel{g}{\to} C_{\bullet}'' \to 0
  \end{align*}
  Then there exist homomorphisms $H_n(C_{\bullet}'') \stackrel{\delta_n}{\to} H_{n-1}(C_{\bullet})$ for $n \in \Z$ such that there is a long exact sequence
  \begin{align*}
    \ldots \to H_n(C_{\bullet}) \stackrel{H_n(f)}{\to} H_n(C_{\bullet}') \stackrel{H_n(g)}{\to} H_n(C_{\bullet}'') \stackrel{\delta}{\to} H_{n-1}(C_{\bullet}) \to \ldots
  \end{align*}
  We call $\delta = (\delta_n)$ the \textbf{connecting homomorphism} of the short exact sequence, which is given by
  \begin{align*}
    \delta_n\scal{c} = \scal{f_{n-1}^{-1}\del'g_n^{-1}c}, \quad \forall c \in Z_n(C_{\bullet}'')
  \end{align*}
\end{thm}
\begin{proof}
  Write $Z_n = \kernel \del_n \subseteq C_n$, $B_n = \image \del_{n+1} \subseteq C_{n+1}$ and $H_n = Z_n/B_n$.
  And similarly for $C_{\bullet}'$, and $C_{\bullet}''$.

  Then the following diagram satisfies the requirements of the snake lemma, where the morphisms arent as written, but induced from them:
  \begin{center}
  \begin{tikzcd}[ ] 
    & C_n/B_n \arrow[]{d}{\del} \arrow[]{r}{f_n}
    & C_n'/B_n' \arrow[]{d}{\del'} \arrow[]{r}{g_n}
    & C_n''/B_n'' \arrow[]{d}{\del''} \arrow[]{r}{}
    & 0
    \\
    0 \arrow[]{r}{}
    & Z_{n-1} \arrow[]{r}{f_{n-1}}
    & Z_{n-1}' \arrow[]{r}{g_{n-1}}
    & Z_{n-1}'' 
  \end{tikzcd}
  \end{center}

  Adding in the kernels and the cokernels, we obtain
  \begin{center}
  \begin{tikzcd}[ ] 
    & 0 \arrow[]{d}{}
    & 0 \arrow[]{d}{}
    & 0 \arrow[]{d}{}
    \\
    & H_n \arrow[]{r}{} \arrow[hook]{d}{}
    & H_n' \arrow[]{r}{} \arrow[hook]{d}{}
    & H_n'' \arrow[hook]{d}{} 
    \\
    & C_n/B_n \arrow[]{d}{\del} \arrow[]{r}{}
    & C_n'/B_n' \arrow[]{d}{\del'} \arrow[]{r}{}
    & C_n''/B_n'' \arrow[]{d}{\del''} \arrow[]{r}{}
    & 0
    \\
    0 \arrow[]{r}{}
    & Z_{n-1} \arrow[]{r}{} \arrow[]{d}{}
    & Z_{n-1}' \arrow[]{r}{} \arrow[]{d}{} 
    & Z_{n-1}'' \arrow[]{d}{}
    \\
    & H_{n-1} \arrow[]{d}{} \arrow[]{r}{}
    & H_{n-1}' \arrow[]{d}{} \arrow[]{r}{}
    & H_{n-1}'' \arrow[]{d}{}
    \\
    & 0
    & 0
    & 0
  \end{tikzcd}
  \end{center}
  The snake lemma provides us with $\delta_n: H_n'' \to H_{n-1}$.
\end{proof}

\begin{prop}[Naturality of the connecting homomorphism]
  Suppose we have a commutative diagram of chain complexeswith exact rows.
  \begin{center}
  \begin{tikzcd}[ ] 
    0 \arrow[]{r}{}
    & A_{\bullet} \arrow[]{r}{f} \arrow[]{d}{i}
    & B_{\bullet} \arrow[]{r}{g} \arrow[]{d}{j}
    & C_{\bullet} \arrow[]{r}{} \arrow[]{d}{k}
    & 0
    \\
    0 \arrow[]{r}{}
    & A_{\bullet}' \arrow[]{r}{f'}
    & B_{\bullet}' \arrow[]{r}{g'}
    & C_{\bullet}' \arrow[]{r}{}
    & 0
  \end{tikzcd}
  \end{center}
  Then there is a commutative diagram of abelian groups with exact rows:
  \begin{center}
  \begin{tikzcd}[ ] 
    \ldots \arrow[]{r}{}
    & H_n(A_{\bullet}) \arrow[]{r}{H_n(f)} \arrow[]{d}{H_n(i)}
    & H_n(B_{\bullet}) \arrow[]{r}{H_n(g)} \arrow[]{d}{H_n(j)}
    & H_n(C_{\bullet}) \arrow[]{r}{\delta_n} \arrow[]{d}{H_n(k)}
    & H_{n-1}(A_{\bullet}) \arrow[]{r}{} \arrow[]{d}{H_{n-1}(i)}
    & \ldots
    \\
    \ldots \arrow[]{r}{}
    & H_n(A_{\bullet}') \arrow[]{r}{H_n(f')} 
    & H_n(B_{\bullet}') \arrow[]{r}{H_n(g')} 
    & H_n(C_{\bullet}') \arrow[]{r}{\delta_n'} 
    & H_{n-1}(A_{\bullet}') \arrow[]{r}{} 
    & \ldots
  \end{tikzcd}
  \end{center}
\end{prop}
\begin{proof}
  The exactness of the rows follows from the previous theorem.
  The first two square commute since $H_n$ is a functor.
  All is left to prove is the commutativity of the right square.

  Let $c \in Z_n(C)$ be a representant of $\scal{c} \in H_n(C_{\bullet})$.
  Since the chain map $g$ is surjective, $g_n:B_n \to C_n$ is surjective.
  Thus there exists a $b \in B_n$ with $g_n(b) = c$. Then
  \begin{align*}
    (H_{n-1}(i) \circ \delta_n) \scal{c} = (H_{n-1}(i) \circ \delta_n) \scal{g_n b}
  \end{align*}
  Let $\del$ denote the boundary operator of $B_{\bullet}$ and $\del'$ the boundary operator of $B_{\bullet}'$.
  The connecting homomorphisms $\delta, \delta'$ are given by
  \begin{align*}
    \delta_n \scal{c} &= \scal{f_{n-1}^{-1}\del g_{n}^{-1}c} \tag{$a$}
    \\
    \delta_n' \scal{c} &= \scal{(f_{n-1}')^{-1}\del' (g_{n}')^{-1}c} \tag{$b$}
  \end{align*}
  Commutativity of the squares in the original diagram give us
  \begin{align*}
    j_{n-1} \circ f_{n-1} &= f_{n-1}' \circ i_{n-1}\tag{$c$}
    \\
    g_{n}' \circ j &= k_{n} \circ g_{n} \tag{$d$}
  \end{align*}
  So we have
  \begin{align*}
    (H_{n-1}(i) \circ \delta_n)\scal{c} 
    &\stackrel{a}{=} H_{n-1}(i) \scal{f_{n-1}^{-1} \del b}
    \\
    &= \scal{(i_{n-1} \circ f_{n-1}^{-1})\del b}\\
    &\stackrel{c}{=} \scal{((f_{n-1}')^{-1} \circ j_{n-1})\del b}\\
    &\stackrel{b}{=} \delta_n' \scal{(g_n' \circ j_n)}\\
    &\stackrel{d}{=} \delta_n' \scal{(k_n \circ g_n)b}\\
    &= \delta' H_n(k) \scal{g_n(b)}\\
    &= (\delta_n' \circ H_n(k)) \scal{c}
  \end{align*}
  Which proves commutativity.
\end{proof}

\begin{prop}[The Five Lemma]
  Suppose we have a commutative diagram of abelian groups, where the two rows are exact:
  \begin{center}
  \begin{tikzcd}[ ] 
    A \arrow[]{d}{f} \arrow[]{r}{}
    & B \arrow[]{d}{g} \arrow[]{r}{}
    & C \arrow[]{d}{h} \arrow[]{r}{}
    & D \arrow[]{d}{k} \arrow[]{r}{}
    & E \arrow[]{d}{l}
    \\
    A' \arrow[]{r}{}
    & B' \arrow[]{r}{}
    & C' \arrow[]{r}{}
    & D' \arrow[]{r}{}
    & E 
  \end{tikzcd}
  \end{center}
  Then
  \begin{enumerate}
    \item $g,k$ injective and $f$ surjective $\implies$ $h$ injective.
    \item $g,k$ surjective and $l$ injective $\implies$ $h$ surjective.
    \item $f,g,k,l$ are isomorphisms $\implies$ $h$ is isomorphism.
  \end{enumerate}
\end{prop}
\begin{proof}
Missing
\end{proof}

\begin{prop}[Barrat-Whitehead Lemma]
  Suppose we have the following commutative diagram of abelian groups, where the two rows are exact:
  \begin{center}
  \begin{tikzcd}[ ] 
    \ldots \arrow[]{r}{}
    & A_n \arrow[]{d}{f_n} \arrow[]{r}{i_n}
    & B_n \arrow[]{d}{g_n} \arrow[]{r}{j_n}
    & C_n \arrow[]{d}{h_n} \arrow[]{r}{k_n}
    & A_{n-1} \arrow[]{d}{f_{n-1}} \arrow[]{r}{}
    & \ldots
    \\
    \ldots \arrow[]{r}{}
    & A_n' \arrow[]{r}{i_n'}
    & B_n' \arrow[]{r}{j_n'}
    & C_n' \arrow[]{r}{k_n'}
    & A_{n-1}' \arrow[]{r}{}
    & \ldots
  \end{tikzcd}
  \end{center}
  Assume each map $h_n: C_n \to C_n'$ is an isomorphism.
  Then there is a long exact sequence:
  \begin{align*}
    \ldots \to A_n \stackrel{(i_n,f_n)}{\to} B_n \oplus A_n' \stackrel{g_n - i_n'}{\to}B_n' \stackrel{k_nh_n^{-1}j_n'}{\to}A_{n-1} \to \ldots
  \end{align*}
\end{prop}
\begin{proof}
  \textbf{$\bm{\image (i_n,f_n) \subseteq \kernel g_n - i_n'}$:}

  Let $a \in A_n$. By commutativity of the first square, we have 
  \begin{align*}
    ((g_n - i_n') \circ (i_n,f_n))(a) 
    = ((g_ni_n) - (i_n' f_n))(a) = 0
  \end{align*}
  
  \textbf{$\bm{\image (i_n,f_n) \supseteq \kernel g_n - i_n'}$:}
  Let $(b,a') \in B_n \oplus A_n'$ such that $g_n(b) = i_n'(a)$.
  Then


  \textbf{$\bm{\image(g_n- i_n') \subseteq \kernel k_nh_n^{-1}j_n'}$:} 
  Let $(b,a') \in B_n \oplus A_n'$. Then by commutativity of the second square and exactness at $B_n'$ and $C_n$:
  \begin{align*}
    ((k_n h_n^{-1}j_n') \circ (g_n - i_n'))(b,a)
    &= (k_n h_n^{-1}j_n'g_n)(b) - (k_nh_n^{-}\underbrace{j_n'i_n'}_{=0})(a)\\
    &= (k_nh_n^{-1}h_nj_n)(b)  = k_nj_n(b) = 0
  \end{align*}
  \textbf{$\bm{\image(g_n- i_n') \supseteq \kernel k_nh_n^{-1}j_n'}$:} 
  Let $b' \in B_n'$ such that $k_n h_n^{-1}j_n'(b') = 0$.
  By exactness at $C_n$, there exists a $b \in B_n$, such that 
  \begin{align*}
    h_n^{-1}j_n'(b') &= j_n(b) 
    \\
    \implies j_n'(b') &=
   h_nj_n(b) = j_n'g_n(b) 
   \\
    \implies g_n(b) - b' &\in \kernel j_n' = \image i_n'
  \end{align*}
  So there is some $a' \in A_n'$ with $i_n'(a') = g_n(b) - b'$. Thus $b' = (g_n - i_n')(b',a')$.


  \textbf{$\bm{\image k_nh_n^{-1}j_n' \subseteq \kernel (i_{n-1},f_{n-1}})$:} 
  \begin{align*}
    ((i_{n-1},f_{n-1}) \circ k_nh_n^{-1}j_n')
    = (\underbrace{i_{n-1}k_n}_{=0}h_n^{-1}j_n,\underbrace{f_{n-1}k_n}_{=k_n'h_n}h_n^{-1}j_n')
    = (0,\underbrace{k_n'j_n'}_{=0}) = 0
  \end{align*}
  where the first braced equality comes from exactness at $A_{n-1}$, the second from commutativity of the third square and the last from exactness at $C_n'$.

  \textbf{$\bm{\image k_nh_n^{-1}j_n' \supseteq \kernel (i_{n-1},f_{n-1}})$:}
  Let $a \in A_{n-1}$ with $i_{n-1}(a) = 0$ and $f_{n-1}(a) = 0$.
  By exactness at $A_{n-1}$, there is some $c \in C_n$ with $k_n(c) = a$. By commutativity of the third square we have
  \begin{align*}
    k_nf_{n-1}(c) = k_n'h_n(c) = 0 \implies h_n(c) \in \kernel k_n' = \image j_n'
  \end{align*}
  So there is some $b' \in B_n'$ with
  \begin{align*}
    j_n'(b) = h_n(c) \implies k_n h_n^{-1}j_n'(b) = k_n(c) = a
  \end{align*}

\end{proof}
