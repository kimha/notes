\section{Chain Complexes}

We introduce a new category $\Comp$ and a new subject: \textbf{homological algera}.

\begin{dfn}[]
A \textbf{chain complex} is a sequence of abelian groups and homomorphisms
\begin{center}
\begin{tikzcd}[ ] 
  \ldots
  \arrow[]{r}{}
  &
  C_{n+1}
  \arrow[]{r}{\del_{n+1}}
  &
  C_n
  \arrow[]{r}{\del_n}
  &
  C_{n-1}
  \arrow[]{r}{}
  &
  \ldots
\end{tikzcd}
\end{center}
such that for each $n \in \Z$ we have
\begin{align*}
  \del^{2} = \del_{n} \circ \del_{n+1} = 0
\end{align*}
We refer to the entire complex as $(C_{\bullet},\del)$ or sometimes just $C_\bullet$.
The maps are called the \textbf{boundary operators} of the chain complex.
\end{dfn}

\begin{xmp}[] 
Let $X$ be a topological space. The chain of singular chains $(C_{\bullet}(X),\del)$ is a singular complex.

In this exaple, the groups are zero for $n \leq 0$, but that is generally not required.

Let $\phi: G \to H$ be a group homomorphism of abelian groups $G,H$.
Then the following is a singular chain
\begin{align*}
  0 \to \kernel \phi \stackrel{\iota}{\to} G \to \faktor{G}{\kernel \phi} \to 0
\end{align*}
\end{xmp}

We can find analogous definitions to $Z_n(X)$ and $B_n(X)$ for any chain complex.
\begin{dfn}[]
Let $(C_{\bullet},\del)$ be a chain complex. Define
\begin{align*}
  Z_n = Z_n(C_{\bullet}) := \kernel \del_n \subseteq C_n\\
  B_n = B_n(C_{\bullet}) := \image \del_{n+1} \subseteq C_n
\end{align*}
Since $\del^{2} = 0$ $B_n \subseteq Z_n$.
We call the elements of $Z_n$ \textbf{$n$-cycles} and the elements of $B_n$ \textbf{$n$-boundaries}.

The \textbf{$n$-th homology group} of the chain complex is the quotient group
\begin{align*}
  H_n = H_n(C_{\bullet}) := \faktor{Z_n(C_{\bullet})}{B_n(C_{\bullet})}
\end{align*}
\end{dfn}

\begin{dfn}[]
A sequence $A \stackrel{f}{\to} B \stackrel{g}{\to}C$ of homomorphisms is said to be \textbf{exact}, if $\image f = \kernel g$.
More generally, a squence
\begin{align*}
  \ldots \to A_{n+1} \stackrel{f_{n+1}}{\to}A_n \stackrel{f_n}{\to}A_{n-1} \to \ldots
\end{align*}
is said to be \textbf{exact}, if it is exact at every $A_n$.
\end{dfn}


\begin{xmp}[] \label{xmp:exact-sequences}
The notion of exactness let us rephrase many definitions from algebra.
Let $f: A \to B$ be a homomorphism of abelian groups.
\begin{itemize}
  \item $f$ is injective, if and only if $0 \to A \stackrel{f}{\to}B$ is exact.
  \item $f$ is surjective, if and only if $A \stackrel{f}{\to}B \to 0$ is exact.
  \item $f$ is an isomorphism if and only if $0 \to A \stackrel{f}{\to} B \to 0$ is exact.
  \item If $A \stackrel{f}{\to}B \stackrel{g}{\to}C \to \stackrel{h}{\to} D$ is exact, then $f$ is surjective if and only if $h$ is injective.
\end{itemize}
\end{xmp}

\begin{dfn}[]
A \textbf{short exact sequence} of abelian groups is an exact sequence of the form
\begin{align*}
  0 \to A \stackrel{f}{\to} B \stackrel{g}{\to} C \to 0
\end{align*}
In this case, $A \iso \image f$ and $\coker f = \faktor{B}{\image F} \iso C$.
A \textbf{long exact sequnce} is one that has potentially infinitely many terms.
\end{dfn}
\begin{dfn}[]
A chain complex $(C_{\bullet},\del)$ is said to be \textbf{acyclic} if $C_{n+1} \stackrel{\del}{\to}C_n \stackrel{\del}{\to}C_{n-1}$ is exact at $C_n$ for all $n$.
\end{dfn}
So a chain complex $C_{\bullet}$ is acyclic if and only if $H_n(C) = 0$ for all $n$.


In order to make the chain complexes into a category, we need a notion of morphisms.
\begin{dfn}[]
Let $(C_{\bullet},\del)$ and $(C_{\bullet}',\del')$ be chain complexes.
A \textbf{chain map} $f: C_{\bullet} \to  C_{\bullet}'$ is a collection of group homomorphisms $f_n: C_n \to C_n'$ such that the following diagram commutes for all $\in \Z$:
\begin{center}
\begin{tikzcd}[ ] 
  \ldots
  \arrow[]{r}{}
  &
  C_{n+1}
  \arrow[]{r}{\del}
  \arrow[]{d}{f_{n+1}}
  &
  C_n
  \arrow[]{r}{\del}
  \arrow[]{d}{f_n}
  &
  C_{n-1}
  \arrow[]{r}{}
  \arrow[]{d}{f_{n-1}}
  &
  \ldots
  \\
  \ldots
  \arrow[]{r}{}
  &
  C_{n+1}'
  \arrow[]{r}{\del'}
  &
  C_n'
  \arrow[]{r}{\del'}
  &
  C_{n-1}'
  \arrow[]{r}{}
  &
  \ldots
\end{tikzcd}
\end{center}
If we write $f$ for the maps $f_n$ this simplifies to the formula
\begin{align*}
  \del' \circ f = f \circ \del
\end{align*}
\end{dfn}
This lets us define the category $\Comp$ where the objects are chain complexes and the morphisms are the chain maps.
The identity morphisms and composition are defined in the obvious way.

\begin{xmp}[]
If $f: X \to Y$ is a continuous map between topological spaces, then $f_{\#}: C_{\bullet}(X) \to C_{\bullet}(Y)$ is a chain map.
\end{xmp}


\begin{prop}[]
There is a functor $\Top \to \Comp$ that associates to a topological space $X$ its singular chain complex $C_{\bullet}(X)$ and to a continuous map $f: X \to Y$ the chain map $f_{\#}: C_{\bullet}(X) \to C_{\bullet}(Y)$.
\end{prop}

We can now interpret $H_n$ as a functor.
Indeed if we keep the notation where for $c \in Z_n(C_{\bullet})$ we write $\scal{c}$ for its equivalence class in $H_n(C_{\bullet})$, then we have that
\begin{align*}
  H_n(f): H_n(C_{\bullet}) \to H_n(C_{\bullet}')
  \quad
  \scal{c} \mapsto \scal{f_n(c)}
\end{align*}
is well defined as $\del' \circ f = f \circ \del'$ implies
\begin{align*}
  f_n(Z_n) \subseteq Z_n' \quad \text{and} \quad 
  f_n(B_n) \subseteq B_n'
\end{align*}

\begin{prop}[]
For each $n \in \N$ there exists a functor $H_n: \Comp \to \Ab$ called the \textbf{$n$-th homology functor} that sends a chain complex $C_{\bullet}$ to $H_n(C_{\bullet})$ and a chain map $f: C_{\bullet} \to C_{\bullet}'$ to the associated map $H_n(f): H_n(C_{\bullet} \to H_n(C_{\bullet}')$
\end{prop}


With this, we can think of the construction of singular homology of a two-stage process.
The first step $X \mapsto C_{\bullet}(X)$ is \emph{topological},
whereas the second step $C_{\bullet}(X) \mapsto H_n(C_{\bullet}(X)) = H_n(X)$ is \emph{algebraic}.

The functor $H_n: \Comp \to \Ab$ is \textbf{additive}, since $H_n(f + g) = H_n(f) + H_n(g)$.


\begin{rem}[]
  We can think of chain complexes as being``made out of'' sequences of abelian groups.
  But there is nothing particularly special about our construction that requires all properties of groups.
  It suffices to be able to talk about analogous things such as subgroups, kernel and image of group morphisms and quotients.

  It turns out that chain complexes themselves fulfill all these nice properties, so in the end, we will be able define generalized versions of chain complexes, that are ``made out of'' sequences of chain complexes.
\end{rem}

\begin{dfn}[]
A \textbf{subcomplex} $(C_{\bullet},\del)$ of a chain complex $(C_{\bullet}',\del')$ is a chain complex such that $C_n \subseteq C_n'$ for all $n \in \Z$ and such that $\del_n = \del_n'|_{C_n}$.

If we denote by $i_n:C_{n} \to  C_{n}'$ the inclusion, the second condition is equivalent to saying that $i: C_{\bullet} \to C_{\bullet}'$ is a chain map.
\end{dfn}

\begin{dfn}[]
Given a subcomplex $(C_{\bullet},\del)$ of $(C_{\bullet}',\del')$, one can form the \textbf{quotient complex} $(\overline{C}_{\bullet},\overline{\del})$, where
\begin{align*}
  \overline{C}_n = \faktor{C_n'}{C_n}
\end{align*}
and $\overline{\del}$ is the induced map.
\end{dfn}

\begin{dfn}[]
Let $f: (C_{\bullet},\del) \to  (C_{\bullet}',\del')$ be a chain map between two complexes.
Then $(\kernel f)_{\bullet}$ is a subcomplex of $C_{\bullet}$ and $(\image f)_{\bullet}$ is a subcomplex of $C_{\bullet}'$, where the boudary operators are 
\begin{align*}
  \del_n|_{\kernel f_n}: \kernel f_n \to  \kernel f_{n-1}
  \del_n'|_{\image f_n}: \image f_n \to  \image f_{n-1}
\end{align*}
The \textbf{cokernel} of $f$ is the chain complex $(\coker f)_{\bullet}$ given by
\begin{align*}
  \coker f_n = \faktor{C_n'}{\image f_n}
\end{align*}
which is a quotient complex of $C_{\bullet}'$ because $(\image f)_{\bullet}$ is a subcomplex of $C_{\bullet}'$.
\end{dfn}

This lets us talk about sequences of complexes.

\begin{dfn}[]
Suppose we have a sequence of chain complexes $(C_{\bullet}^{m},\del^{m})$ for $m \in \Z$ and chain maps $f^{m}: C_{\bullet}^{m} \to C_{\bullet}^{m-1}$.
\begin{center}
\begin{tikzcd}[ ] 
  &
  \vdots
  \arrow[]{d}{}
  &
  \vdots
  \arrow[]{d}{}
  &
  \vdots
  \arrow[]{d}{}\\
  \ldots
  \arrow[]{r}{}
  &
  C_{n+1}^{m+1}
  \arrow[]{r}{f_{n+1}^{m+1}}
  \arrow[]{d}{\del^{m+1}}
  &
  C_{n+1}^{m1}
  \arrow[]{r}{f_{n+1}^{m}}
  \arrow[]{d}{\del^{m}}
  &
  C_{n+1}^{m-1}
  \arrow[]{r}{}
  \arrow[]{d}{\del^{m-1}}
  &
  \ldots
  \\
  \ldots
  \arrow[]{r}{}
  &
  C_{n}^{m+1}
  \arrow[]{r}{f_{n}^{m+1}}
  \arrow[]{d}{\del^{m+1}}
  &
  C_{n}^{m1}
  \arrow[]{r}{f_{n}^{m}}
  \arrow[]{d}{\del^{m}}
  &
  C_{n}^{m-1}
  \arrow[]{r}{}
  \arrow[]{d}{\del^{m-1}}
  &
  \ldots
  \\
  \ldots
  \arrow[]{r}{}
  &
  C_{n-1}^{m+1}
  \arrow[]{r}{f_{n-1}^{m+1}}
  \arrow[]{d}{}
  &
  C_{n-1}^{m1}
  \arrow[]{r}{f_{n+1}^{m}}
  \arrow[]{d}{}
  &
  C_{n-1}^{m-1}
  \arrow[]{r}{}
  \arrow[]{d}{}
  &
  \ldots
  \\
  &
  \vdots
  &
  \vdots
  &
  \vdots
\end{tikzcd}
\end{center}
we say that the sequence $(C_{\bullet}^{m},f^{m})$ is \textbf{exact} if $(\kernel f^{m})_{\bullet} = (\image f^{m+1})_{\bullet}$ for every $m$.
\end{dfn}

\begin{dfn}[]
A \textbf{short exact sequence} of chain complexes is an exact sequence of chain complexes of the form
\begin{align*}
  = \to A_{\bullet} \stackrel{f}{\to} B_{\bullet} \stackrel{g}{\to}C_{\bullet} \to 0
\end{align*}
where $0$ is the chain complex, where all entries are zero.
\end{dfn}
The rows of such sequences are all short exact sequences of abelian groups.


\begin{dfn}[]
Let $C_{\bullet}$ and $C_{\bullet}'$ be two subcomplexes of $C_{\bullet}''$.
The \textbf{intersection} of $C_{\bullet}$ and $C_{\bullet}'$ is the subcomplex $C_{\bullet} \cap C_{\bullet}'$ whose $n$-th term is $C_n \cap C_{n}'$.

Their \textbf{sum} is the subcomplex $C_{\bullet} + C_{\bullet}'$ whose $n$-th term is $C_n + C_{n}'$.

If $\{(C_{\bullet}^{\lambda},\del^{\lambda})\}_{\lambda \in \Lambda}$ is a family of complexes, then their
\textbf{direct sum} is their complex $\bigoplus_{\lambda}C_{\bullet}^{\lambda}$ equipped with the boundary operator $\sum_{\lambda}\del^{\lambda}$.
\end{dfn}


We can define the analogue of Lemma \ref{lem:homology-p}.
\begin{dfn}[]
Let $f,g: (C_{\bullet},\del) \to (C_{\bullet}',\del')$ be two chain maps.
We say that $f$ and $g$ are \textbf{chain homotopic}, written $f \simeq g$ if there exists a sequence of homomorphisms
\begin{align*}
  P: C_n \to C_{n+1}', \quad n \in \Z
\end{align*}
such that
\begin{align*}
  \del' \circ P + P \circ \del = f_n - g_n, \quad \forall n \in \Z
\end{align*}
the sequence $P = (P_n)$ is called a \textbf{chain homotopy} and we write $P: f \simeq g$.

We say that $f: C_{\bullet} \to C_{\bullet}'$ is a \textbf{chain equivalence}, if there exists a $g: C_{\bullet}' \to C_{\bullet}$ such that
\begin{align*}
  g \circ f \simeq \id_{C_{\bullet}} \quad \text{and} \quad 
  f \circ g \simeq \id_{C_{\bullet}'}
\end{align*}
\end{dfn}

\begin{rem}[]
What does such a map between chain complexes have to do with homotopy?
Well if $f,g: X \to Y$ is are homotopic, we have a map
\begin{align*}
  H : [0,1] \times X \to Y \quad \text{with} \quad 
  H(0,x) = f(x), H(1,x) = g(x)
\end{align*}

%Let $X,Y$ be topological spaces and $C_{\bullet}(X), C_{\bullet}(Y)$ their respective simplicial chain complexes.
%For $n = 1$, we get maps $P_0: C_0(X) \to C_1(Y), P_1: C_1(X) \to C_2(Y)$ such that
%\begin{align*}
  %\del_2' \circ P_1 + P_0 \circ \del_1 = f_1 + g_2
%\end{align*}

\end{rem}

\begin{rem}[]
The relation of being chain homotopic is a congruence on $\Comp$ and this can be used to define a category $\textsf{hComp}$.
\end{rem}

\begin{prop}[]
Let $f,g: (C_{\bullet},\del) \to (C_{\bullet}', \del')$ be two chain maps with $f \simeq g$.
Then for all $n$
\begin{align*}
  H_n(f) = H_n(g): H_n(C_{\bullet}) \to H_n(C_{\bullet}')
\end{align*}
In particular, if $f$ is a chain equivalence, then $H_n(f)$ is an isomorphism for each $n$.
\end{prop}
The proof is identical to \ref{lem:homology-p}


As special case of chain homotopy is where one map is the identity and the other is the zero map.


\begin{dfn}[]
A \textbf{contracting homotopy} $Q$ of a chain complex $C_{\bullet}$ is a sequence of maps $Q_n: C_n \to C_{n+1}$ such that
\begin{align*}
  \del Q + Q \del = \id_{C_n}, \quad \forall n \in \Z
\end{align*}
\end{dfn}


\begin{cor}[]\label{cor:contracting-homotopy}
If a chain complex $C_{\bullet}$ has a contracting homotopy then it is acyclic.
\end{cor}
\begin{proof}
  By the previous proposition we see that for all $n$: $H_n(\id_{C_{\bullet}})= H_n(0) = 0$.
  Since $H_n$ is a functor, it follows that $H_n(C_{\bullet}) = 0$.
\end{proof}

\begin{rem}[]
The converse to this corollary is false.
Take for example
\begin{align*}
  C_n :=
  \left\{\begin{array}{ll}
    \Z/2\Z & n = 0\\
    \Z & n = 1,2\\
    0 & \text{otherwise}
  \end{array} \right.
\end{align*}
and define
\begin{align*}
  \del_2 = (k \mapsto 2k): C_2 \to C_1, \quad
  \del_1 = (k \mapsto k \mod 2): C_1 \to C_0
\end{align*}
with zero maps elsewere.

This complex is acyclic but there does not exist a contracting homotopy.
If such a $Q$ existed, then $Q_0: \Z/2\Z \to \Z$ would define a right inverse to $\del_1:\Z \to \Z/2\Z$, but any group homomorphism $\Z/2\Z \to \Z$ is trivial.


However, if $C_{\bullet}$ is a complex where all groups are free abelian groups, then the converse does hold.

The following partial converse does however hold:
If $f: C_{\bullet} \to C_{\bullet}'$ is a chain map between two free chain complexes such that $H_n(f)$ is an isomorphism for all $n$, then $f$ is a chain equivalence.

We will prove this towards the end.
\end{rem}

