\section{Introduction}

Bookmark: 28

\begin{center}
``Es ist nicht so, wie es vielleicht in einigen Vorlesungen mathematischer
Natur aussehen könnte, dass die Terminologie vom Himmel gefallen ist. Die formal
definierten Terme sind immer eine Approximation oder eine Abstraktion intuitiver Begriffe,
deren Formalisierung wir brauchen, um überhaupt exakte Aussagen über gewisse Objekte
und Erscheinungen formulieren zu können und um die Realisierung einer formal sauberen
und eindeutigen Argumentation (Beweisführung) zu ermöglichen.''
\end{center}



\section{Alphabets, Words, Languages, and the representation of problems}

\subsection{Goals}
A computer works with texts that are nothing but sequences of symbols from a given alphabet.
Programs are texts over the alphabet of keys on a keyboard.
Input and ouput can also be modelled as being nothing else but texts over a well-chosen alphabet.

\begin{itemize}
  \item 
    The first goal is to formalize the notion of texts as carriers of information.
    This lets us define the notion of algorithmic problems, algorithms, computers, computation, input, ouput etc.

    The first concepts we define are that of \textbf{alphabet}, \textbf{word} and \textbf{language}.

  \item 
    The second goal is to study how the introduced formalism lets us represent algorithmic problems, which we divide into two categories: \textbf{decision problems} and \textbf{optimisation problems}
  \item 
    The last goal is to think about the compressibility of texts. 
    To do so, we introduce the notion of \textbf{Kolmogorov complexity}.
    This lets us talk about what it means for something to be the shortest representation of texts, and also lets us measure the amount of information contained in a text.
    It also lets us define what it means for a text to be \textbf{random}.
\end{itemize}



\subsection{Alphabets, Words and Languages}

\begin{dfn}[]
An \textbf{alphabet} is a non-empty set $\Sigma$. 
We call the elements of this set \textbf{symbols}, or \textbf{letters}.
\end{dfn}
\begin{xmp}[]
\begin{itemize}
  \item $\Sigma_{\text{bool}} = \{0,1\}$ is the Boolean alphabet.
  \item $\Sigma_{\text{lat}} = \{a,b,c,\ldots,z\}$ is the latin alphabet.
  \item $\Sigma_{\text{keyboard}} =\Sigma_{\text{lat}}  \cup \{A,B,\ldots,Z,\textvisiblespace,<,>,!,[,], \ldots\}$ is the alphabet of all symbols on a computer, where \textvisiblespace{} denotes a whitespace.
  \item $\Sigma_m = \{0,1,\ldots m-1\}$ for $m \in \N^{+}$ is the alphabet of the $m$-apdic representation of numbers.
  \item $\Sigma_{\text{logic}} = \{0,1,x,(,),\land, \lor, \lnot\}$ is an alphabet with which we can express Boolean formulae.
\end{itemize}
\end{xmp}

\begin{dfn}[]
Let $\Sigma$ be an alphabet. A \textbf{word} over $\Sigma$ is a finite sequence of letters in $\Sigma$.
We denote the \textbf{empty word} with $\lambda$ or $\epsilon$.
The \textbf{length} of a word $w$ is denoted with $\abs{w} \in \N$.
\begin{align*}
  \Sigma^{\ast} &:= \{w | w \text{ is a word over }\Sigma\}
  \\
  \Sigma^{+} &:= \Sigma^{\ast} \setminus \{\lambda\}
  \\
             &=\{x_1x_2 \dots x_n \big\vert n \in \N, x_i \in \Sigma\ \forall i = 1, \ldots, n\}
\end{align*}
\end{dfn}
Note that per this definition, ``xnopyt'', ``aaaaaaajjjjjjjjj'', and ``hrrkrkrkrwpfrbrbrbrlablblblblblblblblblwhitooap'' are words over $\Sigma_{\text{lat}}$, despite being pretty much meaningless.


\textbf{Notation:} Although sequences are usually written using comma separation like $x_1,x_2,\ldots,x_n$, we will write them as $x_1x_2\ldots x_n$.


The following is taken from the exercises
\begin{prop}[]
  \begin{itemize}
    \item There exists a function
      \begin{align*}
        \text{Number}:
        \Sigma_{\text{bool}}^{\ast} 
        \to \N,
        \qquad
        x_1\dots x_n 
         \mapsto 
        \sum_{i=1}^{n}x_i \cdot 2^{n-i},
        \quad
        \lambda &\mapsto 0
      \end{align*}
      with right-inverse
      \begin{align*}
        \text{Bin}: \N \to \Sigma_{\text{bool}}^{\ast}, \qquad
        \text{Bin}(0) = 0, \quad
        \text{Bin}(n) = \text{the binary representation of $n$ with leading digit $1$.}
      \end{align*}  
    \item For $n \in \N$, it holds $\abs{\text{Bin}(n)} = \ceil{\log_2(n+1)}$
    \item We can represent boolean formulae with $\Sigma_{\text{logic}} = \{0,1,x,(,),\land,\lor,\lnot\}$ by encoding the variable $x_i$ as $x \text{Bin}(i)$.
      For rexample, the formula
      \begin{align*}
        (x_1 \lor x_7) \land \lnot(x_{12}) \land (x_4 \lor x_8 \lor \lnot(x_2))
      \end{align*}
      can be written as
      \begin{align*}
        (x1 \lor x111) \land \lnot(x1100) \land (x100 \lor x_1000 \lor \lnot(x10))
      \end{align*}
  \end{itemize}
\end{prop}

\begin{dfn}[]
The \textbf{concatenation} over an alphabet $\Sigma$ is the map
\begin{align*}
  \text{cat}: \Sigma^{\ast} \times \Sigma^{\ast} \to \Sigma^{\ast}
  \qquad
  \text{cat}(x,y) = xy
\end{align*}
We will sometimes write $\cdot: \Sigma^{\ast} \times \Sigma^{\ast} \to \Sigma^{\ast}$ instead of $\text{cat}$.
\end{dfn}

\begin{prop}[]
  $(\Sigma^{\ast},\text{cat})$ is a Monoid with neutral element $\lambda$.
  In other words
  \begin{itemize}
    \item $\text{cat}$ is associative: For $x,y,z \in \Sigma^{\ast}$:
  \begin{align*}
    \text{cat}(x, \text{cat}(y,z)) = x \cdot (y \cdot z) = xyz = (x \cdot y) \cdot z = \text{cat}(\text{cat}(x,y),z)
  \end{align*}
    \item For all $x \in \Sigma^{\ast}$ we have $x \cdot \lambda = \lambda \cdot x = x$.
  \end{itemize}
  Moreover, the length of a word is a monoid homomorphism
  \begin{align*}
    \abs{-}: (\Sigma^{\ast},\text{cat}) \to (\N,+)
  \end{align*}
  in other words: for all $x,y \in \Sigma^{\ast}$ it holds $\abs{x \cdot y} = \abs{x} + \abs{y}$ and $\abs{\lambda} = 0$.
\end{prop}

\begin{dfn}[]
  Let $\Sigma$ be an alphabet, and $x = x_1x_2 \dots x_n \in \Sigma^{\ast}$.
  \begin{itemize}
    \item We define the \textbf{reversal} of the word $x$ to be the word
      \begin{align*}
        x^{R} := x_n x_{n-1} \dots x_1 \in \Sigma^{\ast}
      \end{align*}
      with $\lambda^{R} := \lambda$.
    \item 
      For $n \in \N$, define the $n$-th \textbf{iteration} of $x$ recursively as
      \begin{align*}
        x^{0} = \lambda, \quad 
        x^{1} = x, \quad \text{and} \quad x^{n+1} = x \cdot x^{n}
      \end{align*}
    \item Let $u,v \in \Sigma^{\ast}$.
      We say that
      \begin{itemize}
        \item $v$ is a \textbf{subword} of $w$ if $\exists x,y \in \Sigma^{\ast}: w = xvy$
        \item $v$ is a \textbf{prefix} of $w$ if $\exists y \in \Sigma^{\ast}: w = vy$
        \item $v$ is a \textbf{suffix} of $w$ if $\exists x \in \Sigma^{\ast}: w = xv$
        \item $v \neq \lambda$ is a \textbf{true} subword/prefix/suffix of $w$ if $v$ is a subword/prefix/suffix of $w$ and $v \neq w$.
      \end{itemize}
    \item Let $\alpha \in \Sigma$. We define
      \begin{align*}
        \abs{x}_{\alpha} := \abs{\{i = 1, \ldots \abs{x} \big\vert x_i = \alpha\}}
      \end{align*}
  \end{itemize}
\end{dfn}

\begin{prop}[]
Let $\Sigma$ be an alphabet.
\begin{itemize}
  \item For $x,y \in \Sigma^{\ast}$, it holds $(xy)^{R} = x^{R}y^{R}$.
  \item Let $x \in \Sigma^{\ast}$ and $n = \abs{x}$. Then $x$ can have at most
    \begin{align*}
      1 + \sum_{i=1}^{n} n+1 - i = n^{2} + n + 1 - \frac{n(n+1)}{2} = \frac{n(n+1)}{2} + 1
    \end{align*}
    subwords.
    Note that it can have fewer, for example $x = 1111 \in \Sigma_{\text{bool}}^{\ast}$.
  \item Let $x \in \Sigma^{\ast}$. Then
    \begin{align*}
      \abs{x} = \sum_{\alpha \in \Sigma} \abs{x}_{\alpha}
    \end{align*}
\end{itemize}
\end{prop}

Given a finite alphabet, we can order the words just like we do in a dictionary.

\begin{dfn}[]
Let $\Sigma = \{s_1,\ldots,s_m\}$ for $m \in \N^{+}$ be a finite alphabet.
The \textbf{canonical ordering} on $\Sigma^{\ast}$ is the one induced by the ordering $s_i < s_j \iff i < j$ on $\Sigma$.

So for $x,y \in \Sigma^{\ast}$, we have
\begin{align*}
  x < v \iff &\abs{x} < \abs{y}\\
             &\lor \abs{x} = \abs{y} \land x = w \cdot s_i \cdot x' \land y = w \cdot s_j \cdot y' \text{ for some } w,x',y'\in  \Sigma^{\ast} \text{ and } i < j
\end{align*}
so we are first ordering by length and then letterwise from the left to the right.

\end{dfn}


As mentioned earlier, not all words over an alphabet have a meaning associated to them.

\begin{dfn}[]
A \textbf{Language} over an alphabet $\Sigma$ is a subset of $\Sigma^{\ast}$.
\begin{itemize}
  \item $L_{\emptyset} = \emptyset$ is the \textbf{empty Language}.
  \item $L_{\lambda} = \{\lambda\}$ is the language that consists of the empty word.
  \item If $L_1,L_2$ are a language over $\Sigma$, then
    \begin{align*}
      L_1 \cdot L_2 = L_1 L_2 = \{vw \big\vert v \in L_1, w \in L_2\}
    \end{align*}
    is called the \textbf{concatenation} of $L_1$ and $L_2$.
    We define
    \begin{align*}
      L^{0} := L_{\lambda}, \quad 
      L^{1} = L, \quad
      L^{n+1} := L^{n} \cdot L
    \end{align*}
    aswell as
    \begin{align*}
      L^{\ast} &:= \bigcup_{n \in \N}L^{n}\\
      L^{+} &:= L \cdot L^{\ast} = \bigcup_{n \in \N^{+}} L^{n}
    \end{align*}  
    we call $L^{\ast}$ the \textbf{Kleene's star} over $L$.
\end{itemize}
\end{dfn}

The set of all gramatically correct sentences in the english language are a language over $\Sigma_{\text{keyboard}}$. 
The set of all syntactically correct $C++$ programs is a language over $\Sigma_{\text{keyboard}}$.

Since languages are just sets, we can define unions and intersections on languages.
Together with the concatenation, we can explore the properties of these operations.

\begin{lem}[]
  Let $L_1,L_2,L_3$ be languages over an alphabet $\Sigma$. Then
  \begin{enumerate}
    \item Distributivity over $\cup$
  \begin{align*}
    L_1(L_2 \cup L_3) = L_1L_2 \cup L_1L_3
  \end{align*}
    \item Sub-distributivity over $\cap$:
      \begin{align*}
    L_1(L_2 \cap L_3) \subseteq L_1L_2 \cap L_1L_3
      \end{align*}
  \end{enumerate}
\end{lem}
\begin{proof}
  \begin{enumerate}
    \item We show inclusion in both directions
      \begin{itemize}
        \item[``$\subseteq$''] Let $x \in \text{LHS}$, so there exist $u \in L_1$ and $v \in L_2 \cup L_3$ such that $x = uv$. In either case, $x \in \text{RHS}$.
        \item[``$\supseteq$''] Let $x \in \text{RHS}$. There either exist $u \in L_1, v \in L_2$ such that $x = uv$, or $u \in L_1,v \in L_3$ such that $x = uv$. In either case, $x \in \text{LHS}$.
      \end{itemize}
    \item Let $x \in \text{LHS}$. This means there exist $u \in L_1$ and $v \in (L_2 \cap L_3$ such that $x = uv$. Since $v \in L_2$ and $v \in L_3$, $uv \in L_1L_2$ aswell als $uv \in L_1L_3$.
  \end{enumerate}
\end{proof}
The reason the other inclusion in (b) doesn't work is that if $x \in \text{RHS}$, it can be of the form $x = uv = u'v'$, for $u \neq u' \in L_1$ and $v \in L_2, v' \in L_3$.


As noted earlier, $(\Sigma^{\ast},\cdot)$ is a monoid, so it makes sense to sondier monoid homomorphisms between them.
\begin{dfn}[]
Let $\Sigma_1,\Sigma_2$ be alphabets. A \textbf{homomorphism} from $\Sigma_1^{\ast}$ to $\Sigma_2^{\ast}$ is a function $h: \Sigma_1^{\ast} \to \Sigma_2^{\ast}$ such that
\begin{enumerate}
  \item $h(\lambda) = \lambda$
  \item $h(uv) = h(u) h(v)$ for all $u,v \in \Sigma_1^{\ast}$.
\end{enumerate}
\end{dfn}

\begin{rem}[]
  To specify a monoid homomorphism $h: \Sigma_1^{\ast}\to \Sigma_2^{\ast}$ is the same as defining a function on the alphabet $\Sigma_1 \to \Sigma_2$.
  In other words:
  There exists a bijection
  \begin{align*}
    \Hom_{\textsf{Mon}}(\Sigma_1^{\ast},\Sigma_2^{\ast}) \iso \Hom_{\Set}(\Sigma_1,\Sigma_2^{\ast})
  \end{align*}
  so the functor $F: \Set \to \textsf{Mon}$ given by $F \Sigma = \Sigma^{\ast}$ is left adjoint to the forgetful functor $\textsf{Mon} \to \Set$.
\end{rem}





