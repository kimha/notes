\setcounter{section}{-1}
\section{Introduction \& Motivation}

A common equation we consider in linear algebra is a lineaa linear of the form
\begin{align*}
  Ax = b \quad \text{for} \quad  A \in \R^{n \times n}, \quad x,b \in \R^{n}
\end{align*}

We can identify elements of $\R^{n}$ with functions $f: \{1, \ldots, n\} \to \R$ by writing them as
\begin{align*}
  \left(
    f: \{1,\ldots,n\} \to \R
  \right)
  \mapsto (f(i))_{i=1,\ldots,n} \in \R^{n}
\end{align*}

We can think of functional analysis as the extension of linear algebra to infinite dimensional vector spaces.

For example, solving the linear poisson equation
\begin{align*}
  \Delta \phi = f, \quad \text{for} \quad \phi, f: \R \to \R
\end{align*}
is analogous to the example from before, where we can think of the Laplace operator $\Delta$ as an infinite-dimensional analogue to $n \times n$ matrices.


Another object of study in functional analysis is that of Brownian motion, which we can think of as a continuous function $f: [0,1] \to \R$ that is ``random'' in that it has ``equal probabilty'' of going in each direction, at any time.

In order to rigorously define what that means, we need to turn the space $X = C([0,1],\R)$ of continuous functions, into a probability space.

That is we need to ask ourselves if there even exists a $\sigma$-additive function
\begin{align*}
  \IP: \mathcal{B}(X) \to [0,1] \quad \text{with} \quad \IP(\emptyset) = 0
\end{align*}
where $\mathcal{B}(X)$ is the Borel $\sigma$-algebra on $X$.

This is not an easy question to answer, but functional analysis lets us prove that such a $\IP$ exists.


\begin{thm}[]
There exists a function
\begin{align*}
  \eta: L^{2}([0,1],\R) \to L^{2}(\Omega,\mathcal{F},\IP)
\end{align*}
such that
\begin{enumerate}
  \item $\eta$ is a linear isometry:
    \begin{align*}
      \scal{f,g} := \int_{0}^{1} f(s) g(s) ds = \E[\eta(f) \eta(g)]
    \end{align*}
  \item $\eta(f) \sim \mathcal{N}(0,\|f\|_2)$.
\end{enumerate}
\end{thm}
\begin{proof}
  Take any $\Omega$ which contains a sequence of $(X_i)_{i \geq 1}$ i.i.d. $\mathcal{N}(0,1)$.

  Then chose any ONB $(e_i)_{i \geq 1}$ in $L^{2}([0,1],\R)$. (For example a Fourier-basis).

  Using some results from functional analysis, we can show that there exists a linear map
  \begin{align*}
    \eta: L^{2}([0,1],\R) \to L^{2}(\Omega, \Sigma,\IP)
  \end{align*}
  that satisfies $f(e_i) = X_i$.

  Since we have
  \begin{align*}
    \E[X_iX_j] = \E[X_i]\E[X_j] = \delta_{ij}
  \end{align*}
  they form an orthonormal system.

  Now that we can map the basis vectors, we can map an arbitrary element of the space, by writing
  \begin{align*}
    f = \sum_{i=1}^{\infty} \lambda_i e_i \mapsto \eta(f) := \sum_{i=1}^{\infty}\lambda_i X_i
  \end{align*}
  which is well-defined (i.e. converges) by Parseval's theorem 
  \begin{align*}
    \|\eta(f)\|^{2} = \sum_{i=1}^{\infty} \lambda_i^{2} = \|f\|^{2}< \infty
  \end{align*}
\end{proof}

Now, this lets us define a brownian motion by setting $B_t := \eta(\mathds{1}_{[0,t]})$.

And let's check that this is indeed brownia, i.e. that it satisfies
\begin{align*}
  (B_{t_0}, B_{t_1} - B_{t_0}, \ldots, B_{t_n} - B_{t_{n-1}}) \sim \mathcal{N}\left(
    0, \begin{pmatrix}
    {t_0}_{\ddots} & 0\\
    0 &  t_n- t_{n-1}
    \end{pmatrix}
  \right)
\end{align*}

Indeed, we have 
\begin{align*}
  \E[\eta(f)] = 0 \quad \text{and} \quad \E[\eta(f) \eta(g)] = \scal{f,g}
\end{align*}
which lets us calculate the variance:
\begin{align*}
  \variance(B_{t_i+1} - B_{t_i}) = \int_{0}^{1}\mathds{1}_{(t_i,t_{i+1}]}(s) \mathds{1}_{(t_j,t_{j+1}]}(s) ds = \delta_{ij} (t_{i+1} - t_i)
\end{align*}
which checks out.
But we aren't done. 
As we want the space $[0,1]$ and not some abstract space $\Omega$, we still need to check if the mapping $t \mapsto B_t(\omega)$ is continuous.

We won't do that now but we might see that later using \textbf{Sobolev spaces}, which for $1 \leq p < \infty$ is the space
\begin{align*}
  W^{\theta,p}([0,1])
  =
  \left\{
    f: [0,1] \to \R \big\vert \int_{0}^{1}\int_{0}^{1} \frac{\abs{f(t) - f(s)}^{p}}{\abs{t - s}^{p \theta + 1}} dt ds < \infty
  \right\}
\end{align*}
and for $p = \infty$, we recover the Hölder functions of order $\theta$:
\begin{align*}
  W^{\theta,\infty}([0,1]) = \left\{
    f: [0,1] \to \R \big\vert \sup_{t,s} \frac{\abs{f(t) - f(s)}}{\abs{t-s}^{\theta}} < \infty
  \right\}
\end{align*}

The embedding theorem shows us that 
\begin{align*}
  \theta_1 - \frac{1}{p} \geq \theta_2 \implies W^{\theta_1,p} \subseteq W^{\theta_2,\infty}
\end{align*}

since
\begin{align*}
  \E \left[
    \int_{0}^{1}\int_{0}^{1} \frac{\abs{B_t - B_0}^{p}}{\abs{t-s}^{\theta p + 1}} dt ds
  \right]
  =
  \int_{0}^{1}\int_{0}^{1} \frac{\E\left[
      \abs{B_t - B_0}^{p}
  \right]}{\abs{t-s}^{\theta p + q}} dt ds
\end{align*}

There exists a nullset $N$ such that outside of $N$:
\begin{align*}
  t \mapsto  B_t(\omega) \text{ is Hölder for } \theta < \frac{1}{2}
\end{align*}

The hope is that these examples do not seem too arbitrary and give enough motivation.


\section{Completeness, Baire Category}

\subsection{Metric spaces}
\begin{center}
Insert definition of Metric spaces.
Open, closed sets, interior, closure, boundary, denseness.
\end{center}

\begin{dfn}[]
  Let $M$ be a metric space
  \begin{enumerate}
    \item $\Omega \subseteq M$ is \textbf{dense}, if $\overline{\Omega} = M$, or equivalently, if for all non-empty open balls $B$:
      \begin{align*}
        B \cap \Omega \emptyset
      \end{align*}
    \item $\Omega \subseteq M$ is called \textbf{nowhere dense}, if $\overset{\circ}{\overline{\Omega}} = \emptyset$, or equivalently, if for all non-empty open balls $B$
      \begin{align*}
        B \setminus \overline{\Omega} \emptyset \emptyset
      \end{align*}
  \end{enumerate}
\end{dfn}

\begin{lem}[]\label{lem:open-dense}
Let $U \subseteq M$. For $A = U^{c}$, the following are equivalent:
\begin{enumerate}
  \item $U$ is open and dense.
  \item $A$ is closed and nowhere dense.
\end{enumerate}
\end{lem}

\begin{lem}[]
  $A \subseteq M$ is closed if and only if for all sequences $(x_n)_{n \in N}$ in $A$:
  \begin{align*}
    \lim_{n \to \infty}x_n = x \implies x\in A
  \end{align*}
\end{lem}

\begin{dfn}[]
A sequence $\left(x_{n}\right)_{n \in \N}$ is called a \textbf{Cauchy-sequence}, if $\forall  \epsilon > 0$, there exists an $N \in \N$ such that
\begin{align*}
  n,m \geq N \implies d(x_n,x_m) < \epsilon
\end{align*}

A metric space is called \textbf{complete}, if every Cauchy-sequence converges.
\end{dfn}
\begin{xmp}[]
  \begin{enumerate}
    \item With the euclidean metric, $\R$ is complete, $\Q$ isn't.
    \item $C^{0}([0,1])$ with the sup-norm
      \begin{align*}
        \|f\|_{C^{0}} = \sup_{0 \leq x \leq 1} \abs{f(x)} = \max_{0 \leq x \leq 1} \abs{f(x)}
      \end{align*}
      is complete.
    \item $L^{p}(\Omega)$ and $C^{m}(\overline{\Omega})$ for $m \in \N$ are complete.
    \item For $\Omega \subseteq \R^{n}$ open such that $\Omega = \bigcup_{j=1}^{\infty}K_j$ for compact $K_j \subseteq K_{j+1}$, then $C^{0}(\Omega)$ with the metric
      \begin{align*}
        d(f,g) = \sum_{j=1}^{\infty} \frac{1}{2^{j}} \frac{\|f-g\|_{C^{0}(K_j)}}{1 + \|f-g\|_{C^{0}(K_j)}}
      \end{align*}
      is complete.
      The induced topology is called the \textbf{compact-open topology}.
  \end{enumerate}

\end{xmp}


