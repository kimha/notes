isSquare :: Int -> Bool
isSquare 1 = False
isSquare n = truncate(sqrt(x)) * truncate(sqrt(x)) == n
            where x = fromIntegral n
isntSquare = not . isSquare

isFund :: Int -> Bool
isFund x 
  | (x `mod` 4 == 1)
  && isntSquare x             = True
  | (x `mod` 4 == 0)
  && isntSquare (x `div` 4) 
  && (x `div` 4) `elem` [2,3] = True
  | otherwise                 = False

data BQF = BQF (Int,Int,Int)
  deriving Eq

instance Show BQF where
  show (BQF (a,b,c)) 
    = "(" ++ show a ++ "," ++ show b ++ "," ++ show c ++ ")"

discriminant :: BQF -> Int
discriminant (BQF (a,b,c)) = b^2 - 4 * a * c

basicForm :: Int -> BQF
basicForm d
  | (d `mod` 4) == 0 = BQF (1,0,round (-(fromIntegral d)/4))
  | (d `mod` 4) == 1 = BQF (1,1,round (fromIntegral (1-d)/4))
  | otherwise        = error "invalid discriminant"

isDefiniteReduced :: BQF -> Bool
isDefiniteReduced (BQF (a,b,c))
  | d >= 0 = error "form is not definite"
  | otherwise = ie1 && ie2 && ie3
    where
      d = discriminant (BQF (a,b,c))
      ie1 = -(abs a) < b
      ie2 = b <= abs a
      ie3 = abs a <= abs c

isIndefiniteReduced :: BQF -> Bool
isIndefiniteReduced (BQF (a,b,c))
  | d <= 0 = error "form is not indefinite"
  | otherwise = (a > 0) && (c > 0) && (b - a - c) > 0
    where
      d = discriminant (BQF (a,b,c))

isReduced :: BQF -> Bool
isReduced f
  | d > 0 = isIndefiniteReduced f
  | d < 0 = isDefiniteReduced f
  | otherwise = error "discriminant is 0"
    where
      d = discriminant f

redcoeff :: BQF -> Int
redcoeff (BQF (a,b,c)) = ceiling $ (b' + d') / (2 * a')
  where 
    b' = fromIntegral b
    d' = (sqrt . fromIntegral . discriminant) (BQF (a,b,c))
    a' = fromIntegral a

reduceStep :: BQF -> BQF
reduceStep (BQF (a,b,c)) = BQF (a',b',c')
  where
    a' = a * n^2 - b * n + c
    b' = 2 * a * n - b
    c' = a
    n = redcoeff (BQF (a,b,c))

reduceStep a b c = (a * n^2 - b * n + c, 2 * a * n - b, a)

-- appplies reduction steps to f
-- until the first reduced element repeats
reduction :: BQF -> [BQF]
reduction f = aux (iterate reduceStep f) Nothing
  where 
    -- second argument stores the first reduced element
    aux :: [BQF] -> Maybe BQF -> [BQF]
    aux (x:xs) Nothing
      | isReduced x = [x] ++ aux xs (Just x)
      | otherwise   = [x] ++ aux xs Nothing
    aux (x:xs) (Just y)
      | x == y      = [x]
      | otherwise   = [x] ++ aux xs (Just y)

-- using point-free notation
-- reduction' f = flip aux Nothing . iterate reduceStep

reduce :: BQF -> BQF
reduce = head . (dropWhile (not . isReduced)) . (iterate reduceStep)

-- how many reduction stepts does it take to reduce f
stepsTillReduced :: BQF -> Int
stepsTillReduced = (length . (takeWhile (not . isReduced)) . reduction)


-- two forms with D > 0 are equivalent, if and only if they end up in the same reduced cycle
areEquivalent :: BQF -> BQF -> Bool
areEquivalent f g
  | (d /= d') = False
  | d <= 0    = error "not definite forms"
  | otherwise = (reduce f) `elem` (reduction g)
  -- point-free form:
  -- otherwise = (. reduction) . elem . reduce
  where
    d  = discriminant f
    d' = discriminant g

eval :: [Double] -> Double
eval []     = 0
eval (x:[]) = x
eval (x:xs) = x - 1/(eval xs)
-- implementation with foldr 
eval' :: [Double] -> Double
eval' = (\x -> 1/x) . foldr (\n -> \x -> 1/(n-x)) 0

next :: (Double,Double) -> (Double,Double)
next (w,n) = (x,fromIntegral (floor x) + 1)
  where
    x = 1/(n - w)
realToConfrac :: Double -> [Double]
realToConfrac w = map snd $ iterate next (w,fromIntegral (floor w) + 1)

reconvert :: Double -> Int -> Double
reconvert x n = (eval . (take n) . realToConfrac) x

reconvert = flip ((eval . ) . (. realToConfrac) . take)
