\section{Summary of previous talks about Binary quadratic forms}
\begin{dfn}[]
  A \textbf{binary quadratic form} is an expression of the form
\begin{empheq}[box=\bluebase]{align*}
  f(x,y) = ax^{2} + bxy + cy^{2}
\end{empheq}
where $a,b,c$ are fixed \emph{coefficients} and $x,y$ are variables.
\end{dfn}
We will assume that $a,b,c \in \Z$ and that the number of variables $x,y$ is always two, so we will drop the word ``binary''.
We will also omit the word ``quadratic'' aswell and speak of ``forms'', meaning binary quadratic forms.

\begin{rem}[]
We will use the notation where $[a,b,c]$ denotes the bqf $f(x,y) = ax^{2} + bxy + cy^{2}$.
\end{rem}

The main question is whether the equation

\begin{align}\label{eq:bqf}
  f(x,y) = ax^{2} + bxy + cy^{2} = n, \quad n \in \Z
\end{align}
has solutions for $x,y \in \Z$ or not.

If we plot the graph of $z = f(x,y)$, we would expect that transformations such as flipping $x,y$ or doing $90$-degree rotations should preserve the structure of the set of solutions to \ref{eq:bqf}.

Let $\begin{pmatrix}
\alpha & \beta\\
\gamma & \delta
\end{pmatrix}$ with integer coefficients and determinant $1$, i.e. $\begin{pmatrix}
\alpha & \beta\\
\gamma & \delta
\end{pmatrix}
\in \text{SL}(2,\Z)$.

With the substitution
\begin{align*}
  \begin{pmatrix}
  x\\
  y
  \end{pmatrix}
  \mapsto \begin{pmatrix}
  x'\\
  y'
  \end{pmatrix}
  =
  \begin{pmatrix}
  \alpha & \beta\\
  \gamma & \delta
  \end{pmatrix}
  \begin{pmatrix}
  x\\
  y
  \end{pmatrix}
  =
  \begin{pmatrix}
  \alpha x + \beta y\\
  \gamma x + \delta y
  \end{pmatrix}
\end{align*}
equation \ref{eq:bqf} turns into 
\begin{align*}
  f(x',y') 
  &= 
  a' x^{2} + b'' xy + c'y^{2} 
  \\
  &= a(\alpha x + \beta y)^{2} + b(\alpha x + \beta y)(\gamma x + \delta y) + c(\gamma x + \delta y)^{2}
  \\
  &= a'x^{2} + b'xy + c'y^2
\end{align*}
where $a',b',c'$ are given by
\begin{align*}
  a' &= a \alpha^{2} + b \alpha \gamma + c \gamma^{2}\\
  b' &= 2 a \alpha \beta + b(\alpha \delta + \beta \gamma) + 2 c \gamma \delta\\
  c' &= a \beta^{2} + b \beta \delta + c \delta^{2}
\end{align*}

\begin{dfn}[]
  Two forms $[a,b,c]$, $[a',b',c']$ are \textbf{equivalent}, if there exist $\begin{pmatrix}
  \alpha & \beta\\
  \gamma & \delta
  \end{pmatrix}
  \in \text{SL}(\Z)$ such that
\begin{align*}
  a' &= a \alpha^{2} + b \alpha \gamma + c \gamma^{2}\\
  b' &= 2 a \alpha \beta + b(\alpha \delta + \beta \gamma) + 2 c \gamma \delta\\
  c' &= a \beta^{2} + b \beta \delta + c \delta^{2}
\end{align*}
\end{dfn}
One quickly verifies that this defines an equivalence relation on all binary quadratic forms, as $\text{SL}_2(\Z)$ forms a group, so matrix multiplication and inverses exist.


\begin{prop}[]
Given a form $[a,b,c]$, the \textbf{discriminant}
\begin{empheq}[box=\bluebase]{align*}
  D = b^{2} - 4ac
\end{empheq}
is an invariant in the equivalence class of the form.

Moreover, for any $D$ with
\begin{align*}
  D = 0 \text{ or } 1\ \mod\ 4
\end{align*}
there exists a form with discriminant $D$, namely the \textbf{basic form} (Grundform)
\begin{align*}
  f(x,y) = \left\{\begin{array}{ll}
    x^{2} - \frac{D}{4}y^{2} & \text{ if } D= 0\ \mod\ 4\\
    x^{2} + xy + \frac{1-D}{4}y^{2} & \text{ if } D = 1\ \mod\ 4
  \end{array} \right.
\end{align*}
\end{prop}


\begin{thm}[] \label{thm:bqf-1}
Let $D \in \Z$ not a square. 
Then there are only finitely many equivalence classes of quadratic forms with discriminant $D$.

Moreover, every form $ax^{2} + bxy + cy^{2}$ is equivalent to a form $a'x^{2} + b'xy + c'y^{2}$ whose coefficients satisfy
\begin{empheq}[box=\bluebase]{align*}
  \abs{b'} \leq \abs{a'} \leq \abs{c'}
\end{empheq}
\end{thm}
\begin{rem}[]
The theorem is even true if $D \neq 0$ is a square, but we won't consider forms with square discriminant as they reduce to linear factors.
\end{rem}
%\begin{proof}
  %We provide an terminating algorithm.

  %\begin{enumerate}
    %\item Substitute $(a,b,c)$ with
      %\begin{align*}
        %(a, b- 2na, c - nb + n^{2}a)
      %\end{align*}
      %where we chose $n \in \Z$ such that $- \abs{a} < b - 2na \leq \abs{a}$.
    %\item If $\abs{c} \geq \abs{a}$ we're done.
    %\item Else, substitute $(a,b,c)$ with $(c,-b,a)$ and start over.
  %\end{enumerate}
  %After every cycle, $\abs{a}$ decreases.
%\end{proof}


We wish to compute and study the number of equivalence classes with discriminant $D$.
Besides the discriminant, there are other invariants of equivalence classes
\begin{itemize}
  \item The $\gcd$ of the coefficients of $f$
  \item The sign of the first cofficient $a$, if $D < 0$.
\end{itemize}

\begin{dfn}[]
  Let $f(x,y) = ax^{2} + bxy + cy^{2}$ be a quadratic form with discriminant $D < 0$.
  \begin{itemize}
    \item $f$ is called \textbf{positive definite}, if $a >0$, and negative-definite, if $a < 0$.

    \item $f$ is called \textbf{primitive}, if the $\gcd$ of the coefficients is $1$.
  \end{itemize}
\end{dfn}
One quickly checks that 
\begin{align*}
  f(\alpha,\gamma) > 0 \forall (\alpha,\gamma) \neq (0,0) \iff f \text{ positive definite}
  f(\alpha,\gamma) < 0 \forall (\alpha,\gamma) \neq (0,0) \iff f \text{ negative definite}
\end{align*}
By multiplication with $-1$, we see that the negative-definite forms are in $1$-to-$1$ correspondence with positive-definite forms, so it suffices to sutdy the

Note that if $f$ has discriminant $D$ with $\gcd(a,b,c) = r$, then $\frac{f}{r}$ is a primitive form with discriminant $\frac{D}{r^{2}}$.

\begin{cor}[]
  If $f$ is positive definite, then $f(x,y) > 0$ for all $(x,y) \neq (0,0) \in \Z^{2}$.
\end{cor}

\begin{dfn}[]
For $D \in \Z$, we call its \textbf{class number} 
\begin{align*}
  h(D) =
  \left\{\begin{array}{ll}
    \text{Number of equivalence classes of}\\
  \text{primitive forms wth discriminant $D$} & \text{ if } D > 0\\
\text{Number of equivalence classes of \emph{positive-definite} }
\\
\text{primitive forms with discriminant $D$} & \text{ if } D < 0
  \end{array} \right.
\end{align*}
\end{dfn}
By Theorem \ref{thm:bqf-1}, the $h(D)$ are finite.


Now let $f$ be a quadratic form. We want to find out how many solutions $f(x,y) = n$ for in $x,y \in \Z$

Now if we consider the set of solutions $(x,y)$, there is an equivalence relation on it given by the transformation


\begin{table}[h]
\centering
\begin{tabular}{L|L|l}
  \text{Discriminant} & \text{Coefficient} & Label\\
  D < 0 & a > 0 & \textbf{positive definite}\\
  D < 0 & a < 0 & \textbf{negative definite}\\
  & \gcd(a,b,c) = 1 & \textbf{prime}
\end{tabular}
\caption{Definitions of binary quadratic forms}
\end{table}

\begin{lem}[]
If $D = 2$ or $D = 3$ $\mod\ 4$, then there are no forms with discriminant $D$.
\end{lem}

Given an $f = ax^{2} + bxy + cy^{2}$, we want to know for which $n$ there is a solution to $f(x,y) = n$ and how many there are.

The transformations that map $f$ into itself are those that satisfy
\begin{align}
  a \alpha^{2} + b \alpha \gamma + c \gamma^{2} = a\\
  2 a\alpha \beta + b \beta \gamma + b \alpha \delta + 2 c \gamma \delta = b\\
  a \beta^{2} + b \beta \delta + c \delta^{2} = c
\end{align}

\begin{dfn}[]
Let $f = ax^{2} + bxy + cy^{2}$. Set
\begin{align*}
  U_f = \left\{
    \begin{pmatrix}
    \alpha & \beta\\
    \gamma & \delta
    \end{pmatrix}
    \in \text{SL}_2(\Z)
    \big\vert
    (2),(3),(4) \text{ hold}
  \right\}
\end{align*}
$R(n,f)$ is the number solutions of $f(x,y) = n$ that is invariant under $U_f$.

Fix $D$. The total number o
\begin{align*}
  R(n) = \sum_{i=1}^{h(D)} R(n,f_i)
\end{align*}
\end{dfn}

\begin{thm}[]
\begin{align*}
  R(n) = \sum_{m|n}\chi_D(m)
\end{align*}
\end{thm}

\begin{thm}[]
  Fix $D$. Then
\begin{align*}
  h(D) =
  \left\{\begin{array}{ll}
      \frac{w \sqrt{\abs{D}}}{2 \pi}L(1,\chi_D) & \text{ if }D < 0\\
      \frac{\sqrt{D}}{\log \epsilon_0}L(1,\chi_D) & \text{ if }D > 0
  \end{array} \right.
\end{align*}
where 
$\epsilon_0 = \frac{1}{2}(t + u \sqrt{D})$, for $t^{2} -Du^{2} = 4$ with $t,u$ smallest solution,
\begin{align*}\label{eq:w}
  w =
  \left\{\begin{array}{ll}
    2 & \text{ if }D < -4\\
    4 & \text{ if }D = -4\\
    6  & \text{ if }D = -3
  \end{array} \right.
\end{align*}
\begin{align*}
  L(1,\chi_D) = \sum_{k=1}^{\infty}\frac{\chi(k)}{k}
\end{align*}
\end{thm}




