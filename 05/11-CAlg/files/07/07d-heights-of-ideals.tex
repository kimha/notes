
Parallel to the dimension property, integral extensions also have very good properties with respect to heights.

\begin{thm}[Going-Down (ACL 9.5.2)]\label{thm:going-down}
  Let $R \subseteq A$ be an integral domain with
  \begin{enumerate}
    \item $A$ integral over $R$
    \item $R$ integrally closed.
  \end{enumerate}
  Then for any prime chain
  \begin{align*}
    p_0 \subsetneq \ldots \subsetneq p_n \subseteq R
  \end{align*}
  and any $q_n \subseteq A$ prime such that $q_n = R = p_n$, there exists a prime chain
  \begin{align*}
    q_0 \subsetneq \ldots \subsetneq q_n \subseteq A
  \end{align*}
  such that $p_i = q_i \cap R$ for all $i$.

  In particular:
  \begin{align*}
    \hht q \cap R = \hht q \quad \text{for all prime ideals} \quad q \subseteq A
  \end{align*}
\end{thm}

The result for heights holds because the main statement gives us $\hht q \geq \hht q \cap R$, and the previous theorem (Step 3) gives us $\hht q \cap R  \geq q$.

The proof relies on a result of independent interest.

Namely, in the example
\begin{align*}
  q_1 = (1 + 2i), \quad q_2 = (1 - 2i) \subseteq \Z[i] \quad \text{with} \quad q_i \cap \Z = 5\Z
\end{align*}
we see that the complex conjugation $\sigma \in \Aut(\C/\R)$\footnote{For a field extension $L/K$, we write $\Aut(L/K)$ for the field isomorphisms of $L$ preserving $K$.}, maps $q_1$ to $q_2$.
More generally:

\begin{prop}[ACL 9.5.1]
  Let $R$ be integrally closed with fraction field $K$. Let $L/K$ be a normal\footnote{$L$ is a splitting field of a polynomial $f \in K[X]$.} field extension
  and let $A \subseteq L$ be the integral closure of $R$ in $L$.
  Let $p \subseteq R$ prime. 

  Then the automorphism group $\Aut(L/K)$ acts on prime ideals $q \subseteq A$ such that $q \cap R = p$ by 
  \begin{align*}
    \sigma \cdot q := \sigma(q) \subseteq A
  \end{align*}
  and this action is transitive\footnote{For any $q_1,q_2$ such that $q_i \cap R = p_i$, there exists $\sigma \in \Aut(L/K)$ with $\sigma(q_1) = q_2$}.
\end{prop}
We prove this only for $L/K$ is a Galois extension ($f \in K[X]$ must be seperable).
This is the case for instance, when $K$ has charcteristic zero and $[L:K]$ is finite.

The general case is true, but requires infinite Galois theory.

\begin{step}[]
  For $\sigma \in \Aut(L/K)$, it holds $\sigma(A) = A$ and for $I \subseteq A$ an ideal, $\sigma(I) \cap R = I \cap R$.

  Thus, $\sigma$ induces an isomorphism $A/I \to A/\sigma(I)$.

\end{step}
\begin{proof}
Let $x \in A$. By integrality, let $n \in \N, r_i \in R$ with
\begin{align*}
  x^{n} + r_{n-1}x^{n-1} + \ldots + r_1 x + r_0 = 0
\end{align*}
Since $r_i \in R \subseteq K$, we can apply $\sigma$ to see that $\sigma(x)$ is an integral element:
\begin{align*}
  \sigma(x)^{n} + r_{n-1}\sigma(x)^{n-1} + \ldots + r_1 \sigma(x) + r_0 = 0
\end{align*}
Since $A$ is integrally closed, we have $\sigma(x) \in A$.
Applying $\sigma^{-1}$, we get surjectivity, i.e. $\sigma(A) = A$.

For the second part, we have $\sigma(I) \cap R = \sigma(i \cap R) = I \cap R$, which gives a short exact sequence
\begin{align*}
  0 \to I \to A \to A/\sigma(I) \to 0
\end{align*}
\end{proof}

\begin{step}[]
Let $p \subseteq R$ be prime and  $q_1,q_2 \subseteq A$ prime with $q_i \cap R = p$.

Then
\begin{align*}
  q_2 \subseteq \bigcup_{\sigma \in \Aut(L/K)} \sigma(q_1)
\end{align*}
\end{step}
\begin{proof}
Let $x \in q_2$. Define
\begin{align*}
  y = \prod_{\sigma \in \Aut(L/K)} \sigma(x)
\end{align*}
Since $\sigma(y) = y$, for all $\sigma$, and $L/K$ is a Galois extension\footnote{Recall from Algebra II that $L/K$ is a Galois extension if and only if the fixing field $L^{\Aut(L/K)}$ is $K$.} that $y \in K$.
Moreover $y$ is integral over $R$, since $\sigma(x) \in A$ for all $\sigma$.
SO $y \in R$.
Since $R$ is integrally closed in $K$, we have $y \in R$.
Finally, since $x$ is a factor $y$ and $x \in q_2$, we have
\begin{align*}
  y \in q_2 \implies y \in q_2 \cap R = q_1 \cap R \subseteq q_1
\end{align*}
since $q_1$ is prime, it means that some factor $\sigma(x) \in q_1$.
\end{proof}


\begin{step}[]
$q_2 \subseteq \sigma(q_1)$ for some $\sigma \in \Aut(L/K)$.
\end{step}
The proof of this step uses the Lemma below.

Since $q_2 \cap R = \sigma(q_1) \cap R = p$, we conclude from injectivity of chains (Step 3 of the previous theorem), that $q_2 = \sigma(q_1)$.

We conclude transitivity and thus the Proposition.


\begin{lem}[ACL 2.2.12]
  Let $R$ be a ring and $I \subseteq R$ an ideal, $p_{1}, \ldots, p_{n} \subseteq R$ prime ideals. If $I \subseteq \bigcup_{i}p_i$, there is an $i$ such that $I \subseteq p_i$.
\end{lem}
\begin{proof}
  We use induction on $n \geq 1$. The case $n = 1$ is obvious.
  Assume $m \geq 2$ and the result for $n-1$.
  If the conclusion fails, then induction implies $I \not\subseteq \bigcup_{j \neq i}p_j$ for all $i$.
  Pick $x_i \in I$ such that $x_i \notin p_j$ for $j \neq i$.
  Then $x_i$ must be in $p_i$ since $I \subset \bigcup_{i}p_i$.

  Define $x = x_1 + x_2 \cdots x_n \in I$.
  But modulo $p_1$, we have $x \equiv x_2 \cdots x_n \not\equiv 0$.
  Also $x \equiv x_1 \not\equiv 0$ modulo $p_i$ for $i \geq 2$.
  We conclude $x \notin \bigcup_{i}p_i$.

\end{proof}

Now we can prove the ``going-down'' theorem.

\begin{proof}[Proof Theorem]
  Let $L = \text{Quot}(A)$ and $K = \text{Quot}(R)$.

  First assume that $L/K$ is a normal extension

  Then by going-up, we get first get a chain
  \begin{align*}
    \tilde{q}_0 \subsetneq \ldots \subsetneq \tilde{q}_n \subseteq A
  \end{align*}
  with $\tilde{q}_i \cap R = p_i$.
  By the previous proposition, there is a $\sigma \in \Aut(L/K)$ such that $\sigma(\tilde{q}_m) = q_m$.
  Then
  \begin{align*}
    \sigma(\tilde{q}_0) \subsetneq \ldots \subsetneq \sigma(\tilde{q}_n) = q_m
  \end{align*}
  satisfies $\sigma(\tilde{q}_i) = \tilde{q}_i \cap R = p_i$.


  Now, in the general case, we can find an extension $\tilde{L}/L$ such that $\tilde{L}/K$ is normal.
  Let $\tilde{A} \subseteq \tilde{L}$ be the integral closure of $R$ in $\tilde{L}$, so $R \subseteq A \subseteq \tilde{A}$.

  Since $\tilde{A}$ is integral over $A$, we can find $\tilde{q}_n \subseteq \tilde{A}$ prime with $\tilde{q}_n \cap A = q_m$. Then by step $1$ applied to $R \subseteq \tilde{A}$, we can find a prime chain
  \begin{align*}
    \tilde{q}_0 \subsetneq \ldots \subsetneq \tilde{q}_n \subseteq \tilde{A}
  \end{align*}
  with $\tilde{q}_i = R = p_i$, but then just let $q_i = \tilde{q}_i \cap A$. Then we get a prime chain
  \begin{align*}
    q_0 \subsetneq \ldots \subsetneq q_n
  \end{align*}
  with $q_i \cap R = (\tilde{q}_i \cap A) \cap R = \tilde{q}_i \cap R = p_i$.

\end{proof}
