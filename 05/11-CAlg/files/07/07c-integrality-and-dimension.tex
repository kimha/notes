
\begin{thm}[Cohen-Seidenberg ``going-up'' (ACL 9.3.9/10)] \label{thm:going-up}
  Let $R \subseteq A$ be an integral extension.
For any prime chain 
\begin{align*}
q_0 \subsetneq \ldots \subsetneq q_n
\end{align*}
in $A$, there is a a prime chain in $R$
\begin{align*}
  q_0 \cap R \subsetneq \ldots \subsetneq q_n \cap R
\end{align*}
Conversely, for any prime chain
\begin{align*}
  p_0 \subsetneq \ldots \subsetneq p_n
\end{align*}
in $R$, there is a prime chain
\begin{align*}
  q_0 \subsetneq \ldots \subsetneq q_n
\end{align*}
with $p_i = q_i \cap R$. In particular $q \subseteq A$ is maximal $\iff q \cap R$ is maximal.

In particular
\begin{align*}
  \dim A = \dim R
\end{align*}
\end{thm}

\begin{rem}[]
Note that we are \emph{not} saying that $q \mapsto  q \cap R$ is a bijection.

For example, in $\Z[i] = \faktor{\Z[X]}{(X^{2}+1)}$ and $p = 5\Z \subseteq \Z$, there are two ideals
\begin{align*}
  q_1 = (1 + 2i), \quad q_2 = (1 - 2i), \quad \text{such that} \quad q_i \cap \Z = 5\Z
\end{align*}

But as we will see in the next theorem, once we chose the last ideal in the chain $q_n$, there exists a prime chain that ends with $q_n$.
\end{rem}


This theorem lets us calculate many dimensions of rings. For instance, $A = \faktor{\Z[X]}{(X^{n} - 1)}$ is integral over $\Z$ (see Example \ref{xmp:integral-polynomial})

By the theorem, $\dim A = \dim \Z = 1$ (see Proposition \ref{prop:pid-not-a-field}).

Similarly, $\Z[\sqrt{5}] = \faktor{\Z[X]}{(X^{2} -5)}$ has dimension $1$.

We prove the theorem in five steps.

\begin{step}[]
  If $A$ is an integral domain (so also $R$), then $\dim A = 0 \iff \dim R = 0$.
  By Proposition \ref{prop:field-iff-id-dim0}, this means that $A$ is a field $\iff$ $R$ is a field.
\end{step}
\begin{proof}
Suppose $A$ is a field. Let $x \neq 0 \in R$ and $x^{-1} \in A$ its inverse. 
By integrality, let $n \in \N, r_{i} \in R$ such that
\begin{align*}
  (x^{-1})^{n} + r_{n-1}(x^{-1})^{n-1} + \ldots + r_1 x^{-1} + r_0 = 0
\end{align*}
By multiplying with $x^{n}$, we get
\begin{align*}
  1 + r_{n-1}x + \ldots + r_1^{n-1} + r_0x^{n} = 0
\end{align*}
so the inverse of $x$ is given by
\begin{align*}
  - r_{n-1} - \ldots - r_1x^{n-2} - r_0x^{n-1} \in R
\end{align*}

Now suppose that $R$ is a field. Let $a \in A$ non-zero, with $n \in \N, r_i \in R$ such that
\begin{align*}
  a^{n} + r_{n-1}a^{n-1} + \ldots + r_1a + r_0 = 0
\end{align*}
since $a \neq 0$ and $A$ is an integral domain, we can assume $r_0 = 0$.
Then $a$ has an inverse given by
\begin{align*}
  a^{-1} = -r_0^{-1} + (a^{n-1} + r_{n-1}a^{n-2} + \ldots + r_1)
\end{align*}
Thus $A$ is a field.
\end{proof}


\begin{step}[]
$q \subseteq A$ is maximal if and only if $q \cap R \subseteq R$ is maximal.
\end{step}
\begin{proof}
  By Proposition \ref{prop:integrality-properties} part (d), for all $q \subseteq A$ we have an integral morphim
\begin{align*}
  \faktor{R}{q \cap R} \to  \faktor{A}{q}
\end{align*}
Since $A/q$ is an integral domain, the result follows from the first step.
\end{proof}

\begin{step}[]
The map $q \stackrel{f}{\mapsto} q \cap R$ from prime ideals in $A$ to prime ideals in $R$ is \emph{inective on chains}:
\begin{align*}
  \text{if $q_1 \subseteq q_2$:} \quad f(q_1) = f(q_2) \iff q_1 = q_2
\end{align*}
it is not inective in general, as $(1 + 2i) \cap \Z = (1 -2i)\cap \Z = 5\Z$.

In particular:
Any prime chain $q_0 \subsetneq \ldots \subsetneq q_n$ in $A$ gives a prime chain $q_0 \cap R \subsetneq \ldots \subsetneq q_n \cap R$.
\end{step}
\begin{proof}
Let $q \subseteq A$ prime and set $p = q \cap R$.
We claim that $q A_p$ is maximal in $A_p$ (where $A_p = (R - p)^{-1}A$.

Assuming this, we have for $p = q_1 \cap R = q_2 \cap R$ that $q_1 A_p \subseteq q_2 A_p$ and both are maximal, thus equal.

We know from \ref{prop:prime-ideals-localisation}, that $q  \mapsto  qA_p$ is a bijection 
\begin{align*}
  \left\{
    \text{prime ideals in } A_p
  \right\}
  \iso
  \left\{
    \text{prime ideals of $R$ not intersecting } (R - p)
  \right\}
\end{align*}
so $q_1 = q_2$.

To prove the claim, note that we have an integral extension $R_p \subseteq A_p$ (by Proposition \ref{prop:integrality-properties} part (c)) and by Step 2, $qA_p \cap R_p \supseteq pR_p$ is maximal. and $qA_p \subseteq qA_q$, so $1 \notin qA_p \cap R_p$.
Hence $qA_p \cap R_p = pA_p$.
\end{proof}
\begin{step}
  The map $q \mapsto  q \cap R$ is surjective from prime ideals in $A$ to those of $R$.
\end{step}
\begin{proof}
Let $p \subseteq R$ be an ideal.
The morphism $R_p \to A_p$ is integral and injective, so $A_p \neq 0$ and we can find a maximal ideal $m \subseteq A_p$.
Let $q = m \cap A$. Then $q$ is prime and $q \cap (A - p) = \emptyset$, so $f(q) = q \cap R \subseteq p$.
On the other hand, $m \cap R_p$ is maximal according to Step 2, so $m \cap R_p = pR_p$. So
\begin{align*}
  p \subseteq pR_p \subseteq m \cap R_p \subseteq m
\end{align*}
so $p \subseteq M \cap R = (m \cap A) \cap R = q \cap R$.
\end{proof}


\begin{step}
For any prime chain
\begin{align*}
  p_0 \subsetneq \ldots \subsetneq p_n \subseteq R
\end{align*}
there is a prime chain
\begin{align*}
  q_0 \subsetneq \ldots \subsetneq q_n \subseteq A
\end{align*}
such that $q_i \cap R = p_i$ for all $i$.
\end{step}
\begin{proof}
  By induction on $n$, the case $m = 0$ is given by step 4.
  It is also clear that it is enough to handle the case $m = 1$.
  So given $p_0 \subsetneq p_1$ and a $q_0$ with $q_0 \cap R = p_0$, we need to find a $q_1$ with $q_1 \cap R = p_1$ and $q_0 \subseteq q_1$.

  Consider again the integral morphism $R/p_0 \to A/q_0$.
  There is a prime ideal $\tilde{q}_1 \subseteq A/q_0$ such that $\tilde{q}_1 \cap R/p_0 = p_1/p_0$ and $\tilde{q}_1 = q_1/q_0$, where $q_1 \subseteq A$ with $q_1 \cap R = p_1$ and $q_0 \subseteq q_1$.
\end{proof}
