
\begin{dfn}[]
An $R$-algebra $A$ is \textbf{integral over $R$} if all elements of $A$ are integral over $R$.

We say that $R \subseteq A$ is an \textbf{integral extension} and that the structure morphism $\phi: R \hookrightarrow A$ is an \textbf{integral morphism}.
\end{dfn}


\begin{prop}[]
Let $A$ be an $R$-algebra. 
The set $B \subseteq A$ of all elements which are integral over $R$ forms a subalgebra of $A$. 
It is called the \textbf{integral closure} of $R$ in $A$.
\end{prop}
\begin{proof}
We need to show that if the set $B$ is closed with respect to addition and multiplication.

Let $b,b' \in B$.
Clearly, $bb', b+b'\in R[b,b'] = R[b][b']$ which is finitely-generated as an $R$-module, because if $(x_{1}, \ldots, x_{n})$ generate $R[b]$ as an $R$, module and $(y_{1}, \ldots, y_{m})$ generate $R[b][b']$ as an $R[b]$-module, then $(x_iy_j)_{ij}$ generate $R[b][b']$ as an $R$-module.

Moreover, $xR[b,b'] = \{0\} \implies x1 = 0$, so the previous criterion shows that $bb'$ and $b+b'$ are indeed integral over $R$.
\end{proof}


\begin{dfn}[]
Let $R \subseteq A$ be rings. Then $R$ is \textbf{integrally closed} in $A$, it its integral closure is $R$ iteself.

If $R$ is an integral domain, then $R$ is said to be \textbf{integrally-closed} if it is so in its fraction field.
\end{dfn}

By the previous Proposition, any UFD is integrally closed.

\begin{lem}[]
If $R$ is an integrally closed integral domain, and $S \subseteq R$ multiplicative with $0 \notin S$, then $S^{-1}R$ is integrally closed.
\end{lem}
\begin{proof}
The fraction field of $S^{-1}R$ is the same as the fraction field $K$ of $R$.
Let $x = \tfrac{a}{b} \in K$ be integral over $S^{-1}R$ and suppose
\begin{align*}
  x^{n} + \frac{a_{n-1}}{s_{n-1}}x^{n-1} + \ldots + \frac{a_0}{s_0} = 0
\end{align*}
with $s_i \in S$. Then multiplying this equation by $(s_0 \cdots s_{n-1})^{n}$, we get an equation
\begin{align*}
  (sx)^{n} + b_{n-1}(sx)^{n-1} + \ldots + b_0 = 0
\end{align*}
where $s = s_0 \cdots s_{n-1}$ and $b_i \in R$.
So $sx$ is integral over $R$ and $sx \in R$ since $R$ is integrally closed. Thus $x \in S^{-1}R$.
\end{proof}


\begin{xmp}[]\label{xmp:integral-polynomial}
Let $R$ be a ring and $f \in R[x]$ a monic polynomial. Then $A = \faktor{R[X]}{(f)}$ is an integral $R$-algebra.

Indeed, $A$ is generated as an $R$-algebra by the class of $[X]_{\sim} = x$ and we have $f(x) = 0$ in $A$, so $x$ is integral over $R$.
Hence the integral closure of $R$ in $A$ contains $A$, and thus must be equal to $A$.

For instance, $\Z[i]$ and $\Z[\tfrac{1 + i \sqrt{3}}{2}]$ are integral over $\Z$.
\end{xmp}

\begin{prop}[] \label{prop:integrality-properties}
\begin{enumerate}
  \item Let $A$ be a finitely-generated $R$-algebra (as an algebra).
    Then
    \begin{align*}
      A \text{ is integral over $R$ } \iff A \text{ is finitely-generated as an $R$-module}
    \end{align*}
  \item Let $B$ be integral over $A$ and $A$ integral over $R$. Then $B$ is integral over $R$.
  \item If $A$ is integral over $R$ and $S \subseteq R$ multiplicative, then $S^{-1}A$\footnote{For the structure morphism $R \stackrel{\phi}{\to}A$, we write $S^{-1}A = \phi(S)^{-1}A$} is integral over $S^{-1}R$
  \item If $R \stackrel{s}{\to}A$ is integral and $I \subseteq R$ an ideal and $J \subseteq A$ satisfies $s(I) \subseteq J$, then $R/I \to A/J$ is integral.
  \item If $R \stackrel{\phi}{\to} A$ is an integral extension and $I \subseteq R$, $J \subseteq A$ ideals with $\phi(I) \subseteq J$, then $R/I \to A/J$ is an integral morphism.
\end{enumerate}
\end{prop}
\begin{proof}
  \begin{enumerate}
    \item[(a)] 
      Let $(a_{1}, \ldots, a_{n})$ be generators of $A$.
      Basically, we just apply Proposition \ref{prop:integral-elements} to the generators of $A$.

      \textbf{$\bm{\implies}$:} 
      By that Proposition, we know that $R[a_1]$ is a finitely-generated $R$-module and then so is $R[a_1,a_2] = R[a_1][a_2]$. By induction $A = R[a_{1}, \ldots, a_{n}]$ is finitely-generated.

      \textbf{$\bm{\impliedby}$:} By the same Proposition, $a_{n}$ is integral over $R[a_{1}, \ldots, a_{n-1}]$ and $\ldots$ and $a_1$ is integral over $R$.

    \item Because of (a), this surmounts to saying that being finitely-generated is transitive.

      If $(a_i)_i$ are generators of $B$, and $(r_j)_j$ are generators of $A$, then $(a_ir_j)_{i,j}$ are generators of $B$.

    \item Let $x = \tfrac{a}{s} \in S^{-1}A$.
      The there is an equation $a^{n} + r_{n-1}a^{n-1} + \ldots + r_1 a + r_0 = 0$ with $r_i \in R$.
      Then
      \begin{align*}
        \left(
          \frac{a}{s}
        \right)^{n}
        + \frac{r_{n-1}}{s} \left(
          \frac{a}{s}
        \right)^{n-1} + \ldots + \frac{r_1}{s^{n-1}} \frac{a}{s} + \frac{r_0}{s^{n}} = 0, \quad \text{for} \quad \frac{r_i}{s^{n-i}} \in S^{-1}R
      \end{align*}
      so $x$ is integral over $S^{-1}R$.

    \item Let $y \in A/J$ be the class of $x \in A$. Let
      $x^{n} + r_{n-1}x^{n-1} + \ldots + r_1 x + r_0 = 0$.
      Then $y^{n} + r_{n-1}y^{n-1} + \ldots + r_0 = 0$.
      For $s_i$ the class of $r_i$ in $A/I$, we have $y^{n} + s_{n-1}y^{n-1} + \ldots s_0 = 0$.
      
    \item Since $\phi(I) \subseteq J$, this morphism is clearly well-defined.
      So for $\pi: A \to  A/J$, we have $\pi(ra) = ra + J$.
      Let $a \in A$. By integrality of $R \stackrel{\phi}{\to}A$, there exists an $f = \sum_{i = 0}^{n}r_i X^{i}\in R[X]$ monic with $f(a) = 0$.
      Then for $s_i$ the class of $r_i$ in $R/I$, we have
      \begin{align*}
        \tilde{f} := \sum_{i=0}^{n}s_i X^{i} \in R/I[X]
        \quad \text{is monic and satisfies} \quad \tilde{f}(a+J) = 0
      \end{align*}
  \end{enumerate}
\end{proof}
