\begin{dfn}[]
Let $R$ be a ring and $M$ and $R$-module.
\begin{enumerate}
  \item $M$ is \textbf{noetherian}, if any submodule $N \subseteq M$ is finitely generated.
  \item The ring $R$ is \textbf{noetherian}, if $R$ is noetherian as an $R$ module.

    Since the $R$-submodules of $R$ are exactly the ideals, this means that a ring is noetherian if all its ideals are finitely generated.
\end{enumerate}
\end{dfn}

\begin{xmp}[]\label{xmp:noetherian}
\begin{itemize}
  \item PIDs and thus fields are noetherian rings. 
  \item Finite-dimensional vector spaces over a field are neotherian.
  \item Finitely-generated modules over a PID is noetherian.
  \item As per our motivating example, $k[(X_n)_{n \in \N}]$ is not noetherian.
\end{itemize}
\end{xmp}

Note: A non-noetherian ring may have noetherian modules.


\begin{prop}[Alternative definitions ACL 6.3.2]\label{prop:alt-noetherian}
  Let $R$ be a ring, $M$ and $R$-module over $R$. 
  The following are equivalent
  \begin{enumerate}
    \item $M$ is noetherian
    \item Any set $\mathcal{X} \neq 0$ of submodules of $M$, has a maximal element $N$.
      (i.e. there is no $N' \in \mathcal{X}$ s.t. $N \subsetneq N'$)
    \item For any sequence $N_0 \subseteq N_1 \subseteq \ldots \subseteq N_n \subseteq \ldots$ of submodules of $M$ eventually becomes stationary: i.e there exists an $n_0 \geq 0$ such that $N_{n+1} = N_n$, for all $n \geq n_0$.
  \end{enumerate}
\end{prop}

\begin{proof}
  We show (b) $\iff$ (c) and then (a) $\iff$ (c).
  \textbf{(c) $\bm{\implies}$ (b):}
  \quad
  Contraposition:
  Let $N_0 \in \mathcal{X}$ (which exists since $\mathcal{X} \neq 0$. If $N_0$ is maximal in $X$, we are done.
  Otherwise, let $N_1 \in \mathcal{X}$ such that $N_0 \subsetneq N_1$.

  By induction, if $\mathcal{X}$ has no maxmial element for inclusion, then we get an ever-increasing sequence $N_0 \subsetneq N_1 \subsetneq N_2 \subsetneq \ldots$, contradicting (c).

  \textbf{(b) $\bm{\implies}$ (c):}
  \quad

  \textbf{(a) $\bm{\implies}$ (c):}
  \quad

  \textbf{(c) $\bm{\implies}$ (a):}
  \quad

\end{proof}



\begin{prop}[]
Let $R$ be a ring, $M$ an $R$-module, $N \subseteq M$ a submodule. Then
\begin{align*}
  M \text{ is noetherian } \iff N \text{ and } M/N \text{ are noetherian}
\end{align*}
\end{prop}
This is useful because understanding modules often works by finding submodules $N$ for which $N$ and $M/N$ are easier to understand.

\begin{cor}[]
For $R$ a noetherian ring, any finitely generated $R$-module is noetherian.
\end{cor}
\begin{proof}[Proof Corollary]
  Since $M$ is finitely generated, there exists an $n \geq 0$ and a surjective $R$-linear map
  \begin{align*}
    f: R^{n} \mapsto M, \quad (r_i)_{1 \leq i\leq n} \mapsto \sum_{1 \leq i \leq n}r_i m_i
  \end{align*}
  for $m_{1}, \ldots, m_{n}$ generators of $M$.
  Then we get an isomorphism
  $\faktor{R^{n}}{\Ker f} \iso M$.
  By the Proposition, it suffices to show that $R^{n}$ is noetherian.

  This can be proven by induction on $n \geq 0$.
  For $n = 0$ and $n = 1$ it's clear.

  For $n \geq 1$, note that $\faktor{R^{n+1}}{R^{n}} \iso R$, so by induction the proof follows.
\end{proof}


\begin{proof}[Proof Proposition]
  \begin{itemize}
    \item 
      Assume $M$ is noetherian. Then $N \subseteq M$ is also noetherian, since any submodule of $N$ is also a submodule of $M$.

  If $P \subseteq M/N$ is a submodule, then $P = \pi(\pi^{-1}(P))$, where $\pi: M \to M/N$ is the projection. And insce $\pi^{-1}(P) \subseteq M$ is finitely generated, so is its image $P$, hence $M/N$ is noetherian.

  \item 
    Now assume $N$ and $M/N$ are noetherian.
    Let $N_0 \subseteq N_1 \subseteq \ldots$ be a sequence of submodules of $M$.
    Consider the sequences
    \begin{align*}
      N_0 \cap N \subseteq N_1 \cap N \subseteq \ldots\\
      \pi(N_0) \subseteq \pi(N_1) \subseteq \ldots
    \end{align*}
    of submodules of $N$ and $M/N$.
    Since they are notherian, these sequences become stationary after some $n_0 \in \N$, i.e.
    \begin{align*}
      N_n \cap N = N_{n_0} \cap N, \quad \text{and} \quad \pi(N_n) = \pi(N_{n_0}) \quad \text{for} \quad n \geq n_0
    \end{align*}
    The inclusion $N_n \subseteq N_{n+1}$ is already given. Now let $m \in N_{n+1}$. 
    Let $n \geq n_0$. We show that $N_{n+1} = N_n$. By the above
    \begin{align*}
      \pi(m) \in \pi(N_{n+1}) = \pi(N_n)
    \end{align*}
    so there exists an $m' \in N_n$ such that 
    \begin{align*}
      \pi(m') = \pi(m) \in M/N_n
      \iff 
      m -m ' \in N\cap N_{n+1}
      \implies m-m' \in N_n \implies m \in N_n
    \end{align*}
    showing the other inclusion and thus equality.
  \end{itemize}

\end{proof}


