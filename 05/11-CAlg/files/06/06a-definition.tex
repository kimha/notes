
\begin{dfn}[Krull dimension]
  Let $R$ be a ring.
  \begin{enumerate}
    \item For $k \in \N$, a \textbf{prime chain of length $k$} in $R$ is a family of strictly increasing prime ideals in $R$:
      \begin{align*}
        p_0 \subsetneq p_1 \subsetneq \ldots \subsetneq p_k
      \end{align*}
      we say that this is a chain from $p_0$ to $p_k$.
    \item Let $p \subseteq R$ be a prime ideal. The \textbf{height} of $p$ is either the integer $h \in \N$ such that there exists a prime chain in $R$ of length $h$ ending at $p$ and no such chain of length $h+1$ exists, or it is $h = \infty$ otherwise.
    Write $\hht(p)$ for the height of p.
  \item The \textbf{dimension} of $R$ is the supremum of the lenghts of all prime chains in $R$.
  \end{enumerate}
\end{dfn}

\begin{prop}[]\label{prop:field-iff-id-dim0}
$R$ is a field if and only if $R$ is an integral domain and $\dim R = 0$.
\end{prop}

\begin{proof}
  If $R$ is a field, then $\{0\} \subseteq R$ is the only prime ideal, so it has height $0$.

  If $R$ is an integral domain of dimension $0$, then $\{0\} \subseteq R$ is prime and it is maximal, as otherwise we would have a prime ideal of height $1$.
  Since $R$ is local, we have by Remark \ref{rem:units-in-local} that $R^{\times} = R - \{0\}$, i.e. $R$ is a field.
\end{proof}

If we drop the condition that $R$ has to be an integral domain, then the statement is no longer true, as for $R = K_1 \times K_2$ with $K_1,K_2$ fields is zero-dimensional, but not a field.

\begin{prop}[] \label{prop:pid-not-a-field}
If $R$ is a PID and not a field. Then $\dim R = 1$, but the converse is not true, even for integral domains.
\end{prop}
\begin{proof}
Since $R$ is not a field, there exists a prime ideal $p \neq \{0\}$ and $\{0\} \subsetneq p$.
This shows $\dim R \geq 1$.
Suppose now that we had prime ideals $p_1,p_2$ with
\begin{align*}
  \{0\} \subsetneq p_1 \subseteq p_2
\end{align*}
since $R$ is a PID, we can find irreducible elements $x_1,x_2$ such that $p_1 = x_1R$, $p_2 = x_2R$. Then
\begin{align*}
  x_1R \subseteq x_2R \implies x_2|x_1
\end{align*}
since $x_1$ is irreducible and $x_2$ is not a unit, it follows $p_2 = p_1$, so $\dim R = 1$.
\end{proof}

In particular, these propositions show that for a field $K$, we have
\begin{align*}
  \dim K = 0, \quad \dim K[X] = 1
\end{align*}
so one might naturally conjecture the following:
\begin{thm}[] \label{thm:dimension-noetherian-polyomial-ring}
Let $K$ be a field and $n \in \N$. Then $\dim K[X_{1}, \ldots, X_{n}] = n$.

More generally, for any Noetherian ring $R$
\begin{align*}
  \dim R[X_{1}, \ldots, X_{n}] = \dim R + n
\end{align*}
\end{thm}
It is easy to check the lower bound $\dim R[X_{1}, \ldots, X_{n}] \geq n + \dim R$, but the converse is more involved and we will only prove this for fields in a later chapter.

\begin{proof}[Proof of $\geq$]
  Let $A = R[X_{1}, \ldots, X_{n}]$ and let
  \begin{align*}
    p_0 \subsetneq \ldots \subsetneq p_m
  \end{align*}
  be any prime ideal chain in $R$. Then we get a prime chain in $A$ of length $m+n$ given by
  \begin{align*}
    p_0A \subsetneq \ldots \subsetneq p_mA \subsetneq p_mA + X_1A \subsetneq \ldots \subsetneq p_MA + (X_{1}, \ldots, X_{n})A
  \end{align*}
  Indeed, since an ideal is prime if and only if $R/I$ is an integral domain, the variuos terms are prime because of the isomorphism
  \begin{align*}
    \faktor{R[X_{1}, \ldots, X_{n}]}{p_i} 
    &\iso
    (R/p_i)\left[
      X_{1}, \ldots, X_{n}
    \right]\\
    \faktor{R[X_{1}, \ldots, X_{n}]}{p_m + (X_{1}, \ldots, X_{j})}
    &\iso
    (R/p_m)[X_{j+1}, \ldots, X_{n}]
  \end{align*}
  and the right hand sides are clearly integral domains.
\end{proof}



\begin{rem}[]
In the defintion, one must restrict to \emph{prime chains} as otherwise we could get an useless invaraint that would almost always equal to $\infty$. For instance
\begin{align*}
  2^{m}\Z \subseteq 2^{m-1}\Z \subseteq \ldots \subseteq 2\Z
\end{align*}
\end{rem}


\begin{prop}[]
Let $p \subseteq R$ be prime. Then
\begin{enumerate}
  \item $\hht p = \dim R_p$
  \item $\dim R_p + \dim R/p \leq \dim R$.
\end{enumerate}
\end{prop}
\begin{proof}
\begin{itemize}
  \item Let $s: R \to R_p$ be the localisation map. 
    From Proposition \ref{prop:prime-ideals-localisation}, we know that there is an order-preserving bijection
    \begin{align*}
      \left\{
        q \subseteq A_p \text{ prime}
      \right\}
      \iso \left\{
        \tilde{q} \subseteq P \text{ prime}
      \right\}
      , \quad q \mapsto s^{-1}(q)
    \end{align*}
    which implies that prime chains in $R_p$ correspond to prime chains in $R$ that end in an ideal contained in $p$.
    Any such chain cen be (if needed) lengtened to include either the maximal ideal $pR_p \subseteq R_p$, or the prime ideal $p \subseteq R$, so the conlusion $\dim R_p = \hht p$ follows.
  \item Let $\pi: R \to R/p$ be the projection.
    
    We can assume that a prime chain in $R_p$ ends in $pR_p$ because this is the uniqu maximal ideal,  and that a prime chain in $R/p$ starts at $\{0\}$, so given prime chains
    \begin{align*}
      p_0 \subsetneq \ldots \subsetneq p_m = pR_p \subseteq R_p\\
      \{0\} = q_0 \subsetneq \ldots \subsetneq q_k \subseteq R/p
    \end{align*}
    we can again use Proposition \ref{prop:prime-ideals-localisation} together with \ref{prop:ideals-and-quotients}
    to get the prime chain
    \begin{align*}
      s^{-1}(p_0) \subsetneq \ldots \subsetneq s^{-1}(p_m) = p = \pi^{-1}(q_0) \subsetneq \pi^{-1}(q_1) \subsetneq \ldots \subsetneq \pi^{-1}(q_k)
    \end{align*}
    which has length $m + k$.
\end{itemize}
\end{proof}
One does not always have equality in (b), but it is true for most rings.
In particular, we will see that this is true if $R$ is an integral domain and a finitely-generated algebra over a field.

\begin{xmp}[] \label{xmp:ufd-principal-height}
Let $R$ be a UFD, $p \subseteq R$ prime. Then 
\begin{align*}
  \hht p = 1 \iff p \text{ is principal}
\end{align*}
\end{xmp}
\begin{proof}
\begin{itemize}
  \item[$\implies$] We can find an irreducible element $a \in p$ since for any $a \in p - \{0\}$ and expressing it as a product of the fewest possible number of irreducible elements, $a$ cannot be factored non-trivially, as one factor would be in $p$ and involve fewer irreducibles, so $a$ is irreducible.

    Then $\{0\} \subsetneq aR \subseteq p$, and since $\hht p = 1$, it follows $p = aR$ is a principal ideal.
  \item[$\impliedby$] Let $p = aR$ be prime. So $a$ is an irreducible element and we get $\{0\} \subsetneq aR$.
    If there is a prime ideal sandwitched $\{0\} \subsetneq p \subseteq aR$, we must have $a \in p$, as otherwise, let $b \neq 0 \in p$ such that $b = a^{d}b'$, where $b$ is not divisible by $a$. Since $p$ is prime, we get $b' \in p$, but $b' \notin aR, \lightning$.

    Thus, $a \in p \implies aR \subseteq p$, so $\hht p = 1$.
\end{itemize}
\end{proof}


\begin{rem}[]
Later, we will prove two important related results.
\emph{Krull's Principal Ideal Theorem} states that if $R$ is noetherian and $a \in R$ not a zero-divisor, then there are minimal prime ideals $p \subseteq R$ such that $a \in P$ and all these satisfy $\hht p = 1$.

The second result states that a noetherian integral domain $R$ is a UFD if and only if every prime ideal $p \subseteq R$ of height $1$ is principal.
\end{rem}

A somewhat counter-intuitive fact is that there exist infinite-dimensional noetherian rings, which are rings, where there aren't any infinitely long increasing chains of ideals, but prime chains of arbitrary length.
On the other hand one can show that if $R$ is notherian and local, then it has finite dimension.
