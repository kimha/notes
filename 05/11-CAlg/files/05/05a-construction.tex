The tensor product is a way to linearize the concept of \emph{bilinear maps} of $R$-models, where we can view them as a \emph{linear} map from some other module.

\begin{dfn}[]
Let $M,N,P$ be $R$-modules. An $R$-\textbf{bilinear map}
\begin{align*}
  b: M \times N \to P
\end{align*}
is a function such that it is linear in both arguments, i.e.
\begin{itemize}
  \item $b(rm,n) = r b(m,n)$
  \item $b(m,rn) = r b(m,n)$
  \item $b(m + m',n) = b(m,n)  + b(m',n)$
  \item $b(m,n+n') = b(m,n) + b(m,n')$.
\end{itemize}
for $r \in R, m,m' \in M, n,n' \in N$.
\end{dfn}

\begin{lem}[]
The set $\Bil_R(M,N;P)$ of bilinear maps $M \times N \to  P$ is an $R$-module with
\begin{align*}
  (rb + r'b')(m,n) := rb(m,n) + r'b'(m,n)
\end{align*}
given an $R$-module $Q$ and an $R$-linear map $P \stackrel{f}{\to}Q$, there is an $R$-linear map

\begin{align*}
  \Bil_R(M,N;P) \to \Bil_R(M,N,Q), \quad b \mapsto  f \circ b
\end{align*}
In other words, there is a functor $\Bil_R(M,N;-)$ on the category of $R$-modules.
\end{lem}

\begin{xmp}[]
\begin{itemize}
  \item For $M = N = P = R$, the multiplication map $\cdot: R \times R \to R$ is $R$-bilinear.
  \item For $M = R$, $P = N$, the scalar multipliation $R \times N \to N$ is $R$-bilinear.
  \item (Uncurrying) Let $f: M \to \Hom_R(N,P)$ be an $R$-linear map. 
    Since $f$ ``returns'' a linear map, we can think of $f$ being a ``higher-order'' map.
    By \emph{uncurrying}, we get a ``first-order'' $R$-bilinear map
    \begin{align*}
      b_f : M \times N \to P \quad b_f(m,n) = (f m)(n)
    \end{align*}
    In fact, any bilinear map $b: M \times N \to P$ is of this form, as given such a $b$, we can set
    \begin{align*}
      f : M \to \Hom_R(N,P), \quad m \mapsto b(m,-)
    \end{align*}
    and similarly by switching the roles of $M,N$.
    So there exist $R$-linear isomorphisms
    \begin{align*}
      \Hom_R(M,\Hom_R(N,P)) \iso \Bil_R(M,N;P) \iso \Hom_R(N,\Hom_R(M,P))
    \end{align*}
\end{itemize}
\end{xmp}
Our goal is to view $\Bil_R(M,N;P)$ as $\Hom_R(T,P)$ for some other $R$-module $T$ in a natural way.
The tensor product let us find out the what $T$ should look like.

Before we give the definition, we make a small detour to the category $\Set$\footnote{That is not part of the lecture, so you can skip until the definition, but I think it is very insightful.}
\begin{rem}[]
  Let $X,Y,Z$ be \emph{sets}. 
  Given a higher-order function
  \begin{align*}
    f: X \to \Hom_{\Set}(Y,Z), \quad x \mapsto f_x
  \end{align*}
  we can uncurry $f$ to obtain a first-order function
  \begin{align*}
    f: X \times Y \to Z
  \end{align*}
  which (since we are in $\Set$), induces an isomorphism
  \begin{align*}
    \Hom_{\Set}(X,\Hom_{\Set}(Y,Z)) \iso \Hom_{\Set}(X \times Y,Z)
  \end{align*}
  so the ``opposite'' of uncurrying a function is to curry a function from the product $X \times Y$.

  If we write $Y^{X}$ for $\Hom_{\Set}(X,Y)$, this isomorphism can be written in the suggestive form
  \begin{align*}
    (Z^{Y})^{X} \iso Z^{X \times Y}
  \end{align*}
  hinting at the power rule for real numbers ($(a^{b})^{c} = a^{bc}$ for $a > 0$.


  Now, the set $X \times Y$ isn't the only set that let us uncurry higher-order functions $X \to (Y \to Z)$.
  We can, for example take the set $X \times Y \times \{0,1\}$, and define the curried function by ignoring the value of the point in $\{0,1\}$.
  Clearly, this solution is not optimal as the added degree of freedom removes the uniqueness of the uncurried function $X \times Y \to \{0,1\} \to Z$.


  If we return to the category of modules, we can no longer just take $X \times Y$, as the uncurried function is no longer linear, but bilinear.
\end{rem}


\textbf{Notation\footnote{This is neither standard nor used by the lecturer, but we will sometimes use the Lambda-like notation for function application for higher-order functions.
So for $f \in \Hom(X,\Hom(Y,Z))$, we write $f\ x$ for $f(x) \in \Hom(Y,Z)$ and $f\ x \ y$ for $(f(x))(y) \in Z$}}:


\begin{thm}[Whitney]
  Let $M,N$ be $R$-modules.
  Then there exists an $R$-module $M \otimes_R N$ ($M$ tensor $N$ over $R$) and a bilinear map
  \begin{align*}
    \beta: M \times N \to M \otimes_R N, \quad \beta \in \Bil_R(M,N;M \otimes_R N)
  \end{align*}
  which is \emph{universal} in the sense that for any $R$-module $P$ and any $R$-bilinear map $b: M \times N \to P$, there exists a \emph{unique} $R$-linear map $f: M \otimes_R N \to P$ such that the following diagram commutes:
  \begin{center}
  \begin{tikzcd}[ ] 
    M \times N \arrow[]{r}{b} \arrow[]{d}{\beta} & P\\
    M \otimes_R N \arrow[dashed]{ur}{!\exists f}
  \end{tikzcd}
  \end{center}
  The pair $(M \otimes_R N,\beta)$ is unique in the sense that if $(T,\beta')$ satisfies the same condition, there exists a unique $R$-linear morphism $T \stackrel{f}{\to} M \otimes_R N$ such that the folllowing diagram commutes
  \begin{center}
  \begin{tikzcd}[column sep=0.8em] 
    & M \times N \arrow[swap]{dl}{\beta'} \arrow[]{dr}{\beta}\\
    T \arrow[dashed]{rr}{!\exists f}
    && M \otimes_R N
  \end{tikzcd}
  \end{center}
\end{thm}
In other words, there is a natural equivalence 
\begin{align*}
  \Phi: \Hom_R(M \otimes_R N,-) \to \Bil_R(M,N;-), \Phi_P = (f \mapsto  f \circ \beta)
\end{align*}

\textbf{Notation:} We will write
\begin{align*}
  \beta(m,n) = m \otimes n \quad \text{for} \quad (m,n) \in M \times N
\end{align*}

\begin{proof}[Proof Sketch]
  \textbf{Uniqueness:} This actually follows from the Yoneda Lemma (more precisely, Corollary \ref{cor:iso-with-hom}) since we have just described the Hom-functor $\Hom_R(M \otimes_R N,-)$.

  We obtain a less abstract proof by just applying the ``trick'' we used in the proof of Yoneda's Lemma (Taking $P = T$ and $\id_T \in h^{T}(T)$).

  Assume we have two modules with bilinear maps $(T,\beta), (T',\beta')$ satisfying the universal properties.
  From this, we obtain $R$ linear maps $a,b$ such that the following diagram commutes:
  \begin{center}
  \begin{tikzcd}[ ] 
    M \times N \arrow[swap]{d}{\beta} \arrow[]{dr}{\beta'} \arrow[]{r}{\beta} 
    & T\\
    T \arrow[]{r}{g}
    & T' \arrow[]{u}{f}
  \end{tikzcd}
  \end{center}
  Note that the morphism $(f \circ g)$ satisfies the universal property
  \begin{align*}
    (f \circ g) \circ \beta = f \circ (g \circ \beta) = f \circ \beta' = \beta
  \end{align*}
  But the identity $\id_T$ also satisfies this universal property. Since this map must be unique, it follows $(f \circ g) = \id_T$.
  Similarly, one can show $(g \circ f) = \id_{T'}$ which proves $T \iso T'$.

  \textbf{Existence}: (The proof pattern we use here comes up quite regularly for other algebraic constructions, so it is good to have it in mind.)

  We construct $M \otimes N$ as a quotient of a ``huge'' free module with a submodule that imposes the bilinearity by applying the $\Hom$ properties of free modules aswell as those of quotient modules.

  Let $\tilde{T}$ be the free $R$-module with basis induced by the elements of $M \times N$ and write $[(m,n)]$ for the basis vector corresponding to $(m,n)$.
  Let $Z \subseteq \tilde{T}$ be the submodule generaated by the elements
  \begin{align*}
    [(rm,n)] - r[(m,n)], \quad
    [(m,rn)] - r[(m,n)]\\
    [(m+m',n)] - [(m,n)] - [(m',n)], \quad
    [(m',n+n')] - [(m,n)] - [(m,n')]
  \end{align*}
  now define
  \begin{align*}
    M \otimes_R N = \faktor{\tilde{T}}{Z}, \quad
    \beta(m,n) = \pi([(m,n)]), \quad \text{where} \quad \pi: \tilde{T} \to \faktor{\tilde{T}}{Z}
  \end{align*}
  This pair $(M \otimes_R N,\beta)$ satisfies the conditions we want.
  
  Let $P$ be an $R$-module. 
  By the characterisation of the hom-functor for quotient maps, we have
  \begin{align*}
    \Hom_R(M \otimes N,P) \stackrel{\iso}{\to} \left\{
      g: \tilde{T} \to P \big\vert Z \subseteq \kernel g
    \right\}, \quad f \mapsto  f \circ \pi
  \end{align*}
  By the characterisation of free modules, we have a bijection
  \begin{align*}
    \Hom_R(\tilde{T},P) \iso \Hom_{\Set}(M \times N,P)
  \end{align*}
  Therefore, af $f \in \Hom_R(M \otimes_R N,P)$ corresponds to a function
  \begin{align*}
    h: M \times N \to P \quad \text{such that} \quad h)(m,n) = f(\pi([m,n]))
  \end{align*}
  which is exactly bilinearity of $h$.
  We conclude
  \begin{align*}
    \Hom_R(M \otimes_R N,P) \iso \Bil_R(M,N;P)
  \end{align*}
\end{proof}


\begin{rem}[]
Since $\beta$ is bilinear, the elements in $M \otimes_R N$ satisfy
\begin{align*}
  rm \otimes n = r(m \otimes n) = m \otimes rn\\
  (m + m') \otimes n = m \otimes n + m' \otimes n\\
  m \otimes (n+n') = m \otimes n + m \otimes n'
\end{align*}

\textbf{Warning:} Not all elements of $M \otimes_R N$ are \emph{pure tensors} of the form $m \otimes n$, but instead finite linear combinations
\begin{align*}
  \sum_{i=1}^{n}m_i \otimes n_i, \quad n \geq 0, m_i \in M, n_i \in N
\end{align*}
so the collection of elements $m \otimes n$ generate $M \otimes_R N$.
\end{rem}
It is useful to know that
\begin{align*}
  0 \otimes n = (0 \cdot 0) \otimes n = 0 \cdot (0 \otimes n) = 0 = m \otimes 0
\end{align*}
\begin{xmp}[]
If we know that there cannot be any non-trivial bilinear maps in $\Bil_R(M,N;P)$, then we know that $M \otimes_R N = \{0\}$.

For example, let $m,n$ be coprime integers. Then
\begin{align*}
  \Z/m\Z \otimes_\Z/n\Z = \{0\} \quad \text{because} \quad 1 = am + bn, \text{ for } a,b \in \Z
\end{align*}

\end{xmp}
