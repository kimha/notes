The next topic is one of the really important theoretical applications of the tensor product.
For vector spaces, it is often useful to represent a linear map $U \stackrel{f}{\to}V$ as a real matrix, then view it as a complex matrix to study its properties.

For an $\R$-vector space $V$, physicists often consider the space $V_{\C} := V \otimes_{\R} \C$ and call this the \textbf{complexification} of a vector space.

The next result lets us generalize this to alebras an modules.

\begin{prop}[ACL 8.1.12]
  Let $R$ be a ring and $R \stackrel{s}{\to}A$ be an $R$-algebra. (In particular, $A$ is an $R$-module with $r \cdot a = s(r) a$.)
  Let $M$ be an $R$-module. Then the $R$-module
  \begin{align*}
    M_A := M \otimes_R A
  \end{align*}
  has a (unique) structure of an $A$-module such that
  \begin{align*}
    a \cdot (m \otimes b) = m \otimes ab, \quad \text{for all} \quad a,b \in A, m \in M
  \end{align*}
\end{prop}
\begin{dfn}
The module $M_A$ is called the \textbf{base-change} of $M$ to $A$.
\end{dfn}

\begin{proof}[Proof Proposition]
  Fix $a \in A$. Then the map
  \begin{align*}
    M \times A \to  M \otimes_R A, \quad (m,b) \mapsto  m \otimes ab
  \end{align*}
  is $R$-bilinear, hence there is a unique $R$-linear map
  \begin{align*}
    \lambda_a: M \otimes_R A \to M \otimes_R A
  \end{align*}
  such that
  \begin{align*}
    \lambda_a(m \otimes b) = m \otimes ab, \quad \text{for} \quad b \in A, m \in M
  \end{align*}
  the uniqueness implies that
  \begin{align*}
    \lambda_{a + a'} = \lambda_a + \lambda_a'
  \end{align*}
  because both are $R$-linear and sent $m \otimes b$ to $m \otimes ab+ m \otimes a'b$.
  It follows that the scalar multiplication $a \cdot x = \lambda_a(x)$ defines a structure morphism on $M_A$.
\end{proof}

\begin{rem}[]
If we have a ring morphism $R \stackrel{\phi}{\to}A$, then any $A$-module is an $R$-module.
\end{rem}

\begin{xmp}[]
Let $M$ be the free module over $R$ with basis $(e_i)$. Then $M_A$ is free over $A$ with basis $(e_i \otimes_R 1_A)$. Indeed, we have $M = \bigoplus_{i}R e_i$, so by distributivity, $M_A \iso \bigoplus_{i}(Re_i \otimes_R A)$ and since $Re_i \otimes_R A \iso R \otimes_R A \iso A$, it is free of rank $1$ with basis $e_i \otimes_R 1_A$.
\end{xmp}
In particular, if $M$ is an $\R$-vector space of dimension $d$, then $M_{\C}$ is a $\C$-vector space of $\C$-dimension $d$.


This construction is also functorial.

\begin{prop}[]
Let $R \stackrel{s}{\to}A$ be an $R$-algebra and $M \stackrel{f}{\to}N$ an $R$-linear map.
Then the map
\begin{align*}
  f_A := f \otimes \id_A: M_A \to N_A
\end{align*}
is $A$-linear for the $A$-module structures defined above.
Moreover, $(\id_R)_A = \id_A$ and if $N \stackrel{g}{\to}P$ is $R$-linear, then $(g \circ f)_A = g_A \circ f_A$.

In other words, the structure morphism $R \stackrel{s}{\to}A$ induces a functor
\begin{align*}
  R_{\text{Mod}} \to A_{\text{Mod}}, \quad M \mapsto M_A, \quad f \mapsto  f_A
\end{align*}
\end{prop}
\begin{proof}
Since $f \otimes \id_A$ is $R$-linear, it is $\Z$-linear by additivity, so it suffices to check $f_A(ax ) af_A(x)$.
Let $\lambda_a = (x \mapsto  ax)$. It is $R$-linear and we need to check that $f_A \circ \lambda_a = \lambda_a \circ f_A$.
Both are linear maps $M \otimes_R A \to N \otimes_R A$ and satisfy
\begin{align*}
  (f_A \circ \lambda_a)(m \otimes b) = f_A(m \otimes b) = f_A(m \otimes ab) = f(m) \otimes ab\\
  (\lambda_a \circ f_A)(m \otimes b) = \lambda_a(f(m) \otimes b) = f(m) \otimes ab
\end{align*}
by the universal property of the tensor product, they must be equal.

The composition formulas are easy to check.
\end{proof}

\begin{xmp}[]
Let $K$ be a field and $M,N$ finite-dimensional $K$-vector spaces.
Let $f: M \to N$ be $K$-linear.
Chose bases $(x_i)$ and $(y_j)$ of $M$ and $N$ and let $(a_{ij})$ be the matrix representing $f$ with respect ot these.

For a field extension $K \subseteq L$, the matrix of $f_L$ with respect to the bases $(x_i \otimes 1_L)$, $(y_j \otimes 1_L)$ is also $(a_{ij})$ (when viewing $a_{ij} \in L$).

Indeed, by definition we have
\begin{align*}
  f(x_k) = \sum_{j}a_{jk}y_j \implies
  f_A(x_K \otimes 1) = f(x_K) \otimes 1 = (\sum_{j} a_{jk} y_j) \otimes 1 = \sum_{j} a_{jk} (y_j \otimes 1)
\end{align*}


The following is an application of base change.
Consider two elements $\alpha,\beta \in \C$ which are algebraic over $\Q$, 
i.e. have non-zero polynomials $p_{\alpha},p_{\beta} \in \Q[X]$ of which they are roots of.

From field theory, we know that $\alpha + \beta$ and $\alpha \beta$ are also algebraic.
We now would like to know specific polynomials for which they are roots.

First, we find matrices $A_{\alpha} \in M_n(\Q), A_{\beta} \in M_m(\Q)$, where $n = \degree p_{\alpha}$ and $m = \degree p_{\beta}$ whose characteristic polynomials are $p_{\alpha}$ and $p_{\beta}$.

Then let $\Q^{n} \stackrel{u_{\alpha}}{\to} \Q^{n}, \Q^{m} \stackrel{u_{\beta}}{\to}\Q^{m}$ be the linear maps associated to $A_{\alpha},A_{\beta}$.

Let $A_{\alpha \beta}$ be the matrix of $u_{\alpha} \otimes u_{\beta}$ with respect to the basis $(e_i \otimes f_j)_{ij}$ of $\Q^{n} \otimes \Q^{m}$.

Likewise, set $A_{\alpha + \beta}$ the matrix of $u_{\alpha} \otimes \id_{\Q^{m}} + \id_{\Q^{n}} \otimes u_{\beta}$.

Then the characteristic polynomials $p_{\alpha \beta}$ of $A_{\alpha \beta}$, and $p_{\alpha + \beta}$ of $A_{\alpha + \beta}$ satisfy
\begin{align*}
  p_{\alpha \beta}(\alpha \beta) = 0, \quad
  p_{\alpha + \beta}(\alpha + \beta) = 0
\end{align*}

To see this, let $x_{\alpha} \in \C^{n} = \Q^{n} \otimes_{\Q} \C, x_{\beta} \in \C^{m} = \Q^{m} \otimes_{\Q} \C$ be eigenvectors of $A_{\alpha},A_{\beta}$ with eigenvalues $\alpha, \beta$ when viewing $A_{\alpha}$ as a matrix with coefficients in $\C$.

This translates to
\begin{align*}
  (u_{\alpha} \otimes \id_\C) x_{\alpha} = \alpha x_{\alpha}\\
  (u_{\beta} \otimes \id_\C) x_{\beta} = \beta x_{\beta}
\end{align*}
Now let $x_{\alpha \beta} =  x_{\alpha} \otimes x_{\beta} \in \C^{n} \otimes \C^{m}$.
Then
\begin{align*}
  (u_{\alpha \beta} \otimes \id_\C)(x_{\alpha \beta}) = ((u_{\alpha} \otimes \id_\C) \otimes (u_{\beta} \otimes \id_\C))(x_{\alpha} \otimes x_{\beta}) = u_{\alpha}(x_{\alpha}) \otimes u_{\beta}(x_{\beta}) = \alpha \beta (x_{\alpha} \otimes x_{\beta})
\end{align*}
Similarly for $u_{\alpha + \beta}$.
\end{xmp}

\subsection{Examples of base change}

\subsubsection*{Quotients}
\begin{prop}[]
Let $R$ be a ring, $I \subseteq R$ an ideal. For any $R$-module $M$, there is an isomorphism
\begin{align*}
  M \otimes R/I \stackrel{u}{\to} M/IM
\end{align*}
of $R/I$-modules such that
\begin{align*}
  u(m \otimes (r + I)) = rm + IM
\end{align*}
\end{prop}
\begin{proof}
As usual, we construct an bilinear map
\begin{align*}
  M \times R/I \to M/IM, \quad rm \mapsto IM
\end{align*}
which is well-defined because if we replace $r$ with $r + i$, then $rm$ is replaced with $rm + im$ and $im \in IM$.

By the universal property, this defines a linear map
\begin{align*}
  M \otimes_R R/I \stackrel{u}{\to} M/IM
\end{align*}
both are $R/I$-modules and $u$ is also $R/I$ linear because
\begin{align*}
  u((s+I)(m \otimes (r + I))) = u(m \otimes (rs + I)) = rsm + IM = (s+I)(rmIM)
\end{align*}
In order to obtain an inverse map, we define 
\begin{align*}
  \tilde{v}: M \to M \otimes_R R/I, \quad \tilde{v}(m) = m \otimes 1_{R/I}
\end{align*}
which induces a linear map on the quotient by $IM$, because $IM \subseteq \kernel \tilde{v}$:
\begin{align*}
  \tilde{v}(im) = im \otimes 1_{R/I} = i(m \otimes 1_{R/I}) = m \otimes (i + I) = 0
\end{align*}
therefore, this induces an $R$-linear map
\begin{align*}
  v: M/IM \to M \otimes_R R/I
\end{align*}
with $v(m + IM) = m \otimes 1_{R/I}$. Then check that $v$ is $R/I$-linear and an inverse of $u$.
\end{proof}


\subsubsection*{Localisation}
\begin{prop}[]
Let $R$ be a ring, $S \subseteq R$ multiplicative.
For any $R$-module $M$ there is an $S^{-1}R$-linear isomorphism
\begin{align*}
  M \otimes_R S^{-1}R \stackrel{u}{\to} S^{-1}M
\end{align*}
where the right-ahnd side is $S \times M/_{\sim}$, where
\begin{align*}
  \frac{m}{s} = \frac{m'}{s'} \iff  \exists t \in S:\ t(s'm - sm') = 0\\
  \frac{r}{s} \cdot \frac{m}{s'} = \frac{rm}{ss'}, \quad \frac{m}{s} + \frac{m'}{s'} = \frac{s'm + sm'}{ss'}
\end{align*}
this isomorphism is characterized by
\begin{align*}
  u(m \otimes \tfrac{r}{s}) = \frac{rm}{s}
\end{align*}
\end{prop}
\begin{proof}
  We need to check that $S^{-1}M$ is well-defined and is an $S^{-1}M$ module. Then we define the bilinear map
  \begin{align*}
    b: M \times S^{-1}R \to S^{-1}M, \quad (m,\tfrac{r}{s}) \mapsto \frac{rm}{s}
  \end{align*}
  This induces an $R$-linear map
  \begin{align*}
    u: M \otimes_R S^{-1}R \to S^{-1}M
  \end{align*}
  with $u(m \otimes \tfrac{r}{s}) = \tfrac{rm}{s}$.
  This is also $S^{-1}R$-linear as we have
  \begin{align*}
    u\left(
      \frac{r'}{s'} (m \otimes \frac{r}{s})
    \right)
    = u(m \otimes \tfrac{rr'}{ss'})
    = \frac{u'k}{ss'} = \frac{r'}{s'} u(m \otimes \tfrac{r}{s})
  \end{align*}
  Again, for the inverse, we start with
  \begin{align*}
    \tilde{v}: M \to M \otimes S^{-1}R, \quad m \mapsto  m \otimes 1
  \end{align*}
  and then extend the domain $\tilde{v}$ to $S^{-1}M$ by
  \begin{align*}
    v(\tfrac{m}{s}) = \tfrac{\tilde{v}(m)}{s} = m \otimes \tfrac{1}{s}
  \end{align*}
  (Note that we can do this for any $S^{-1}R$-module $N$ and $R$-linear map $f: M \to N$.)

  Lastly, we check that $u \circ v$ and $v \circ u$ are the identity maps.
\end{proof}


\begin{cor}[ACL 7.5.12]
  If the sequence
  \begin{align*}
    0 \to M' \stackrel{f}{\to}M \stackrel{g}{\to}M'' \to 0
  \end{align*}
  is exact, then
  \begin{align*}
    0 \to M' \otimes_R S^{-1}R \stackrel{f_S}{\to} M \otimes_R S^{-1}R \stackrel{g_S}{\to} M'' \otimes_R S^{-1}R \to 0
  \end{align*}
  is exact.
\end{cor}
\begin{proof}
  By the proposition, the resulting sequence is equivalent to
  \begin{align*}
    0 \to S^{-1}M' \to S^{-1}M \to S^{-1}M'' \to 0
  \end{align*}
  so it suffices to show that $f_S = f \otimes \id_{S^{-1}R}$ is injective.
  With the above isomorphisms, $f_S$ is characterized by
  \begin{align*}
    f_S(\tfrac{m'}{s}) = (f \otimes \id_{S^{-1}R})(m' \otimes \tfrac{1}{s})
    = f(m') \otimes \tfrac{1}{s} = \tfrac{f(m')}{s}
  \end{align*}
  let $\tfrac{m'}{s} \in \kernel f_S$. Then there exists a $t \in S$ such that
  \begin{align*}
    t f(m') = 0 \implies tm' \in \kernel f \implies tm' = 0 \implies \tfrac{m'}{s} = 0 \in S^{-1}M'
  \end{align*}
\end{proof}

\subsubsection*{PIDs}
Let $R$ be PID and $K$ its fraction field.
\begin{prop}[]
Let $M$ be a finitely-generated $R$-module.
Then $M_K$ is a vector space over $k$ of dimension equal to the rank $n \geq 0$ of the free part of $M$: $M \iso R^{n} \otimes T$, where $T$ is a torion $R$-module.

In other words:
\begin{align*}
  R \otimes_R K \iso K \quad \text{and} \quad 
  R/I \otimes_R K = \{0\}
\end{align*}
for any ideal $0 \neq I \subseteq R$. (This also holds for any integral domain)
\end{prop}
\begin{proof}
  We only need to prove the second part.
  Let $I\neq 0$ be an ideal of $R$.
  For $i \in I - \{0\}$, we can write
  \begin{align*}
    (r + I) \otimes 1_K = (r + I) \otimes \tfrac{i}{i} = (ir + I) \otimes \tfrac{1}{i} = 0
  \end{align*}
  for any $r \in R$, so $R/I \otimes_R K = 0$.
\end{proof}



