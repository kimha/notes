
We will start to compute the tensor product for simple cases. (Such as free modules like vector spaces).

More generally, we will show how the tensor product behaves bilinearly with respect to direct sums.

Given a family $(M_i)_{i \in I}$ of $R$-modules, recall that $\bigoplus_{i \in I}M_i$ is an $R$-module with linear maps
\begin{align*}
  \phi_j: M_j \to \bigoplus_{i \in I}M_i, \quad \psi_j: \bigoplus_{i \in I}M_i \to M_j\\
  x_j \mapsto (0,\ldots,0,x_j,0,\ldots,0), \quad (m_i)_{I \in I} \mapsto  m_j
\end{align*}
which satisfy
\begin{align*}
  \psi_j \circ \phi_j = \id_{M_j} 
  \quad \text{and} \quad
  \bigoplus_{i \in I}(\phi_j \circ \psi_j) = \id_{\bigoplus_{i \in I}M_j}
\end{align*}

The direct sum consists of sums $\sum_{i \in I}'m_i$, where all but finitely many $m_i$ are zero.
For finite index sets $I$, the direct sum is equal to the direct product.

\begin{prop}[ ``Distributivity''] \label{prop:distributivity-tensor}
Let $M,N$ be $R$-modules.
\begin{enumerate}
  \item For any collection $(M_i)_{i \in I}$, (resp. $(N_i)_{i \in I}$) and any $j \in I$ there is an isomorphism of $R$-modules
    \begin{align*}
      &\bigoplus_{i \in I}(M_i \otimes N) \to \left(\bigoplus_{i \in I}M_i\right) \otimes N\\
      &m_j \otimes  n \mapsto  \phi_j(m_j) \otimes n\\
      \text{resp.} \quad &\bigoplus_{i \in I}(M \otimes N_i) \to M \otimes \left(\bigoplus_{i \in I}N_i\right)\\
      &m \otimes  n_j \mapsto  m \otimes \phi_j(n_j)
    \end{align*}
  \item The map $M \otimes_R R \to M$ (resp. $R \otimes_R N \to N$)
    \begin{align*}
      m \otimes r \mapsto rm , \quad (\text{resp.} \quad r \times n \mapsto  rn)
    \end{align*}
    is an isomorphism of $R$-modules.
\end{enumerate}
\end{prop}

To make the proof a little shorter (and because this is useful on its own), we first prove the following
\begin{lem}[]
Let $(V_i)_{i \in I}$ be a collection of $R$-modules and $W$ another $R$-module. 
Then there is a bijection
\begin{align*}
  \Hom_R\left(
    \bigoplus_{i \in I}V_i, W
  \right)
  \iso
  \bigoplus_{i \in I}\Hom_R(V_i,W)
\end{align*}
so to define a linear map from the direct sum is equivalent to defining a linear map from each component.
\end{lem}
\begin{proof}
  Set 
  \begin{align*}
    &\Phi:\Hom_R\left(
     \bigoplus_{i \in I}V_i, W
    \right)
    \to 
    \bigoplus_{i \in I}\Hom_R(V_i,W),
    \quad
      f \mapsto \bigoplus_{i \in I} (f \circ \phi_i)\\
    &\Psi:
    \bigoplus_{i \in I}\Hom_R(V_i,W)
    \to 
    \Hom_R\left(
      \bigoplus_{i \in I}V_i, W
    \right),
    \quad
    \bigoplus_{i \in I}f_i \mapsto \bigoplus_{i \in I}(f_i \circ \psi_j)
  \end{align*}
  and show that they are inverses of eachother.
\end{proof}

\begin{proof}[Proof Proposition]
  We begin with (b).
  Since the module structure is a bilinear map $R \times M \to M$, there exists a linear map $R \otimes M \stackrel{u}{\to}M$ with $u(r \otimes m) = rm$.
  Conversely, define the linear map.
  \begin{align*}
    v: M \to R \otimes M, \quad m \mapsto  1 \otimes m
  \end{align*}
  Because the pure tensors generate $R \otimes M$, it suffices to show that the composition $(v \circ u)$ is the identity on the pure tensors.
  \begin{align*}
    (u \circ v)(m) = u(1 \otimes m) = m 
    \quad \text{and} \quad 
    (v \circ u)(r \otimes m) = v(rm) = 1 \otimes rm = r(1 \otimes m) = r \otimes m
  \end{align*}
  which shows $M \iso R \otimes M$.

  Now for (a). first check the the map 
  \begin{align*}
    m_j \otimes n \mapsto \phi_j(m_j) \otimes  n
  \end{align*}
  is well-defined. We define
  \begin{align*}
    u_j = \phi_J \otimes \id_n
  \end{align*}
  and by the lamma, there is a map
  \begin{align*}
    f_j: M_j \otimes N \to \left(
      \bigoplus_{i \in I}M_i
    \right)
    \otimes N
  \end{align*}
  as required.
  To check that $f$ is an isomorphism, we define the bilinear map
  \begin{align*}
    \bigoplus_{i \in I}M_i \times N \to \bigoplus_{i \in I}(M_i \otimes N), \quad
    ((m_i)_{i \in I},n) \mapsto  \sum_{i}' m_i \otimes n
  \end{align*}
  which is well-defined because all but finitely many $m_i$ are zero.

  By the universal propety, this defines a linear map
  \begin{align*}
    g: \left(
      \bigoplus_{i \in I}M_i
    \right)
    \otimes N
    \to \bigoplus_{i \in I}M_i \otimes N
  \end{align*}
  One easily checks that $f$ and $g$ are inverses of eachother:
  \begin{align*}
    (f \circ g)((m_i)_{i \in I} \otimes n)
    &= f\left(
      \sum_{i \in I}'m_i \otimes n
    \right)
    \\
    &= \sum_{i\in I}' f_i(m_i \otimes n)\\
    &= \sum_{i\in I}' \phi_i(m_i) \otimes n\\
    &= \left(
      \sum_{i \in I}' \phi_i(m_i) \otimes n
    \right)\\
    &= (m_i)_{i\in I} \otimes n
  \end{align*}
  and for the other way, it suffices to show this for generators of the module:
  \begin{align*}
    (g \circ f)(m_j \otimes n) = g(\phi_j(m_j) \otimes n) = m_j \otimes n, \quad \text{for} \quad j \in I
  \end{align*}
\end{proof}

\begin{cor}[]
If $M$ is free with basis $(m_i)_{i \in I}$ and $N$ is free with basis $(n_j)_{j \in J}$, then $M \times N$ is free with basis $(m_i \times n_j)_{(i,j) \in I \times J}$.

In particular, if $K$ is a field and $V,W$ finite-dimensional $K$-vector spaces. Then so is $V \times_K W$ and
\begin{align*}
  \dim(V \otimes_K W) = (\dim V) (\dim W)
\end{align*}
\end{cor}
\begin{proof}[]
  Let $M_i = R e_i, N_j = R f_j$ so that 
  $M = \bigoplus_{i\in I}M_i$, $N = \bigoplus_{j \ni J}N_j$. By the proposition, we get an isomorphism
  \begin{align*}
    M \otimes N = \left(
      \bigoplus_{i\in I}M_i
    \right) \otimes \left(
      \bigoplus_{j \in J}N_j
    \right)
    \iso \bigoplus_{(i,j) \in I \times J} M_i \otimes N_j
  \end{align*}
  From the second part of the proposition, we see that
  \begin{align*}
    M_i \otimes N_j = Re_i \otimes N_j \iso N_j
  \end{align*}
  so it is free of rank $1$.
  Since it is generated by $e_i \otimes f_j$, this element is a basis, the above means that $M \otimes N$ is free with basis $(e_i \otimes f_j)_{(i,j) \in I \times J}$.

\end{proof}


\begin{xmp}[]
Let $K$ be a field and take $M = N = K^{2}$ with basis $(e_1,e_2)$. 
Then $M \otimes_K N$ is a $K$-vector space of dimension $4$ with basis
\begin{align*}
  e_1 \otimes e_1, \quad e_1 \otimes e_2, \quad e_2 \otimes e_1, \quad e_2 \otimes e_2
\end{align*}
using this we can determine the basis of pure tensors.
For an example of a pure tensor that is not a basis element, let
\begin{align*}
  v_1 = x_1 e_1 + x_2 e_2 \in M, \quad v_2 = y_1 e_1 + y_2 e_2 \in N, \quad x_i,y_i \in K
\end{align*}
then by bilinearity, we find
\begin{align*}
  v_1 \otimes v_2 = z_{11}e_1 \otimes e_1 + z_{12} e_1 \otimes e_2 + z_{21} e_2 \otimes e_1 + z_{22} e_2 \otimes e_2
\end{align*}
where $z_{ij} = x_i y_i$.

So a general element of the form
\begin{align*}
  v = ae_1 \otimes e_1 + b e_1 \otimes e_2 + c e_2 \otimes e_1 + d e_2 \otimes e_2
\end{align*}
is a pure tensor if there exist $x_1,x_2,y_1,y_2$ such that $a,b,c,d$ satisfy
\begin{align*}
  a = x_1y_1, \quad b = x_1y_2, \quad c = x_2y_1, \quad d = x_2y_2
\end{align*}
Which requires that $ad - bc = 0$.
\end{xmp}

Note for pure tensors, the representation $v = v_1 \otimes v_2$ is not unique, as we could also write $v = xv_1 \otimes x^{-1}v_2$ for a unit $x \in K^{\times}$.



