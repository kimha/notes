\begin{exr}[]
  Let $\Vec_\C$ be the category of vector spaces over $\C$ and $\Set$ the category of sets.
  Let $F$ be the forgetful functor $\Vec_\C \to \Set$.
  \begin{enumerate}
    \item Show that one can define a functor $G: \Set \to \Vec_\C$ by $G(X) = \C^{(X)}$ (the vector space with basis $X$, or the vector space of functions from $X$ to $\C$ which are zero for all but finitely many $x \in X$),
      with $G(X \stackrel{f}{\to}Y)$ the linear map $\C^{(X)} \to \C^{(Y)}$ such that the basis vector $x \in X$ of $\C^{(X)}$ is mapped to the basis vector $f(x) \in Y$.
    \item Show that for any set $X$ and vector space $V$, there is a bijective map of sets
      \begin{align*}
        \Hom_{\Vec_\C}(GX,V) \to \Hom_{\Set}(X,FV)
      \end{align*}
  \end{enumerate}
\end{exr}
\begin{proof}[Solution]
  To avoid a clash of notation, we will sometimes enclose elements of $\C^{(X)}$ with square brackets to differentiate them from elements of $X$, so $x \in X$ and $[x] \in \C^{(X)}$.

  \begin{enumerate}
    \item We first check that it is well-defined and then show the functor laws.
      For any set $X$, the image $G(X) = \C^{(X)}$ is -- by construction -- indeed a $\C$-vector space.
      Linearity follows by definition as we defined $G(f)$ on a basis, so a general element $\bm{x} \in \C^{(X)}$ gets mapped as follows 
      \begin{align*}
        \bm{x} = \sum_{x \in X}' \lambda_x x \stackrel{Gf}{\mapsto}\sum_{x \in X}' \lambda_x f(x) \quad \text{for} \quad \lambda_x \in X
      \end{align*}
      where only finitely many $\lambda_x$ are non-zero.

      By linearity, it is sufficient to check the functor laws on basis elements.
      Let $X$ be a set and $x \in X$.
      It is clear that $G\id_X = \id_{GX}$ since 
      \begin{align*}
        (G\id_X)([x]) \stackrel{\text{def. } Gf}{=} [\id_X(x)] = [x] = \id_{\C^{(X)}}([x])
      \end{align*}
      Finally, we have to show that for any $X \stackrel{f}{\to} Y \stackrel{g}{\to} Z$, it holds $Gg \circ Gf = G(g \circ f)$
      Again, it suffices to check this for the basis elements.
      For any $x \in X$, 
      \begin{align*}
        (Gg \circ Gf)([x])
        &= (Gg)\Big((Gf)([x])\Big)
        \\
        &= (Gg)\Big([f(x)]\Big)
        \\
        &= [g(f(x))]
        = [(g \circ f)(x)]
        \\
        &= G(g \circ f)([x])
      \end{align*}
      Note that we have to evaluate the second expression from the inside-out as there is a-priori no guarantee that $(G(f))(x)$ is a basis vector.

    \item Let $X$ be a set and $V$ a $\C$-vector space.
      Every linear map $f \in \Hom_{\Vec_\C}(GX,V)$ is uniquely determined by its action on a basis, of which $\{x\}_{x \in X}$ is one.
      Moreover, for any collection $\{v_x\}_{x \in X}$ with $v_x \in V$, there exists a unique linear map $f \in \Hom_{\Vec_\C}(GX,V)$ such that $f(x) = v_x, \forall x \in X$.

      The above is just a characterisation of an element in $\Hom_{\Set}(X,FV)$ so the proof follows.

  \end{enumerate}
\end{proof}



\begin{edfn}[]\label{edfn:fully-faithful}
  Let $F: \textsf{C} \to \textsf{D}$ be a functor.
  \begin{itemize}
    \item $F$ is said to be \textbf{fully faithful} if for any objects $X$ and $Y$, the map of sets
      \begin{align*}
        \Hom_{\textsf{C}}(X,Y) \to \Hom_{\textsf{D}}(FX,FY), \quad f \mapsto Ff
      \end{align*}
      is a bijection.
    \item $F$ is said to be \textbf{essentially surjective} if for any object $Y$ of $D$, there exists an object $X$ of $\textsf{C}$ such that $Y$ is isomorphic to $FX$.
  \end{itemize}
\end{edfn}

\begin{exr}[]
  Which of the following functors are fully faithful?
  \begin{enumerate}
    \item The forgetful functor from groups to sets.
    \item The forgetful functor from topological spaces to sets.
    \item The unit functor from rings to groups.
    \item The duality functor from $\C$-vector spaces to the opposite category of $\C$-vector spaces.
    \item The duality functor from finite-dimensional $\C$-vector spaces to the opposite category of finite-dimensional $\C$-vector spaces.
  \end{enumerate}
\end{exr}

\begin{proof}[Solution]
  \phantom{a}
  \begin{enumerate}
    \item Since the forgetful functor maps every group homomorphism $G \stackrel{\phi}{\to}H$ to the underlying function, a bijection $\Hom_{\Grp}(G,S) \iso \Hom_{\Set}(G,S)$ would imply that every function is a group homomorphism, which is absurd.

    \item Same argument as above: Not every function is continuous.
    \item Consider the rings $X = Y = \C[X]$. The unit groups are both isomorphic to $\C^{\times}$.
      Then both maps
      \begin{align*}
        \id, \quad \text{and} \quad (f \mapsto f(0)) \in \Hom_{\Ring}(C[X],C[X]) 
      \end{align*}
      would get mapped to $\id_{C^{\times}}$ by the unit functor, so there would be an isomorphism of hom-sets.
    \item If $V$ is infinitely dimensional, then $V^{\ast}$ has strictily larger dimension, so there cannot be a surjective map
      \begin{align*}
        \Hom_{\C}(V,W) \to \Hom_{\C}(W^{\ast},V^{\ast})
      \end{align*}
    \item See Linear Algebra II, the isomorphism is given by
      \begin{align*}
        \Phi: \Hom_\C(V,W) \to \Hom_{\C}(W^{\ast},V^{\ast})\\
        f \mapsto (\beta \mapsto \beta \circ f: V \to \C)
      \end{align*}
  \end{enumerate}
\end{proof}


\begin{exr}[]
  Which of the following functors are essentilly surjective?
  \begin{enumerate}
    \item The forgetful functor from groups to sets.
    \item The forgetful functor from topological spaces to sets.
    \item The unit functor from rings to groups.
    \item The duality functor from $\C$-vector spaces to the opposite category of $\C$-vector spaces.
    \item The duality functor from finite-dimensional $\C$-vector spaces to the opposite category of finite-dimensional $\C$-vector spaces.
  \end{enumerate}
\end{exr}
\begin{proof}[Solution]

  \begin{enumerate}
    \item No. There cannot be a group structure on the empty set.
      However, if we only look at non-empty sets, then the statement is equivalent to the axiom of choice.
    \item If we allow topological spaces to be empty, then consider the discrete topology on $X$. Then $F (X,\tau_{\text{disc}}) = X$.
    \item (Since this is \emph{commutative} algebra, every ring is commutative, but not every group is abelian.)

      This statement is also false if we consider non-commutative rings aswell.
      Take for example the cyclic group $\Z/5\Z$.
      For any ring $R$, either $\charac R = 2$, or $-1 \neq 1$.
      But if $R^{\times} = \Z/5\Z$, then the group of units cannot have characteristic $2$.
      Therefore, $R$ contains the field $\mathbb{F}_2$.
      Moreover, it cannot contain any larger field or else it would contain a $2^{n}-1$-th root of unity, which only divides $5$ iff $n = 1$.

      Missing rest of proof.
    \item False, same reasioning as in the previous exercise.
    \item Yes, but note that the bijection is not a natural isomorphism because the isomorphism depends on the choice of basis.
  \end{enumerate}
\end{proof}



\begin{exr}[]
Let $\textsf{C}$ be the category with objects the set $\N$ of natural numbers and with
\begin{align*}
  \Hom_{\textsf{C}}(m,n) = \Mat(n,m,\C)
\end{align*}
the set of matrices with $n$ rows and $m$ columns.
\begin{enumerate}
  \item Check that the identity matrix $1_n$ with composition given by the product of matrices, $\textsf{C}$ does indeed form a cateogry.
  \item Let $\textsf{fd-Vec}$ be the category of finitely-dimensional $\C$-vector spaces.
    Show that $F: \textsf{C} \to \textsf{fd-Vec}$ defined by
    \begin{align*}
      F(n) = \C^{n}, \quad F(A) = (x \mapsto  Ax)
    \end{align*}
    is a functor.
  \item Show that $F$ is fully faithful and essentially surjective.
    Such a functor is called an \textbf{equivalence of categories}.
    It means that the categories behave in the same way, although the sets of objects and morphisms are very different.

  \item Define a functor $G: \textsf{fd-Vec} \to \textsf{C}$ natural transformations $F \circ G \to \id_{\textsf{fd-Vec}}$ and $G \circ F \to \id_{\textsf{C}}$.

    \emph{Hint: You can use the axiom of choice to obtain a basis of any fd-vector spaces.}
\end{enumerate}
\end{exr}

\begin{proof}[Solution]
  \begin{enumerate}
    \item Composition is well-defined as for any $A \in \Mat(n \times m, \C) = \Hom_{\textsf{C}}(m,n)$ and $B \in \Mat(l \times n,\C) = \Hom_{\textsf{C}(n,l)}$ we have
      \begin{align*}
        BA \in \Mat(l \times m,\C) = \Hom_{\textsf{C}}(m,l)
      \end{align*}
      It obviously also respects the identity.

    \item Clearly, $F(n) = \C^{n}$ is a vector space and for each $A \in \Hom(m,n)$, we have that 
      \begin{align*}
        F(A) = (x \mapsto Ax): \C^{m} \to \C^{n}
      \end{align*}
      is well defined.
      Now, let $A \in \Hom(m,n)$ and $B \in \Hom(n,l)$. Then
      \begin{align*}
        F(B \circ A) = (x \mapsto BAx) = F(B) \circ (x \mapsto Ax) = F(B) \circ F(A)
      \end{align*}

    \item $F$ fully faithful as, by definition:
      \begin{align*}
        \Hom(m,n) = \Mat(n \times m,\C) = \Hom_{\textsf{C}}(\C^{m},\C^{n})
      \end{align*}
      It is also essentially surjective, because if $V$ is a $\C$-vector space of dimension $n < \infty$, then 
      \begin{align*}
        V \iso \C^{n} = F(n)
      \end{align*}

    \item First let's define $G$. For $V$ be a $\C$-vector space of dimension $n < \infty$ let $G(V) = n$.
      If $V$ has dimension $n$ and $W$ has dimension $m$, then using the axiom of choice, we can fix a basis $v_{1}, \ldots, v_{n}$ of $V$ and $w_{1}, \ldots, w_{m}$ of $W$.

      For $f \in \Hom(W,V)$ define $Gf \in \Hom_{\textsf{C}}(m,n)$ as follows:
      \begin{align*}
        Gf(f) = A = (a_{ij})_{i,j}
        \quad \text{where} \quad 
        f(v_i) = \sum_{j=1}^{m}a_{ij} w_j
      \end{align*}

      Then define
      \begin{align*}
        \Phi: F \circ G \to \id_{\textsf{fd-Vec}} \quad \text{and} \quad 
        \Psi: G \circ F \to \id_{\textsf{C}}
      \end{align*}
      given by
      \begin{align*}
        \Phi_V : \C^{n} \to V, \quad e_i \mapsto v_i\\
        \Psi_n = \id_n: n \to n
      \end{align*}
      Check for yourself that the following diagrams commute:
      \begin{center}
      \begin{tikzcd}[ ] 
        \C^{n} \arrow[]{d}{\Phi_V}
        \arrow[]{r}{(F \circ G) f}
        & \C^{m} \arrow[]{d}{}
        \\
        V \arrow[]{r}{f}
        & W
      \end{tikzcd}
      \quad
      \begin{tikzcd}[ ] 
        n \arrow[]{r}{A}  \arrow[]{d}{\Psi_n}
        & m \arrow[]{d}{\Psi_m}
        \\
        n \arrow[]{r}{A} 
        & m
      \end{tikzcd}
      \end{center}
  \end{enumerate}

\end{proof}



