\begin{thm}[Krull]\label{thm:krull-prime-ideal}
Let $R$ be a noetherian ring, $a \in R - R^{\times}$.
Then
\begin{enumerate}
  \item there exists a minimal prime ideal $p \subseteq R$ such that $a \in p$.
  \item For any such $p$, we have $ht(p) \leq 1$ with equality if $a$ is not a zero-divisor in $R$.
\end{enumerate}
\end{thm}
For example, if $R = \Z$ and $a \in \Z - \{\pm 1\}$, $a \in p\Z$ for any prime divisor $p$ of $a$.

For (a), we just have to apply the following proposition:

\begin{prop}[]
  Let $I \subseteq R$ an ideal, $q \supset I$ a prime ideal (which exists if $I \neq R$).
  Then there exists a minmal prime ideal $p \subseteq R$ such that
  \begin{align*}
    I \subseteq p \subseteq q
  \end{align*}
\end{prop}
For example, if $I = \{0\}$, then there are minimal prime ideals in $R$. There is only the ideal $\{0\}$, if $R$ is an integral domain.

\begin{proof}
  Apply Zorn's Lemma to 
  \begin{align*}
    O = \left\{
      p \text{ prime such that } I \subseteq P \subseteq q
    \right\}
  \end{align*}
  which is non-empty, as $p = q$ is in it.
\end{proof}

\begin{proof}[Proof of (b) in Theorem \ref{thm:krull-prime-ideal}]
  Let $a \in R - R^{\times}$ and $p$ minimal with $a \in p$.
  Assume that there exists a $q \subseteq R$ prime such that $q \subsetneq q$. (Otherwise, $ht(p) = 0$), so $ht(p) \geq 1$.

  Let $q_1 \subseteq q$ prime. We need to show $q_1 = q$.

  \begin{itemize}
    \item[Step 1] Replace $(R,a,p,q,q_1)$ by $(R/q_1,a + q_1, p/q_1,q/q_1,\{0\})$ to get $q_1 = \{0\}$.
      Then the ring becomes an integral domain.

      Then replace $(R,a,p_1,q,\{0\})$ by $(Rp,\frac{a}{1}, pR_p, qR_p, \{0\})$.
      Which forms a local ring. As localisation preserves the noetherian property,
      we can assume that $R$ is a local noetherian integral domain with maximal ideal $p$ and $\{0\} \subseteq q \subsetneq p$.
      and we need $p = q_1 = \{0\}$.

      We now claim that the quotient $R/aR$ is then an artinian ring.
      Indeed $R/aR$ is noetherian and has dimension $0$ because $p$ is the unique prime ideal containing $a$ and it is maximal, so we can apply Akizuki's Theorem.
      
    \item[Step 2] It is enough to prove that $q^{n} = \{0\}$ for some $n \geq 1$ or even that $q^{n}R_q = \{0\}$.
      Because $R$ is an integral domain, if $I^{n} =\{0\}$, then for any $x \in I: x^{n} = 0$, but as $R$ is an integral domain, $x = 0$.

    \item[Step 3]  For $n \geq 1$, let $s_n = q^{n}R_q \cap R \subseteq R$. Then
      \begin{enumerate}
        \item $s_{n+1} \subseteq s_n$ for $n \geq 1$ and $q^{n} \subseteq s_n$.
        \item $s_n = \{x \in R \big\vert \exists y \notin q, yx \in q^{n}, y \in R\}$.
          This is easy using the definition of $R_q$
        \item $s_n R_q = q^{n}R_q$ for $n \geq 1$.
          This is true, as $q^{n}R_q \subseteq s_nR_q$ since $q^{n} \subseteq s_n$.
          conversely let $\frac{x}{y}\in s_nR_q$ (i.e. $x \in s_n, y \notin q$).
          By (b), there is a $z\notin q$ such that $zx \in q^{n}$.
          Then $\frac{x}{y} = \frac{xz}{yz} \in q^{n}R_q$.
      \end{enumerate}
    \item[Step 4] For $n$ large enough, we have
      \begin{align*}
        s_{n+1} + aR = s_n + aR
      \end{align*}
      since $(s_n + a \faktor{R}{aR})$ must be stationary in $R/aR$, which is artinian.

      Moreover $s_n = s_{n+1} + qs_n$, as $s_{n+1} + qs_n \subseteq s_n$.
      For the other inclusion, let $x \in s_n$. By the above, $\exists y \in s_{n+1}$ and $b \in R$ such that $x = y + ab$.

      If we can show that $b \in s_h$ and $a \in p$, it follows $x = y + ab \in s_{n+1} + ps_n$.

      Note that $ab = x - y \in s_n$, so by (ii) in the previous step, there is a $c \notin q$ such that $abc \in q^{n}$, where $ac \notin q$.
      So $b\in s_n$ by (b) once again.

    \item[Step 5] For the same $n$ as in Step $4$, we can show that $s_{n+1} = s_n$.
      Since $s_n = s_{n+1} + p s_n$, where $p$ is the Jacobson radical of $R$ we have $\faktor{s_n}{s_{n+1}} = p \faktor{s_{n}}{s_{n+}}$, so by Nakayama's Lemma, $\faktor{s_n}{s_{n+1}} = \{0\}$.
      (It is applicable as $R$ is noetherian, so $s_n$ is finitely generated.)

    \item[Step 6] For the same $n$, show that $q^{n}Rq = \{0\}$:

      As we have for $p$ the Jacobson radical of $R_q$ and
      \begin{align*}
        q q^{n}R_q =
        q^{n+1}R_q \stackrel{(c)}{\to} s_{n+1}R_q = s_nRq = q^{n}Rq
      \end{align*}
      by Nakayama's Lemmma it follows $q^{n}R_q = \{0\}$.
  \end{itemize}
\end{proof}
What's noticable about this theorem is that while the statement doesn't say anything about artinian rings, the proof uses them as a tool.


\begin{cor}[]
  Let $R$ be a noetherian integral domain. Then
  \begin{align*}
    R \text{ is a UFD } \iff \text{ every prime ideal } p \subseteq R \text{ of height $1$ is principal}
  \end{align*}
\end{cor}
\begin{proof}
We saw in chapter IV that in a UFD, every prime ideal of height $1$ is principal.

For $\impliedby$, it is the standard proof that existence of factorisation into irreducibles follows from the noetherian condition.
For details, see Book.

For uniqueness, it is enough to prove that if $a \in R$ is irreducible, then $aR$ is a prime ideal.
To see this, $a \notin R^{\times}$ so by Krull's Theorem there exists a prime ideal $p \subseteq R$ such that $a \in p$, $ht(p) \leq 1$.
Then in fact $ht(p) = 1$ because $R$ is an integral domain, so $\{0\} \subsetneq p$, as $a \neq 0,a \in p$.
By assumption, $p$ is principal, so $p = bR$. So
$aR \subseteq bR$, so $b | a$ as $a$ is irreducible and $b \notin R^{\times}$. This means $aR = bR = p$, so $p$ is prime.
\end{proof}


\begin{thm}[ACL 9.4.3, (Krull)]
Let $R$ noetherian, $p \subseteq R$ a prime ideal. Then
\begin{enumerate}
  \item $ht(p)$ is finite.
  \item $ht(p)$ is the smallest $n \geq 0$ such that there are $n$ elements $a_{1}, \ldots, a_{n} \in R$ with $a_i \in p$ and $p$ is minimal with this property.
\end{enumerate}
\end{thm}
\begin{proof}
See book.
\end{proof}


\begin{cor}[]
  Let $R$ be a noetherian local ring, then $\dim(R) < \infty$.
\end{cor}
\begin{proof}
  $\dim(R)$ is exactly the height of the unique maximal ideal.
\end{proof}

