
\begin{dfn}[]
Let $R$ be a ring, $M$ an $R$-module.
\begin{itemize}
  \item $M$ is \textbf{artinian} if it satisfies one of the following equivalent properties
    \begin{itemize}
      \item Any non-empty set of submodules of $M$ has a minimal element (with respect to inclusion)
      \item Any decreasing sequence of submodules is stationary.
    \end{itemize}
  \item $R$ is \textbf{artinian} if it is artinian as a mdoule over itself.
\end{itemize}
\end{dfn}
Contrast this with the definition of noetherian rings. There is no analogue statement about modules being finitely generated.
The proof of equivalence of these statements is the same as the proof of equivalance of their noetherian analogues.

\begin{xmp}[]
  Any ring with finitely many ideals (for example fields or finite rings) is artinian.
  Any simple module is artinian.

  Typically it is difficult to find a non-noetherian ring. On the other hand, there are plenty of non-artinian rings.
  $R = \Z$ is not artinian, as we have the sequence
  \begin{align*}
    \Z \supsetneq p\Z \supsetneq p^{2}\Z \supsetneq \ldots \supsetneq p^{n}\Z \supsetneq
  \end{align*}
\end{xmp}

\begin{prop}[]\label{prop:artinian-implies}
Let $R$ be artinian.
\begin{enumerate}
  \item $R$ has only finitely many maximal ideals.
  \item $\dim(R) = 0$
  \item If $R$ is an integral domain, then $R$ is a field.
\end{enumerate}
\end{prop}
\begin{proof}
\begin{itemize}
  \item[(c)] Let $a \in R$. Then we have a sequence
  \begin{align*}
    R \supsetneq aR \supsetneq a^{2}R \supsetneq \ldots \supsetneq a^{k}R \supsetneq
  \end{align*}
  if $R$ is artinian, there exists an $n$ with $a^{n}R = a^{n+1}R$, so $\exists b\in R$ with $a^{n}=a^{n+1}b$, and if $a \neq 0$ and $R$ is an integral domain, it follows $ab = 1$, so $a \in R^{\times}$.
\item[(b)] Let $p$ be a prime ideal in $R$.  If $q$ is a prime ideal $p \subseteq q$, we get
$\faktor{q}{p} \subseteq \faktor{R}{p}$ is a prime ideal and $R/p$ is an artinian (see next proposition)
  By (c) it is a field, so $q/p = \{0\}$ showing $q = p$ which shows $\dim(R) = 0$.
\item[(a)] Let $m_1,m_2,\ldots$ be a sequence of pariwise distinct maximal ideals in $R$.
  Then we get a strictly-decreasing sequence of ideals
  \begin{align*}
    R \supsetneq m_1 \supsetneq m_1 \cap m_2 \supsetneq m_1 \cap m_2 \cap m_3 \supsetneq \ldots
  \end{align*}
  so $R$ cannot be artinian.
  Indeed, the sequence is strict because by the chinese remainder theorem, there is an isomorphism
  \begin{align*}
    \faktor{R}{m_1 \cap \ldots \cap m_k} \iso \faktor{R}{m_1} \times \ldots \times \faktor{R}{m_k}
  \end{align*}
\end{itemize}
\end{proof}

\begin{prop}[]
  Let $0 \to M' \to M \to M'' \to 0$ be a short exact sequence of $R$-modules.
  Then $M$ is artinian $\iff M',M''$ are artinian.

  In particular if $M$ is artinian and $N \subseteq M$, then $N$ and $M/N$ are artinian.
\end{prop}
\begin{proof}[Proof sketch]
  Label the maps as follows
  \begin{align*}
    0 \to M' \stackrel{f}{\to} M \stackrel{g}{\to} M'' \to 0
  \end{align*}
  Given a sequence of decreasing submodules in $M$, we can apply $f^{-1}$ and $g$ to get sequences of decreasing submodules in $M'$ and $M''$, respectively.
\end{proof}

\begin{cor}[]
A finite direct sum of artinian modules is artinian.
\end{cor}
\begin{prop}[]
  Let $R$ be artinian (resp. noetherian)
  \begin{itemize}
    \item For any $S \subseteq R$ multiplicative, the ring $S^{-1}R$ is artinian (resp. noetherian)
    \item For any ideal $I \subseteq R$, the ring $R/I$ is artinian.
  \end{itemize}
\end{prop}
\begin{proof}
  tpilaaettr\footnote{The proof is left as an exercise to the reader.}
\end{proof}

\begin{prop}[ACL 6.4.8]
  Let $M$ be an $R$-module. Then
  \begin{align*}
    M \text{ has finite length } \iff \text{ $M$ is artinian and noetherian}
  \end{align*}
\end{prop}
\begin{proof}
  \begin{itemize}
    \item[$\implies$] 
      Let $M_0 \supseteq M_1 \supseteq \ldots$ be a decreasing sequence of $M$. Since $\infty > l(M) \geq l(M_i) \geq l(M_{i+1}) \geq 0$, the integer-sequence of lengths must stabilize after some $n_0$. 
      For $n \geq n_0$: 
      \begin{align*}
        0 \to M_{n} \to M_{n_0} \to \faktor{M_{n_0}}{M_n} \to 0
      \end{align*}
      By the additivity of the length, and $l(M_n) = l(M_{n_0})$, we get $l(\faktor{M_{n_0}}{M_n}) = 0$, so $\faktor{M_{n_0}}{M_n} = \{0\}$.
      Therefore, $M$ is artinian.

      Similarly, Let $M_0 \subseteq M_1 \subseteq \ldots$ be a sequence of submodules of $M$.
      Then $l(M_i) \leq l(M_{i+1}) \leq l(M) < \infty$.
      Again, the integer-sequence of lengths must stabilize after some $n_0$, and by the same argument as before, $M$ is noetherian.

    \item[$\impliedby$]
      Let $X$ be the set of submodules $N \subseteq M$ with $l(N) < \infty$.

      $X$ is non-empty, since $\{0\} \in X$.
      By the noetherian property, $X$ has a maximal element $N \in X$.
      If $N \neq M$, then the set $Y$ of submodules $N' \supsetneq N$ is non-empty, so by the artinian property, there is a minmal element $N' \in Y$.

      Since $N'$ is minimal, $N'/N$ is simple and the sequence $0 \to N \to N' \to \faktor{N'}{N} \to 0$ shows that $N'$ has finite length, which contradicts the maximality of $N$ in $X$.
  \end{itemize}
\end{proof}

We can find examples that show that artinian and noetherian do not imply eachother directly.
\begin{xmp}[]
  From the previous proposition, we see that
  any finitely-generated $\Z$-module with infinite length is an example of a noetherian, but not artinian module.

  For $M = \faktor{\Z[\tfrac{1}{p}]}{\Z} \subseteq \faktor{\Q}{\Z}$, we can check that $M$ is artinian and has infinite length.

\end{xmp}

\begin{thm}[Akizuki-Hopkins-Levitzki, (ACL 6.4.11,6.4.14)]
  A ring $R$ is artinian $\iff$ $R$ is noetherian and $\dim(R) = 0$.
\end{thm}
\begin{proof}
  In both directions, we will deduce that $R$ has finite length and use the previous proposition.

  \begin{itemize}
    \item[$\implies$] We already know that $\dim(R) = 0$ (see Proposition \ref{prop:artinian-implies}).
      Let $m_1,\ldots,m_k$ be the different maximal ideals in $R$ (we know that there are only finitely many).
      If $I_1 + I_2 = R$, then $I_1I_2 = I_1 \cap I_2$, so 
      \begin{align*}
        R \supseteq m_1 \supseteq m_1m_2 \supseteq \ldots m_1 \dots m_k = m_1 \cap \ldots \cap m_k = J\\
        J \supseteq m_1J \supseteq m_1m_2J \supseteq \ldots \supseteq J^{2} \supseteq m_1J^{2} \supseteq \ldots \supseteq J^{d}\supseteq \ldots
      \end{align*}
      But we will see that for $R$ artinian, there is some $d \geq 1$ with $J^{d} = \{0\}$.
      Observe that the successive quotients in this sequence are of the form $\faktor{I}{mI}$ for some $I \subseteq R$, $m$ maximal.
      These are $R/m$-vector spaces and artinian as $R/m$ modules and are thus finite-dimensional\footnote{Because $\infty$-dim. vector spaces are not artinian as we can generate a decreasing sequence of $\infty$-dim. subspaces.}.
      thus they have finite length as $R$-modules.

      As we only have finitely many quotients, $R$ has finite length.

      We now have to prove that the Jacobson radical $J$ is nilpotent.
      By the artinian property for $ \supseteq J^{2} \supseteq \ldots$ there exists a $d$ such that $J^{d+1} = J^{d}$.
      If we know that $J^{d}$ is finitely-generated, then by Nakayama's lemma the result follows.
      Let
      \begin{align*}
        X = \{I \subseteq R \big\vert IJ^{d} \neq \{0\}\}
      \end{align*}
      if $J^{d} \neq \{0\}$, then $J \in X$ since $J^{d+1} = J^{d} \neq \{0\}$.
      By the artinian property, we could find a maximal ideal $I \in X$.
      Since $IJ^{d} \neq \{0\}$, so for some $x \in I$, $xJ^{d} \neq \{0\}$, which means $xR \in X$, so $I = xR$.

      Now we check that $IJ = I$.
      Nakayama's Lemma gives $I = \{0\}$, which contradicts $IJ^{d} \neq \{0\}$.

      Indeed, $IJ \subseteq I$ and we have $IJ \in X$, which implies $IJ = I$. 

    \item[$\impliedby$] Let $R$ noetherian wtih $\dim(R) = 0$.
      Assume by contrapostion that $l(R) = \infty$.
      This means
      \begin{align*}
        \{I \subseteq R \big\vert l(\faktor{R}{I}) = \infty\} \neq \emptyset
      \end{align*}
      because $\{0\}$ is in it.
      By the noetherian assumption, the collection must have a maximal element $I$ and for any $J \supsetneq I$, it holds $l(R/J) < \infty$.
      Clearly, $R/I$ also is noetherian and has $\dim(R/I) = 0$, so we can repeat the argument for $R/I$ and conclude that $I = \{0\}$.

      But if that's the case, we claim that $R$ is an integral domain.
      From this it would follow that $R$ is a field (because it's an integral domain of dimension $0$), which implies $l(R) = 1$ which is a contradiction.

      To check the claim, pick $a,b \in R$ with $ab = 0$ and consider the sequence
      \begin{align*}
        0 \to bR \to R \to R/bR \to 0
      \end{align*}
      and the mapping
      \begin{align*}
        R/aR \stackrel{f}{\to} bR \to 0, x \mapsto bx \mapsto 0
      \end{align*}
      which is well-defined as $ab = 0$.
      By additivity of the length, $l(R) = l(R/bR) + l(bR) = \infty$, it means that either $l(R/bR)$ or $l(bR)$ is infinite.

      In the first case, it would mean that $b = 0$, since otherwise $bR \neq 0$.
      In the second case, since $bR$ is isomorphic to a quotient of $R/aR$.
      Then $l(R/aR) = \infty$, so $a = 0$.
  \end{itemize}
\end{proof}


\begin{thm}[Akizuki]
Let $R$ be a ring. TFAE:
\begin{enumerate}
  \item $R$ is artinian.
  \item $R$ is noetherian and all of its prime ideals are maximal ideals.
  \item $R$ has finite length.
\end{enumerate}
\end{thm}

