
Let $R$ be a base ring.

\begin{dfn}[]
A module $M$ over $R$ is \textbf{simple}, if $M \neq \{0\}$ and the only submodules of $M$ are $\{0\}$ and $M$ itself.

$M$ is \textbf{semi-simple}, if it is the \emph{finite} direct sum of simple modules.
\end{dfn}

\begin{xmp}[]
For $k$ a field, and $V$ a vector space over $k$, then
$V$ is simple $\iff \dim V = 1$
and $V$ is semi-simple $\iff \dim V < \infty$.
\end{xmp}

\begin{prop}[]
  Let $M$ be an $R$-module. 
  \begin{enumerate}
    \item  If $m \subseteq R$ is maximal, then $R/m$ is a simple $R$-module.
    \item If $M$ is simple, then the annihilator ideal $I = \Ann(M) = \{x \in R \big\vert xM = \{0\}\}$ is a maximal ideal and for any $m \neq 0$ in $M$, the linear map
  \begin{align*}
    R/I \to M, \quad r + I \mapsto rm
  \end{align*}
  is an isomorphism of $R$-modules.
  \end{enumerate}
\end{prop}
\begin{proof}
  \begin{enumerate}
    \item 
  From Proposition \ref{prop:ideals-and-quotients}, we know that for $m \subseteq R$ an ideal the submodules of $R/m$ correspond to ideals $I \subseteq R$ that contain $m$.
  But if $m$ is maximal, only $I = m$ and $I = R$ have this property.
  So the submodules of $R/m$ are exactly $\{0\}$ and $R/m$, thus $R/m$ is simple.

  \item Suppose now $M$ is simple. 
    Fix $a \neq 0 \in M$, then the submodule $Ra \neq \{0\} \subseteq M$ must be equal to $M$.
    By the first isomorphism theorem for modules, we get an isomorphism
    \begin{center}
    \begin{tikzcd}[ ] 
      R \arrow[]{r}{u = (r \mapsto ra)}
      \arrow[]{d}{\pi}
      & Ra = M
      \\
      R/\kernel u
      \arrow[dashed]{ur}{\sim}
    \end{tikzcd}
    \end{center}
    Clearly, $\kernel u = \{r \in R \big\vert ra = 0\}$.
    But because $M$ is simple, so is $R/\kernel u$. This means that $\kernel u$ must be maximal (Or else an ideal $\kernel u \subsetneq J \subsetneq R$ would induce a non-trival submodule $\{0\} \subsetneq R/J \subseteq R/\kernel u$).
    Since $\Ann(M) = \{r \in R \big\vert rM = \{0\}\} \subsetneq R$ is an ideal containing $\kernel u$, we conclude $\Ann(M) = \kernel u$.
  \end{enumerate}
\end{proof}
Note that if $\text{Ann}(M)$ is maximal, $M$ must not necessarily be simple. 
For example, if $R$ is a field, then $M = R^{2}$ as annihliator $\Ann(M) = \{0\}$.

\begin{xmp}[]
  For $R = \Z$, the simple modules are the finite cyclic groups of prime order ($\iso \Z/p\Z$).

  For $\C[X_{1}, \ldots, X_{n}]$ it follows from the Nullstellensatz that $\C$ is the only simple module.
\end{xmp}

\begin{prop}[Schur's Lemma]
  Let $M,N$ be $R$-modules and $u: M \to N$ $R$-linear and $u \neq (m \mapsto 0)$
  \begin{itemize}
    \item If $M$ is simple, then $u$ is injective.
    \item If $N$ is simple, then $u$ is surjective.
    \item If $M,N$ are simple, then $u$ is an isomorphism
  \end{itemize}
\end{prop}
This follows from the fact that $\Ker(u) \subseteq M$ and $\Image(u) \subseteq N$ are submodules of simple modules.


\begin{dfn}[]
Let $M$ be an $R$-module.
The \textbf{length} $l_R(M)$ is the supremum of the length of chains
\begin{align*}
  M_0 \subsetneq M_1 \subsetneq \ldots \subsetneq M_k \subseteq M
\end{align*}
We say that $M$ has finite length, if $l_R(m) < \infty$.
\end{dfn}
If the length is finite, it means that there exists a chain of such length.

\begin{xmp}[]
\phantom{a}
\begin{itemize}
  \item If $R = k$ is a field, then $l_R(M) = \dim_k(M)$ and $M$ has finite length $\iff $ it is finite-dimensional.
    Indeed, of $\dim_k(M) < \infty$, take a basis $e_{1}, \ldots, e_{d}$ and consider the chain
    \begin{align*}
      \{0\} \subsetneq Re_1 \subsetneq Re_1 \oplus Re_2 \subsetneq \ldots \subsetneq Re_1 \oplus \ldots \oplus Re_d = 0
    \end{align*}
    Which shows $l(M) \geq \dim(M)$.
    And for any other chain $M_0,M_1,\ldots,M_n = M$, we have that $\dim(M_i) \geq i$, so $n \leq \dim(M)$

  \item Take $R = \Z$. Then $\Z$ itself has infinite length because for any $k \geq 1$, we can construct a chain
    \begin{align*}
      2^{k} \Z \subsetneq 2^{k-1}\Z \subsetneq \ldots \subsetneq \Z
    \end{align*} 
    In fact, the $\Z$-modules of finite length are exactly the finite abelian groups.
  \item $l_R(M) = 0 \iff M = \{0\}$ and $l_R(M) = 1 \iff M$ is simple.
\end{itemize}
\end{xmp}


\begin{prop}[]
Let the following be a short exact sequence
\begin{align*}
  0 \to M' \to M \to M'' \to 0
\end{align*}
Then the following are equivalent:
\begin{enumerate}
  \item $M$ has finite length
  \item $M',M''$ have finite length and 
  \begin{align*}
    l_R(M) = l_R(M') + l_R(M'')
  \end{align*}
\end{enumerate}
In particular, if $M$ has finite length and $M' \subseteq M$, then $M'$ and $M/M'$ have finite length and
\begin{align*}
  l_R(M) = l_R(M') + l_R(M/M')
\end{align*}
\end{prop}
\begin{proof}
  \textbf{(b)$\bm{\implies}$(a):} is trivial.

  Now assume $M$ has finite length.
  Label the maps as follows
  \begin{align*}
    0 \to M' \stackrel{f}{\to} M \stackrel{g}{\to} M'' \to 0
  \end{align*}

  \textbf{$\bm{l_R(M) \geq l_R(M') + l_R(M'')}$:}
  Let
  \begin{align*}
    \{0\} = {M'}_0 \subsetneq \ldots \subsetneq {M'}_{k'} = M'\\
    \{0\} = {M''}_0 \subsetneq \ldots \subsetneq {M''}_{k''} = M''
  \end{align*}
  be chains in $M',M''$.
  Then in $M$, we get the chain in $M$
  \begin{align*}
    \{0\} = f({M'}_0) \subsetneq \ldots \subsetneq f(M') = g^{-1}(\{0\}) \subsetneq g^{-1}({M''}_1) \subsetneq \ldots \subsetneq M
  \end{align*}
  of length $k+k''$.
  Since $l_R(M) < \infty$, $M',M''$ also have finite length.
  
  \textbf{$\bm{l_R(M) \leq l_R(M') + l_R(M'')}$:} 
  Let
  \begin{align*}
    M_0 \subsetneq M_1 \subsetneq \ldots \subsetneq M_k
  \end{align*}
  be a chain in $M$. Then we get sequences (not necessarily chains)
  \begin{align*}
    f^{-1}(M_0) &\subseteq \ldots \subseteq f^{-1}(M_k) \subseteq M'\\
    g(M_0) &\subseteq \ldots \subseteq g(M_k) \subseteq M''
  \end{align*}
  What we do is we start at the left of both chains and simultaneously go to the right. Each time there is a non-equality ($\subsetneq$), we make a click.
  The total number of clicks will then give a lower bound of $l_R(M') + l_R(M'')$.
  We claim that at every step, there must be at least one click.
  
  More precisely, we show that for all $i$, we cannot have both
  \begin{align*}
    \text{(1):} \quad f^{-1}(M_i) = f^{-1}(M_{i+1}) \quad \text{and} \quad \text{(2):} \quad g(M_i) = g(M_{i+1})
  \end{align*}
  If that were the case, we will have $M_i = M_{i+1}$ because:
  Let $x \in M_{i+1}$. By (2), there exists a $y \in M_i$ with $g(y) = g(x)$.
  Therefore $x - y \in \Ker(g)$ and by exactness, $x-y = f(z)$ for some $z \in f^{-1}(M_{i+1}) = f^{-1}(M_i)$ (by (1)).
  So $f(z) + y = x \in M_i$.


  If omit parts of the sequences in $M',M''$, we get chains of lengths $k',k''$.
  The previous argument shows $k' + k'' \geq k$.

\end{proof}

If a vector space has dimension $k< \infty$, then a chain can only have length $k$ if the dimension of each step increases only by $1$.
The following theorem shows an analogue for modules.

\begin{thm}[Jordan-Hölder (ACL 6.2.11)]
  An $R$-module $M$ has finite length if and  only if it has a \textbf{composition series}, i.e. a finite chain
  \begin{align*}
    \{0\} = M_0 \subsetneq M_1 \subsetneq \ldots \subsetneq M_k = M
  \end{align*}
  such that successive quotients $\faktor{M_i}{M_i-1}$ are simple modules.
  Then $l_R(M) = k$.

  If $M$ has finite length, then the successive quotients of a composition series are unique with multiplicity up to order.

  More precicely, for any two composition series, there is a bijection
  \begin{align*}
    \sigma: \{1,\ldots,l(M)\} \to \{1,\ldots,l(M)\}
  \end{align*}
  such that for $1 \leq i \leq k-1$.
  \begin{align*}
    \faktor{M_i}{M_{i-1}}  \stackrel{\iso}{\to} \faktor{N_{\sigma(i)}}{N_{\sigma(i) - 1}}
  \end{align*}
\end{thm}
\begin{proof}
  If $l(M) < \infty$, any chain in $M$ of length $l(M)$ is a composition series because if
  \begin{align*}
    \{0\} = M_0 \subsetneq \ldots \subsetneq M_{l(M)} = M
  \end{align*}
  we cannot insert a module $N$ with $M_{i-1} \subsetneq N \subsetneq M_i$, as it would increase the length of the chain.
  This means that the $\faktor{M_i}{M_{i-1}}$ is simple.

  Conversely, assume that
  \begin{align*}
    \{0\} = M_0 \subsetneq \ldots \subsetneq M_{k} = M
  \end{align*}
  is a composition series. We can show by incudtion that
  \begin{align*}
    l(M_1) = 1, l(M_2) = 2, \ldots l(M) = k < \infty
  \end{align*}
  Clearly, $l(M_0) = l(\{0\}) = 0$. And for $i \geq 1$, we get a short exact sequence
  \begin{align*}
    0 \to M_{i-1} \to M_i \to \faktor{M_i}{M_{i-1}} \to 0
  \end{align*}
  so by the previous proposition we have
  \begin{align*}
    l(M_i) = l(M_{i-1}) + l(\faktor{M_i}{M_{i-1}}) = (i-1) + 1 = i
  \end{align*}

  To show that the successive quotiens are unique (up to permutation), let
  \begin{align*}
    \{0\} = M_0 \subsetneq \ldots \subsetneq M_{l} = M, \quad
    \{0\} = N_0 \subsetneq \ldots \subsetneq N_{k} = M
  \end{align*}
  be two composition series.

  We wish to interpolate the second seires in the first one, so given an $1 \leq i \leq l$, we consider the sequnce (not chain)
  \begin{align*}
    M_{i-1} = M_{i,0} \subseteq M_{i,1} \subseteq \ldots \subseteq M_{i,k} = M_i \quad \text{for} \quad M_{i,j} = M_{i-1} + N_j \cap M_i
  \end{align*}
  Because $\faktor{M_i}{M_{i-1}}$ is simple, we have exactly one $j$ such that $M_{i,j-1} \subsetneq M_{i,j}$ with all others inclusions being equalities.

  This gives the map $\sigma$ sending $i$ to that $j$.

  Reversing the roles of $M_i$ and $N_i$, we consider $N_{j,i} = N_{j-1} + M_i \cap N_j$ and get a mapping $\tau: j \mapsto i$.
  In both directions, we are adding the term $M_i \cap N_j$
  for all $i,j$, we have isomorphisms
  \begin{center}
  \begin{tikzcd}[ ] 
    \faktor{M_{i,j}}{M_{i,j-1}} 
    \arrow[]{r}{\stackrel{\alpha}{\iso}}
    &
    \faktor{M_{i} \cap N_j}{\underbrace{M_{i-1} \cap N_j + M_i \cap N_{j-1}}_{P_{i,j}}}
    \\
    &
    \faktor{N_{j,i}}{N_{j,i-1}} 
    \arrow[]{u}{\stackrel{\beta}{\iso}}
  \end{tikzcd}
  \end{center}
  which are induced by
  \begin{align*}
    \alpha(m_{i-1} + n_j) = n_j + P_{i,j} \quad \text{and} \quad 
    \beta(n_{j-1} + m_i) = m_i + P_{i,j}
  \end{align*}

  To see this, we can with the quotient
  \begin{align*}
    \faktor{M_{i,j}}{M_{i,j-1}} = \faktor{M_{i-1} + (N_j \cap M_i)}{M_{i-1} + (N_{j-1} \cap M_i)}
  \end{align*}
  clearly, $M_{i-1}$ is already in the kernel, so the image must lie in some quotient of $N_j \cap M_i$.
  Moreover, we have to intersect $M_{i-1}$ with $N_j$, giving us the image of $\alpha$
  \begin{align*}
    \faktor{M_i \cap N_j}{M_{i-1} \cap N_j + M_i \cap N_{j-1}}
  \end{align*}
  To check that $\alpha(m_{i-1} + n_j) = n_j + P_{i,j}$ is well-defined note that if
  \begin{align*}
    m_1 + n_1 = m_2 + n_2 \text{ with } m_1,m_2 \in M_{i-1}, n_1,n_2 \in M_{i} \cap N_j
  \end{align*}
  then we have
  \begin{align*}
    m_1 - m_2 = n_2 - n_1 \in M_{i-1} \cap N_j \subseteq P_{i,j} \implies n_1 + P_{i,j} = n_2 + P_{i,j}
  \end{align*}
  Moreover, if $m \in M_{i-1}, n \in N_{j-1} \cap M_i$, then $\alpha(m + n) = n + P_{i,j} = P_{i,j}$.
  Analogously for $\beta$.
  
  Lastly, we define the inverse $\gamma = \alpha^{-1}$ given by
  \begin{align*}
    \gamma(m + P_{i,j}) = m + M_{i,j-1} \quad \text{for} \quad m \in M_i \cap N_j
  \end{align*}
  This is well-defined because
  \begin{align*}
    P_{i,j} = M_{i-1} \cap N_j + M_i \cap N_{j-1} \subseteq M_{i-1} + M_{i} \cap N_{j-1} = M_{i,j-1}
  \end{align*}
  lastly we see
  \begin{align*}
    \alpha( \gamma ( m+ P_{i,j})) = \alpha (m + M_{i,j-1}) = m + P_{i,j} \quad \text{for} \quad m \in M_i \cap N_j\\
    \gamma (\alpha ( m + n + M_{i,j-1}) = \gamma(n + P_{i,j}) = n + M_{i,j-1} = m + n + M_{i,j-1} \quad \text{for} \quad m \in M_{i-1},n \in M_i \cap N_j
  \end{align*}

  
  By the characterisation of $\sigma(i), \tau(j)$ we have $\sigma \circ \tau = \id$ and $\beta^{-1} \circ \alpha$ is an isomorphism of the successive quotients.
\end{proof}

\begin{xmp}[]
  Let $K$ be a field, $E$ a finite-dimensional $K$-vector space.
  A composition serios is just a sequence
  \begin{align*}
    \{0\} = E_0 \subsetneq E_1 \subsetneq \ldots \subsetneq E_d = E
  \end{align*} 
  where $\dim(E_i) = i$.
  Such things are also called flags.
  For $K = \R$ or $K = \C$ and $d \geq 2$, there are uncountably many distinct flags.

  One can show that for $R = \Z$ the simple modules are $\Z/p\Z$ for $p$ prime.
  And a $\Z$-module $M$ has finite length if and only if $M$ has finite cardinality.

  If $M$ has a composition series $M_0, \ldots, M_k$ with $\faktor{M_i}{M_{i-1}} \iso \Z/p_i\Z$ then one checks by induction that
  $\abs{M_i} = p_1 \dots p_{i}$ since we have a short exact sequence $0 \to  M_{i-1} \to  M_i \to \Z/p_i\Z \to 0$.
  Therefore, $\abs{M} = \prod_{i=1}^{k}p_i$ is finite.

  Note that while the sequence of consecutive factors $\faktor{M_i}{M_{i-1}}$ tells us the cardinality of the module, it does not say anything about the structure of $M$ up to isomorphism.

  For example, the composition series
  \begin{align*}
    \{0\} \subseteq \Z/2\Z \subseteq \Z/4\Z, \quad \text{and} \quad 
    \{0\} \subseteq \Z/2\Z \subseteq \Z/2\Z \times \Z/2\Z
  \end{align*}
  have identical successive quotients, but clearly $\Z/4\Z \not\iso \Z/2\Z \times \Z/2\Z$.
\end{xmp}

For any PID $R$ with $R/aR$ as simple modules, a irreducible module of finite length are $\iso \bigoplus_{i=1}^{n}R/a_iR$, $a_i$ irreducible.

The uniqueness part of the Jordan-Hölder is equivalent to the uniqueness of the factorisation for PIDs.
For instance, for $R = \Z$, a decomposition series of $\Z/n\Z$ corresponds to the prime factorisation.
