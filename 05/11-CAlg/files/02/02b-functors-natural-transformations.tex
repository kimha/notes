
\begin{dfn}[Functor]
Let $\textsf{C},\textsf{D}$ be categories.
A \textbf{functor} $F$ from $\textsf{C}$ to $\textsf{D}$, denoted $F: \textsf{C} \to  \textsf{D}$ is a rule assigning
\begin{enumerate}
  \item an object $F(X)$ of $\textsf{D}$ for any objects $X$ of $\textsf{C}$
  \item A morphism $F(f): F(X) \to F(Y)$ to any morphism $f: X \to Y$.
\end{enumerate}
such that $F(\id_X) = \id_{F(X)}$ and for any pair of composable morphisms: $(X \stackrel{f}{\to}  Y), (Y \stackrel{g}{\to}Z)$ in $\textsf{C}$, the following diagram commutes (in $\textsf{D}$)
\begin{center}
\begin{tikzcd}[] %\arrow[bend right,swap]{dr}{F}
  F(X) \arrow[]{r}{F(f)} \arrow[bend right]{rr}{F(g \circ f)} \arrow[bend left]{rr}{F(g) \circ F(f)}&
  F (Y) \arrow[]{r}{F(g)} & 
  F (Z)
\end{tikzcd}
\end{center}
\end{dfn}


From now on, we will omit parenthesis for functor application unless necessary so instead of $F(X)$ or $F(f)$, we write $FX$ or $Ff$.


\begin{xmp}[]
  Functors are plentiful in Algebra and Topology.
  \begin{enumerate}
    \item If we have a group $(G,\cdot,e)$, we can forget the group structure and only look at the underlying set $G$.
      Group homomorphisms turn into functions.
      We call this the \textbf{forgetful functor}.
      The same construction works for topological spaces, where we forget the topology of a topological space.
    \item We can turn every field into a group, by removing the $0$. Field homomorphisms then become grougroup.
    \item Given any ring $R$, we can obtain the group of units $R^{\times}$.
      This is the \textbf{units functor} from the category of rings to the category of groups.


  \item As you should know from Topology last semester, the \textbf{fundamental group} $\pi_1$ is a functor from the category of pointed topological spaces to the category of groups.
    
  \end{enumerate}
\end{xmp}

Consider rule $\textsf{F}: \textsf{Vec}_k \to  \textsf{Vec}_k$ which maps vector spaces to their dual space and linear maps to their dual maps.

So for vector spaces $V,W$ and a linear map $f \in \Hom(V,W)$ we define
\begin{align*}
  F V = V^{\ast}, \quad \text{and} \quad Ff =: f^{\ast}: W^{\ast} \to  V^{\ast}, \quad f^{\ast} \beta = \beta \circ f \in V^{\ast}
\end{align*}
You might notice that the direction of $Ff$ is opposite to that of $f$, so it doesn't technically fit the definition we gave earlier.


\begin{dfn}[]
Let $\textsf{C}$ be a category.
The \textbf{opposite category} $\textsf{C}^{\text{op}}$ is the category with the same objects as $\textsf{C}$ and $\Hom_{\textsf{C}^{\text{op}}}(X,Y) = \Hom_{\textsf{C}}(Y,X)$.

\begin{center}
\begin{tikzcd}[] 
  & \textsf{C} & & & & \textsf{C}^{\text{op}}\\
  \\
  X \arrow[]{r}{f} 
  \arrow[bend left]{rr}{g \circ f}
  &
  Y \arrow[]{r}{g}
  & Z
  & &
  X &
  Y \arrow[]{l}{f^{\text{op}}}
    &
  Z \arrow[]{l}{g^{\text{op}}}
  \arrow[bend right,swap]{ll}{f^{\text{op}} \circ g^{\text{op}} = (g \circ f)^{op}}
\end{tikzcd}
\end{center}
A functor $F: \textsf{C} \to \textsf{D}^{\text{op}}$ is called a \textbf{contravariant} functor from $\textsf{C}$ to $\textsf{D}$.
\end{dfn}

We will stil call it a functor and (if necessary) specify wheter the functor is \textbf{covariant} or contravariant.


\begin{xmp}[Hom-functor]
  Since $\Hom_{\textsf{C}}(X,Y)$ is a set, we can define two set-valued functors.
  Fix an object $X$
  \begin{enumerate}
    \item The \textbf{covariant Hom-functor} is
      \begin{align*}
        h^{X}: \textsf{C} \to \Set, \quad h^{X}(A) = \Hom_{\textsf{C}}(X,A), \quad h^{X}(f) = (f \circ -) = (g \mapsto f \circ g)
      \end{align*}
      \begin{center}
      \begin{tikzcd}[ ] 
        X
        \arrow[]{r}{g}
        &
        A
        \arrow[]{r}{f}
        &
        B
      \end{tikzcd}
      \end{center}
    \item The \textbf{contravariant Hom-functor} is
      \begin{align*}
        h_{X}: \textsf{C} \to \Set, \quad h_X(A) = \Hom_{\textsf{C}}(A,X), \quad h_X(f) = (- \circ f) = (g \mapsto g \circ f)
      \end{align*}
      \begin{center}
      \begin{tikzcd}[ ] 
        A 
        \arrow[]{r}{f}
        &
        B
        \arrow[]{r}{g}
        &
        X
      \end{tikzcd}
      \end{center}
  \end{enumerate}

  \textbf{Warning for Physicists:} The positionig of the index is opposite to that for tensors.
  For example, the covariant derivative $\del_{\mu}$ has a downstairs index, whereas the covariant Hom-functor $h^{X}$ has it upstairs.

  A mnemonic is that $h^{X}$ has morphisms that \emph{fall} from the upstairs $X$ and $h_X$ has morphisms that fall downstairs into $X$.
  And if we look at $h^{X}(f)$ from above to below, the $f$ comes \emph{after} $X$, so $h^{X}(f)$ is \emph{post-composition} with $f$ and $h_X(f)$ is \emph{pre-composition} with $f$.
\end{xmp}



