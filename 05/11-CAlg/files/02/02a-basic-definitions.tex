The Eilenberg-MacLane notion of a category is as follows:

\begin{dfn}[Category]
A category $\textsf{C}$ consists of the following data:
\begin{itemize}
  \item A ``collection'' of \textbf{objects} $X$ of $\textsf{C}$.
  \item For any objects $X,Y$ of $\textsf{C}$, we define a \emph{set} $\Hom_{\textsf{C}}(X,Y)$ of \textbf{morphisms} from $X$ to $Y$ in $\textsf{C}$.

    Instead of writing $f \in \Hom(X,Y)$, we will usually write $X \stackrel{f}{\to}Y$ or $f: X \to Y$.

  \item For every object $X$ of $\textsf{C}$, there exists an \textbf{identity-morphism} $\id_X: X \to X$.
  \item Morphisms can be composed. For objects $X,Y,Z$, there exists a map 
    \begin{align*}
      \Hom_{\textsf{C}}(X,Y) \times \Hom_{\textsf{C}}(Y,Z) &\to \Hom_{\textsf{C}}(X,Z)\\
      (g,f) &\mapsto f \circ g
    \end{align*}
    \begin{center}
      \begin{tikzcd}[ ] 
        X \arrow[]{r}{g} \arrow[bend left]{rr}{f \circ g}& Y \arrow[]{r}{f}& Z
      \end{tikzcd}
    \end{center}
\end{itemize}
This data must respect the following axioms.
\begin{itemize}
  \item Identity laws:
    $
      \id_Y \circ f = f \quad g \circ \id_x = g
    $
  \item Composition is associative: $f \circ (g \circ h) = (f \circ g) \circ h$
\end{itemize}
\end{dfn}


\begin{rem}[]
We are intentionally being obtuse when talking about ``collection'' of objects.
The reason is that we want to talk about the ``collection'' of \emph{all} sets.
Using the normal set theory we know from the first semester, we quickly run into problems as there is no such thing as a set of all sets.

There are a number of solutions to this though.
\begin{itemize}
  \item There are set theories that make use of \emph{classes} to enlarge the notion of set.
  \item Restrict what sets we are allowed to talk about. For example using \emph{Grothendieck universes}.
  \item Ignore the problem.
\end{itemize}
We will chose the third option.
If you still feel unfomfortable, one can say that there is some logical formuale $\Phi_{\textsf{C}}(X)$ in one variable, which evaluates to \texttt{true}, if $X$ is an object in the category $\textsf{C}$.
\end{rem}

\begin{xmp}[]
  When we say that something is a category, we usually specify what the objects are and what the morphisms (and composition) look like.
  Feel free to skip those you are not familiar with.

  \begin{itemize}
    \item The category of sets $\Set$, where the objects are sets and morphisms are functions.
    \item The category $\Grp$, where the objects are groups and morphisms are group homomorphisms.
    \item $\Top$ the category of topological spaces, where the objects are topological spaces (with their topology) and where the morphisms are \emph{continuous} functions.
    \item $\Top_{\ast}$, with Topological spaces with a basepoint $(X,\tau,x_0)$ as objects and where the morphisms are continous, base-point preserving functions.
    \item $\Ring$ with Rings as objects and ring homomorphisms as morphisms.
    \item Given a fixed ring $R$, the category $R-\textsf{Mod}$ of $R$-modules and linear maps.
    \item \textsf{Graph} has graphs as objects and graph homomorphisms as morphisms.
    \item For a fixed field $K$, the category $\textsf{Vec}_K$ has $K$-vector spaces as objects with linear maps as morphisms.
    \item One rather weird category is the category of fields with field homomomorphisms.
  \end{itemize}

  The examples above are categories were objects were \emph{sets with structure} and the morphisms were \emph{structure preserving} maps.
  But the objects need not be sets, and morphisms need not be functions!

  \begin{itemize}
    \item The trivial category, which consists of a single object $\ast$ and its identity morphism $\ast \stackrel{\id}{\to} \ast$.
    \item A group (or monoid) $(G,\cdot,e)$ can be viewed as a category with a single object $\ast$, where the morphisms $g: \ast \to \ast$ are the elements $g \in G$ and where composition of morphisms is defined as the multiplication in $G$: $g \circ h := g \cdot h$.
    \item A poset $(P,\leq)$ is a category where the objects are the elements of $P$ and there exist unique morphisms $x \to y$ if and only if $x \leq y$.
    \item \textsf{Hask}, the idealized cateogory of Haskell types and functions.
    \item Given a category $\textsf{C}$, we can talk about its \textbf{arrow category}, where the objects are morphisms $(X \stackrel{f}{\to} Y)$ in $\textsf{C}$ and a morphism between objects $(X \stackrel{f}{\to} Y), (X' \stackrel{g}{\to}Y')$ are pairs of $\textsf{C}$-morphisms $\alpha: X \to X'$ and $\beta: Y \to Y'$ such that the following diagram commutes:
      \begin{center}
      \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
        X \arrow[]{r}{f} \arrow[]{d}{\alpha}& Y \arrow[]{d}{\beta}\\
        X' \arrow[]{r}{g}& Y'
      \end{tikzcd}
      \end{center}
      i.e. such that $g \circ \alpha = \beta \circ f$.
  \end{itemize}
\end{xmp}

In general, always specify what category you are working in. 
The real numbers $\R$ are a Set, Topological space, Group, Field, Poset etc.

\begin{rem}[]
  We say that a triangle 
  \begin{tikzcd}[column sep=0.8em] 
    & \cdot \arrow[]{dr}{h}\\
    \cdot \arrow[]{ur}{g}
    \arrow[]{rr}{f}
    & & \cdot
  \end{tikzcd}
  \textbf{commutes}, if $g \circ h = f$.
  A diagram is commutative, if every triangle of that diagram commutes.
  This includes non-obvious triangles, such as those formed by morphisms obtained through composition or the identity morphisms.
\end{rem}

