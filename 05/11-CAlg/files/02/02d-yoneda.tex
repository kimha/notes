The Yoneda Lemma is a first non-trivial result about categories.
The Hom-functor $h_X = \Hom_{\textsf{C}}(-,X)$ \emph{characterizes} an object $X$ up to a ``canonical'' isomorphism.


\begin{dfn}[]\label{dfn:natural-transformation}
Let $F$ and $G$ be two functors $\textsf{C} \to \textsf{D}$.
A \textbf{natural transformation} $\Phi: F \to G$ is a rule that to every object $X$ of $\textsf{C}$ assigns a morphism $\Phi(X) : FX \to GX$ such that for any $X \stackrel{f}{\to}Y$, the following diagram (in $\textsf{D}$) commutes:

\begin{center}
\begin{tikzcd}[] 
  FX 
  \arrow[]{r}{Ff}
  \arrow[swap]{d}{\Phi(X)}
  &
  FY
  \arrow[]{d}{\Phi(Y)}
  \\
  GX
  \arrow[]{r}{Gf}
  &
  GY
\end{tikzcd}
\end{center}
$\Phi$ is called a \textbf{(natural) isomorphism}, if $\Phi(X)$ is an isormophism for all $X$.
If that is the case, then $\Psi(X) = \Phi(X)^{-1}$ defines a natural transformation $\Psi: G \to F$.

We write $\Nat(F,G)$ for the set\footnote{Since Categories can be large, it is not clear why this would be a set.
This however follows from Yoneda's Lemma.} of natural transformations from $F$ to $G$.

\end{dfn}

The morphism $\Phi(X)$ is called the \textbf{component} of $\Phi$ at $X$ and we will sometimes write $\Phi_X$ instead.


\begin{xmp}[]
  Take $\textsf{C} = \textsf{D} = \Vec_{K}$ for some field $K$.
  Let $F = \id_{\textsf{C}}$ be the identity functor and $G$ the bidual functor
  \begin{align*}
    GV = (V')' = \Hom(\Hom(V,K),K), Gf = (f')'
  \end{align*}

  We know from linear algebra that for any vector space $V$, there exists a canonical map
  \begin{align*}
    V \to (V')', \quad v \mapsto \left(
      \alpha \mapsto \alpha(v)
    \right)
    \quad \text{for} \quad \alpha \in V', v \in V
  \end{align*}
  This defines a natural transformation $\Phi: \id_{\textsf{C}} \to G$.
  Check for yourself that the diagram commutes.
\end{xmp}

Recall the (contravariant) Hom-functors $h_X = \Hom(-,X),h_Y = \Hom(-,Y): \textsf{C} \to \Set$.

\begin{lem}[The Yoneda Lemma]
  Let $X,Y$ be objects of $\textsf{C}$.
  There is a bijection
  \begin{align*}
    \Hom_{\textsf{C}}(X,Y) \stackrel{\sim}{\to} \Nat(h_X,h_Y)
  \end{align*}
\end{lem}

\begin{proof}
  \begin{itemize}
    \item 
      Let $X \stackrel{f}{\to}Y \in \Hom(X,Y)$.
      We will use this to define a natural transformation.

      For all $A$ in $\textsf{C}$, we can define a mapping
      \begin{align*}
        \Phi_f(A) = (f \circ -) = (a \mapsto f \circ a): h_X(A) \to h_Y(A)
      \end{align*}
      \begin{center}
        \begin{tikzcd}[ ] 
          A
          \arrow[]{r}{a}
    &
    X
    \arrow[]{r}{f}
    &
    Y
        \end{tikzcd}
      \end{center}
      we can check that $\Phi_f$ is a natural transformation $\Phi_f: h_X \to h_Y$:

      Indeed, let $A \stackrel{g}{\to}B$ be a morphism in $\textsf{C}$, and consider the diagram
      \begin{center}
        \begin{tikzcd}[column sep=4em] 
          h_X(A)
          \arrow[swap]{d}{\Phi_f(A) = (f \circ -)}
    &
    h_X(B)
    \arrow[swap]{l}{h_X(g) = (- \circ g)}
    \arrow[]{d}{\Phi_f(B)= (f \circ -)}
    \\
    h_Y(A)
    &
    h_Y(B)
    \arrow[]{l}{h_Y(g) = (- \circ g)}
        \end{tikzcd}
      \end{center}
      Starting from the top-right with a morphism $B \stackrel{h}{\to} X \in h_X(B)$, the left-down path gives us
      \begin{align*}
        (B \stackrel{h}{\to}X) \mapsto (A \stackrel{h \circ g}{\to} X) \mapsto (A \stackrel{f \circ (h \circ g)}{\to} Y)
      \end{align*}
      and the down-left path gives us
      \begin{align*}
        (B \stackrel{h}{\to}X) \mapsto (B \stackrel{f \circ h}{\to} Y) \mapsto (A \stackrel{(f \circ h) \circ g}{\to} Y)
      \end{align*}
      which checks out.

    \item For the other way, let $\Phi: h_X \to h_Y$ be a natural transformation.
      We show that it is of the form $\Phi_f$ for some $f \in \Hom(X,Y)$.

      Since $\Phi$ is given, for every $A \stackrel{g}{\to}B$, we get a commutative diagram
  \begin{center}
    \begin{tikzcd}[ ] 
      h_X(A)
      \arrow[swap]{d}{\Phi_A}
    &
    h_X(B)
    \arrow[swap]{l}{h_X(g)}
    \arrow[]{d}{\Phi_B}
    \\
    h_Y(A)
    &
    \arrow[swap]{l}{h_Y(g)}
    h_Y(B)
    \end{tikzcd}
  \end{center}  
  The trick is to take $B = X$ and follow the diagram for the identity morphism $\id_X \in h_X(B)$ in the top-right.
  The left-down path gives us
  \begin{align*}
    \id_X \mapsto  (\id_X \circ g) = g \mapsto \Phi_A(g)
  \end{align*}
  The down-left path gives
  \begin{align*}
    \id_X \mapsto \Phi_X(\id_X) 
    \mapsto (\Phi_X(\id_X) \circ g)
  \end{align*}
  But if we chose our $f \in h_Y(X)$ to be $f := \Phi_X(\id_X)$, then commutativity just reads 
  \begin{align*}
    \Phi_A(g) \stackrel{!}{=} (\underbrace{\Phi_X(\id_X)}_{=: f} \circ g) = (f \circ g) = \Phi_f(A)(g)
  \end{align*}
  for all objects $A$. 
  Another way to view this is that the component $\Phi_A$ is determined by the element $\Phi_X(\id_X) \in h_Y(X)$, \emph{for all objects} $A$.
  \end{itemize}
\end{proof}


The following are more properties and were not part of the lecture
\begin{rem}[]
In the proof, we didn't need any special properties of $h_Y$, so the proof generalizes for any other set-valued functors $F$, i.e.
\begin{align*}
  \Hom(X,Y) = h_Y(X) \iso \Nat(h_X,h_Y) \text{ generalises to }
  F(X) \iso \Nat(h_X,F)
\end{align*}
Yoneda Lemma also shows functorial properties.
If $\Phi: F \to  G$ and $\Psi: G \to H$ are natural transformations, we can define their \textbf{vertical composition} $\Psi \bm{\cdot} \Phi: F \to  H$.
(Draw a diagram to see why it's called this way).

In particular, if we have morphisms $X \stackrel{f}{\to}Y \stackrel{g}{\to}Z$ with induced natural transformations $\Phi_f, \Phi_g$, then we have
\begin{align*}
  \Phi_g \bm{\cdot} \Phi_f = \Phi_{g \circ f}
\end{align*}
So the Yoneda Lemma says that there is a functor $C \to \text{Func}(C,\text{Set})$ which preserves hom-sets and is ``surjective''. (The categoric definition here is that the functor is \textbf{fully-faithful}).
\end{rem}




\begin{cor}[] \label{cor:iso-with-hom}
The functors $h_X$ and $h_Y$ are isomorphic if and only $X$ and $Y$ are isomorphic.
\end{cor}
What this means is that to define an object $X$, it is enough to describe its hom-functor.

\begin{proof}
  The Yoneda Lemma tells us that the mapping $\textsf{C} \to \textsf{Fun}(\textsf{C},\textsf{Set})$ is a fully faithul functor (See Definition \ref{edfn:fully-faithful} from Exercise sheet 1), where the objects in $\textsf{Fun}(\textsf{C},\textsf{Set})$ are functors and morphisms are natural transformations.

  We show that any fully faithful functor respects isomorphisms.

  Let $F: \textsf{C} \to  \textsf{D}$ be a fully faithful functor and let $\Phi: F(X) \to F(Y)$ be an isomorphism.
  Chose a morphism $f: X \to Y$ that induces $\Phi$. (In the case of the Yoneda Lemma, this was $f = \Phi_X(\id_X)$).
  By fullness there exists a morphism $g: Y \to X$ that induces $\Phi^{-1}$.
  By functoriality $f \circ g$ and $g \circ f$ induce the identity of $F(Y)$ and $F(X)$, and by faithfullness, they must be the identity on $Y$ and $X$.
\end{proof}

\begin{exr}[]
  Prove the Yoneda Lemma for the covariant functors $h^{X},h^{Y}$.
\end{exr}


