
\section{Overview}
The lecture content is mostly guided by Antoine Chamber-Loir's Book (Mostly) Commutative Algebra.
We will not follow it one-to-one, but chapter numbers will be provided on the go.
There will be some extra topics covered that are not in the book.

Definitions and theorems enclosed with parenthesis are not part of the lecture and (mostly) come from the exercise sheets.
\subsection{Introduction/Motivation}
\subsubsection*{What is it?}

Commutative Algebra is the study of commutative Rings and related objects such as ideals, modules etc.

We will use the convention that rings, (unless specified otherwise) are all commutative with unit.
We will also require that ring morphisms preserve the unit. 
In particular $\Z \to \Z, x \mapsto 0$ is not a morphism of rings.

\subsubsection*{Why study this?}
One reason to study commutative algebra is that it is the ``local'' side of algebraic geometry, which is the study of geometric objects such as solution sets of systems of polynomial equations.

One reason to study those is that they are a natural generalisation of linear algebra.
However there are other good motivations, such as the tensor product which proves itself as a universally useful construction.


\subsubsection*{What are some results?}

\begin{thm}[Hilbert/Noether]
  Let $n \geq 1$ be an integer.
  Let $(P_i)_{i \in I}$ be a family of polynomials $P_i \in \C[X_{1}, \ldots, X_{n}]$ with $I$ arbitrary.

  There exist finitely many polynomials $Q_{1}, \ldots, Q_{l} \in \C[X_{1}, \ldots, X_{n}$ such that both families shares the same solution set i.e.
  \begin{align*}
    \left\{x = (x_{1}, \ldots, x_{n}) \in \C^{n} \big\vert \forall i \in I: P_i(x) = 0\right\}
    =
    \left\{x \in \C^{n} \big\vert \forall j \in \{1, \ldots, l\}: Q_i(x) = 0\right\}
  \end{align*}
\end{thm}

\begin{exr}[]
  Prove this result if all $P_i$ have degree $\leq 1$.
\end{exr}

Suppose I give you five polynomials in five variables.
The first question one might ask if there exist any solutions.
What Hilbert proved was that if there is not solution, then there has to be an ``obvious'' solution.

By just algebraic manipulation of the equations, we should be able to prove that $1 = 0$ and find the contradiction.
\begin{thm}[Hilbert's Nullstellensatz]
  Let $n \geq 1, m \geq 0$ and $P_{1}, \ldots, P_{m} \in \C[X_{1}, \ldots, X_{n}]$.
  
  Then there always exists a $x \in \C^{n}$ suc that $P_i(x) = 0$ for all $i = 1, \ldots, m$
  \textbf{unless}
  there exist $Q_1,\ldots,Q_m$ such that
  $P_1Q_1 + \ldots + P_mQ_m = 1$.
\end{thm}

The contradiction lies in that fact that when we plug in the common solution $x$, we end up with $0 = 1$.

\begin{exr}[]
Prove this result in the case $\deg P_i \leq 1$.
\end{exr}

Hilbert's Nullstellensatz fails for $\R$. 
We need to use that $\C$ is algebraically closed.
Consider for example $P = X^{2} + 1 \in \R[X]$.


The following theorem about recurrent sequences was proven in three steps.

\begin{thm}[Skolem-Mahler-Lech]
  Let $\left(u_{n}\right)_{n \in \N}$ be a sequence of complex numbers such that there exist some $k \geq 1$ such that
  \begin{align*}
    \forall n \geq 0: u_{n+k} + a_1 u_{n+k-1} + \ldots + a_k u_n = 0
  \end{align*}
  with $a_i \in \C$.
  Then the set
  \begin{align*}
    N = \{n \geq 0 \big\vert u_n = 0\}
  \end{align*}
  is a finite union of sets of the form
  \begin{align*}
    A_{a,b} = \{am + b \big\vert m \in \N\}
  \end{align*}
\end{thm}
An example of such a set would be the set of odd numbers or the numbers with remainder $12$ modulo $17$.

The fibonnaci sequence is such an example as it satisfies $u_{n+1} - u_{n+1} - u_{n} = 0$.



\subsection{Some quick guidelines}
There are many definitions that at by themselves aren't hard to understand, but often seem unmotivated at first sight.
However that is not the case. Many of the concepts we introduce are motivated by the following:
\begin{center}
  We want to study more complicated objects using properties of simple ones we understand well.
  In the case of Commutative Algebra, we understand linear algebra quite well. For example we know what a dimension of a kernel of a linear map is.
  The generalisation is the Krull dimension.

  The second object we understand well are PID's such as $K[X]$ or $\Z$.
\end{center}

\subsection{Reminders and Notation}
\begin{itemize}
  \item The natural numbers should index the possible dimensions of vector spaces. Since $\{0\}$ is a vector space. $\N$ should contain $0$. $\N = \{0,1,2,\ldots\}$
  \item For a ring $R$, we denote the group of units by $R^{\times}$.
  \item An element $x \in R$ is \textbf{nilpotent}, if $\exists n \in \N: x^{n} = 0$
  \item A ring called \textbf{reduced}, if it has no non-zero nilpotent elements.
  \item An $R$-module $M$ is an abelian group $(M,+)$ with ring-multiplication
    \begin{align*}
      R \times M \to  M, (x,m) \mapsto  x \cdot m
    \end{align*}
    such that $1_R \cdot m = m, 0_R \cdot m = 0_M, x \cdot 0_M = 0_M$.
    With distributivity and associativity.
  \item An ideal $I \subseteq R$ is an $R$-submodule of $R$.
    From this point of view, it is more flexible to study modules instead of ideals.
  \item Let $I \subseteq R$ be an ideal. The following are equivalent
    \begin{itemize}
      \item $I$ is \textbf{prime}.
      \item $I \neq R$ and $R/I$ is an inegral domain
      \item $I \neq R$ and $rs \in I \implies$ $r \in I$ or $s \in I$.
    \end{itemize}
  \item The following are equivalent:
    \begin{itemize}
      \item $I \subseteq R$ is \textbf{maximal}
      \item $R/I$ is a field.
      \item $I \neq R$ and there is no ideal other than $R$ and $I$ itself that contains $I$.
    \end{itemize}
\end{itemize}

\subsubsection*{Facts}
\begin{itemize}
  \item If $f: R \to S$ is a ring morphism, $P \subseteq S$ is a prime ideal, then $f^{-1}(P)$ is also prime.
    To prove it, all we need is to see that $f$ induces an injective map $\faktor{R}{f^{-1}(P)} \stackrel{f}{\hookrightarrow} \faktor{S}{P}$.
    
    This fact is false for maximal ideals. Consider the inclusion map $f: \Z \hookrightarrow \Q$.
    As $\Q$ is a field $\{0\}$ is a maximal ideal, but $f^{-1}(\{0\}) = \{0\}$ is not.
  \item Krull: Let $I \neq R$ be an ideal of $R$. Then there exists a maximal ideal containing $I$. (See [ACL p. 57]
  \item Let $R$ be a UFD, then $p \in R - \{0\}$ is prime if and only if $p$ is irreducible.
\end{itemize}

\begin{dfn}[Algebra]
  Let $R$ be a ring.
  An \textbf{algebra over $R$} (or $R$-algebra) is a ring-morphism $\phi: R \to S$.
  Often, we just say that $S$ is an $R$-algebra and call $\phi$ the \emph{structure morphism} of the algebra.

  If $R \stackrel{\psi}{\to}T$ is another $R$-algebra, a morphism of $R$-algebras is a ring homomorphism $f: S \to T$ such that the following diagram commmutes

  \begin{center}
    \begin{tikzcd}[column sep=0.8em] 
      S \arrow[]{rr}{f}&& T\\
        & R \arrow[]{ul}{\phi} \arrow[swap]{ur}{\psi}
    \end{tikzcd}
  \end{center}
\end{dfn}

In practice: We can multiply elements of $S$ by those of $R$ by writing
\begin{align*}
  r \cdot s := \phi(r) s
\end{align*}
using this notion, then $f: S \to T$ is an $R$-algebra morphism if and only if
\begin{align*}
  \forall r \in R: \quad
  f(r \cdot s) = f( \phi(r) s) = f(\phi(r)) f(s) = \psi(r) f(s) = r \cdot f(s)
\end{align*}

\begin{xmp}[]
\begin{enumerate}
  \item Any ring $S$ is a $\Z$-algebra, where the structure morphism $\Z \mapsto S$ is uniquely determined, because
    \begin{align*}
      1 \mapsto  1_S \implies n \mapsto \underbrace{1_S + \ldots + 1_S}_{n \times}
    \end{align*}
    What this means in practice is that if some theorem holds for algebras, then it also holds for rings.
  \item If $R \subseteq R$ is a subring, then $S$ is an $R$-algebra, where the structure morphism is the inclusion mapping.

    In particular, $\C$ is an $\R$-algebra and a $\Q$-algebra. More generally, if $L/K$ is a field extension, then $L$ is a $K$-algebra.
  \item Warning! On a given ring $S$, there may be more than on $R$-algebra structure on $S$ (structure morphisms $R \to S$)

    For example $\C$ can be seen as $\C$-algebra with the following strucutre morphisms:
    \begin{align*}
      z \cdot w = zw (\phi(z) = z), \quad z \cdot w = \overline{z} (\phi(z) =\overline{z})
    \end{align*}
    more generally, every automorphism defines a structure morphism.
\end{enumerate}
\end{xmp}

