\begin{thm}[Matsamura 11.2]
Let $R$ be a local noetherian integral domain with maximal ideal $m$ and residue field $k = R/m$.
The following are equivalent:
\begin{enumerate}
  \item $R$ is a normalized DVR for some valuation $v$.
  \item $\dim(R) = 1$ and $R$ is integrally closed.
  \item $\dim(R) \geq 1$ and $\dim_{k}(\faktor{m}{m^2}) = 1$.
  \item $\dim(R) \geq 1$ and $m$ is principal
  \item $R$ is a PID and not a field.
\end{enumerate}
\end{thm}
\begin{proof}
Many implications are trivial:

\textbf{(a) $\bm{\implies}$ (b):} See Propositions \ref{prop:valuation-ring-ordering-dim1} and \ref{prop:valuation-ring-is-integrally-closed}

\textbf{(a) $\bm{\implies}$ (d):} By the previous theorem, $R$ is a PID and thus a UFD.
Moreover, as $\hht m = 1$, it follows from Example \ref{xmp:ufd-principal-height} that $m$ is principal.
Because $\dim(R) = 1$, the maxmial ideal is a PID.

\textbf{(a) $\bm{\implies}$ (e):} $R$ is PID by the previous theorem.

It is also not a field, because $\dim R \neq 0$. (See Proposition \ref{prop:field-iff-id-dim0}).

\textbf{(e) $\bm{\implies}$ (d):} See Proposition \ref{prop:pid-not-a-field}, where we show that if $R$ is a PID and not a field, then $\dim R = 1$.

\vspace{1\baselineskip} 

We are missing (d) $\implies$ (a), (b) $\implies$ (d) and (c) $\iff$ (d).

For both (d) $\implies$ (a) and (b) $\implies$ (d) we will use Krull's Intersection theorem.

\vspace{1\baselineskip} 

\textbf{(d) $\bm{\implies}$ (a):} 
Let $\pi \in m$ be a generator of the maximal ideal.
There is only one possible valuation on $R$ to make it a DVR:
For $x \in R$, take
\begin{align*}
  v(x) = \max \{k \geq 0 \big\vert x \in \pi^{k}R\}
\end{align*}
This is well-defined by Krull's intersection theorem.
The inequalities
\begin{align*}
  v(x+y) \geq \min \{v(x),v(y)\}, \quad v(xy) \geq v(x) + v(y)
\end{align*}
are easy to show. For ``$\leq$'' in the second inequality, assume $xy \in \pi^{v(x) + v(y) +1}R$. Then
\begin{align*}
  \underbrace{\frac{x}{\pi^{v(x)}}}_{\in R}
  \underbrace{\frac{y}{\pi^{v(y)}}}_{\in R}
  \in \pi R = m
\end{align*}
since $m$ is prime, one of the factors is an element of $\pi R$, contradicting the definition of $v$.

Thus $v$ is a normalized discrete valuation on $R$ and we now must show that $R_v = R$.

Clearly, $R \subseteq R_v$. Conversely, let $x = \frac{a}{b}\in R_v$, where $v(a) \geq v(b)$.

Writing
\begin{align*}
  a = \pi^{v(a)}a', a' \notin \pi R, b = \pi^{v(b)}b', b' \notin \pi R
\end{align*}
we get
\begin{align*}
  \frac{a}{b} = \underbrace{\pi^{v(a) - v(b)}}_{\in R} \frac{a'}{b'}
\end{align*}
and $b' \notin m$ so $b' \in R^{\times}$, which shows $\frac{a}{b} \in R$, which shows $R_v \subseteq R$.

\textbf{(b) $\bm{\implies}$ (d):} 
We want to show that $m$ is principal.
By Krulls' intersection theorem, we have
$\bigcap_{k \geq 0} m^{k} = \{0\}$, so $m^{2} \neq m$.
Fix $\pi \in m \setminus m^{2}$. We check that $\pi$ is a generator of $m$.
Clearly, $\pi R \subseteq m$, for the converse we claim that 
there is an injective $R$-linear morphism
\begin{align*}
  \faktor{R}{m} \hookrightarrow \faktor{R}{\pi R}
\end{align*}
We will prove this later in the case of associated primes.
Equivalently, we show that
\begin{align*}
  \exists x \in R: \quad
  \left\{
    y \in R \big\vert xy \in \pi R
  \right\}
  = m
\end{align*}
this is equivalent because we then have a map of the form
\begin{align*}
  f_x: \faktor{R}{m} \to \faktor{R}{\pi R}, \quad y \mapsto xy
\end{align*}
Note that
\begin{align*}
  m = \{y \in R \big\vert xy \in \pi R\}
  = \{y \in R \big\vert ay \in R\} \quad \text{where} \quad a = \frac{x}{\pi} \in \Quot(R)
\end{align*}
so we have $am \subseteq R$ by definition, but as $am$ is an ideal and $R$ is local, we either have $am = R$ or $am \subseteq m$.

In the second case this means that $a$ is integral over $R$, and since $R$ is integrally closed in $\Quot(R)$, it follows $a \in R$.
But $a$ cannot be an element of $R$ as we would get
\begin{align*}
  1 \cdot x = a \pi \in \pi R \implies 1 \in m = \{y \big\vert yx \in \pi R\}
\end{align*}
which would mean $m = R, \lightning$.

Therefore, we have $am = R \iff xm = \pi R$.
We show that $x$ is a unit. Clearly, $x \notin m$, or else $\pi \in \pi R = xm \subseteq m^{2}$, but $\pi \not\in m^{2}$.
Therefore, $x \in R \setminus m = R^{\times}$ which gives $xm = m = \pi R$.

\textbf{(c) $\bm{\iff}$ (d):} 
If $m$ is principal, then $\faktor{m}{m^{2}}$ has dimension $1$.
The converse follows because if $\pi  + m$ generates $\faktor{m}{m^{2}}$ as an $R/m$-vector space, then by Nakayama's Lemma, $\pi$ generates $m$.

\textbf{Proof claim:} 
We now prove the claim above that there exists an injective map
$\faktor{R}{m} \hookrightarrow \faktor{R}{\pi R}$.

For $x \in R \setminus \pi R$, let
\begin{align*}
  I_x = \{y \in R \big\vert xy \in \pi R\}
\end{align*}
Then the set of ideals $\{I_x \big\vert x \in R \setminus \pi R\}$ is not empty and $I_x \neq R$, because $1 \in I_x \implies x \in \pi R$.
Since $R$ is noetherian, there exists an $x \in R \setminus \pi R$ such that $I_x$ is maximal among all other such ideals.
We now show that this $I_x$ is the maximal ideal by showing that $I_x$ is prime.

Let $\alpha \beta \in I_x$, so $\alpha \beta \in \pi R$.
If $\beta \notin I_x$, we have $x \beta \notin \pi R$, so among the other ideals is the ideal $I_{\beta x}$ and $\alpha \in I_{\beta x}$.

But this shows that $I_{\beta x} \supseteq I_x$, and by maximality of $I_x$ they are equal, so $\alpha \in I_x$ showing that $I_x$ is prime.
It is also non-empty, as $\pi R \subseteq I_x$.
As $R$ is local and of dimension $1$, it follows that $I_x = m$.

This completes the proof.
\end{proof}

