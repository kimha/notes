\begin{dfn}[]
Let $R$ be a ring. A (rank 1) \textbf{valuation} $v$ on $R$ is a map $v: R \to \R \cup \{+ \infty\}$ such that
\begin{itemize}
  \item $v(x) = + \infty \iff x = 0$
  \item $v(xy) = v(x) + v(y)$ for all $x,y \in R$ 
  \item $v(x+y) \geq \min(v(x), v(y))$ for all $x,y \in R$.
\end{itemize}
    with the usual ordering and the convention $z + \infty = \infty$ and $\min(z,\infty) = z$ for all $z \in \R$.
\end{dfn}
Note that we always must have $v(1) = 0$, or else $v(x) = v(1x) = v(1) + v(x)$.

\begin{xmp}[]
Let $R$ be a UFD and $p \in R$ irreducible. Define
\begin{align*}
  v_p: &R \to \Z \cup \{+ \infty\}\\
  v_p(b) &= \max \left\{
    k \geq 0 \big\vert\ p^{k} | b
  \right\}
  = \max \left\{
    k \geq 0 \big\vert\ b \in p^{k}R
  \right\}
\end{align*}
Then $v_p$ is a valuation on $R$ called the \textbf{$p$-adic valuation}\footnote{So in a sense, $v_p$ measures the $p$-ness of a number. (Do not read that sentence out loud)}.

For $R = \Z$, we get the p-adic valuations for any prime number $p$.

For a field $K$, we can take $R = K[X]$ and for $f \in R$ irreducible, we get the $f$-adic valuation.
If we take $f = X - a$ for $a \in K$, $v_f$ is the \textbf{order of vanishing} of a polynomial at the point $a$.
\end{xmp}

\begin{lem}[]
Let $v$ be a valuation on $R$. Then $R$ is an integral domain and $v$ extends uniquely to a valuation on $\Quot(R)$ such that
\begin{align*}
  v(\tfrac{a}{b}) = v(a) - v(b)
\end{align*}
Conversely, if $v$ is a valuation on $K$, then its restriction to any subring $K \subseteq K$ is a valuation on $A$.
\end{lem}
\begin{proof}
For $x,y$ non-zero we have $v(xy) = v(x) + v(y) \neq \infty$, so $xy\neq 0$, so $R$ is an integral domain.

To extend it to $\text{Quot}(R)$, we show that $v(\tfrac{a}{b}) = v(a) - v(b)$ is well-defined. 
\begin{align*}
  \frac{a}{b} = \frac{c}{d} \iff ad = bc \iff v(a) + v(d) = v(b) + v(c) 
\end{align*}
and $a = 0 \iff \frac{a}{b} = 0$. Moreover
\begin{align*}
  v\left(
    \frac{a}{b} \cdot \frac{c}{d}
  \right)
  = v(a) - v(b) + v(c) - v(d) = v(\tfrac{a}{b}) + v(\tfrac{c}{d})
\end{align*}
And for addition:
\begin{align*}
  v\left(
    \frac{a}{b} + \frac{c}{d}
  \right)
  &= v(ad + bc) - v(b) - v(d)\\
  &\geq \min \{v(ad),v(bc)\} - v(b) - v(d)\\
  &= \min \{v(a) + v(d), v(b) + v(c)\} - v(b) - v(d)\\
  &= \min \{v(a) - v(b), v(c) - v(d)\}\\
  &= \min \{v(\tfrac{a}{b}), v(\tfrac{c}{d})\}
\end{align*}
The final property is clear.
\end{proof}

Note that if $v$ is a valuation on $R$, we get another valuation $\tilde{v}$, by defining $\tilde{v}(x) = \lambda v(x)$, for some $\lambda \in \R_{\geq 0}$.
In particular, $\tilde{v}(x) = 0$ for all $x \neq 0$ is a trivial valuation that we aren't interested in.

\begin{dfn}[]
Let $R$ be a ring, $v$ a valuation on $R$.
The \textbf{value group} of $v$ is the subgroup
\begin{align*}
  \Gamma_v = v\left(
    \Quot(R)^{\times}
  \right) \subseteq \R
\end{align*}
the valuation $v$ is called a \textbf{discrete} valuation if $\Gamma_v \subseteq \R$ is discrete, or equivalently if there exists a $\alpha \in \R$ such that $\Gamma_v = \alpha \Z$.
The valuation is \textbf{normalized} if one can chose $\alpha = 1$, i.e. $\Gamma_v = \Z$.
\end{dfn}

In the definition above, we used the fact that subgroup $\Gamma \subseteq \R$ is discrete if and only if there exists a $\alpha \in \R$ such that $\Gamma = \alpha \Z$.
Indeed, if $\Gamma \neq \{0\}$, then there is a smallest $\alpha > 0$ in $\Gamma$ and by using an argument similar to euclidean division, one checks that $\Gamma = \alpha \Z$.


\begin{xmp}[]
The $p$-adic valuation of the previous examples are normalized discrete valuations with $v_p(p) = 1$.

We will concentrate on these valuations but first check out this simple example of a non-discrete valuation:
Let $R = \C[X,Y]$ and set
\begin{align*}
  v\left(
    \sum a_{m,n}X^{m}Y^{n}
  \right)
  =
  \min \{m + \sqrt{2}n \big\vert a_{m,n} \neq 0\}
\end{align*}
One sees that $\Gamma_v = \Z + \sqrt{2}\Z \subseteq \R$ is not discrete as we can go arbitrarily close to an integer.
\end{xmp}

