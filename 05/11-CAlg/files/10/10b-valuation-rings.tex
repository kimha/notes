
\begin{lem}[] \label{lem:valuation-ring}
Let $v$ be a valuation on a ring $R$ and $K = \Quot(R)$.
Assume $\Gamma_v \neq \{0\}$. 
Then the set
\begin{align*}
  R_v = \{x \in K \big\vert v(x) \geq 0\}
\end{align*}
is a subring of $K$ called the \textbf{valuation ring} of $v$.
It is a local ring with maximal ideal
\begin{align*}
  m_v = \{x \in K \big\vert v(x) > 0\} \neq \{0\}
\end{align*}
with $K \neq R_v$ as its fraction field.
\end{lem}
\begin{proof}
  If $x,y \in R_v$, then
  \begin{align*}
    v(xy) = v(x) + v(y) \geq 0 \implies xy \in R_v\\
    v(x + y) \geq \min (v(x),v(y)) \geq 0 \implies x + y \in R_v
  \end{align*}
  so $R_v \subseteq K$ is a subring.
  For $x,y \in m_v, r \in R_v$, we have
  \begin{align*}
    v(x + y) \geq \min(v(x),v(y)) > 0, \quad v(rx) = v(r) + v(x) > 0
  \end{align*}
  so $m_v \subseteq R_v$ is an ideal.

  From Remark \ref{rem:units-in-local}, we know that
  \begin{align*}
    R_v^{\times} = R_v - m_v \iff R_v \text{ is local and $m_v$ is its maximal ideal}
  \end{align*}
  We show that the left hand side is true:
  Let $x \in R_v - \{0\} \subseteq K^{\times}$. Because
  $v(x^{-1}) = -v(x)$ we have
  \begin{align*}
    x \in R_v^{\times} \iff 
    x^{-1} \in R_v \iff v(x) = 0 \iff x \in R_v - m_v
  \end{align*}
  Moreover, since $\Gamma_v \neq \{0\}$, there exists an element $x \in K^{\times}$ with $v(x) > 0$ and $v(x^{-1}) < 0$.
  This means $m_v \neq \{0\}$ and the element $\tfrac{1}{x} \text{Quot}(R_v)$ is not in $R_v$, showing $R_v \neq K$.
\end{proof}

\begin{dfn}[]
A (rank 1) \textbf{valuation ring} is a pair $(R,v)$ of a ring with a valuation such that $R$ is the valuation ring $R_v$.
A \textbf{discrete valuation ring} (short DVR) is a valuation ring $(R,v)$ with $v$ discrete. 
It is \textbf{normalized} if $\Gamma_v = \Z$.
\end{dfn}

The nice thing about valuation rings is that we can divide by elements in $R$ as long as the valuation stays non-negative.
So if $x,y \in R$ have $v(x) \geq v(y)$, then 
\begin{align*}
  \frac{x}{y} \in R = \{a \in \text{Quot}(R) \big\vert v(a) \geq 0\}
\end{align*}

\begin{xmp}[]
  For $R = \Z$, $p \in \Z$ prime, the valuation ring of the $p$-adic valuation $v_p$ is
  \begin{align*}
    R_{v_p} = 
    \left\{
      x \in \Q \big\vert v_p(x) \geq 0
    \right\}
    =
    \left\{
      \tfrac{a}{b} \in \Q \big\vert p \not| b
    \right\}
    = (\Z \setminus p\Z)^{-1}\Z = \Z_{p\Z}
  \end{align*}
  More generally, for a UFD $R$ and $p \in R$ irreducible, the valuation ring of the $p$-adic valuation $v_p$ is the localization $R_{pR}$ of $R$ with respect to the prime ideal $pR$.

Indeed, for $x = \frac{a}{b} \in \Quot(R)$, we have
\begin{align*}
  x \in R_{v_p} \iff v_p(a) \geq v_p(b) 
\end{align*}
so if $x \in R_{pR}$, it means $b \notin pR \implies v_p(x) = v_p(a) \geq 0$ which means $x \in R_{vp}$.

Conversely, if $x = \tfrac{a}{b} \in R_{vp}$, then
\begin{align*}
  \frac{a}{b} = \frac{p^{v_p(a) - v_p(b)}a'}{b'} \in R_{pR}
\end{align*}
Therefore $R_{pR} = R_{v_p}$.
\end{xmp}


\begin{dfn}[]
If $(R,v)$ is a DVR, then any element $\pi \in R$ such that $v(\pi)$ generates $\Gamma_v$ (which is isomorphic to $\Z$) is called a \textbf{uniformizer} of $R$.
If $v$ is normalized, then $v(\pi) = 1$.
\end{dfn}
\begin{xmp}[]
For the valuation ring $R_{pR}$ associated to an irreducible element $p$ of a UFD, $p$ itself is a uniformizer.

For an arbitrary DVR $R$ with uniformizer $\pi$, all uniformizers are of the form $u \pi$, where $u \in R_v^{\times}$.
\end{xmp}


\begin{lem}[]
Let $v$ be a valuation ring on a ring $R$.
Then
\begin{align*}
  v(x) \neq v(y) \implies v(x+y) = \min \{v(x),v(y)\}
\end{align*}
\end{lem}
\begin{proof}
We may assume WLOG $v(x) < v(y)$. In particular, $x \neq 0$. Then in $K = \text{Quot}(R)$ we can write
\begin{align*}
  v(x+y) = v(x) + v\left(
    1 + \frac{y}{x}
  \right)
\end{align*}
But by assumption $v(\tfrac{y}{x}) > 0$, so $\tfrac{y}{x} m_v$ and $1 + \tfrac{y}{x} \in R_v$.
In fact, it is in $R_v - m_v$, or otherwise $1 \in m_v$.
Therefore
\begin{align*}
  1 + \tfrac{y}{x} \in R_v - m_v = R_v^{\times} \implies v(1 + \tfrac{y}{x}) = 0 \implies v(x+y) = v(x) = \min \{v(x), v(y)\}
\end{align*}
\end{proof}

\begin{prop}[] \label{prop:valuation-ring-is-integrally-closed}
Let $(R,v)$ be a valuation ring. Then $R$ is integrally closed.
\end{prop}
\begin{proof}
Let $x$ be an element of $\Quot(R)$ inegral over $R$, $d \geq 1$ and $a_i \in R$ such that
\begin{align*}
  x^{d} + a_{d-1}x^{d-1} + a_1 x + a_0 = 0
\end{align*}
If $x \notin R$. Then $v(x)< 0, \tfrac{1}{x}\in R$.
Multiplying by $x^{-d}$, we get
\begin{align*}
  1 + \underbrace{a_{d-1} \frac{1}{x} + \ldots +a_1 \frac{1}{X^{d-1}} + \frac{a_0}{X^{d}}}_{\in m_v} = \underbrace{0}_{\in m_v}
\end{align*}
Since $m_v$ is an ideal, it would imply that $1 \in m_v$, but $v(1) = 0 \lightning$.
\end{proof}

