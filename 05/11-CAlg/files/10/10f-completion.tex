Given a valuation $v$ on a ring, one can obtain a topology/metric on the ring.
Where $x_n \to 0$ if $v(x_n) \to \infty$

\begin{prop}[]
  Let $v$ be a valuation on a ring $R$. Define
  \begin{align*}
    \|0\|_v :0, \quad
    \|x\|_v := 2^{-v(x)}, \quad \text{for} \quad x \neq 0
  \end{align*}
  and $d_v(x,y) := \|x-y\|_v$.

  Then $d_v$ is a metric on $R$ and
  \begin{align*}
    \|xy\|_v = \|x\|_v \|y\|_v, \quad \text{and} \quad \|x+y\|_v \leq \max\left(
      \|x\|_v, \|y\|_v
    \right) \leq \|x\|_v + \|y\|_v
  \end{align*}
  and addition, multiplication are continuous.
\end{prop}
\begin{proof}
The proof follows easily by the properties of the valuation $v$.
\end{proof}

\begin{rem}[]
The triangle inequality is slightly strengthened and takes the form of the \emph{non-archimedian} triangle inequality.
\begin{align*}
  d_v(x,z) \leq \max\left(
    d_v(x,y), d_v(y,z)
  \right)
\end{align*}
The name shows the connection with the archimedian property of the real nubers, since for $R = \Z, v = v_p$ for some prime $p$:
\begin{align*}
  \|k \cdot x\|_v \leq \|x\|_v \quad \text{for} \quad k \geq 1
\end{align*}
in contrast to the archimedian property
\begin{align*}
  \forall \epsilon > 0, A > 0, \quad
  \exists k \geq 0: \quad k \cdot \epsilon \geq A
\end{align*}

Moreover, every ``triangle'' in $R$ has two sides of the same length. i.e. WLOG $d(y,x) = d(y,z)$ (up to renaming).
\end{rem}


In general, $R$ with a valuation $v$ does not form a complete metric space, but we can construct the completion of the space in which $R$ is dense.


\begin{thm}[]
Let $(R,v)$ be a DVR, $m \subseteq R$ the maximal ideal and $\pi \in m$ a uniformizer (so $m = \pi R$).
Define
\begin{align*}
  \hat{R} := \{
    \left(
      x_k
    \right)_{k \geq 1}
    \big\vert
    x_k \in \faktor{R}{\pi^{k}R},
    x_{k+1} = x_k \mod \pi^{k}R
  \}
\end{align*}
Then $\hat{R}$ is a ring with coordinate-wise addition and multiplication and the function and
\begin{align*}
  \hat{v}: \hat{R} \to \Z \cup \{+\infty\}, \quad \hat{v}(0) = \infty\\
  \hat{v}((x_k)_{k \geq 1}) = \min \left\{
    k \big\vert x_k \neq 0
  \right\}
  -1
\end{align*}
makes $(\hat{R},\hat{v})$ a normalized DVR with uniformizer
\begin{align*}
  \hat{\pi} = \left(
    \pi + \pi^{k}R
  \right)
  _{k \geq 1}
\end{align*}
Moreover, let 
\begin{align*}
  \iota: R \to \hat{R}, \quad x \mapsto (x + \pi^{k}R)_{k \geq 1}
\end{align*}
is an injective isometric ring morphisms satisfing $\hat{v} \circ \iota = v$ with dense image.

Finally, $\hat{R}$ is complete and $\text{Quot}(\hat{R}) = \hat{R}[\tfrac{1}{\hat{\pi}}]$.
\end{thm}

\begin{xmp}[]
For $R = \Z_{p\Z}$, $\hat{R}$ contains the element
\begin{align*}
  (1 + pR, 1 + p + p^{2}R, 1 + p + p^{3}R, \ldots)
\end{align*}
moreover, if we know the $k$-th component, we can obtain all the previous components by taking modulos $\pi^{k}R$.
\end{xmp}


\begin{proof}[Proof theorem]
  The proof is mostly elementary so we only cover two interesting claims:
  That $\iota$ has dense image and that $\hat{R}$ is complete.

  Let $x = (x_k)_{k \geq 1} \in \hat{R}$.
  Fix $m \geq 1$, let $\tilde{x} \in R$ be such that $\tilde{x} \mod \pi^{m}R = x_m$. 
  Then by definition of $\hat{R}$, we have
  \begin{align*}
    \tilde{x} - x = \left(
      \underbrace{0, 0, \ldots 0}_{m \text{ times}}, \ast, \ldots
    \right)
  \end{align*}
  so $\|i(\tilde{x}) - x\|_{\hat{v}} \leq 2^{-m}$, which converges to $0$ as $m \to \infty$.
  So $x$ is the limit of a sequence of elements in $\iota(R)$, showing density.

  For completeness, let $m \geq 1$ and
  \begin{align*}
    f_m: \hat{R} \to \faktor{R}{\pi^{m}R}, \quad (x_k)_{k \geq 1} \mapsto  x_m
  \end{align*}
  and let $(x_n)$ be a Cauchy sequence in $\hat{R}$.
  If $\hat{v}(x_n - x_{n'}) \geq m$, then $f_m(x_n) = f_m(x_{n'})$.
  By the Cauchy condition, $\hat{v}(x_n - x_{n'}) \geq m$, so if $n,n' \geq N(m)$ for some $N$ large enough, the sequence
  $(f_m(x_n))_{n \geq 1}$ is constant to some $y_m \in \faktor{R}{\pi^{m}R}$.

  Let $y = (y_m)_{m \geq 1}$. Then one checks that
  \begin{align*}
    x_n \stackrel{n \to \infty}{\to} y \text{ because } f_m(x_n) = f_n(y) \text{ for $n$ large enough}
  \end{align*}
\end{proof}


\begin{xmp}[]
For $R = \Z_{p\Z}$ $p$ prime, the completion $\hat{R}$ is the ring of $p$-adic integers $\Z_p$.
The fraction field of $p$-adic numbers is denoted $\Q_p$.


As $\Z_p \subseteq \prod_{k \geq 1}\faktor{\Z}{p^{k}}\Z$ is closed, it follows from Tychonoff's theorem, that $\Z_p$ is compact and $\Q_p$ is locally compact.


\end{xmp}

One can show that 
\begin{align*}
  R \text{ is a complete DVR } \iff 
  R = \hat{R}_v \text{ for some DVR $R_v$}
\end{align*}
\begin{prop}[]
If $R$ is a complete DVR, then
\begin{align*}
  \sum_{n \geq 1} a_n, a_n \in R \text{ converges } \iff \lim_{n \to \infty}a_n = 0
\end{align*}
\end{prop}
\begin{proof}
Indeed, $\implies$ is easy. For $\impliedby$, by non-archimedian triangle inequality:
\begin{align*}
  \|\sum_{n=N}^{m}a_n\|_v
  \leq
  \max_{N \leq n \leq M} \|a_n\|_v
\end{align*}
and if $a_n \to 0$, then $\|a_n\|_v \leq \epsilon$ for all $n$ larger than some $n_0(\epsilon)$. 
Therefore
$\|\sum_{n = N}^{M}a_n\|_v < \epsilon$
and if $N,M \geq n_0(\epsilon)$, the partial sums form a Cauchy sequence.
\end{proof}


