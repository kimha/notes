Given a valuation ring, we would like to know if it is a discrete valuation ring.
In the next section, we will extend this to characterize them among all rings.


\begin{prop}[] \label{prop:valuation-ring-ordering-dim1}
Let $(R,v)$ be a valuation ring with $\Gamma_v \neq \{0\}$. Then
\begin{enumerate}
  \item The set of ideals in $R$ is totally ordered by inclusion
  \item $\dim R = 1$.
\end{enumerate}
\end{prop}
\begin{proof}
\begin{enumerate}
  \item Let $I,J \subseteq R$ be ideals.
    We start with the following observation: If $x \in I, y \in J$ and $v(x) \leq v(y)$, then $v(\tfrac{y}{x}) \geq 0$, so
    \begin{align*}
    y = \underbrace{x}_{\in I} \cdot \underbrace{\frac{y}{x}}_{\in R} \in I
    \end{align*}
    So if $J \not\subseteq I$, then there exists a $y_0 \in J$ such that $v(y_0) < v(x)$ for every $x \in I$.
    This on the other hand implies $I \subseteq J$, hence we have a total ordering.

  \item Recall from Lemma \ref{lem:valuation-ring} that $R_v$ is local with maximal ideal $m_v = \{x \in K \big\vert v(x) > 0\} \neq \{0\}$.
    We have a chain of length $1$ given by $\{0\} \subsetneq m_v$, so $\dim(R) \geq 1$.

    For the other inequality, we show that any prime ideal contains and thus must equal $m_v$.
    
    Let $p \neq \{0\} \subseteq R$ be a prime ideal and let $x \in p - \{0\}$ and $\alpha = v(x) > 0$.
    Let $y \in m_v$. Then for some integer $n \geq 1$ large enough, we have
    \begin{align*}
      v(\tfrac{y^{n}}{x}) = n v(y) - \alpha \geq 0 \implies
      y^{n} = \underbrace{x}_{\in p} \cdot \underbrace{\frac{y^{n}}{x}}_{\in R_v}
    \end{align*}
    Since $p$ is prime, we also have that $y \in p$, hence $m_v \subseteq p$ and by maximlaity $m_v = p$.
    Thus there cannot be a longer chain, showing $\dim(R) \leq 1$.
\end{enumerate}
\end{proof}

In particular, a non-noetherian valuation ring will give an example of a non-noetherian local ring with finite dimension.

\begin{thm}[Krull's Intersection Theorem]
  \label{thm:krulls-intersection}
  Let $R$ be a noetherian local integral domain. Then
  \begin{align*}
    \bigcap_{k\geq 0}m^{k} = \{0\}
  \end{align*}
\end{thm}
The proof of this is given in the next section \ref{thm:krull-intersection}.

\begin{thm}[Matsumura 11.1]
  Let $(R,v)$ be a valuation ring with $\Gamma_v \neq \{0\}$.
  The following are equivalent:
  \begin{enumerate}
    \item $(R,v)$ is a DVR
    \item $R$ is a PID
    \item $R$ is noetherian.
  \end{enumerate}
\end{thm}
\begin{proof}
  \textbf{(a) $\bm{\implies}$ (b):} 
  Let $\pi$ be a uniformizer and $\alpha = v(\pi)$.
  Let $I \subseteq R$ be an ideal with $I \neq \{0\}$, and
  \begin{align*}
    k = \min \{v(x) \big\vert x \in I - \{0\}\}  \subseteq \alpha \Z
  \end{align*}
  We claim that $I = \pi^{k}R$ (and in particular a principal ideal).

  To show ``$\subseteq$'', let $x \in I$.
  Then $v(x) = n \alpha$ for some $n \geq k$.
  Since $v(\tfrac{x}{\pi^{k}}) = (n - k) \alpha \geq 0$ we have in particular
  \begin{align*}
    x = \pi^{k} \underbrace{\frac{x}{\pi^{k}}}_{ \in R}
    \in \pi^{k}R
  \end{align*}
  For ``$\supseteq$'', let $x_0 \in I$ such that $v(x_0) = k \alpha$. Then $v(\tfrac{\pi^{k}}{x_0}) = 0$, meaining
  \begin{align*}
    \pi^{k} = \underbrace{x_0}_{\in I} \underbrace{\frac{\pi^{k}}{x_0}}_{\in R^{\times}} \in I
  \end{align*}

  \textbf{(b) $\bm{\implies}$ (c):} 
  Every PID is Noetherian, as every ideal is finitely generated (by a single element).

  \textbf{(c) $\bm{\implies}$ (a):} 
  Assume by contraposition that $R$ is not a DVR.
  Then there is a sequence $(\alpha_n)_{n \in \N} \in \Gamma_v$ such that $0 < \alpha_{n+1} < a_n$ for all $n$.

  Let $x_n \in R$ such that $v(x_n) = \alpha_n$.
  Then $\tfrac{x_{n}}{x_{n+1}} \in R$, so $x_n = x_{n+1} \tfrac{x_n}{x_{n+1}}$ means $x_n R \subseteq x_{n+1}R$.
  And the inclusion is strict as $x_{n+1} \notin x_n R$ (or else $v(x_{n+1}) = v(x_n) + v(r) \geq v(x_n) > v(x_{n+1})$).

  Thus we get an infinite chain of increasing chain of ideals.
  \begin{align*}
    x_0R \subsetneq x_1R \subsetneq \ldots \subsetneq x_n R \subsetneq \ldots
  \end{align*}
  so $R$ is not Noetherian.
\end{proof}

The proof is complete here, but we saw a direct proof of (c) $\implies$ (b) in the lecture.\footnote{
I decided to remove one part because it is incomplete, too complicated and I haven't worked out all the details
If you wish to see it, check the commit history on the Gitlab page.
}
Let $I \subseteq R$ be an ideal. By assumption, $I$ is finitely generated, so
\begin{align*}
  I = a_1R + \ldots + a_k R
\end{align*}
by the previous proposition, the ideals are totally ordered so either $a_1R \subseteq a_2R$ or $a_2R \subseteq a_1R$, so without loss of generality, $a_1 R + a_2 R = a_2'R$.
Then $I = a_{2}'R + \ldots a_kR$ and by induction, $I = a_k'R$ is principal.

