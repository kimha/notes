From Algebra I, we know that if $R$ is an integral domain, we can form a field of fractions with elements of the form $\frac{a}{b}$ with $a,b \in R, b \neq 0$.

We can generalize this definition by restricting what sort of elements can appear in the denominator.
However, if we want to multiply fractions $\frac{a}{b} \cdot \frac{c}{d}$, we need the element $bd$ to be eligible as a denominator.

\begin{dfn}[]
Let $R$ be a ring. A subset $S \subseteq R$ is called \textbf{multiplicative}, if $1 \in S$ and $a,b \in S \implies ab \in S$.
\end{dfn}

\begin{xmp}[]
\begin{enumerate}
  \item For $R$ an integral domain, $S = R - \{0\}$ is multiplicative.
  \item For a ring $R$ with $P \subseteq R$ a prime ideal, then $S = R - P = P^{c}$ is multiplicative.
  \item Let $a \in R$, then $\{a^{n} \big\vert n \geq 0\}$ is multiplicative.
  \item Let $R_1 \stackrel{f}{\to}R_2$ to be a morphism of rings.
    \begin{itemize}
      \item If $S_1 \subseteq R_1$ is multiplicative, then $f(S_1) \subseteq R_2$ is multiplicative.
      \item If $S_2 \subseteq R_2$ is multiplicative, then $f^{-1}(S_2) \subseteq R_1$ is multiplicative.
    \end{itemize}
  \item $R^{\times} \subseteq R$ is multiplicative.
\end{enumerate}
Note, the constructions (b) and (c) are very different. Consider $R = \Z$ and $p = 2\Z$ and $a = 2$.
Construction (b) gives the odd integers, whereas (c) gives $S = \{1,2,4,8,\ldots\}$.
\end{xmp}

Our goal is for $S \subseteq R$ multiplicative, we want to construct a ring $S^{-1}R$, where the elements of $S$ become invertible, which we will calll \textbf{$R$ localized at $S$}.

\begin{thm}[] \label{thm:localisation}
Let $S \subseteq R$ be multiplicative. There exists an $R$-algebra
\begin{align*}
  \phi_S: R \to S^{-1}R
\end{align*}
such that for any $R$-algebra $A$, there exists a bijection
\begin{align*}
  \Hom_{R\Alg}(S^{-1}R,A) &\stackrel{\sim}{\to} \{g \in \Hom_{R\Alg}(R,A) \big\vert g(S) \subseteq A^{\times}\}\\
  f &\mapsto f \circ \phi_S
\end{align*}
\begin{center}
\begin{tikzcd}[] 
  R
  \arrow[]{r}{\phi_S}
  \arrow[swap]{dr}{f \circ \phi_S}
  &
  S^{-1}R
  \arrow[]{d}{f}
  \\
  & A
\end{tikzcd}
\end{center}
\end{thm}

\begin{xmp}[]
  For $R$ an integral domain, $S = R - \{0\}$, the localisation $S^{-1}R$ is isomorphic to the fraction field $\text{Quot}(R)$.

  Given a morphism $f: R \to A$ with $f(r) \in A^{\times}$ if $r \neq 0$, we can extend $f$ to $\text{Quot}(R)$ by defining $f(\frac{s}{r}) = \frac{f(s)}{f(r)}$.

\end{xmp}
\begin{rem}[]
  Since we have identified $\Hom_{R\Alg}(S^{-1}R,-)$, the ring $S^{-1}R$ is unique up to isomorphism by the Yoneda lemma (see \ref{cor:iso-with-hom}).

  A less ``abstract nonsense'' proof of uniqueness can be obtained by using the same trick in the Yoneda Lemma.
  For any other $\psi: R \to T$ with the same property, we plug in $A = S^{-1}R$ and $f = \id_{S^{-1}R}$ and in the end, we obtain an isomorphism $T \iso S^{-1}R$.
\end{rem}
\begin{proof}[Proof Theorem]
  We construct the algebra $S^{-1}R$ similar to how we construct $\text{Quot}(R)$, but by only allowing elements of $S$ to be in the denominator.
  However this restriction introduces a subtlety on when two fractions are considered equivalent.

  Let $X = R \times S$. We define
  \begin{align*}
    \tilde{+}: \quad
    &\left\{\begin{array}{lll}
        X \times X & \to & X\\
        ((r_1,s_1),(r_2,s_2)) & \mapsto & (r_1s_2 + r_2s_1, s_1s_2)
    \end{array} \right.
    \\
    \tilde{\cdot}: \quad
    &\left\{\begin{array}{lll}
        X \times X & \to & X\\
        ((r_1,s_1),(r_2,s_2)) & \mapsto & (r_1r_2, s_1s_2)
    \end{array} \right.
    \\
    \tilde{\phi_S} &: R \to X, \quad r \mapsto  (r,1_R)
  \end{align*}
  Next, we define the quivalence relation as follows
  \begin{align*}
    (r_1,s_1) \sim (r_2,s_2) \iff \exists t \in S: \quad t (r_1s_2 - r_2s_1) = 0
  \end{align*}
  The $\exists t$ clause is only necessary if $S$ has zero divisors in order to ensure that it does indeed form an equivalence relation.
  If $S$ is an integral domain, this definition is equivalent to $r_1s_2 - r_2s_1 = 0$.

  \begin{enumerate}
    \item $\sim$ forms an equivalence relation and we set $S^{-1}R = X/\sim$.
    \item We can inherit addition and multiplication from $X$.
      Since $X$ is not a ring, we have to check that it is well defined on $S^{-1}R$.
    \item The mapping $R \stackrel{\tilde{\phi_S}}{\to}X \stackrel{\iota}{\to} S^{-1}R$ defines a ring morphism $\phi_S: R \to S^{-1}R$.
  \end{enumerate}
  \begin{center}
  Missing rest of proof. Just check the above claims.

  Then check that the Hom-functor does indeed follow the characterisation
  \end{center}
\end{proof}




\begin{rem}[]
$\phi_S$ is not always injective.
\begin{align*}
  \Ker \phi_S = \left\{r \in R \big\vert \frac{r}{1} = \frac{0}{1} \text{ in }S^{-1}R\right\} = \left\{r \in R \big\vert \exists t\in S \big\vert t(r-0) = rt = 0\right\}
\end{align*}
\begin{itemize}
  \item If $S$ has no zero-divisors, then $\phi_S$ is injective.
  \item If $0 \in S$ or if $S$ contains a nilpotent element, then $S^{-1}R$ is the zero ring $\{0\}$.
\end{itemize}
\end{rem}

\begin{xmp}[]
  Set $R = \Z$
\begin{itemize}
  \item For $S = \Z - \{0\}$, $S^{-1}R = \Q$
  \item If $S = \Z - p\Z$, then
    $
      S^{-1}R = \{\frac{a}{b} \in \Q \big\vert p \not| b\}
    $
  \item $S = \{1,a,a^{2},\ldots\} \implies S^{-1}R = \{\frac{x}{y} \in \Q \big\vert y = a^{n}\}$.
\end{itemize}
\end{xmp}

\textbf{Notation}: For $R$ a ring
\begin{enumerate}
  \item If $a \in R$, write $R_a = \{a^{n}\}^{-1}R$
  \item For $p \subseteq R$ a prime ideal, write $R_p = (R - p)^{-1}R$.
\end{enumerate}


\begin{prop}[ACL 2.2.7]\label{prop:prime-ideals-localisation}
  Let $S \subseteq R$ multiplicative. The map
  $q \mapsto  \phi_S^{-1}(q)$
  gives an order preserving bijection
  \begin{align*}
    \{\text{prime ideals } q \subseteq S^{-1}R\} \to \{\text{prime ideals }p \subseteq R \text{ with } p \cap S = \emptyset\}
  \end{align*}
\end{prop}
\begin{proof}
In fact, we will provide the inverse map
\begin{align*}
  p \mapsto \text{ideal generated by } \phi_S(p)
\end{align*}
First, we check that the first map is well defined, so if $q \subseteq S^{-1}R$ is a prime ideal, then $\phi_S^{-1}(q)$ should be a prime ideal and doesn't intersect $S$.


It's easy to check that $\phi_S^{-1}(q)$ is prime and if $x \in \phi_S^{-1}(q) \cap S$, then
\begin{align*}
  \phi_S(x) \in q \cap (S^{-1}R)^{\times} \implies q \text{ contains a unit } \implies q = R \lightning
\end{align*}
Now let $p \subseteq R$ prime with $p \cap S = \emptyset$. The ideal generated by $\phi_S(p)$ is
\begin{align*}
  q = \left\{\frac{r}{s} \big\vert r \in p, s \in S\right\}
\end{align*}
this is indeed a prime ideal because 
\begin{align*}
  \frac{r_1}{s_1} + \frac{r_2}{s_2} = \frac{r_1s_2 + r_2s_1}{s_1s_2}
\end{align*}
where the numerator is clearly in $p$ and the denominator is an element of $S$.
Now we check that $\phi_S^{-1}(q) = p$.

The inclusion $\subseteq$ is trivial by definition. Conversely, if $r \in \phi_S^{-1}(q)$, then $\frac{r}{1} = \frac{r_1}{s_1}$ for some $r_1 \in p$. So there exists a $t \in S$ such that
\begin{align*}
  t(r s_1 - r_1) = 0 \implies \underbrace{ts_1}_{\notin p} \implies r \in p
\end{align*}
where we used that $p \cap S = \emptyset$ and $p$ is a prime ideal.

Now we check that the inverse map is well-defined, i.e. that $q$ is prime in $S^{-1}R$.

Suppose $\frac{r_1}{s_1} \frac{r_2}{s_2} \in q$. Then
\begin{align*}
  \exists r \in p, \exists s \in S: \quad \frac{r_1r-2}{s_1s_2} \in \frac{r}{s}
\end{align*}
so there exists a $t \in S$ such that
\begin{align*}
  t (s r_1r_2 r s_1s_2) 0 \implies \underbrace{ts}_{\notin p}r_1r_2 \in p \implies r_1r_2 \in p
\end{align*}
so either $r_1 \in p$ or $r_2 \in p$. By this either $\frac{r_1}{s_1} \in q$ or $\frac{r_2}{s_2} \in q$.


Now we check that the two maps are inverses:
\begin{enumerate}
  \item Start with $p \subseteq R$, then construct $q$. By showing both sides of the inclusion, we already showed $p = \phi_S^{-1}(q)$.
  \item Starting with $q$, let $p = \phi_S^{-1}(q) \subseteq R$ and let $q_1$ be the ideal generated by $\phi_S(p)$.
    Then $\phi_S(p) \subseteq q \implies q_1 \subseteq q$.
    Conversely, let $\frac{r}{s} \in q$. Then
    \begin{align*}
      \phi_S(r) = \frac{r}{s} = \frac{s}{1} \frac{r}{s}  \in Q \implies r \in p
    \end{align*}
    which shows that
    \begin{align*}
      \frac{r}{s} = \frac{r}{1} \cdot \frac{1}{s} = \phi_S(p) \frac{1}{S} \in q_1
    \end{align*}
\end{enumerate}
\end{proof}

\begin{rem}[]
The bijections respect inclusion, but are not maximal ideals in general.

A counterexample would be the inclusion
\begin{align*}
  \Z &\hookrightarrow \Q\\
  \{0\} &\mapsto \{0\}
\end{align*}
\end{rem}


\begin{xmp}[]
\begin{itemize}
  \item For $a \in R$, the prime ideals in $R_a$ correspond to the prime ideals of $R$ that do not contain $a$.
\end{itemize}
\end{xmp}


\begin{prop}[]
Let $R$ be a ring. Then
\begin{align*}
  \{\text{ nilpotent elements in }R\} = \bigcap_{p \subseteq R \text{ prime }} p
\end{align*}
This set is called the \textbf{nilradical} of $R$.
\end{prop}
\begin{proof}
  Let $a \in R$ nilpotent and $p \subseteq$ be a prime ideal. So $\exists n \in \N: a^{n} = 0 \in p$.
  Since $p$ is prime, $a \in p$.
  This shows ``$\subseteq$''.

  Conversely, let $a \in R$ be not nilpotent. 
  We find a prime ideal $p$ such that $a \notin p$.
  By the previous example, this corresponds to finding a prime ideal $q$ in $R_a = \{a^{n}\}^{-1}R$.

  This exists because as $a$ is not nilpotent, $0 \notin S = \{a^{n}\}$ and therefore $R_a = \{a^{n}\}^{-1}R \neq \emptyset$ 
\end{proof}

\begin{cor}[]
  Let $q \subseteq R$ be a prime ideal and let $S = R - q$.

  Then there exists a bijection
  \begin{align*}
    \{ \text{prime ideals in }R_q\} \stackrel{\sim}{\to} \{\text{prime ideals }p \subseteq R \text{ with }p \cap S = \emptyset \}
  \end{align*}
  Note that $p \cap S = p \cap (R - q)$ so $p \cap S = \emptyset \iff p \subseteq q$
\end{cor}
So when we want to find out things about subsets of prime ideals $q$, we can just look at all prime ideals of $(R - q)^{-1}R$

In particular, the maximal ideals in $R_q$ correspond to the prime ideals $p \subseteq R$ that are maximal under those that are contained in $q$.
But the only such ideal is $q$ itself, so $R_q$ has the property of having a unique maximal ideal.

\begin{dfn}[]
A ring $R$ is called a \textbf{local ring} if $R$ has a unique maximal ideal $m_R \subseteq R$.

We know from Algebra I that this means $R/m_R$ is a field, which we call the \textbf{residue field} of the local ring.
\end{dfn}

\begin{xmp}[]
  \begin{itemize}
    \item For $q \subseteq R$ prime, the localisation $R_q$ is a local ring.
    \item Every field is a local ring, with $m_R = \{0\}$.
  \end{itemize}
\end{xmp}

Localisation is a general stategy when solving problem in commutative algebra.
Say we want to find solutions to diophantine equations.
We solve these equations by ``reducing mod $p$'' for multiple $p$ and try to stitch this knowledge together to find solutions in general rings.

You might wonder is ``local'' local rings.

\begin{rem}[]
  The word ``local ring'' comes from algebraic geometry motivated by the following examples.

  \begin{itemize}
    \item Let $R = \{\left(a_{n}\right)_{n \in \N} \big\vert a_n \in \C \text{ such that } \sum_{n=0}^{\infty}a_n z^{n} \text{ has radius of convergence } > 0\}$.

      This does indeed form a ring with the usual sum and the product induced by the formal multiplication of the power series (Cauchy product).

      Moreover this ring natural ring to look at if you are interested in holomorphic functions in the neighborhood of $0 \in \C$.

      If you wanted to study local properties of holomorphic functions at a point $a \in \C$, you should be studying power series $\sum_{n\in \N}a_n (z - a)^{n}$ instead.

      Furthermore, this ring is a local ring.
      Let $m \subseteq R$ be the set
      \begin{align*}
        \{\left(a_{n}\right)_{n \in \N} \big\vert a_0 = 0\}
      \end{align*}
      that consists of holomorphic functions that are zero at $z = 0$.

      $m$ is an ideal because it is the kernel of the ring morphism
      \begin{align*}
        R &\to \C\\
        \left(a_{n}\right)_{n \in \N} &\mapsto  f(0)
      \end{align*}
      Since this mapping is surjective, we have that $\faktor{R}{m} \iso \C$ is a field $\implies$ $m$ is maximal.

      For uniqueness, it is enough to show that if $a = \left(a_{n}\right)_{n \in \N} \in R - m$, then $a$ is invertible, because from this follows that any ideal not contained in $m$ contains units and thus is equal to $R$.

      To see this, we can use the fact that if $f(0) \neq 0$, then $\frac{1}{f}$ is holomorphic in some small region around $0$.


    \item 
      Consider the ring $\R[X]$.
      If we pick a point $a \in \R$ and add an inverse to the element $(X -a) \in \R[X]$, then the localisation consists of rational functions that are defined everywhere, \emph{except possibly} at the point $a$.
      This is called \textbf{localisation away from} $a$, or localisation away from the ideal $I$ generated by $(X-a)$.

      If we put an inverse to every element of $\R[X]$ that is \emph{not} in $I$, i.e. set $S = \R[X] - I$, we obtain the ring of rational functions that are defined \emph{at least} at the point $a$.
      We call this \textbf{localisation at $a$}, or localisation at the ideal $I$.
  \end{itemize}
\end{rem}

