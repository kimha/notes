We can now prove key properties of height and dimension for finitely-geneerated $K$-algebras.

\begin{thm}[]
  \begin{enumerate}
    \item For all $n \geq 0$, we have
      \begin{align*}
        \dim K[X_{1}, \ldots, X_{n}] = n
      \end{align*}

    \item If $R$ is a finitely-generated $K$-algebra and is an integral domain, then
      \begin{align*}
        \dim R = \text{trdeg}_K(\text{Quot}(R))
      \end{align*}

    \item If $R$ is a finitely-generated $K$-algebra and is an integral domain, then
      \begin{align*}
        \hht p + \dim R/p = \dim R
      \end{align*}
      for any prime ideal $p \subseteq R$.
  \end{enumerate}
\end{thm}
\begin{proof}
  Note that (b) implies (a) by taking $R = K[X_{1}, \ldots, X_{n}]$.

  We prove (b) by induction on $t = \text{trdeg}_K(\text{Quot}(R))$.
  \begin{enumerate}
    \item[(b)] If $t = 0$, then $\text{Quot}(R)$ is a finite algebraic extension of $R$, so $R$ is a field and has dimension $0$.

      Now let $t \geq 1$ and assume the result for smaller transcendence degree.
      We already proved the inequality ``$\geq$'' in Chapter \ref{sec:dimension}
      By Noether's Normalisation theorem, we get get an integral morphism $\phi$
      \begin{center}
      \begin{tikzcd}[column sep = 0.8em] 
        K \arrow[]{rr}{} \arrow[]{dr}{}
        && R\\
        & K[X_{1}, \ldots, X_{n}] \arrow[]{ur}{\phi}
      \end{tikzcd}
      \end{center}
      So $\dim R = \dim K[X_{1}, \ldots, X_{n}] \geq n$.
      
      Note that we have
      \begin{align*}
        \text{trdeg}_K(\text{Quot}(R)) = \text{trdeg}_K(K(X_{1}, \ldots, X_{n})) = n
      \end{align*}
      let $\{0\} = p_0 \subsetneq p_1 \subsetneq \ldots \subsetneq p_m$ be a chain in $K[X_{1}, \ldots, X_{n}]$ and let
      \begin{align*}
        A = \faktor{K[X_{1}, \ldots, X_{n}]}{p_1}
      \end{align*}
      then we have a chain of length $m-1$ in $A$ by reducing mod $p_1$.
      On the other hand, take $f \neq 0$ in $p_1$. Then $f(X_{1}, \ldots, X_{n}) = 0$ in $A$, i.e. the classes of $X_{1}, \ldots, X_{n}$ are algebraically dependent in $L = \text{Quot}(A)$.

      Since these classes generate $L$ over $K$, we must have $\text{trdeg}_K(L) \leq n -1$.
      Hence by induction, me have $\dim(A) \leq n-1$, so $m-1 \leq n-1$.
      Which shows the other inequality $\dim R \leq n$.

    \item[(c)] We proceed by induction on $\dim R$.

      If $\dim R = 0$, then $R$ is a field, so $p = (0)$ is the only prime ideal and $\hht(p) = \dim R/P = 0$.

      Suppose now that $\dim R \geq 1$ and that the statement holds for $K$-algebras of dimesion $< \dim R$.

      Applying Noether's Theoren again, we get an integral morphism $\phi: K[X_{1}, \ldots, X_{n}] \to R$, so $n =\dim R$.

      By the ``going-down'' theorem, we also have $\hht_R(p) = \hht_A \phi^{-1}(p)$, where $A = K [X_{1}, \ldots, X_{n}]$.

      Moreover, the morphism $\faktor{A}{\phi^{-1}(p)} \to  R/p$ is also integral and we have $\dim R/p = \dim A/\phi^{-1}(p)$.

      This means that we can reduce to the case where $R = A = K[X_{1}, \ldots, X_{n}]$.

      \textbf{Case $\bm{p = \{0\}}$:} Then $\hht p = 0$ and the equality holds.
      
      \textbf{Case $\bm{p \neq \{0\}}$:} Then we can find $f \in p$ irreducible.
      By Example \ref{xmp:ufd-principal-height}, 
      the principal ideal $fA$ has height $1$, because $A$ is a UFD.

      Then define
      \begin{align*}
        B = A/fA, \quad p' = p/fA \subseteq B
      \end{align*}
      so that $p'$ is a prime ideal in $B$.
      By the Lemma below, we also have
      \begin{align*}
        \text{trdeg}_K B = \text{trdeg}_K A - 1 = n-1
      \end{align*}
      moreover, be the third isomorphism theorem:
      \begin{align*}
        B/p' = \faktor{A/fA}{p/fA} \iso A/p \implies \dim B/p' = \dim A/p
      \end{align*}
      finally, $\hht_B(p') \leq \hht_A(p) - 1$.
      Using the induction hypothesis, we know that
      \begin{align*}
        \hht_B(p') + \dim (B/p') = \dim B
      \end{align*}
      so we get
      \begin{align*}
        \hht_A(p) + \dim(A/p) 
        &\geq 1 + \hht_B(p') + \dim(B/p')\\
        &= 1 + \dim(B)\\
        &= 1 + \text{trdeg}_K(B)\\
        &= 1 + n - 1 = \dim A
      \end{align*}
  \end{enumerate}

\end{proof}

In the proof, we used the following Lemma:
\begin{lem}[]
Let $n \geq 1$ and $f$ irreducible in $K[X_{1}, \ldots, X_{n}]$.
Then
\begin{align*}
  \text{trdeg}_K(\text{Quot}(K[X_{1}, \ldots, X_{n}]/(f))) = n-1
\end{align*}
\end{lem}
\begin{proof}
  SInce $f$ is irreducible, it is not constant, so by permuting the variables, we can assume that $X_n$ occurs in $f$ with degree $\geq 1$.

  Then we claim that the classes $Y_{1}, \ldots, Y_{n-1}$ of the variables $X_{1}, \ldots, X_{n-1}$ in $K[X_{1}, \ldots, X_{n}]/(f)$ form a transcendence basis of the fraction field $L$.

  This proves the lemma.
  Indeed, the equation $f(Y_{1}, \ldots, Y_{n}) = 0$ shows that the $(Y_i)_{1 \leq i \leq n}$ are algebraically dependent.
  Since $Y_{1}, \ldots, Y_{n}$ generate $K[X_{1}, \ldots, X_{n}]/(f)$ as a $K$-algebra, this implies that $L$ is algebraic over $K(Y_{1}, \ldots, Y_{n-1})$. 
  Moreover, we claim that the morphism
  \begin{align*}
    K[X_{1}, \ldots, X_{n-1}] \to  L, \quad X_i \mapsto  Y_i
  \end{align*}
  is injective.
  If $g$ is in the kernel, then $g(Y_{1}, \ldots, Y_{n-1}) = 0$ means that $g \in K[X_{1}, \ldots, X_{n-1}]$ is a multiple of $f$. But since $\deg_{X_n}f \geq 1$, this is only possible if $g = 0$.

  So $(Y_{1}, \ldots, Y_{n-1})$ are algebraically independent over $K$ and therefore form a transcendence basis.

\end{proof}
