As most of the proofs are not relevant to the rest of the lecture, we will omit them and focus on the results.

\begin{dfn}[]
  Let $L/K$ be a field extension and $(x_i)_{i \in I}$ a family of elements of $L$.
  If either of the following conditions hold,  one says that $(x_i)_{i \in I} \subseteq L$ is \textbf{algebraically independent} over $K$.
  \begin{enumerate}
    \item The morphism
      \begin{align*}
        K[(X_i)_{i \in I}] \to L, \quad f \mapsto f((x_i)_{i \in I})
      \end{align*}
      is injective
    \item There exists a field morphism
      \begin{align*}
        K((X_i)_{i \in I}) = \text{Quot}(K[(X_i)_{i \in I}]) \stackrel{\phi}{\to}L
      \end{align*}
      such that $\phi(X_i) = x_i$, for all $i$
  \end{enumerate}
  An $x \in L$ is \textbf{transcendental}, if it is algebraically independent (as a single element)
\end{dfn}

Property (a) is equivalent to
\begin{align*}
  f \in K[(X_i)_{i \in I}], f \neq 0 \implies f((x_i)_{i \in I}) \neq 0
\end{align*}
which is easier to show.


\begin{xmp}[]
  $\pi$ in the extension $\C/\Q$ is transcendental and so is $\pi^{2}$, but $(\pi,\pi^{2})$ is not algebraically independent. (Take $f = X_2 - X_1^{2}$.

  If $(x_i)_{i \in I}$ are algebraically independent, then the image of the morphism $\pi$ is denoted as $K\left(
    (x_i)_{i \in I})
  \right)$

  This corresponds to the field generated b the $x_i$ in $L$ and is isomorphic to $K((X_i)_{i \in I})$.

\end{xmp}


\begin{dfn}[Transcendence basis]
  If $L/K$ is a field extension, a family $(x_i)_{i \in I}$ in $L$ is a transcendence basis of $L$ over $K$, if both:
  \begin{itemize}
    \item $(x_i)_{i \in I}$ are algebraically independent over $K$
    \item $L/K((x_i)_{i\in I})$ is an algebraic extension of $K$.
  \end{itemize}
\end{dfn}

\begin{thm}[ACL 4.8.4, 4.8.5]
  Let $L/K$ be a field extension.
  \begin{enumerate}
    \item There exists a transcendence basis of $L/K$.
    \item If $B \subseteq L$ is algebraically independent and $B \subseteq A$ such that $L/K((a)_{a \in A})$ is algebraic, 
      then there exists a $B \subseteq C \subseteq A$ such that $C$ is a transcendence basis of $L/K$.
  \end{enumerate}
\end{thm}


\begin{table}[h]
\centering
\begin{tabular}{l|l}
  $V$ is a vector space over $K$
  &
  $L/K$ is a field extension.
  \\
  \hline
  Linear independence
  &
  algebraic independence
  \\
  \hline
  Generating set
  &
  $(x_i)_{i \in I}$ such that $L/K((x_i)_{i \in I})$ is algebraic
  \\
  \hline
  Basis
  &
  Transcendence basis.
  \\
  \hline
  If $B \subseteq A \subseteq V$ and 
  \\$B$ linearly independent,
  $A$ is generating, then 
  &
  The previous theorem
  \\
  $A$ contains a basis of $V$ containing $B$.\\
  \hline
  All basis have the same size
  &
  The next theorem.
\end{tabular}
\caption{Analogy between Linear Algebra and Field Extensions}
\end{table}



\begin{thm}[]
  Let $L/K$ a field extension and $(x_i)_{i \in I}$, $(x_j)_{j \in J}$ are transcendence basis, then $I,J$ are bijective, i.e. have the same cardinality.
\end{thm}

\begin{dfn}[]
  This common cardinal is called the \textbf{transcendence degree} of $L$ over $K$, denoted by $\text{trdeg}_K(L) = \text{trdeg}(L/K)$.
\end{dfn}


\begin{xmp}[]
\begin{itemize}
  \item $\text{trdeg}(L/K) = 0 \implies L/K$ is algebraic.
  \item $\text{trdeg}(K(x)/K)$ for $x \in L$ is either $0$ or $1$, depending on wheter $x$ is algebraic over $K$ or not.

    In particular, if $(x_i)_i$ are algebraically independent, then they are each transcendental over $K$.
    But converse is not true, as $(\pi,\pi^{2})$ is not independent.
  \item Given any cardinal, we can construct a field extension of that transcendence degree. In particular:
    \begin{align*}
      \text{trdeg}(K((X_i)_{i \in I})/K) = \text{card}(I)
    \end{align*}
    extensions isomorphic to such fields are called \textbf{purely transcendental.}
  \item Let
    \begin{align*}
      L = \text{Quot}\left(
        \faktor{\C[X,Y]}{(Y^{2} - X^{3} - X)}
      \right)
    \end{align*}
    Note that $Y^{2} - X^{3} -X$ is irreducible.
    Then $L$ has transcendence degree $1$ over $\C$ with the equivalence class of $X$ a transcendence basis.

    Clearly, $L$ is algebraic over $\C(X)$ because $Y^{2} = X^{3} + X$.
    This gives us $\C \stackrel{\text{transc.}}{\subset} \C(X) \stackrel{\text{alg.}}{\subseteq} L$.
    Which shows $\text{trdeg}(L/\C) \leq 1$.
    And the mapping
    \begin{align*}
      \C[X] \to L, \quad f \mapsto  f(X)
    \end{align*}
    is injective.

    This is therefore an example of a non-algebraic extension that is not purely transcendental.
  \item From the two theorems before, it follows that if $L_1,L_2$ are fields of characteristic $0$ with the same transcendence degree over $\Q$ and are algebraically closed, then they are isomorphic.

    Indeed, as they have the same $\text{trdeg}$, we have some $I$ with that size and
    \begin{center}
    \begin{tikzcd}[column sep=0.8em] 
      L_1 & & L_2\\
          & \Q((X_i)_{i \in I})
          \arrow[swap]{ul}{\text{alg.}}
          \arrow[]{ur}{\text{alg.}}
      \\
          & \Q
          \arrow[]{u}{\text{trans.}}
    \end{tikzcd}
    \end{center}
    so both $L_1,L_2$ are isomorphic to an algebraic closure of the intermediary field.

    A really cool application of this is that the fields $L_1 = \C, L_2 = \overline{\Q_p}$\footnote[1]{Here, $\Q_p$ denotes the \href{https://en.wikipedia.org/wiki/P-adic_number}{$p$-adic numbers.}} for $p$ prime, are isomorphic.
  
\end{itemize}

\end{xmp}

