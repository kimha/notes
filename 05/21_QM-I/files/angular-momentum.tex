\section{Angular Momentum}

In many cases, a quantum system consists of multiple parts.
If we understand the state space of each part, then we can understand the state space of all parts by their tensor product.

Indeed, if we have a system with $10$ possible states and add another system with $10$ possible states, then as the state of the first system is independent of the state of the second one, there are $10 \cdot 10$ possible states.

But in quantum mechanics, we consider linear combinations of such states. So if $\mathcal{H}_1$ is a Hilbert spaces with basis $\psi_1,\ldots,\psi_n$ (i.e.\ $\mathcal{H}_1$ has dimension $n$) and $\mathcal{H}_2$ is a Hilbert space with basis $\phi_1,\ldots,\phi_m$, then the Hilbert spaces of the combined states must have dimension $n \cdot m$.
The tensor product $\mathcal{H}_1 \otimes \mathcal{H}_2$ has such dimension and a basis of it is given by $(\psi_i \otimes \phi_j)_{ij}$.

One such example was the Hilbert space of the two-body-problem, where $L^{2}(\R^{3}) \otimes L^{2}(\R^{3}) = L^{2}(\R^{6})$.

The structure of a Hilbert space of the tensor product space is given by its inner product, which (on pure tensors) is defined as
\begin{align*}
  \braket{v_1 \otimes w_1|v_2 \otimes w_2} = \braket{v_1|v_2} \cdot \braket{w_1|w_2}
\end{align*}
that can be linearly extended to general (i.e.\ non-pure) tensors.


\subsection{Addition of angular momentum}

Now consider the case where both Hilbert spaces have a (projective) representation of $\text{SO}(3)$. 
We saw in the two-body-problem, that the Hamiltonian is no longer invariant under all of $\text{SO}(3)$.

In this case, the Hamiltonian only commutes with the infinitesimal generators of the rotations: the Lie Algebra $\mathfrak{so}(3)$.

So assume that the spaces $\mathcal{H}^{(1)}, \mathcal{H}^{(2)}$ have a representation of $\text{SU}(2)$.
So for each $g \in \text{SU}(2)$, there exists a linear operator
\begin{align*}
  U^{(i)}(g) : \mathcal{H}^{(i)} \to  \mathcal{H}^{(i)}
\end{align*}
This means that on the tensor product, we have a representation of the product group $\text{SU}(2) \times \text{SU}(2)$ given by
\begin{align*}
  (g,h) \mapsto U^{(1)}(g) \otimes U^{(2)}(h): \quad (\psi_1 \otimes \psi_2) \mapsto U^{(1)}(g) \psi_1 \otimes U^{(2)}(h) \psi_2
\end{align*}
which can be linearly extended to non-pure tensors.

We first consider the rotations common to both systems, which are exactly the representations of $\text{SU}(2)$ given by
\begin{align*}
  g \mapsto  U^{(1)}(g) \otimes U^{(2)}(g)
\end{align*}
the resepective infinitesimal generators are defined as before:
\begin{align*}
  \Omega = \frac{d }{d t}g(t)|_{t=0}
\end{align*}
On the tensor product, it follows from the Leibnitz rule for derivatives that
\begin{align*}
  (U^{(1)} \otimes U^{(2)})(\Omega) = \ldots = U^{(1)} \otimes \id + \id \otimes U^{(2)}(\Omega)
\end{align*}
so the Lie algebra acts by adding the effects of each component.
