\subsection{$\text{SO}(3)$ and $\text{SU}(2)$}

The spherical harmonics describe the representations of $\mathfrak{so}(3)$ for which $j$ is an integer.
These are exactly the Lie Algebra representations $\rho_{\ast}$ that correspond to Lie Group representations $\rho$ of $\text{SO}(3)$.

The non-integer representations cannot be expressed in such a way, but can be in terms of the universal covering group $\text{SU}(2)$ of $\text{SO}(3)$.


We can write any element of $\text{SU}(2)$ as
\begin{align*}
  A = \begin{pmatrix}
  \alpha & \beta\\
  -\beta^{\ast} & \alpha^{\ast}
  \end{pmatrix}
  \quad \text{where} \quad 
  \abs{\alpha}^{2} + \abs{\beta}^{2} = 1
\end{align*}
One can show that $\mathfrak{su}(2) \iso \mathfrak{so}(3)$, since the former consists of trace-less anti-hermitian complex $2 \times 2$ matrices and the latter of trace-less anti-symmetric real $3 \times 3$ matrices.

Elements of $\mathfrak{su}(2)$ can be described using the \textbf{Pauli matrices}
\begin{align*}
  \sigma_1 = \begin{pmatrix}
  0 & 1\\
  1 & 0
  \end{pmatrix}
  , \quad
  \sigma_2 = \begin{pmatrix}
  0 & -i\\
  i & 0
  \end{pmatrix}
  , \quad
  \sigma_3 = \begin{pmatrix}
  1 & 0\\
  0 & -1
  \end{pmatrix}
\end{align*}
Every $A \in \mathfrak{su}(2)$ can be thus written as
\begin{align*}
  A = - \frac{i}{2}
  \begin{pmatrix}
  a_3 & a_1 - ia_2\\
  a_1 + ia_2 & -a_3
  \end{pmatrix}
  = - \frac{i}{2}
  \vec{\sigma} \cdot \vec{a}
\end{align*}
Thus $\mathfrak{su}(2)$ has (real) dimension $3$ and a natural basis is

\begin{align*}
  A_j := - \frac{i}{2} \sigma_j, \quad j = 1,2,3
\end{align*}
The pauli matrices satisfy
\begin{align*}
  \sigma_i \sigma_j = \delta_{ij} + i \epsilon_{ijk} \sigma_k
\end{align*}
where $\epsilon_{ijk}$ is the totally anti-symmetric tensor. In vector notation, one has
\begin{align*}
  (\vec{\sigma} \cdot \vec{a})(\vec{\sigma} \cdot \vec{b}) = (\vec{a} \cdot \vec{b})1 + i \vec{\sigma} \cdot (\vec{a} \wedge \vec{b})
\end{align*}
Therefore
\begin{align*}
  [A(\vec{a}),A(\vec{b})] = A(\vec{a} \wedge \vec{b})
  \quad \text{and} \quad [A_1,A_2] = A_3 \text{ (and cyclic)}
\end{align*}
This gives us an isomorphism of Lie-Algebras
\begin{align*}
  \mathfrak{su}(2) \to \mathfrak{so}(3), \quad A(\vec{\omega}) \mapsto  \Omega(\vec{\omega})
\end{align*}
Although the Lie-Algebras of $\text{SU}(2)$ and $\text{SO}(3)$ are the same, the groups are different.
For each $\vec{x} \in \R^{3}$, define
\begin{align*}
  \tilde{x} := \sum_{j=1}^{3}x^{j}\sigma_j = \begin{pmatrix}
  x^{3} & x^{1} - ix^{2}\\
  x^{1} + ix^{2} & -x^{3}
  \end{pmatrix}
\end{align*}
This map is invertible since
\begin{align*}
  x^{j} = \frac{1}{2} \trace(\tilde{x} \sigma_j)
\end{align*}
If $\vec{x}$ is real, then $\tilde{x}$ is hermitian and vice versa.

Moreover, we have
\begin{align*}
  \det \tilde{x} = - \vec{x} \cdot \vec{x}
\end{align*}
For every $A \in \text{SU}(2)$, we can consider the map
\begin{align*}
  \tilde{x} \mapsto \tilde{x}' = A \tilde{x}A^{\dagger}
\end{align*}
this is a linear map that that maps real vectors to real vectors.
By the determinant property, it also preserves the length of $\vec{x}$ and thus represents a rotation.
This construction defines a group homormorphism
\begin{align*}
  \text{SU}(2) \to \text{SO}(3)
\end{align*}
It's easy to see that this morphism has kernel $\pm 1 \in \text{SU}(2)$.
Moreover, it is surjective and continuous and we thus see that this gives us a double covering, aswell as an isomorphism
\begin{align*}
  \faktor{\text{SU}(2)}{\{\pm 1\}} \iso \text{SO}(3)
\end{align*}
A simple example of a representation of $\text{SU}(2)$ that does not constitute a representation of $\text{SO}(3)$ is that of the eigen-space with non-integer eigenvalue $\mathcal{D}_{\tfrac{1}{2}}$.

Clearly, the matrix $-1$ does not act trivially and thus does not factor through $\text{SO}(3)$.

Consider the rotation
\begin{align*}
  R(\vec{e}_3,\phi) = \exp\left(
    \begin{pmatrix}
    0 & -\phi & 0\\
    \phi & 0 & 0\\
    0 & 0 & 0
    \end{pmatrix}
  \right)
  = e^{-i M_3 \phi}
\end{align*}
In particular, on the vector $\ket{j,m}$ acts by
\begin{align*}
  \rho(R(\vec{e}_3,2 \pi)) \ket{j,m} = e^{- 2 \pi i M_3} \ket{j,m} = e^{-2 \pi i m} \ket{j,m} = e^{- 2 \pi i } \ket{j,m}
\end{align*}
where we used that $m$ and $j$ differ by an integer.

We see that the 360 rotation $2 \pi$, which should represent the identity in $\text{SO}(3)$ is only represented trivially if $j$ is an integer.

If $j$ is a half-integer, then the $2 \pi$ rotation is nologer represented by a trivial action.

\subsection{Projective representations}

Until now, we only studies unitary representations of a group.
Generally, that must not always be the case.
Maybe a physical state is not described by a vector in a hilbert space, but by a collection of vectors.
For example, instead of the euclidean space $\R^{3}$, we only care about vectors up to any scaling and identify $v \sim v'$ if and only if $v = \lambda v'$ for $\lambda \in R \setminus \{0\}$.

So in general, we don't really need all properties of a representation, and it suffices if the representation acts nice \emph{up to some phase}
\begin{align*}
  \rho(g) \rho(h) = c(g,h) \rho(gh)
\end{align*}
where the phases must satisfy the cocyclic condition
\begin{align*}
  c(g_1,g_2,g_3)c(g_2,g_3) = c(g_1g_2,g_3)c(g_1,g_2)
\end{align*}
Given a ``normal'' representation, we can artificially make it projective by defining
\begin{align*}
  \tilde{\rho}(g) = \rho(g) c(g)
\end{align*}
for $c(g)$ some pre-specified phase and setting
\begin{align*}
  c(g,h) = \frac{c(g)c(h)}{c(gh)}
\end{align*}

Conversely, we might wish to convert a prjective one to a normal one.

One can show that the projective representations of a Lie group $G$ correspond to the representations of the universal covering group of $G$.

In the case of the rotation group $\text{SO}(3)$, the  universal covering group is $\text{SU}(2)$, whose representations are parametrized by the eigenspaces $\mathcal{D}_j$.


\subsection{Electron Spin}

A situation where the angular impulse number $j$ is a half-integer can be shown by the Zeeman-Effect, where degenerate energy level states of an electron seperate under the influence of an external magnetic field.

To make sense of this, we introduce another degree of freedom in our hilbert space we call \textbf{spin}.
The Hilbert space of a single electron is no longer $L^{2}(\R^{3})$, but nw
\begin{align*}
  \mathcal{H} = L^{2}(\R^{3}) \otimes \C^{2}
\end{align*}
on which an $A \in \text{SU}(2)$ acts by
\begin{align*}
  \rho(A) = \rho_0(R(A)) \otimes A
\end{align*}
where $\rho_0(R)$ is the representation of $R \in \text{SO}(3)$.
In the case of an electron, the additional degree of freedom manifests itself in the representation of $\mathcal{D}_{\tfrac{1}{2}}$, which is given by
\begin{align*}
  M_j = \frac{\sigma_j}{2} \implies M_{+} = \begin{pmatrix}
  0 & 1\\
  0 & 0
  \end{pmatrix}
  , \quad
  M_{-} = \begin{pmatrix}
  0 & 0\\
  1 & 0
  \end{pmatrix}
\end{align*}
which gives us the basis vectors of $\C^{2}$.
\begin{align*}
  \ket{\tfrac{1}{2},\tfrac{1}{2}} = \begin{pmatrix}
  1\\
  0
  \end{pmatrix}
  =: \ket{e_3}
  , \quad
  \ket{\tfrac{1}{2},-\tfrac{1}{2}} = \begin{pmatrix}
  0\\
  1
  \end{pmatrix}
  =: \ket{-e_3}
\end{align*}
which we call \textbf{spin-up} and \textbf{spin-down} with respect to the quantization $e_3$.
Similarly, the basis of $\C^{2}$ given by $M_1$ and $M_2$, are
\begin{align*}
  M_1: \quad 
  &\ket{e_1} = \frac{e^{-i \pi/4}}{\sqrt{2}} \begin{pmatrix}
    1\\1 
  \end{pmatrix}
  ,
  \quad
  \ket{-e_1} = \frac{e^{i \pi/4}}{\sqrt{2}} \begin{pmatrix}
    -1\\1 
  \end{pmatrix}
  \\
  M_2: \quad 
  &\ket{e_2} = \frac{e^{i \pi/4}}{\sqrt{2}} \begin{pmatrix}
    1\\i 
  \end{pmatrix}
  ,
  \quad
  \ket{-e_2} = \frac{e^{-i \pi/4}}{\sqrt{2}} \begin{pmatrix}
    i\\1 
  \end{pmatrix}
\end{align*}

\subsection{Wigner's Theorem}

As we saw earlier, the representation of a symmetry group doesn't have to be an actual representation and it suffices if it is projective.

From the point of view of quantum mechanics, it is enough if the probability of the wave functions is invariant under the symmetry operations.
\begin{align*}
  \frac{\abs{\braket{\phi|\psi}}^{2}}{\braket{\phi|\phi}\braket{\psi|\psi}}
\end{align*}
Clearly, if the representation is unitary, then this is indeed the case.
But generally, it also suffices if the symmetry group is represented by \textbf{anti-unitary} operators which have the property
\begin{align*}
  \braket{\rho(g) \phi|\rho(g)\psi}
  =
  \overline{\braket{\phi|\psi}}
  = \braket{\psi|\phi}
\end{align*}
And that is all that is allowed.
This is the content of:

\begin{thm}[Wigner's Theorem]
  Let $G$ be a group acting on the set of hilbert states $\mathcal{H}$ that preserves the probabilities shown before.
  Then the action can be described by a projective representation of unitary or anti-unitary operations $\psi \mapsto \rho(g)\psi$.
\end{thm}

There are many important examples of symmetry that are represented by anti-unitary operators.
For example, many physical systems are invariant under time-reversal.
We therefore can define the \textbf{time reversal operator} $\mathcal{T}$ given by
\begin{align*}
  (\mathcal{T}\Psi)(t,w) = \overline{\Psi}(-t,x)
\end{align*}
However, most symmetires are represented by unitary operators.
This is a consequence of:

\begin{lem}[Weyl's Lemma]
  Let $\rho$ be a projective representation on $\mathcal{H}$.
  then $\rho(g^{2})$ is unitary for all $g \in G$.
\end{lem}
Which follows directly from the fact that
\begin{align*}
  \braket{\rho(g)^{2} \phi|\rho(g)^{2}\psi} = \overline{\braket{\rho(g) \phi|\rho(g) \psi}} = \braket{\phi|\psi}
\end{align*}

It follows from this that every rotation is unitary, since it is equal to the square of the rotation of half the angles.
