The interpretation of this formula tells us the following
\begin{itemize}
  \item[(d')] The possible results after measuring $A$ are the eigenvalues $\lambda_n$
  \item[(e')] The probability of getting the result $\lambda$ is
    \begin{align*}
      \mathbb{P}[A = \lambda] = \sum_{\lambda_n = \lambda} \abs{\braket{\psi_n|\psi}}^{2}
    \end{align*}
\end{itemize}
these are the generalisations of the postulates (d) and (e) from the previous section.

The other Postulates (a), (b), (c) from the beginning of this chapter can also also be generalized.
\begin{itemize}
  \item[(a')] For a given system, the set of states is a Hilbert space $\mathcal{H}$, on which there is a self-adjoint Hamilton operator $H$.
    
    Elements of $\mathcal{H}$ are streaks $\psi(t)$ which are equivalence classes of normed vectors $\chi$, where $\chi_1 \sim \chi_2$ if $\chi_1 = e^{i \alpha}\chi_2$.

  \item[(b')] The time evolution is described with the Schrödinger Equation
    \begin{align*}
      i \hbar \frac{\del }{\del t} \psi(t) = H \psi(t)
    \end{align*}
    note that the time evolution is well defined on the equivalence classes.

  \item[(c')] Observables are described by self-adjoint operators.
\end{itemize}

\begin{xmp}[Position and Momentum Basis]
  Let $\mathcal{H} = L^{2}(\R)$ be the Hilbert space of $L^{2}$-integrable functions on $\R$.

  For every $x \in \R$, the delta distribution is the covector
  \begin{align*}
    \bra{x} \in \Hom(\mathcal{H},\C), \quad \psi \mapsto  \braket{x|\psi} = \psi(x)
  \end{align*}
  which \emph{evaluate} a wave-fucntion $\ket{\psi}$ at \emph{position} $x$.

  If we identify the delta distributions with be their dual analogue $\ket{x} = \delta_x$ in $\mathcal{H}$, 
  given by
  \begin{align*}
    \delta_x(y) = \braket{y|x} = \left\{\begin{array}{ll}
      1 & y = x\\
      0 & y\neq x
    \end{array} \right.
  \end{align*}
  then we can write
  \begin{align*}
    \ket{\psi} = \int_{\R}dx \braket{x|\psi} \ket{x}
  \end{align*}

  Now, let's consider the momentum basis of $\mathcal{H}$ which for $p \in \R$ are given by
  \begin{align*}
    \braket{x|p} = e^{i px/\hbar}
  \end{align*}

  Unlike in the position basis, where smooth wave-functions require uncountable linear combinations of the delta funcations, 
  the momentum basis vectors let us write many common wave-functions as just a finite linear combination.
  For example for $\psi(x) = \cos(k x)$, we can write
  \begin{align*}
    \ket{\psi} = \tfrac{1}{2}\left(
      \ket{p = -i \hbar k} + \ket{p = i \hbar k}
    \right)
  \end{align*}
  Where we specify the basis, as 
  $\ket{2}$ could both refer to either of the functions
    \begin{align*}
      \ket{p = 2} =
      2^{i2x/ \hbar} \quad \text{or} \quad \ket{x = 2}=  \delta_{2,x} = \left\{\begin{array}{ll}
        1 & x = 2\\
        0 & x \neq 2
      \end{array} \right.
    \end{align*}

    Quite often, it is useful to switch between the position and momentum basis.
    As with any hilbert space, we have
    \begin{align*}
      \ket{\psi} = \int_{\R}dx \braket{x|\psi} \ket{x} = \int_\R dp \braket{p|\psi} \ket{p}
    \end{align*}
    so if we know $\psi(x) = \braket{x|\psi}$, then we can recover the fourier transform $\tilde{\psi}(p) = \braket{p|\psi}$ with
    \begin{empheq}[box=\bluebase]{align*}
      \tilde{\psi}(p) = \braket{p|\psi} = \int_{\R} dx \braket{x|\psi} \braket{p|x} = \int_{\R} dx \psi(x) e^{-ipx/\hbar}\\
      \psi(x) = \braket{x|\psi} = \int_{\R}dp \braket{p|\psi} \braket{x|p} = \int_{\R} dp \tilde{\psi}(p) e^{i px/\hbar}
    \end{empheq}
\end{xmp}

If we have linear transformation $T: \R^{3} \to \R^{3}$, then with a basis $e_1,e_2,e_3$, we can recover the coefficient of the corresponding matrix $A \in \Mat(3,\R)$ by
\begin{align*}
  a_{ij} = e_i^{T}A e_j
\end{align*}
so more generally, for a linear operator $\hat{T}: \mathcal{H} \to  \mathcal{H}$ and a basis $\mathcal{B} = (\ket{i})_{i \in I}$, the corresponding coefficient is given by
\begin{align*}
  B_{ij} = \braket{i|T|j}
\end{align*}

\begin{xmp}[Translation]
  For $a \in \R$, consider the translation operator $\hat{T_a}: \mathcal{H} \to  \mathcal{H}$ that shifts a wave-function to the right by $a$.
  In the momentum Basis $\mathcal{X} = (\ket{x})_{x \in \R}$, (with $\hat{x} \ket{x} = x \ket{x}$)
  \begin{align*}
    \braket{x|T_a|y} = \braket{x-a|y} \implies
    \braket{x|T_a|\psi} = \psi(x - a)
  \end{align*}
  but in the position basis $\mathcal{P} = (\ket{p})_{p \in \R}$, (with $\hat{p} \ket{p} = p \ket{p}$ and $e^{\hat{p}}\ket{p} = e^{p}\ket{p}$) we have
  \begin{align*}
    \hat{T_a} \ket{p} =  e^{-ia \hat{p}/\hbar} \ket{p} = e^{-iap/\hbar} \ket{p}
  \end{align*}
\end{xmp}





\subsection{Generalisation of $\infty$-dimensional Hilbert spaces}
To generalize the above notions to the infinite dimensional case, we need some more maths.

\begin{dfn}[]
Let $A: \mathcal{H} \to \mathcal{H}$ be an operator.
Its \textbf{spectrum} $\sigma(A) \subseteq \C$ consists of values $\lambda \in \C$ such that
\begin{align*}
  \forall \epsilon > 0\ \exists \psi \in \mathcal{H}, \|\psi\| = 1 \quad \text{such that} \quad \|(A - \lambda \id)\psi\| \leq \epsilon
\end{align*}
\end{dfn}
In the finite dimensional case, the spectrum is exactly the set of eigenvalues.

As we saw earlier, if $A$ is self-adjoint, then all its eigenvalues are real. 
One can also show that $\sigma(A) \subseteq \R$.

With this, we can postulate that the set of possible results of an operator $A$ is exactly the spectrum $\sigma(A)$.

\begin{xmp}[]
  The spectrum can be discrete, continuous or in parts both:
  \begin{itemize}
    \item For a free particle ($V = 0$), we have the following spectra:
      \begin{align*}
        \sigma(\hat{x}) = \sigma(\hat{p}) = \R, \quad \sigma(H) = \R^{+}
      \end{align*}
    \item For the particle in a finite well, we saw
      \begin{align*}
        \sigma(H) = \{E_{1}, \ldots, E_{n}\} \cup \R^{+}
      \end{align*}
      where the $E_i$ were the energies of the bound states given as the intersection of a tangent curve and a circle.
    \item For the harmonic oscillator, we se
      \begin{align*}
        \sigma(H) = \{\hbar \omega(\frac{1}{2} + n \big\vert n = 0,1,2,\ldots\}
      \end{align*}
  \end{itemize}
\end{xmp}


In the finite dimensional case, let $A: \mathcal{H} \to \mathcal{H}$ be an operator with discrete spectrum and $f: \C \to  \C$ any function.
This induces an operator
\begin{align*}
  f(A) := \sum_{\lambda \in \sigma(A)}f(\lambda) P_{\lambda} = \sum_{\lambda \in \sigma(A)} f(\lambda) \ket{\psi_{\lambda}} \bra{\psi_{\lambda}}: \quad \mathcal{H} \to \mathcal{H}
\end{align*}
This association satsfies
\begin{align*}
  (\alpha_1 f_1 + \alpha_2 f_2)(A) 
  &= \alpha_1f_1(A) + \alpha_2 f_2(A), \quad \forall \alpha_i \in \C\\
  (f_1 f_2)(A) 
  &= f_1(A) f_2(A)\\
  \overline{f}(A)
  &= f(A)^{\dagger}
\end{align*}
In particular, if $f= \id_\C$, then $f(A) = A$ and if $f(x) = 1$, then $f(A) = \id_{\mathcal{H}}$.


The spectral theorem tell us, that even in the infinite dimensional case, there exists a unique association
\begin{align*}
  f \mapsto f(A)
\end{align*}
that satisfies the above properties.


With the spectral theorem, we can now generalize the probabilistic interpretation.
If $A$ is self-adjoint, its sepctrum is real. 
So let $I \subseteq \R$ be an interval and $P_{I}(x)$ its characteristic function.
Then $P_I(A)$ is an orthogonal projector. Moreover, for disjoint intervals $I,J$ we have
\begin{align*}
  P_{I \sqcup J}(A) = P_I(A) + P_J(A)
\end{align*}
Then, for a given state $\psi$, we can define a probability measure on $\R$ given by
\begin{align*}
  \mu_{\psi}(I) = \braket{\psi|P_I(A)|\psi}
\end{align*}
We interperet $\mu_{\psi}(I)$ to be the probability that in the state $\psi$, $A$ measures a value of $a \in I$.

With this, we can define the expectationvalue of $A$ in state $\psi$ to be
\begin{empheq}[box=\bluebase]{align*}
  \braket{A}_{\psi} = \int \lambda \mu_{\psi}\left( 
    (-\infty,\lambda]
  \right)
  d \lambda
  = 
  \braket{\psi|A|\psi}
\end{empheq}
