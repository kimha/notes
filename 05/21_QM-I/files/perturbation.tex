\section{Perturbation theory}
Many interestinc physical systems cannot be solved exactly.
Perturbation theory treats them as systems, which are almost like a solvable system with some small correction terms.

We want to solve the schroedinger equation
\begin{align*}
i \hbar \frac{\del }{\del t}\Psi(x,t) = H \Psi(x,t)  
\end{align*}
where the Hamiltonian can be written as the sum
\begin{align*}
  H = H_0 + H'
\end{align*}
where $H_0$ is the hamiltonian of a system we can solve and $H'$ is some small error term.

\subsection{Non-degenerate time-independent perturbation theory}
Since we can solve the TISE for the hamiltonian $H_0$, we can assume that we have ONB of eigenfunctions $(\phi_n)_{n \in \N}$ satisfying
\begin{align*}
  H_0 \phi = \mathcal{E}_n \phi_n
\end{align*}
where we additionally assume that the $\mathcal{E}_n$ are pairwise different (hence non-degenerate).

Since we assume that $H'$ is some ``small error'' term that we ideally can control, we will write
\begin{align*}
  H = H_0 + \lambda H'
\end{align*}
If we analyze the order of $\lambda$ by writing
\begin{align*}
  \Psi &= \Psi_0 + \lambda \Psi_1 + \lambda^{2} \Psi_2 + \ldots
  \\
  E &= E_0 + \lambda E_1 + \lambda^{2} E_2 + \ldots
\end{align*}
we get a set of equations that $\Psi$ must satisfy in order to meet $H \Psi = E \Psi$, namely
\begin{align*}
  \mathcal{O}(1): \quad
  (H_0 - E_0)\Psi_0 
  &= 0\\
  \mathcal{O}(\lambda): \quad
  (H_0 - E_0)\Psi_1 &= 
  (E_1 - H')\Psi_0\\
  \mathcal{O}(\lambda^{2}): \quad
  (H_0 - E_0)\Psi_2 &= 
  (E_1 - H')\Psi_1 + E_2 \Psi_0\\
  \mathcal{O}(\lambda^{3}): \quad
  (H_0 - E_0)\Psi_3 &= 
  (E_1 - H')\Psi_2 + E_2 \Psi_1 + E_3 E_0
\end{align*}
which generalizes to
\begin{empheq}[box=\bluebase]{align}
  \mathcal{O}(\lambda^{s}): \quad
  (H_0 - E_0)\Psi_s &= 
  - H' \Psi_{s-1} + \sum_{j=1}^{s}E_j \Psi_{s-j},\label{eq:ndti-order-s} \quad s \geq 1 
\end{empheq}
Obviously, in the $0$-th order approximation, we get an eigenvector of the exact system, i.e. 
\begin{align*}
  \Psi_0 = \psi_n, \quad E_0 = \mathcal{E}_n
\end{align*}
for some $n \in \N$.
In order to find the other $\Psi_k$, note that since on the right hand-side of the equation (\ref{eq:ndti-order-s}) only terms with $\Psi_l$ for $l < k$ appear,
they can be found iteratively by first solving for $\Psi_1$, then $\Psi_2$ and so on.

Well, not quite.
As we are dealing with operators, the equations will be similar as linear equations of the form
\begin{align*}
  Ax = b
\end{align*}
for some matrix $A$ and vectors $x,b$, so 
the solutions are only unique up to $\Ker(H_0 - E_0)$.
But since we assume that the eigenvalues $\mathcal{E}_i$ are non-degenerate, the kernel equals the span of $\Psi_0$, so we can chose the $\Psi_k$ such that they are orthogonal to the kernel, i.e.
\begin{align*}
  \braket{\Psi_0|\Psi_k} = \delta_{0k}
\end{align*}
and since $\Psi_0 = \phi_n$, this is equivalent to
\begin{align*}
  \braket{\phi_n|\Psi} = 1
\end{align*}
\begin{rem}[]
This scaling does not necessarily imply that $\Psi$ itself has the correct normalisation, we still have to normalize it in the end!
\end{rem}

Now, to actually solve the equations \ref{eq:ndti-order-s} for each $k$, we
take the inner product with $\Psi_0$ on both sides and get
\begin{align*}
 \underbrace{\braket{\Psi_0|H_0 - E_0| \Psi_s}}_{= 0} = \underbrace{\braket{\Psi_0|E_1 - H'|\Psi_{s-1}}}_{= \delta_{s1}E_1 - \braket{\Psi_0|H'|\Psi_{s-1}|}} + \sum_{j=2}^{s}E_j \underbrace{\braket{\Psi_0|\Psi_{s-j}}}_{\delta_{sj}}
\end{align*}
from which we obtain
\begin{align*}
  E_s = \braket{\phi_n|H'|\Psi_{s-1}}, \quad s \geq 1
\end{align*}
So from the expansion $E = E_0 + \lambda E_1 + \lambda^{2} E_2, \ldots$ we get
\begin{align*}
  E
  &=
  E_0 + \sum_{s=1}^{\infty}\lambda^{s}E_s
  \\
  &=
  E_0 + \lambda \sum_{s=1}^{\infty}\lambda^{s-1} \braket{\phi_n|H'|\Psi_{s-1}}\\
  &=
  \mathcal{E}_n + \lambda \braket{\phi_n|H'|\Psi}
\end{align*}
and to solve for $\Psi = \Psi_0 + \lambda \Psi_1 + \ldots$ we use the fact that the $\phi_l$ form an ONB, so we can write
\begin{align*}
  \ket{\Psi_s} = \sum_{l} \braket{\phi_l|\Psi_s} \ket{\phi_l} = \sum_{l \neq n} \braket{\phi_l|\Psi_s}\ket{\phi_l}, \quad \forall s \geq 1
\end{align*}
where we used the fact that we chose $\Psi_s$ such that $\braket{\phi_n|\Psi_s} = \delta_{0s}$.

Applying $\bra{\phi_l}$ on both sides of Equation \ref{eq:ndti-order-s} yields
\begin{align*}
  \underbrace{\braket{\phi_l|H_0}\Psi_s}_{\mathcal{E}_l \braket{\phi_l|\Psi_s}}
  - \underbrace{E_0}_{\mathcal{E}_n}\braket{\phi_l|\Psi_s}
  =
  \sum_{j=1}^{s}E_j \braket{\phi_l|\Psi_{s-j}} - \braket{\phi_l|H'|\Psi_{s-1}}
\end{align*}
which yields
\begin{align*}
  \braket{\phi_l|\Psi_s} = \frac{1}{\mathcal{E}_n - \mathcal{E}_l}
  \left[
    \braket{\psi_l|H'|\Psi_{s-1}}
    -
    \sum_{j=1}^{s}E_j \braket{\phi_l|\Psi_{s-j}}
  \right]
\end{align*}
which gives us the coefficient of the $\ket{\phi_l}$ term in the ONB expansion resulting in the formula
\begin{align*}
  E_s
  &=
  \braket{\phi_n|H'|\Psi_{s-1}}
  \\
  \ket{\Psi_s}
  &=
  \sum_{l\neq n} 
  \frac{
    \braket{\phi_l|H'|\Psi_{s-1}} - \sum_{j=1}^{s-1}E_j \braket{\phi_l|\Psi_{s-j}}
  }{
    \mathcal{E}_n - \mathcal{E}_l} \ket{\phi_l
  }
\end{align*}

We will use the following notation, where the ONB vectors are denoted simpy by their indices $\ket{\phi_l} =: \ket{l}$.

\textbf{To summarize:} If we want to do $k$-th order perturbation theory in the non-degenerate case, we first solve (or more realistically, copy the solution of) the known TISE \begin{align*}
  H_0 \phi_n = \mathcal{E}_n \phi_n
\end{align*}
to obtain an ONB of solutions $(\phi_n)_{n \in \N}$.
Then we set
\begin{empheq}[box=\bluebase]{align*}
  E_0 &= \mathcal{E}_n, \quad \ket{\Psi_0} = \ket{\phi_n}
\end{empheq}
for some $n$ such that $\phi_n$ is as simple as possible.

Then, for $s = 1,2, \ldots k$ we iteratively solve
\begin{empheq}[box=\bluebase]{align*}
  E_s &= \braket{n|H'|\Psi_{s-1}}\\
  \ket{\Psi_s} &= 
  \sum_{l\neq n} 
  \frac{
    \braket{l|H'|\Psi_{s-1}} - \sum_{j=1}^{s-1}E_j \braket{l|\Psi_{s-j}}
  }{\mathcal{E}_n - \mathcal{E}_l} 
  \ket{l}
\end{empheq}
And retrieve our $k$-th order approximation by setting
\begin{empheq}[box=\bluebase]{align*}
  E &\simeq E_0 + \lambda E_1 + \lambda^{2}E_2 + \ldots + \lambda^{k}E_k\\
  \ket{\Psi} &\simeq \ket{\Psi_0} + \lambda \ket{\Psi_1} + \ldots +\lambda^{k}\ket{\Psi_k}
\end{empheq}


Since for most purposes, the second order is good enough, we usally stop there. Which means that they are used quite often, so we write down the steps explicitly here:


\begin{align*}
  E_0
  &= \mathcal{E}_n\\
  \ket{\Psi_0} &= \ket{n}\\
  E_1
  &= \braket{n|H'|n}\\
  \ket{\Psi_1} 
  &= \sum_{l\neq n} \frac{\braket{l|H'|n}}{\mathcal{E}_n - \mathcal{E}_l} \ket{l}\\
  E_2 
  &= \sum_{l \neq n} \frac{\abs{\braket{l|H'|n}}^{2}}{\mathcal{E}_n - \mathcal{E}_l}\\
  \ket{\Psi_2}
  &= \sum_{l \neq n}\left[
    \sum_{k \neq n} 
    \frac{\braket{l|H'|k} \braket{k|H'|n}}{(\mathcal{E}_n - \mathcal{E}_l)(\mathcal{E}_n - \mathcal{E}_k)}
    -
    \frac{\braket{l|H'|n} \braket{n|H'|n}}{(\mathcal{E}_n - \mathcal{E}_l)^{2}}
  \right]
  \ket{l}
\end{align*}

In in the case of first order perturbation theory (i.e. $\Psi = \Psi_0 + \lambda\Psi_1$), the normalisation is
\begin{align*}
  \braket{\Psi|\Psi} =  1 + \lambda^{2} \sum_{l \neq n}
  \frac{\abs{\braket{l|H'|n}}^{2}}{(\mathcal{E}_n - \mathcal{E}_l)^{2}}
\end{align*}


\subsection{Perturbed harmonic oscillator}
Consider the perturbed $1$-dimensional harmonic oscillator $H = H_0 + H'$ with
\begin{align*}
  H_0 = \frac{p^{2}}{2m} + \frac{m}{2}\omega^{2} q^{2}, \quad H' = - Fq
\end{align*}
We saw that the unperturbed problem has the solution


