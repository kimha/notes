\section{Wave Mechanics}
Starting with de Broglie's postulate, we want to heuristically derive the Schrödinger equation.

We do this by asking oursevles, how a wave with the characteristics described by the postulate should behave.

\subsection{Schrödinger Equation}

Consider a particle with impulse $p^{\mu} = (E,\vec{p})$.
Its de Broglie wavelength $\lambda$ and angular frequency $\omega$ are
\begin{align*}
  \lambda = \frac{2 \pi \hbar}{p} \quad \omega = \frac{E}{\hbar}
\end{align*}

Now we find a wave with such wavelength and angular frequency is.
One such wave is
\begin{align*}
  \Psi(x,t) = \Psi(0,0) e^{\frac{2 \pi i x}{\lambda}} e^{-i \omega t}
\end{align*}
where $\Psi(0,0)$ is some constant defining the amplitude of the wave.

This wave is characterized by the equations
\begin{align*}
  \frac{\del }{\del x} \Psi(x,t) &= \frac{2 \pi i}{\lambda} \Psi(x,t) = i \frac{p}{\hbar} \Psi(x,t)
  \\
  \frac{\del }{\del t}\Psi(x,t) = - i \omega \Psi(x,t) = - i \frac{E}{\hbar} \Psi(x,t)
\end{align*}
So we therefore want it to solve
\begin{empheq}[box=\bluebase]{align*}
  p \Psi(x,t) &= - i \hbar \frac{\del }{\del x} \Psi(x,t)
  \\
  E \Psi(x,t) &= i \hbar \frac{\del }{\del t}\Psi(x,t)
\end{empheq}
Since for a free particle, we can relate energy and momentum by
\begin{align*}
  E = \frac{1}{2m}p^{2}
\end{align*}
we obtain 
\begin{align*}
  i \hbar \frac{\del }{\del t}\Psi(x,t) = - \frac{\hbar^{2}}{2m} \frac{\del^2}{\del x^2} \Psi(x,t)
\end{align*}

And if the particle isn't free but under the influence of a potential $V(x,t)$ the right hand side gets an additional $V(x)$ term, so we end up with the \textbf{time-dependent Schrödinger Equation} 

\begin{empheq}[box=\bluebase]{align*}
  i \hbar \frac{\del }{\del t}\Psi(x,t) = \left(
    - \frac{\hbar^{2}}{2m} \frac{\del^2}{\del x^2} + V(x,t)
  \right)
  \Psi(x,t)
\end{empheq}
But what even is $\Psi$?
Well, it is a complex function in the variables $x,t$.
Bu ff it would describe a light-wave, $\abs{\Psi(x,t)}^{2}$ would describe the \emph{intensity} of the light at the coordinates $(x,t)$.

Since Born 1926, we interpret $\abs{\Psi(x,t)}^{2}$ to be the \emph{probabilty} that the particle is at point $x$ at the time $t$.

Since all probabilities should add up (or rather integrate) to $1$, we additionally \textbf{normalize} $\Psi$, by requiring that
\begin{align*}
  \int_{\R} dx \abs{\Psi(x,t)}^{2} = 1 \quad \text{ for all } t
\end{align*}

It isn't obvious if this is even possible to satisfy this for all $t$.
Because the Schrödinger equation already determines $\Psi(x,t)$ for all $t$, it could be that even if we normalize it for the time $t = 0$, we might lose the noramlizsation by developing $\Psi(x,t)$ through time.

As we will find out, if it is normalized for some time $t$, then it will also be normalize for all times $t$.


Here are some of the most important postulates of wave mechanics:
\begin{enumerate}
  \item The probabilty to find the particle at time $t$ at the positition $x$ is given by $\abs{\Psi(x,t)}^{2}$.
  \item The wave function $\Psi(x,t)$ is square-integrable over $x$, for all $t$ and is normalized.
  \item The wave function $\Psi(x,t)$ is a complex-valued function that satisfies the time-dependent Schrödinger equation
    \begin{empheq}[box=\bluebase]{align*}
      i \hbar \frac{\del }{\del t}\Psi(x,t) = H \Psi(x,t)
    \end{empheq}
    where $H$ is the \textbf{Hamilton operator} given by
    \begin{empheq}[box=\bluebase]{align*}
      H = - \frac{\hbar^{2}}{2m} \frac{\del^2}{\del x^2} + V(x,t)
    \end{empheq}
\end{enumerate}


In this definition of the Hamilton operator we see for the first time that \emph{observables} such as momentum $p$ can be described as \emph{linear differential operators}\footnote{
One might complain that not all functions are differentiable, but we can just take a dense subset of all square-integrable functions and ignore the problem.
}, where we recover the observable by measuring the \emph{Eigenvalues} of the differential operator.

So we define the impulse $p$ to be the operator
\begin{align*}
  p := i \hbar \frac{\del }{\del x}
\end{align*}

Another consequence is that (in general), linear operators (think of them as matrices) do not commute.
What in classical mechanics would just be taking two measurements now is senstive to the order in which the measurements are taken.
A famous example of this is the Heisenberg's uncertainty principle.




\subsection{Probability current and the continuity equation}
We define the probability density to be
\begin{align*}
  \rho(x,t) := \abs{\Psi(x,t)}^{2} = \Psi^{\ast}(x,t) \Psi(x,t)
\end{align*}
Since we know that $\Psi$ satisfies the Scrhödinger quation, we can use it to see how $\rho$ develops over time using the product rule:
\begin{align*}
  \frac{\del }{\del t}\rho(x,t)
  &=
  \Psi(x,t)^{\ast}
  \frac{\del }{\del t}
  \Psi(x,t) 
  + \Psi(x,t)
  \frac{\del }{\del t}\Psi(x,t)^{\ast}
  \\
  &=
  i \frac{\hbar}{2m}
  \left(
    \Psi(x,t)^{\ast} 
    \frac{\del^{2}}{\del x^{2}} \Psi(x,t)
    - \Psi(x,t) \frac{\del^{2}}{\del x^{2}} \Psi(x,t)^{\ast}
  \right)
  \\
  &\phantom{=} - \frac{i}{\hbar}
  \underbrace{
    \left(
      \Psi(x,t)^{\ast} V(x,t) \Psi(x,t) - \Psi(x,t) V(x,t) \Psi(x,t)^{\ast}
    \right)
  }_{= 0}
  \\
  &=
  - \frac{i \hbar}{2m} \frac{\del }{\del x}
  \left(
    \Psi(x,t)^{\ast} 
    \frac{\del}{\del x} \Psi(x,t)
    - \Psi(x,t) \frac{\del}{\del x} \Psi(x,t)^{\ast}
  \right)
\end{align*}
This can be simplified to the equation
\begin{empheq}[box=\bluebase]{align*}
  \frac{\del }{\del t}\rho(x,t)  + \frac{\del }{\del x} j(x,t) = 0
\end{empheq}
where $j(x,t)$ is the \textbf{probability current}

\begin{empheq}[box=\bluebase]{align*}
  j(x,t) = \frac{i \hbar}{2m}
  \left(
    \Psi(x,t)^{\ast} 
    \frac{\del}{\del x} \Psi(x,t)
    - \Psi(x,t) \frac{\del}{\del x} \Psi(x,t)^{\ast}
  \right)
\end{empheq}

this is very reminiscent of the continuity equation of electrodynamics
\begin{align*}
  \del_t \rho + \del_x j = 0
\end{align*}
which states that charge cannot be created or destroyed.
The interpretation of the continuity equation for our wave function $\Psi$ is as follows.

If the probabilty that the particle is inside some interval $[x_0 - \delta, x_0 + \delta]$, changes, that that amount must flow out of the interval into its neighboring regions.

The probabilty density thus behaves like some incompressible fluid that cannot be destroyed or created.

In particular, this means that the normalisation will be preserved over time, as
\begin{align*}
  \frac{\del }{\del t} \int_\R dx \rho(x,t) = - \int_{\R} dx \frac{\del }{\del x}j(x,t) = \left[
    - j(x,t)
  \right]_{- \infty}^{\infty} = 0
\end{align*}
since nothing can ``flow out'' of the entire space $\R$.

\subsection{Ehrenfest's Theorem}

If we think of $\abs{\Psi(x,t)}^{2}$ as some probability measure on $\R$, we can talk about the \emph{expectation value} of random variables.

For example the expectation value of a measurement in space is
\begin{align*}
  \scal{x} 
  &= \mathbb{E}[x] = \int_{\R} dx x \abs{\Psi(x,t)}^{2}
\end{align*}

and the time derivative of the expectation value can be calculated using the Schrödinger equation:
\begin{align*}
  \frac{d }{d t}\scal{x}
  &=
  \int_{\R}dx x \frac{\del }{\del t}
  \left(
    \Psi(x,t)\Psi(x,t)^{\ast}
  \right)
  \tag{$\ast$}
  \\
  &=
  \int_\R dx x \left[
    \left(
      \frac{i \hbar}{2m} \frac{\del^{2}}{\del x^{2}} \Psi(x,t) - \frac{i}{\hbar} V(x,t) \Psi(x,t) 
    \right)
    \Psi(x,t)^{\ast}
    +
    \Psi(x,t)
    \left(
      - \frac{i \hbar}{2m} \frac{\del^{2}}{\del x^{2}} \Psi(x,t)^{\ast} + \frac{i}{\hbar}V(x,t) \Psi(x,t)^{\ast}
    \right)
  \right]
\end{align*}
The terms with $V(x,t)$ cancel.
Doing partial integration for the first term, we get
\begin{align*}
  \frac{i \hbar}{2m} \int_\R dx x \Psi(x,t)^{\ast} \frac{\del^{2}}{\del x^{2}} \Psi(x,t)
  &=
  \frac{i \hbar}{2m}
  \left[
    x \Psi(x,t)^{\ast}
    \frac{\del }{\del x} \Psi(x,t)
  \right]_{x = -\infty}^{x = \infty}
  \\
  &\phantom{=}
  - \frac{i \hbar}{2m} \int_\R dx \left(
  \Psi(x,t)^{\ast} \frac{\del }{\del x}\Psi(x,t) + x \frac{\del }{\del x}\Psi(x,t)^{\ast} \frac{\del }{\del x} \Psi(x,t)
  \right)
\end{align*}
since $\Psi$ is square-integrable, the boundary term vanishes.
By the definition of the impulse operator, its expectation value is
\begin{align*}
  \scal{p} = i \hbar\int_\R dx \Psi^{\ast} \frac{d }{d x}\Psi(x,t) 
\end{align*}
so the first term can finally be re-written as
\begin{align*}
  \int_{\R} dx x \frac{i \hbar}{2m} \frac{\del^{2}}{\del x^{2}} \Psi(x,t) \Psi(x,t)^{\ast} = \frac{1}{2m}\scal{p}
\end{align*}
we again do partial integration for the the second-to-last term in ($\ast$) and in the end recover another $\frac{1}{2m} \scal{p}$ term.
Putting it all together, we get
\begin{align*}
  \frac{d }{d t}\scal{x} = \frac{1}{m} \scal{p}
\end{align*}
which is just the usual $p = m v$ from classical mechanics.
With some extra calculations, we can show that $\scal{x}$ satisfies the Newtonian equation $F = ma$ in the form
\begin{align*}
  m \frac{d^{2}}{d t^{2}}\scal{x} = - \scal{\frac{\del }{\del x}V(x,t)}
\end{align*}
this result is called \textbf{Ehrenfest's Theorem}.


\subsection{Physical observables are real}
In order to make physical sense of the observables, we want them to be real-valued.

We could just us the fact from Linear algebra that the Eigenvalues of hermitian operators are real, but we can also give another proof.

By square-integrability of $\Psi$, we know that it vanishes for $\abs{x} \to \infty$, so
by partial integration:
\begin{align*}
  \scal{p}^{\ast} 
  &= i \hbar \int_\R dx \Psi(x,t) \frac{\del }{\del x} \Psi(x,t)^{\ast}\\
  &= i \hbar\left[\Psi(x,t) \Psi(x,t)^{\ast}\right]
  - i \hbar \int_\R dx \Psi(x,t)^{\ast} \frac{\del }{\del x} \Psi(x,t)
  \\
  &= \scal{p}
\end{align*}

similarly, one can also show that the expectation value of energy $\scal{H}$ is also real-valued.


\subsection{Time independent Schrödinger equation}

If we assume that the potential $V(x,t) = V(x)$ does not depend on time, then we can use the Separationsansatz
\begin{align*}
  \Psi(x,t) = \psi(x) \chi(t)
\end{align*}
Putting this into the Schrödinger equation, we obtain
\begin{align*}
  \psi(x) i \hbar \dot{\chi}(t) = \chi(t) \left(
    - \frac{\hbar^{2}}{2m} \frac{d^{2}}{d x^{2}} \psi(x) + V(x) \phi(x)
  \right)
\end{align*}
by dividing by $\psi(x) \chi(t)$\footnote{We can do that because it's non-zero almost everywhere} we obtain an equation where the left hand side depends on $t$ and the right hand side depends on $x$.

So both sides are constant and we obtain the equations
\begin{align*}
  i \hbar \dot{\chi}(t) &= E \chi(t)\\
  H \psi(x) &= E \phi(x)
\end{align*}
The first equation is easy to solve. The solution is
\begin{align*}
  \chi(t) = \exp\left(
    - \frac{i E}{\hbar}t
  \right)
\end{align*}
so if $\psi(x)$ is a solution to $H \phi(x) = E \phi(x)$, then a solution to the Schrödinger equation is
\begin{empheq}[box=\bluebase]{align*}
  \Psi(x,t) = \psi(x) \exp\left(
    - \frac{i E}{\hbar}t
  \right)
\end{empheq}

so generally, if we know that $V(x,t) = V(x)$, we can solve the time-independent Schrödinger equation (TISE) 

\begin{empheq}[box=\bluebase]{align*}
  H \psi(x) = E \phi(x) \quad \text{where} \quad H = - \frac{\hbar^{2}}{2m} \frac{\del^{2}}{\del x^{2}} + V(x)
\end{empheq}
to obtain a range of solutions for $\psi(x)$ and then multiply by $\exp\left(
  - \frac{i E}{\hbar}t
\right)$ to obtain solutions for $\Psi(x,t)$.

And lastly, we find the correct constants by solving $\int_{\R}dx \abs{\Psi(x,t)}^{2} = 1$.

Moreover, since $E$ is real valued, we have
\begin{align*}
  \abs{\Psi(x,t)} = \abs{\psi(x)} \underbrace{\abs{\exp\left(
      - \frac{i E}{\hbar}t)
\right)}}_{= 1} = \abs{\psi(x)}
\end{align*}


The Schrödinger equation easily generalizes to higher dimensions. We get
\begin{align*}
  i \hbar \frac{\del }{\del t} \Psi(\vec{x},t) = H \Psi(\vec{x},t), \quad \text{for} \quad H = - \frac{\hbar^{2}}{2m} \Delta + V(\vec{x},t)
\end{align*}
and the TISE stays the same, where $H$ has a different domain
\begin{align*}
  \psi: \R^{3} \to \C, \quad H: L^{2}(\R^{3}) \to L^{2}(\R^{3})
\end{align*}

There is a connection between the TISE and the time-independent Hamilton-Jacobi equation.

We make the Ansatz
\begin{align*}
  \Psi(\vec{x},t) = \psi(\vec{x}) e^{- \frac{i E}{\hbar}t}, \quad \psi(\vec{x}) = A(\vec{x}) e^{i \frac{S(\vec{x})}{\hbar}}
\end{align*}
Calculating the Laplacian $\Delta \psi(\vec{x})$ yields
\begin{align*}
  \Delta \psi(x) = (\Delta A) e^{- \frac{S(x)}{\hbar}} + \frac{2i}{\hbar} (\nabla S) (\nabla A) e^{- \frac{S(x)}{\hbar}} + \frac{i}{\hbar}(\Delta S) A e^{i \frac{S(x)}{\hbar}}
  - \frac{1}{\hbar^{2}}(\nabla S)^{2} A e^{i \frac{S(x)}{\hbar}}
\end{align*}
Entering this into the TISE
\begin{align*}
  \left(
    - \frac{\hbar^{2}}{2m} \Delta + V(x)
  \right)
  \psi(x)
  = E \psi(x)
\end{align*}
and separating real and imaginary parts, we obtain
\begin{align*}
  \frac{(\nabla S)^{2}}{2m} + (V(x) - E) 
  &=
  \frac{\hbar^{2}}{2m} \frac{\Delta A}{A}
  \\
  (\nabla A) (\nabla S) + \frac{A}{2} \Delta S 
  &= 0
\end{align*}
If we think of $\hbar$ as some parameter that describes the deformation of classical physics\footnote{See \url{https://ncatlab.org/nlab/show/classical+limit}}, we can consider the limit $\hbar \to 0$
and obtain the time-independent Hamilton-Jacobi equation
\begin{align*}
  \frac{(\nabla S)^{2}}{2m} + V(x) 
  = H(x, \nabla S) = E
\end{align*}



