
\subsection{Energy Eigenstates}

Starting with the TISE $H \psi = E \psi$, we can think of $H$ as some infinite matrix, as it's just a linear operator.
Since a diagonalisable $n \times n$ matrix has $n$ solutions, one would assume that, if $H$ were diagonalisable, we would find that $H \psi = E \psi$ has $n = \infty$ many solutions.

What we can do is add a parameter and solve $H \psi_n = E_n \psi_n$ instead and hope to find all (and hopefully unique) solutions to this.

If $\psi_n$ form a basis of the solution space, then we can say that any other solution to the Schrödinger equation is a linear combination of the basis vectors, i.e.
\begin{align*}
  \psi(x) = \sum_{n \in \N} a_n \psi_n(x)
\end{align*}
and in order to develop it over time, we instead develop the $\psi_n$ over time and find that
\begin{align*}
  \Psi(x,t) = \sum_{n \in \N} a_n \psi_n(x) \exp\left(
    - \frac{i E_n}{\hbar}t
  \right)
\end{align*}
Then $\Psi(x,t)$, being a linear combination of solutions of the TISE, should also be a solution for the boundary problem.

What is nice is that we only have to solve for the $\psi_n$ once, and then for any other problem, we can reuse the $\psi_n$.


\subsection{Energy measurements}

For now, let's assume that the $(\psi_n)_{n \in \N}$ form an orthonormal basis of Eigenfunctions of $H \psi = E_n \psi_n$.
Another assumption we often make is that there is no \textbf{degeneracy}, i.e. that $E_n \neq E_m$ for $n \neq m$.

Orthonormalcy means that
\begin{align*}
  \int_\R dx \psi_n^{\ast}(x) \psi_m(x) = \delta_{nm}
\end{align*}
In fact, it is possible to prove this if we assume that there is no degeneracy.

\begin{proof}[No degeneracy $\implies$ ONB]
  Since they satisfy the schrödinger equation, we have
  \begin{align*}
    E_n \int_{\R} dx \psi_n^{\ast}(x) \psi_m(x) 
    &=
    \int_{\R} dx \psi_n^{\ast}(x) H \psi_m(x)\\
    &=
    - \frac{\hbar^{2}}{2m} \int_{\R} dx \psi_n^{\ast}(x) \psi_m''(x)
    +
    \int_\R dx \psi_n^{\ast}V(x) \psi_m(x)
  \end{align*}
  Since $V(x)$ is real-valued, we can write the integrand of the second term as $(V(x) \psi_n(x))^{\ast} \psi_m(x)$

  Then by doing partial integration of the first term, we get
  \begin{align*}
    - \frac{\hbar^{2}}{2m}
    \int_{\R} dx \psi_n^{\ast}(x) \psi_m''(x) 
    &=
    \ldots
  \end{align*}
  in the end, we obtain
  \begin{align*}
    E_m \int_{\R} dx \psi_n^{\ast}(x) \psi_m(x) = \int_{\R} dx (H \psi_n)^{\ast} \psi_m(x) = E_n^{\ast} \int_{\R} \psi_n^{\ast}(x) \psi_m(x)
  \end{align*}
  in particular, if $n = m$, we see that $E_n = E_n^{\ast}$ so the energy is real.


  Subtracting both sides, we see that
  \begin{align*}
    (E_m - E_n) \int_{\R} dx \psi_n^{\ast}(x) \psi_m(x) = 0
    \quad \text{for} \quad m \neq n
  \end{align*}
\end{proof}

This shows that the general solution to the SE can always be written in the form
\begin{empheq}[box=\bluebase]{align*}
  \Psi(x,t) = \sum_{n \in \N} a_n \psi_n(x) \exp\left(
    - \frac{i E_n}{\hbar}t
  \right)
\end{empheq}

Now, let's calculate the expectation value for the energy.
It is given by
\begin{align*}
  \scal{H}(\Psi)
  = \braket{\Psi|H|\Psi}
  &= \int_\R \Psi(x,t)^{\ast} H \Psi(x,t)\\
  &=
  \sum_{m \in \N} \sum_{n \in \N}
  a_m^{\ast} a_n
  \exp\left(
    - \frac{i (E_n- E_m)}{\hbar} t
  \right)
  \underbrace{
  \int_{\R} dx \psi_m^{\ast}(x) \overbrace{H \psi_n(x)}^{= E_n \psi_n(x)}}_{= E_n \delta_{mn}}\\
  &=
  \sum_{n \in \N} \abs{a_n}^{2} E_n
\end{align*}

This shows that the possible measurements of a system are exactly the Eigenvalues of the Hamiltonian $H$.



