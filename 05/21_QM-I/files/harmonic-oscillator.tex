\section{Harmonic oscillator}
The harmonic oscillator is a simple but really interesting example of a dynamic system in quantum mechanics.

In classical mechanics, the Hamiltonian is
\begin{align*}
  H = \frac{1}{2m}(p^{2} + m^{2} \omega q^{2})
  = \frac{p^{2}}{2m} + \frac{f}{2}q^{2}
\end{align*}

And for our purposes, $p$ is replaced by the operator $\hat{p} = - \hbar \frac{\del }{\del t}$.

Introducing the dimensionless complex variable $x$ given by
\begin{align*}
  x = \sqrt{\frac{m \omega}{\hbar}}q
\end{align*}  
we can rewrite
\begin{align*}
  q = \sqrt{\frac{\hbar}{m \omega}}x, \quad \text{and} \quad \frac{\del }{\del q} = \sqrt{\frac{m \omega}{\hbar}} \frac{\del }{\del x}
\end{align*}
So the hamiltonian turns into
\begin{align*}
  H = \frac{\hbar \omega}{2}(- \del_x^{2} + x^{2}) 
\end{align*}

And we now turn to solving the TISE, which has the form
\begin{align*}
  (\del_x^{2} + \lambda - x^{2})\psi(x) = 0
\end{align*}

The bound solution solutions must satisfy
$\lim_{\abs{x} \to \infty} \psi(x) = 0$.

\subsection{The classic solution}
An approximative solution is given by
\begin{align*}
  \psi(x) = e^{-\tfrac{x^{2}}{2}}
\end{align*}
an in order to make it exact, we use the Ansatz $\psi(x) = H(x) e^{-x^{2}/2}$, which gives us the following differential equation for $H(x)$\footnote{The function $H(x)$ should not be confused with the Hamiltonian.
The usage of the letter $H$ is historic and stands for (as we will see later) the mathematician Charles Hermite.}:
\begin{align*}
  H'' - 2x H' - (\lambda - 1)H = 0\tag{$\ast$}
\end{align*}
And to solve this one, we use the Fuch's Ansatz
\begin{align*}
  H(x) = x^{s}\sum_{n=0}^{\infty}a_nx^{n}, \quad \text{where} \quad a_0 \neq 0, s \geq 0
\end{align*}
By looking at the power series expansion, we see
\begin{align*}
  H(x) &= \sum_{n=0}^{\infty}a_nx^{s+n}
  \\
  2xH'(x) &= \sum_{n=0}^{\infty}2(s+n)a_n x^{s+n}\\
  H''(x)  &= \sum_{n=0}^{\infty}(s+n)(s+n-1)a_nx^{s+n-2}\\
          &= s(s-1)a_0x^{s-2} + (s+1)sa_1x^{-1} \\
          &+ \sum_{n=0}^{\infty}(s+n+2)(s+n+1)a_{n+2}x^{s+n}
\end{align*}
So in order to solve $(\ast)$, we see that the coefficients must satisfy:
\begin{align*}
  s(s-1)a_0 = 0
  , \quad 
  (s+1)sa_1 &= 0\\
  (s + n + 2)(s + n + 1)a_{n+2} - (2s - 2n + 1 - \lambda)a_n &= 0, \quad n \geq 2
\end{align*}
Since $a_0 \neq 0$, the first equation requires that $s = 0,1$. 
So either $a_1$ must equal zero, or there is no requirement for $a_1$, which means there are multiple solutions.
Either way, we can assume without loss of generality that $a_1 = 0$.

So there are types of solutions:
\begin{itemize}
  \item $s = 0$: \quad $H(x) = a_0 + a_2x^{2} + \ldots$ is even in $x$.
  \item $s = 1$: \quad $H(x) = x(a_0 + a_2x^{2} + \ldots)$ is odd in $x$.
\end{itemize}
Here we see that because the potentia is symmetric ($V(q) = V(-q)$, it suffices to find symmetric and anti-symmetric solutions.

Now, let's consider what happens to the coefficient in the limit $n \to \infty$.
We see that
\begin{align*}
  \tfrac{a_{n+2}}{a_n} = \frac{2n + \mathcal{O}(1)}{n^{2} + \mathcal{O}(n)}
  \to
  \frac{2}{n} \quad \text{for large $n$}
\end{align*}
So unless the sequence $a_n$ terminates, the function $H(x)$ would grow faster than the exponential function (which satisfies $\tfrac{a_{n+2}}{a_{n}} \to \tfrac{1}{n^{2}}$
If that were the case, our Ansatz $\psi(x) = H(x) e^{-x^{2}/2}$ would not work, as the super-exponential growth would outdo the exponential decay of $e^{-x^{2}/2}$, so $\psi$, could not satisfy the condition $\lim_{x \to \infty}\psi(x) = 0$.

We therefore conclude that the sequence $(a_n)_{n \in \N}$ must terminate at some point.

This is only possible, if in the recurrence relation, we have $\lambda = 2n + 1$.
Then, the differential equation becomes
\begin{align*}
  H'' - 2xH' + 2nH = 0
\end{align*}
for which the solution is an $n$-th order polynomial.
We call these the \textbf{$n$-th Hermite polynomials} $H_n(x)$, of which the first few are
\begin{align*}
  H_0 = 1, H_1 = 2x, H_2 = 4x^{2} - 2
\end{align*}
or more generally 
\begin{align*}
  H_n(x) = (-1)^{n} e^{x^{2}} \del_x^{n}e^{-x^{2}}
\end{align*}
We therefore get the solutions
\begin{align*}
  \psi_n(x) = N_n H_n(x) e^{-x^{2}/2}
\end{align*}
where $N_n$ is some normalisation factor and who have eigenvalues
\begin{align*}
  \lambda_n = 2n + 1, \quad \text{and} \quad H_n = \hbar \omega (n + \tfrac{1}{2})
\end{align*}
One can show that the correct normalisation is
\begin{align*}
  N_0 = \pi^{-1/4}, \quad N_n = \frac{N_0}{\sqrt{2^{n}n!}}
\end{align*}

This shows the general method to solve such systems.
We separate the asymptotic behaviour (in this case $e^{-x^{2}/2}$) and then find the correction (here $H(x)$) by making a power series Ansatz.

The boundary condition requires that the power series must terminate and provides us with a spectrum $E_n$ and the polynomial eigenfunctions.

But there is a more elegant solution to the problem that will be much more useful to compute the solutions.


\subsection{The elegant solution}

This solution introduces the so-called \textbf{ladder operators} $a$ and $a^{\dagger}$.
They are defined as
\begin{align*}
  a &:= \frac{1}{\sqrt{2}}(x + \del_x) 
  = \frac{1}{\sqrt{2}}\left(
    \sqrt{\frac{m \omega}{\hbar}}q + \frac{i}{\sqrt{m \hbar \omega}}p
  \right)\\
    a^{\dagger} &:=
    \frac{1}{\sqrt{2}}(x - \del_x) 
    = \frac{1}{\sqrt{2}}\left(
    \sqrt{\frac{m \omega}{\hbar}}q - \frac{i}{\sqrt{m \hbar \omega}}p
  \right)
\end{align*}
The converse relations are
\begin{align*}
  x = \frac{1}{\sqrt{2}}(a + a^{\dagger}), \quad
  \del_x = \frac{1}{\sqrt{2}}(a - a^{\dagger})
\end{align*}
With this, the Hamiltonian now becomes
\begin{align*}
  H 
  &= \frac{\hbar \omega}{2}(- \del_x^{2} + x^{2})\\
  &= \frac{\hbar \omega}{4} \left(
    -(a - a^{\dagger})(a-a^{\dagger}) + (a+ a^{\dagger})(a + a^{\dagger})
  \right)
  \\
  &= \hbar \omega \left(
    a^{\dagger} a + \frac{1}{2}[a,a^{\dagger}]
  \right)
\end{align*}
If we compute the commutator $[a,a^{\dagger}]$, we find
\begin{align*}
  [a,a^{\dagger}] = [\del_x,x] = 1
\end{align*}
where the identity $[\del_x,x] = 1$ comes from the product rule
\begin{align*}
  \del_x x f = 1 \cdot f + x \cdot f' = (1 + x \del_x)f
\end{align*}
So if we define the \textbf{number operator} $N = a^{\dagger}a$, we see
\begin{align*}
  H = \hbar \omega (N + \tfrac{1}{2})
\end{align*}
So finding eigenvectors to the eigenvalue problem $H \Psi = E \Psi$ is equivalent to finding eigenvectors
\begin{align*}
  N \ket{n} = a^{\dagger}a \ket{n} = n \ket{n}
\end{align*}
From now on, we switch to the Dirac notation where we use $\ket{n}$ to denote the eigenvector $\Psi_n$ whose eigenvalue to $H$ is $E = \hbar \omega(n + \tfrac{1}{2})$.

Note that although we are using the letter $n$ to denote the eigenvalues, we a priori do not know what values $n$ could be.

It's easy to see that the commutators of $N$ with $n^{\dagger}$ and $a$ are
\begin{align*}
  [N,a^{\dagger}] = a^{\dagger}
  , \quad \text{and} \quad 
  [N,a] = -a
\end{align*}
We can show that $a^{\dagger} \ket{n}$ and $a \ket{n}$ are again Eigenvectors to $N$, with Eigenvalues $n+1$ and $n-1$
\begin{align*}
  N a^{\dagger} \ket{n} 
  = (a^{\dagger}N + [N,a^{\dagger}])\ket{n} 
  = a^{\dagger}n \ket{n} + a^{\dagger} \ket{n} = (n+1)a^{\dagger}\ket{n}\\
  N a \ket{n} = (aN + [N,a])\ket{n} = an \ket{n} - a \ket{n} = (n-1)a \ket{n}
\end{align*}
So the operators $a^{\dagger},a$ increase/decrease the Eigenvalue of the eigenvector $\ket{n}$ by $1$.
We therefore call $a^{\dagger}$ the \textbf{raising} operator and $a$ the \textbf{lowering operator} and refer to them both collectively as the \textbf{ladder operators}.

The vectors $a^{\dagger}\ket{n}$ and $a \ket{n}$ aren't generally normalized.
If we assume that $\ket{n}$ is normalized (i.e.\ $\braket{n|n} = 1$), then
\begin{align*}
  \braket{n|a^{\dagger}a|n} = \braket{n|N|n} = n\\
  \ket{n|aa^{\dagger}|n} = \braket{n|1 + a^{\dagger}a|n} = n+1
\end{align*}
so in order to normalize them, we define
\begin{align*}
  \ket{n-1} = \frac{1}{\sqrt{n}}a \ket{n}\\
  \ket{n+1} = \frac{1}{\sqrt{n+1}}a^{\dagger} \ket{a}
\end{align*}
By repeated application of $a$, we see that
\begin{align*}
  a^{k}\ket{n} = \sqrt{n(n-1)(n-2) \cdots(n-k+1)}\ket{n-k}
\end{align*}
which is an eigenvector with eigenvalue $n-k$. But since
\begin{align*}
  n = \braket{n|N|n} = \braket{an|an} \geq 0
\end{align*}
we can only have non-negative eigenvalues.
So we cannot simply apply $a$ arbitrarily many times to get an eigenvector with negative eigenvalue.
Therefore, it follows that $n$ must be an integer and there exists a state with smallest eigenvalue $\ket{0}$ for which
\begin{align*}
  a \ket{0} = 0
\end{align*}
so $a$ \emph{annihilates} the \emph{ground state} $\ket{0}$.
Therefore, all eigenvectors $\ket{n}$ can be obtained by repeated application of the raising operator $a^{\dagger}$ on the ground state $\ket{0}$:
\begin{align*}
  \ket{n} = \frac{(a^{\dagger})^{n}}{\sqrt{n!}}\ket{0}, \quad n \in \N
\end{align*}
So we see that $n$ measures how many energy quanta $\hbar \omega$ are present in the eigenstate.

Note that the ground state $\ket{0}$ has non-zero energy $E_0 = \frac{\hbar \omega}{2}$ which is the Heisenberg uncertainty principle at play:
The particle cannot simply stay at rest at $x = 0$.

In order to compute $\ket{0}$, we solve for $a \ket{0} = 0$.
In the position-basis, this is
\begin{align*}
  0 = \sqrt{2} \braket{x|a|0} = (x + \del_x) \braket{x|0} = (x + \del_x)\psi_0(x)
\end{align*}
The solution to this differential equation is simply $\psi_0(x) \propto e^{-x^{2}/2}$, and with the right normalisation:
\begin{align*}
  \psi_0(x) = \frac{1}{\sqrt[4]{\pi}} e^{-x^{2}/2}
\end{align*}
Writing the other states in position basis $\braket{x|n}$ follows from repeated application of $a^{\dagger}$:
\begin{align*}
  \braket{x|1} = \braket{x|a^{\dagger}|0} = \frac{1}{\sqrt{2}}(x - \del_x) \psi_0(x) = \frac{\sqrt{2}}{\sqrt[4]{\pi}}x e^{-x^{2}/2}
\end{align*}
and more generally
\begin{empheq}[box=\bluebase]{align*}
  \braket{x|n}
  &=
  \frac{1}{\sqrt{2^{n}n! \sqrt{\pi}}}(x - \del_x)^{n} e^{-x^{2}/2}\\
  &=
  \frac{1}{\sqrt{2^{n}n! \sqrt{\pi}}}H_n(x) e^{-x^{2}/2}
\end{empheq}
where the first few Hermite Polynomials are
\begin{align*}
  H_0 = 1
  , \quad
  H_1 = 2x
  , \quad
  H_2 = (2x)^{2} - 2\\
  H_3 = (2x)^{3} - 6(2x)
  , \quad
  H_4 = (2x)^{4} - 12(2x^{2}) + 12
\end{align*}
And in the variable $q = \sqrt{\frac{\hbar}{m \omega}}x$, we get
\begin{align*}
  \braket{q|n}
  =
  \sqrt{
    \sqrt{\frac{m \omega}{\hbar \pi}\frac{1}{2^{n}n!}} 
  }
  H_n(\sqrt{\frac{m \omega}{\hbar}q}
  e^{-(m \omega q^{2})/2 \hbar}
\end{align*}

\subsection{Classical Limit}

We know that the energies $E_0,E_1$ are really small.
But in ``human-scale'' physics, the energies are extremely high.
Actually, for large $E_n$, we recover the classical solution of the harmonic oscillator.
The classical probability density and energies are given by
\begin{align*}
  W_{\text{class.}} = \frac{1}{\pi q_0 \sqrt{1 - (q/q_0)^{2}}}\\
  E_{\text{class.}} = \frac{1}{2}m \omega^{2} q_0^{2}
\end{align*}
where the ampliude $q_0$ is defined by the approximation $E_n = E_{\text{class.}}$

The formula for the probability density $W_{\text{class.}}$ follows from conservation of energy: Since
\begin{align*}
  \frac{1}{2}m v^{2} = \frac{1}{2}m \omega^{2}(q_0^{2}-q^{2})
\end{align*}
we can solve for $v = \frac{dq}{dt}$ to get
\begin{align*}
  dt = \frac{dq}{\omega q_0 \sqrt{1 - (q/q_0)^{2}}}
\end{align*}

The probability to find the particle at $q$ is then proportional to $(1 - (q/q_0)^{2})^{-1/2}$.
To get the correct normalisation, we integrate from $-q_0$ to $q_0$ by using the substitution $q = q_0 \sin \theta$.


