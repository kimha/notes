One way to interpret this is to say that the values $E_n$ are the possible values of energy and the coefficient $\abs{a_n}^{2}$ determines the probability that $E = E_n$.

This gives us another postulate of wave mechanics.
\begin{enumerate}
  \setcounter{enumi}{3}
  \item The possible values for measurements correspond to the Eigenvalues of the Hamiltonian.
  \item The probability to measure the energy $E_n$ is given by 
    \begin{align*}
    \abs{\braket{\psi_n|\Psi}}^{2}
      = \abs{\int dx\psi_n^{\ast} \Psi}^{2}
      = \abs{\sum_{m} \underbrace{\braket{\psi_n|a_m \psi_m \exp\left(
        - \frac{i E_m t}{\hbar}
\right)}}_{= \delta_{nm}}
}^{2}
      =
      \abs{a_n}^{2}
    \end{align*}
\end{enumerate}
Let's assume we measure some energy $E = E_n$ and we let the system ``idle'', then we should expect that over time, its energy should not change.

If we were to measure the energy again some time later, we would expect to measure the same energy $E = E_n$ again, so the probability $\abs{a_n}^{2} = 1$ and for all other $m \neq 0$, we have $a_m = 0$.


This means that \emph{after the measurement}, the wave function must be given by
\begin{align*}
  \Psi = A \psi_n \exp\left(- \frac{i E_n t}{\hbar}
  \right)
\end{align*}

In other words: After the measurement, the wave function \emph{collapses} into the state $n$.

This diverges from our physical intuition of measureing things.
Looking at a ball doesn't really change its position or momentum.
But one way to expect this collapse of the wave function is that in order to \emph{measure} its position, we need to shine some light on it and have it bounce back to us.

But this photon will carry some momentum and therefore bouncing off of some particle will of course change its trajectory.

There are many types of interpretations about the collapse of wavefunctions, but mathematically speaking, they shouldn't really affect how we move forwards.




\section{Example of simple systems}
Before we formalize quamtum mechanics, we will build some intuition by solving simple, one-dimensional examples.



\subsection{Particle in a Box}
We consider a particle that is trapped in a 1D Box, say the interval $[0,a]$.
This can be described by the potential
\begin{align*}
  V(x) =
  \left\{\begin{array}{ll}
    0 & 0 \leq x \leq a\\
    \infty & \text{ otherwise}
  \end{array} \right.
\end{align*}
We start solving the TISE by looking for a basis of Eigenfunctions $\psi$ satisfying $H \psi = E \psi$.

Since the potential outside of the box is infinite, we need that $\psi(x) = 0$, or else the particle would have infinite energy.

Inside of the box, the TISE reads
\begin{align*}
  H \psi = 
  - \frac{\hbar^{2}}{2m} \frac{d^{2}}{d x^{2}} = E \psi
\end{align*}
The most general solutions for this is given by
\begin{align*}
  \psi(x) = 
  \left\{\begin{array}{ll}
      A \cosh\left(
        \sqrt{2m \abs{E} \frac{x}{\hbar}}
      \right) 
      +
      B \sinh\left(
        \sqrt{2m \abs{E} \frac{x}{\hbar}}
      \right) 
      & \text{ if } E < 0\\
      A + Bx & \text{ if } E = 0
      \\
      A \cos\left(
        \sqrt{2m \abs{E} \frac{x}{\hbar}}
      \right) 
      +
      B \sin\left(
        \sqrt{2m \abs{E} \frac{x}{\hbar}}
      \right) 
             &
             \text{ if } E > 0
  \end{array} \right.
\end{align*}
Now we look for boundary conditions.
Since we are taking a second derivative, we need that the $\psi$ and its derivative must at least be continuous.

In particular, continuity implies $\psi(0) = 0$, so in all three cases, we have $A = 0$.
The condition $\psi(a) = 0$ means that
in the cases $E \leq 0$, we find $B = 0$,
so if $E \leq 0$, we only have the trivial solution, which cannot be normalized, so we discard it.

This leaves us with the case $E > 0$.
In order for the sine to vanish at $x = a$, we get the condition 
$\sqrt{2m \abs{E_n}} = \frac{n \pi \hbar}{a}$,
so the possible values for Energy are\footnote{Make sure to remember this formula as problems like these occure quite frequently in exams :)}
\begin{empheq}[box=\bluebase]{align*}
  E_n = \frac{n^{2} \pi^{2} \hbar^{2}}{2 m a^{2}}
  \quad \text{for} \quad n \in \N
\end{empheq}

and the non-trivial solutions are
\begin{align*}
  \psi_n = B_n \sin\left(
    \frac{n \pi x}{a}
  \right)
\end{align*}
in order to determine the constant $B_n$, we use the normalisation condition
\begin{align*}
  1 = \abs{B_n}^{2} = \int_0^{a} dx \sin^{2} \left(
    \frac{n \pi x}{a}
  \right)
  = \frac{1}{2} a \abs{B_n}^{2}
\end{align*}
which is solved for $B_n = \sqrt{\frac{2}{a}}$.

We note the following
\begin{rem}[]
  \begin{enumerate}
    \item The energies are discrete
    \item $E = 0$ is not a valid solution!
      So the particle cannot sit still, which is an example of the Heisenberg uncertainty principle in action. We will prove it later.
    \item The Energy corresponds to the number of roots of the function $\abs{\psi}$.
  \end{enumerate}
\end{rem}






\subsection{Finite square well}

We consider a particle in a potential
\begin{align*}
  V(x) =
  \left\{\begin{array}{ll}
      - V & \text{ if } x \in [-a,a]\\
     0 &  \text{ otherwise}
  \end{array} \right.
\end{align*}
Again, we look for an Eigenbasis of solutions $H \psi = E \psi$.

Moreover, we want our particle to be \textbf{bounded}, i.e. $\abs{\psi(x)}^{2} \to 0$ as $\abs{x} \to \infty$.

\begin{itemize}
  \item[$x > a$] We are solving $- \frac{\hbar^{2}}{2m} \frac{d^{2}}{d x^{2}} \psi(x) = E \psi(x)$.
    This has the general solution
    \begin{align*}
      \psi(x) = A e^{i \kappa x} + D e^{- i \kappa x} \quad \text{where} \quad E = - \frac{\hbar^{2} \kappa^{2}}{2m} \implies \kappa = \frac{\sqrt{-em E}}{\hbar}
    \end{align*}
    in order to obtain a bounded solution, we need $\kappa \in \R_{> 0}$ and thus $E < 0$.
    And to ensure that it can be normalized, we need $A = 0$, so
    \begin{align*}
      \psi(x) = D e^{- i \kappa x}
    \end{align*}
  \item[$x < a$] Here we get the same as in the case $x > a$, but since $x < 0$, we get
    \begin{align*}
      \psi(x) = A e^{i \kappa x}
    \end{align*}

  \item[$-a \leq x \leq a$]
    Since $V(x) = -V$, the TISE reads
    $- \frac{\hbar^{2}}{2m} \frac{d^{2}}{d x^{2}} \psi = (E + V) \psi$.
    with general solution
    \begin{align*}
      \psi(x) = B e^{i k x} + C e^{- i k x} \quad \text{and} \quad V + E = - \frac{\hbar^{2} \kappa^{2}}{2m} \implies k = \sqrt{\frac{2m (V + E)}{\hbar^{2}}}
    \end{align*}
\end{itemize}

Now we solve the boundary and continuity conditions.
But solving this is quite tedious and would require lots of calculations.

One trick we can use is to make use of \textbf{symmetry}.

Note that the Hamiltonian $H$ is invariant under the transformation $x \mapsto  -x$.
Therefore,
\begin{align*}
  \psi(-x) = \psi(x) &\implies H \psi(-x) = H \psi(x)
  \\
  \psi(-x) = \psi(x) &\implies H \psi(-x) = -H \psi(x)
\end{align*}
so the hamiltonian maps even solutions to even ones, and odd ones to odd ones.

Therefore, it suffices to find all even solutions and all odd solutions.

\subsubsection*{Even solutions}
The even solutions require $A = D$ and $B = C$, so we find.
\begin{align*}
  \psi(x) =
  \left\{\begin{array}{ll}
    A e^{- \kappa x} & \text{ if } x > a\\
    2B\cos kx & \text{ if } -a \leq x \leq a\\
    A e^{\kappa x} & \text{ if } x < -a
  \end{array} \right.
\end{align*}
since the function is even, it's enough to consider the continuity conditions at $x = a$, as the ones for $x = -a$ follow immediately. We see
\begin{align*}
  \psi \text{ continuous:} \quad
  &A e^{- \kappa x} = 2B \cos ka
  \frac{d \psi}{d x}\text{ continuous:} \quad
  &\psi(x) = \kappa A e^{- \kappa x} = 2B \sin ka
\end{align*}
dividing both equations, we get
\begin{align*}
  \kappa = k \tan(k a)
\end{align*}
and by the characterisation for $k, \kappa$ from beore, we get
\begin{align*}
  \kappa^{2} + \kappa^{2} = \frac{2mV}{\hbar^{2}}
\end{align*}
For convenience, it's nice to introduce the variables $\eta = \kappa a$ and $\xi = ka$, giving us
\begin{align*}
  \eta = \xi \tan \xi \quad \text{and} \quad \eta^{2} + \xi^{2} = \frac{2ma^{2}}{V}\hbar^{2}
\end{align*}
Plotting this in an $f(\xi) = \eta$ graph, we will intersect the graph of $\tan$ and a circle.
And we will do this with the restriction that $\xi, \eta > 0$.

Graphically, one sees that there is always going to be $1$ solution, no matter how small the radius of the circle is.

And the radius gets larger gets larger, there are more and more solutions.
With some calculations, we can find that if $N$ marks the number of solutions, then
\begin{align*}
  (N-1)^{2} \pi^{2} < \frac{2 m a^{2} V}{\hbar^{2}} < N^{2} \pi^{2}
\end{align*}

\subsubsection*{Odd solutions}
Similar to the even case, one quickly finds that $A = -D$ and $B = -C$.
The Ansatz is
\begin{align*}
  \psi(x) =
  \left\{\begin{array}{ll}
    -A e^{-\kappa x} & \text{ if }x > a\\
    2iB \sin(kx) & \text{ if }-a < x < a\\
    A e^{\kappa x} & \text{ if }x < -a
  \end{array} \right.
\end{align*}

And with a similar calculation as before, we get
\begin{align*}
  \eta =  - \xi \cot \xi \quad \text{and} \quad \eta^{2} + \xi^{2} = \frac{2ma^{2}V}{\hbar^{2}}
\end{align*}
Unlike earlier, there is no odd solution if
\begin{align*}
  \frac{2ma^{2}V}{\hbar^{2}} < \frac{\pi^{2}}{4}
\end{align*}
This corresponds to our remark earlier, where we showed that the smallest energy value corresponds to the solution with the last amount of roots, which must produce an even function.


\begin{rem}[]
If we constrast this with the naive approach of simply solving the ODE numerically, then one finds that simply guessing $\kappa$ will generate a solution that explodes as $x \to \infty$.

We \emph{need} to use symmetry to find out that the spectrum of values for $\kappa, k$ is discrete.
\end{rem}

What if we consider solutions that unbound?
The following example gives a nice intuition behind them.

\subsection{Step potential}
The potential is given by
\begin{align*}
  V(x) =
  \left\{\begin{array}{ll}
    0 & \text{ if }x \leq 0\\
    V_0 & \text{ if }x \geq 0\\
     & 
  \end{array} \right.
\end{align*}
And as always, we solve for eigenfunctions to the TISE $H \psi = E \psi$.

In the region $x < 0$, the general solution is again
\begin{align*}
  \psi_k(x) = A e^{\frac{ikx}{\hbar}} + B e^{\frac{-ikx}{\hbar}} \quad \text{where} \quad k = \sqrt{2m E}
\end{align*}
Again, normalisation is only possible if $k \in \R$, and so $E > 0 $.

The term with coefficient $A$ describes a wave moving from the left side, and $B$ a wave reflecting off the potential.
And the reflection probability is given by $R = \abs{\frac{B}{A}}^{2}$.

In the region $x \geq 0$, the TISE reads
\begin{align*}
  \frac{d^{2}}{d x^{2}} \psi = - \frac{2m}{\hbar^{2}} (E - V_0) \psi
\end{align*}

Classically, we expect a difference for the case $E < V_0$, where the particle bouncess off the potential, and for the case $E > V_0$, where the particle can go ``over'' the potential.





