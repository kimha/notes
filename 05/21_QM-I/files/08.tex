
\section{Formalism of Quantum Mechanics}

In wave mechanics, we saw that particles were represented by wavefunctions that satisfy the \underline{linear} Schrödinger equation
\begin{align*}
  i \hbar \del_t\Psi(x,t) = \left(- \frac{\hbar^{2}}{2m} \del_x^{2} + V(x,t)\right) \Psi(x,t
\end{align*}
since this equation is linear, it means that the space of solutions to this equation forms a $\C$-vector space.

Moreover, for any fixed $t$, the vector space consists of $\C$-valued functions on $\R^{n}$.

Letting $V = \Hom_{\textsf{Vec}-\C}(\R^{n},\C)$, we can say that such a wave-function corresponds to a function 
\begin{align*}
  \Psi: \R \to V, \quad t \mapsto  \Psi(-,t)
\end{align*}
We will use upper-case letters $\Psi,\Phi,F,G$ for such functions, and lower-case letters $\psi,\phi,f,g$ for $\Psi(t),\Phi(t)$ etc. for fixed $t$.

Moreover, we saw that the solutions should be square-integrable. So we also want 
\begin{align*}
  \Psi(-,t) \in L^{2}(\R^{n}) = \left\{f: \R^{n} \to \C : \int_{\R^{n}}\abs{f(x)}^{2} < \infty\right\}
\end{align*}
We also introduced a inner product
\begin{align*}
  \braket{\psi|\phi} := \int dx \psi^{*}(x) \phi(x) 
\end{align*}
which turns out to give us all the requirements of a \emph{Hilbert space}.

\begin{dfn}[]
A \textbf{Hilbert space} is a $\C$-vector space $\mathcal{H}$ with an inner product
\begin{align*}
  \braket{-|-}: \mathcal{H} \times \mathcal{H} \to \C
\end{align*}
that is \emph{complete}\footnote{Every Cauchy-sequence is convergent} with respect to the norm $\|f\| := \sqrt{\braket{f|f}}$.

A sesqulinear map is
\begin{itemize}
  \item linear in the second argument and semi-linear in the first argument:\footnote{Mathematicians often take the other convention that sesquilinear forms be linear in the first and semi-linear in the second.
    For our purposes, the physicist notation makes quite a few things nicer.}
    For all $\alpha \in \C, f,g,h \in \mathcal{H}$:
    \begin{align*}
      \braket{f|\alpha g + h} = \alpha \braket{f|g} + \braket{f|h}\\
      \braket{\alpha f + g|h} = \alpha^{*} \braket{f|h} + \braket{g|h}
    \end{align*}
  \item positive definite: $\forall f \in \mathcal{H}$:
    \begin{align*}
      \braket{f|f} \geq 0, \quad \text{and} \quad \braket{f|f} = 0 \iff f = 0
    \end{align*}
\end{itemize}

A Hilbertt space is called \textbf{separable}, if there exists a countable basis.
This means that there exists $\psi_{1}, \psi_{2}, \ldots$ in $\mathcal{H}$ such that for all $\psi \in \mathcal{H}$, there exist
$a_1,a_2,\ldots \in \C$ such that
\begin{align*}
  \psi = \sum_{n \in \N} a_n \psi_n
\end{align*}
\end{dfn}


\begin{xmp}[]
The simplest example of a Hilbertt space is $\C^{n}$ with the inner product
\begin{align*}
  \braket{(v_i)_{1 \leq i \leq n}|(w_i)_{1 \leq i \leq n}} := \sum_{i=1}^{n} v_i^{\ast}w_i
\end{align*}

$L^{2}(\R^{n})$ is a separable Hilbertt space with inner product
\begin{align*}
  \braket{f|g} = \int dx f^{\ast}(x)g(x)
\end{align*}
\end{xmp}


\begin{lem}[]
  (Assuming Zorn's Lemma:)
Every separable Hilbert space has an orthonormal basis.
\end{lem}
\begin{proof}
This follows form Schmid's orthonormalisation process which you have seen in Linear Algebra II:
By ``randomly chosing'' linearly independent $f_1,f_2,\ldots$ in $\mathcal{H}$, we set
\begin{align*}
  h_1 &:= \frac{f}{\|f\|}\\
  g_2 &:= f_2 - \braket{h_1|f_2}h_1, \quad h_2 := \frac{g_2}{\|g_2\|}\\
  g_3 &:= f_3 - \braket{h_1|f_3}h_1 - \braket{h_2|f_3}h_2, \quad h_3 := \frac{g_3}{\|g_3\|}
\end{align*}
and so on.
\end{proof}

In the chapter about wave-functions, we saw that physical observables (such as position, momentum etc.) corresponded to \emph{eigenvalues of linear operators}
\begin{align*}
  A: \mathcal{H} \to \mathcal{H}
\end{align*}
In particular, we saw the \textbf{momentum operator}
\begin{align*}
  \hat{p}: L^{2}(\R) \to L^{2}(\R), \quad f(x) \mapsto (\hat{p}f)(x) := -i \hbar \del_x f(x)
\end{align*}
Of course, not all functions in $L^{2}(\R)$ are differentiable, but a result in functional analysis is that the differentiable functions form a \emph{dense} subset of $L^{2}(\R)$.
So, we have to broaden the definition of operators to be that operators are allowed to only be defined on dense subsets of $\mathcal{H}$.

The \textbf{position operator} was defined as
\begin{align*}
  \hat{x}: L^{2}(\R) \to L^{2}(\R), \quad f(x) \mapsto (\hat{x}f)(x) = x \cdot f(x)
\end{align*}
whose eigenvectors were the delta functions

\begin{dfn}[]
  Let $\mathcal{H}$ be a Hilbert space.
  An \textbf{operator} is a linear map 
  \begin{align*}
    A: D(A) \subseteq \mathcal{H} \to \mathcal{H}
  \end{align*}
  where $D(A)$ is some dense subspace of $\mathcal{H}$.

  For a \emph{state} $\psi \in \mathcal{H}$, the \textbf{expectation value} of an operator $A$ is
  \begin{align*}
    \scal{A}_{\psi} := \scal{\psi| A\psi}
  \end{align*}
  The \textbf{adjoint} of an operator $A$ is the operator $A^{\dagger}$ uniquely determined by 
  \begin{align*}
    \forall f \in D(A^{\dagger}),g \in D(A): \exists A^{\dagger}f \in \mathcal{H}: \quad 
    \braket{f|A g} = \braket{A^{\dagger}f|g} 
  \end{align*}
  An operator $A$ is called \textbf{self-adjoint} if
  \begin{align*}
    A^{\dagger} = A
  \end{align*} 
\end{dfn}
In the finite dimensional case $\mathcal{H} = \C^{n}$, the adjoint of a matrix $A \in \C^{n \times n}$ is the conjugate-transpose ${A^{\ast}}^{T}$.


\begin{rem}[]
  There is a slightly weaker version of self-adjoint operators, namely an operator $A$ is called \textbf{symmetric}, if for all $\psi \in D(A): \braket{\psi|A \psi} = \braket{A \psi|\psi}$.


  The difference is that if $A$ is symmetric, it's actual adjoint could have domain $D(A^{\dagger}) \neq D(A)$.

  One can show that if $A$ is symmetric, one always has the inclusion $D(A) \subseteq D(A^{\dagger})$
\end{rem}

\subsection{Dirac Notation}
We call the vectors of $\mathcal{H}$ ``\emph{kets}'' and the covectors of $\mathcal{H}^{\ast} = \Hom(\mathcal{H},\C)$ as ``\emph{bras}''.
\begin{align*}
  \ket{\psi} \in \mathcal{H}, \quad \bra{\psi} \in \mathcal{H}^{\ast}
\end{align*}
where application is denoted by
\begin{align*}
  \braket{\alpha|f} := \bra{\alpha}( \ket{f}) \in \C
\end{align*}
Obviously, dual vectors are projectors, i.e. they satisfy the relation
\begin{align*}
  \bra{\alpha} \circ \bra{\alpha} = \bra{\alpha}
\end{align*}

\begin{thm}[Sepctral theorem]
  Let $(\psi_n)_{n \in \N}$ be an ONB of a Hilbert space $\mathcal{H}$. Then
  \begin{align*}
    P_n = \ket{\psi_n} \bra{\psi_n}: \mathcal{H} &\to \mathcal{H}\\
    \psi_m &\mapsto \ket{\psi_n} \cdot \underbrace{\braket{\psi_n|\psi_m}}_{\in \C} 
  \end{align*}
  is a projector where its image is the subspace generated by $\ket{\psi_n}$.

  In particular, every operator $A: D(A) \to \mathcal{H}$, can be decomposed into a linear combination of projectors:
  \begin{align*}
    A = \sum_{n \in \N} \lambda_n P_n
  \end{align*}
\end{thm}

As a result, one finds that the expectation value of $A$ for some state $\psi$ is given by
\begin{align*}
  \scal{A}_{\psi} = \braket{\psi|A \psi} 
  = \sum_{n \in \N} \lambda_n \braket{\psi|\left(\ket{\psi_n} \bra{\psi_n}\right)(\psi)} = 
  \sum_{n \in \N} \lambda_n \braket{\psi|\psi_n} \braket{\psi|\psi_n} =
  \sum_{n \in \N}\lambda_n \abs{\braket{\psi|\psi_n}}^{2}
\end{align*}
