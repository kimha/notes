
\subsubsection*{Case I: $E > V_0$}
Here, the TISE is
\begin{align*}
  - \frac{\hbar^{2}}{2m} \frac{d^{2}}{d x^{2}}\psi = (E - V_0) \psi
\end{align*}
and the general solution is
\begin{align*}
  \psi(x) = C e^{\frac{ilx}{\hbar}} + D e^{- \frac{ilx}{\hbar}} \quad \text{where} \quad l = \sqrt{2m (E - V_0)}
\end{align*}
But the term with coefficent $D$ would describe a particle coming from the right side, which we do not want. 
So we are only looking for solutions with $D = 0$.

The continuity condition at $x = 0$ for $\psi$, $\frac{d }{d }\psi$ gives us
\begin{align*}
  A + B = C \quad \text{and} \quad ik(A - B) = i l C \implies (A-B) = \frac{l}{k}C
\end{align*}
solving this is easy. In the end, one finds
\begin{align*}
  A = \frac{1}{2} C(1 + \frac{l}{k}), \quad B = \frac{1}{2}C(1 - \frac{l}{k}) 
\end{align*}
and the reflection and transmission probability is
\begin{align*}
  R &= \abs{\frac{B}{A}}^{2} =  \frac{(1 - \tfrac{l}{k})^{2}}{(1 + \tfrac{l}{k})^{2}}\\
  T &= \frac{l}{k} \abs{\frac{C}{A}}^{2} = \frac{l}{k} \frac{4}{(1 + \tfrac{l}{k})^{2}}
\end{align*}
indeed, one verifies that $T + R = 1$, which is expected.

For a sanity check, lets consider the case where $E \gg V_0$. Then we have that $l \sim k$, so most of the wave is transmitted and ``makes it trough'' the barrier.
And if  $E$ is just barely bigger than $V_0$, $l \sim 0$ so almost nothing makes it through.

\textbf{Case II:} $E \leq V_0$

Classically, the particle would not make it past the barrier.
The general solution in the region $x \geq 0$ would be
\begin{align*}
  \psi(x) = C e^{- \frac{\kappa x}{\hbar}} + D e^{\frac{\kappa x}{\hbar}} \quad \text{where} \quad \kappa = \sqrt{2m(V_0 - E)}
\end{align*}
since the exponent is real, we have to discard solutions with coefficient $D \neq 0$, or else it would explode as $x \to \infty$.

Solving the boundary conditions yield
\begin{align*}
  A = \frac{1}{2} C(1 + \frac{i \kappa}{k}), \quad 
  B = \frac{1}{2} C(1 - \frac{i \kappa}{k})
\end{align*}
Calculating the probability of reflection, we find that $R = \abs{\frac{B}{A}}^{2} = 1$.

But nevertheless, one finds that $\psi \neq 0$ in the region $x \geq 0$ and instead only finds exponential decay.

However, this does not change anything about the probability current, because since the wave-function is real in that region, we have
\begin{align*}
  J = \left(
    \psi^{\ast} \del_x \psi - \psi \del_x \psi^{\ast} 
  \right)
  = 0 \quad \text{ for} \quad x > 0
\end{align*}


In the limit case $V_0 \to \infty$, we also have $\kappa \to 0$ and thus the wave-function is identitally zero in this region.


\subsection{Normalisation and Wavepackets}

Consider the general eigenvector to the TISE for a free particle $\psi_k(x) = e^{\frac{i k x}{\hbar}}$, $k = \sqrt{2m E}$.
Such wave-functions are not square-integrable and cannot be normalized!

But we don't care. These are just the basis vectors of our solution space. An actual solution is a linear combination of these eigenvectors
\begin{align*}
  \Psi(x,t) = \sum_{k} a_k \psi_k(x) e^{- \frac{i E_k t}{\hbar}} \quad \text{for} \quad E_k = \frac{k^{2}}{2m}
\end{align*}
or, in the continuous case
\begin{align*}
  \Psi(x,t) = \int dk\ f(k) \psi_k(x) e^{- \frac{i E_k t}{\hbar}}
\end{align*}
The normalisation conditions then becomes
\begin{align*}
  \int dx \abs{\Psi(x,t)}^{2}
  &=
  \frac{1}{2 \pi}
  \int dx \int dk  \int d \tilde{k} f(k) e^{\frac{ikx}{\hbar}} e^{- \frac{i E_kt}{\hbar}}
  f^{\ast}(\tilde{k}) e^{- \frac{i \tilde{k}x}{\hbar}} e^{\frac{i E_{\tilde{k}}t}{\hbar}}
  \\
  &=
  \frac{1}{2 \pi}
  \int d \tilde{k} \int d k f(k) f^{\ast}(\tilde{k}) e^{- \frac{t}{\hbar}(E_{\tilde{k}} - E_k)} \underbrace{\int dx e^{\frac{ix}{\hbar}(k - \tilde{k})}}_{= 2 \pi \delta(k - \tilde{k})}
  \\
  &= 
  \int dk \abs{f(k)}^{2} \stackrel{!}{=}1
\end{align*}
so the normalisation condition of the wave-function is not a condition about the form of the eigenvectors of the TISE, but rather a condtion about \emph{how these eigenvectors are combined}.



