# notes
Contains my lecture notes, summaries etc.


# How to use
To keep the repository size small, this repo won't contain pre-compiled files (such as .pdf files) with every commit.

Instead, you can download the .pdf files from [my personal homepage](https://n.ethz.ch/~kimha) or download the source-code and compile it yourself.

## Compiling it yourself
- Clone this repository
```bash
git clone https://gitlab.ethz.ch/kimha/notes.git
```
- `cd` into into target directory (such as `notes/04/12_Topo/lecture-notes/`) and compile the .pdf yourself.
- To update the local copy, do
```bash
git pull
```


# Contributing
Contributors are welcome. Feel free to fork this repository and create pull requests for fixing errors or other additions, but please note the GNU Free Documentation License (GNU FDL).

