\section{Scratchpad}
Unordered collection of explanations and results from outside the lectures.
Some content is from the exercise classes.

Given a field extension $L/k$ and $\alpha \in E$, consider the evaluation mapping
\begin{align*}
  \ev_{\alpha}: k[X] \to L, \quad f \mapsto f(\alpha)
\end{align*}

\begin{itemize}
  \item If $\alpha$ is a root of some non-constant polynomial $f \in k[X]$, we say that $\alpha$ is \textbf{algebraic} over $k$.
    If that is the case, then $\Ker \ev_{\alpha} = (m_{\alpha})$ is a principal (and thus maximal) ideal for some $\mu_{\alpha} \in k[X]$ called the \textbf{minimal polynomial} of $\alpha$.
    As $k[X]/(m_{\alpha})$ is already a field, the smallest field containing $\alpha$ is
    \begin{align*}
      k(\alpha) = \Image \ev_{\alpha} \iso \faktor{k[X]}{(m_{\alpha})}
    \end{align*}
  \item If the evaluation mapping is injective, we say that $\alpha$ is \textbf{transcendental}.
    Therefore $k[\alpha] \iso k[X]$, and the smallest field containing $\alpha$ is
    \begin{align*}
      k(\alpha) = \text{Quot}(k[\alpha]) \iso \text{Quot}(k[X]) = k(X) = \{\frac{f}{g} \big\vert f,g \in k[X], g \neq 0\}
    \end{align*}
\end{itemize}

The notation $k[\alpha]$ for $\Image \ev_{\alpha}$ coincides with the notation we use when writing $\Q[i]$.
Moreover, if we think of the ``free variable'' $X$ as a ``foreign'' transcendental number living in some extension, we recover the definition of the polynomial ring $k[X]$



\begin{ex}[]

Say we know some $\alpha \in \C$ has minimal polynomial $X^{4} - X - 1 \in \Q[X]$.
From our above consideration, we know that $k(\alpha) = \Image \ev_{\alpha}$.
Now if we want to understand what $\Image \ev_{\alpha}$ actually looks like, we plug in $\alpha$ into all polynomials $f \in k[X]$ to get numbers of the form
\begin{align*}
  f(\alpha) = \sum_{k=0}^{n} a_k\alpha^{k} \in \C
\end{align*}
and identify them using the minimal polynomial, which tells us that $\alpha^{4} - \alpha - 1 = 0$, or in other words: $\alpha^{4} = 1 + \alpha$.
This tells us that anytime the exponent of $\alpha$ is $\geq 4$, we can reduce it. 
For example, the number $\alpha^{6}$ can be re-written as
\begin{align*}
  \alpha^{6} = \alpha^{2} \alpha^{4} = \alpha^{2} (1 + \alpha) = \alpha^{2} + \alpha^{3}
\end{align*}
It's easy to see that then $1,\alpha,\alpha^{2},\alpha^{3}$ form a basis of $\Q(\alpha)$.
To make sure that numbers of the form
\begin{align*}
  a_0 + a_1 \alpha + a_2 \alpha^{2} + a_3 \alpha^{3}
\end{align*}
form a field with the induced multiplication, let's compute the inverse of $\alpha$ and $\alpha+1$.

The inverse of $\alpha$ should satisfy $\alpha^{-1} \alpha = 1$.
And another way of writing $1$ is $1 + 0 = 1 + \alpha^{4} - \alpha - 1 = \alpha^{4} - \alpha$. Giving us
\begin{align*}
  \alpha^{-1} \alpha = 1 = \alpha^{4} - \alpha \implies \alpha^{-1} = \alpha^{3} - 1
\end{align*}
For $\alpha + 1$, we know that $\alpha^{4} = \alpha + 1$, so
\begin{align*}
  (\alpha +1)^{-1} = (\alpha^{4})^{-1} = (\alpha^{-1})^{4} = (\alpha^{3} - 1)^{4}
\end{align*}
The term on the right will contain some $\geq 4$ order terms, but as mentioned earlier, they can easily be reduced.
\end{ex}




