
\begin{cor}[]
  If $\gcd(p,n) = 1$, then $[\F_p[n]:\F_p]$ is a power of $p$ modulo $n$.
\end{cor}
To better understand what the group $\Z/n\Z^{\times}$ is like we show that we can decompose it into smaller copies, if $n$ is not a prime.

\begin{thm}[]
  If $\gcd(n,m) = 1$, then
  \begin{align*}
    (\Z/mn\Z)^{\times} \iso (\Z/m\Z)^{\times} \times (\Z/n\Z)^{\times}
  \end{align*}
  and if $p$ is prime $> 2$, then
  \begin{align*}
    (\Z/p^{r}\Z)^{\times} \iso \Z/p^{r-1} \times \Z/(p-1)\Z
  \end{align*}
  in particular, we have
  \begin{align*}
    (\Z,2^{r}\Z)^{\times} \iso \Z/2\Z \times (\Z/2^{r-2}\Z)
  \end{align*}
\end{thm}

For example we can calculate
$(\Z/7\Z)^{\times} = \{1,2,3,4,5,6\} \iso \Z/6\Z$
The possible exponents are $1,2,3,6$.
\begin{itemize}
  \item For $p = 2$, we have $2^{3} = 8 = 1 \mod 7$ and thus $[\F_p[7]:\F_2]= 3$ with 
    \begin{align*}
      \Phi_7(T) = (T^{3} + T + 1)(T^{3} + T^{2} + ) \in \F_[T]
    \end{align*}
  \item For $p = 3$, we need exponent $6$, and $\Phi_7$ is irreducible mod $3$.
  \item The prime $p = 13$, is of order $2$, because $13^{2} = 24 \cdot 7 + 1$. We then have
    \begin{align*}
      \Phi_7(T) = (T^{2} + 3T + 1)(T^{2} + 5T + 1)(T^{2} + 6T + 1)
    \end{align*}
  \item $p = 29$ is of order $1$ and
    \begin{align*}
      \Phi_7(T) = (T-7) (T-16)(T-20)(T-23)(T-24)(T-25)
    \end{align*}
\end{itemize}


\begin{thm}[]
  If $\gcd(p,n) = 1$, then the unitary irreducible factors of $\Phi_n$ if $\F_p[X]$ are all different and have the same degree of order $p \mod n$ in $(\Z(n\Z)^{\times}$.
\end{thm}

\begin{proof}
  Because $X^{n} -1$ has no multiple roots the same holds for $\Phi_n(X)$, therefore all irreducible factors have to be different.

  Since $\F_p[n]$, is a splitting field of $X^{n}-1$, we have that $\Phi_n(X)$ splits into linear factors in $\F_p[n][X]$, so the irreducible factors of $\Phi_n(X)$ must be of the Form $\text{irr}(\alpha,\F_p)$ for some $\alpha \in \F_p[n]$ with $\Phi_n(\alpha) = 0$.

  So it suffices to show that if $\Phi_n(\alpha) = 0$, then $\alpha$ must be a primitive $n$-th root of unity.
  From this, it would follow that $\F_p[n] = \F_p(\alpha)$ and the minimal polynomial $\text{irr}(\alpha,\F_p)$ has degree $[\F_p[n] : \F_p]$.

  Now assume that $\alpha$ is \emph{not} a primitive $n$-th root of unity with $\Phi_n(\alpha) = 0$.

  Then there exists a $1 \leq m <n$ with $\alpha^{m} = 1$ and suc that $m$ divides $n$.
  And we can write
  \begin{align*}
    0 = X^{m} - 1 = \prod_{l | m} \Phi_d(X) 
  \end{align*}
  so there has to be a $d_0 | m$ with $\Phi_{d_0}(\alpha) = 0$.
  But we can also write
  \begin{align*}
    X^{m} -1 = \Phi_n(X) \prod_{d|n, d < n} \Phi_d(X)
  \end{align*}
  so since $d_0$ divides $m$ and is a true divisor of $n$ we have that $\alpha$ must be a root of multiplicity at least two, which is a contradiction to the first part.
\end{proof}


Dirichlet proved that given some $a \in (\Z/n\Z)^{\times}$, there are finitely primes $p$ with $p = a \mod n$.

In particular, for $a = 1$, it means that there are infinitely many primes $p$ such that
$\Phi_n \mod p$ splits in $\F_p[X]$



We can prove this special case using the properties of the cyclotomic polynomials.
The general case however, requires some machinery from analytic number theory.

\begin{thm}[]
Let $n \in \N$. Then there are infinitely many primes $p$ with $p = 1 \mod n$.
\end{thm}
Before we prove it, first note that for $n \geq 3$, since the roots $\rho$ lie in the unit circle and are farther away from $n$ than $1$ does, we have
\begin{align*}
\abs{\Phi_n(n)} = \prod \abs{n - \rho} \geq \prod \abs{(n-1)} \geq 2
\end{align*}

in particular, $\Phi_n(n)$ is an integer $\neq \pm 1$.

\begin{proof}
It is enough to show that for all $n \in \N$, there exists \emph{at least one} prime $p$ with this property.

Because assume there were only finitely many $p_1,\ldots,p_t$ primes with this property, we then can let $p$ be a prime with
\begin{align*}
  p = 1 \mod n \cdot p_1 \cdot \ldots \cdot p_t
\end{align*}
then also $p = 1 \mod n$, and $p = 1 \mod p_i$.
Therefore, we have found a new $p$ with this property, contradicting the claim that there are only finitely many.

As $\Phi_n(n)$ is an integer $\neq \pm 1$, we claim that if $p$ divides $\Phi_n(n)$, then
$p = 1 \mod n$.
Then all we have to do is factorize $\Phi_n(n)$ and the proof follows.

To prove the claim, asssume $p | \Phi_n(n)$. As the cyclotomic polynomial also divides $X^{n}-1$, we have $p | n^{n} -1$.

Moreover $n^{n} = 1$ in$\F_p^{\times}$.

Let $k$ be the order of $n$ is $\F_p^{\times}$.

Then $n$ is divisible by $k$ and we show $k = n$.
This means $p-1$ is divisible by $n$ and the claim follows.

Assume $1 \leq t < n$. Then
\begin{align*}
  \frac{X^{n}-1}{X^{k}-1} = \Phi_n(X) \prod_{d} \Phi_d(X)
\end{align*}
where $d$ ranges over all true divisors of $n$ that do not divide $t$.
This means that
$\frac{n^{n}-1}{n^{k}-1}$ is divisble by $\Phi_n(n)$.

But $n$ is divisible by $k$, so
\begin{align*}
  \frac{n^{n}-1}{n^{k}-1} = \frac{(n^{k})^{n/k}}{n^{k}-1}= \underbrace{(n^{k})^{n/k - 1} + \ldots + 1}_{n/k \text{ times}}
  = n/t \mod p
\end{align*}

Since $p$ divides $\Phi_n(n)$ and thus $\frac{n^{n}-1}{n^{k}-1}$, it follows that $n/t$ and thus $n$ are divisible by $p$.
But
\begin{align*}
  \Phi_n(X) = X^{\phi_n(X)} + \ldots + 1
\end{align*}
which contradicts $\Phi(n) = 1 \mod p$.
\end{proof}


\begin{center}
Missing Second Half 28.05.21
\end{center}

