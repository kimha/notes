
\begin{thm}[Dominated Convergence Theorem]
  Let $f, (f_k)_{k \in \N}: \Omega \to  \overline{\R}$ $\mu$-measurable with $f_k \to f$ $\mu$-a.e..

  If there exists a $\mu$-summable function $g: \Omega \to [0,\infty]$ with $\abs{f_k} \leq g$ $\mu$-a.e., then
  \begin{align*}
    \lim_{k \to \infty} \int_{\Omega} \abs{f_k - k} d \mu = 0
  \end{align*}
  and in particular
  \begin{align*}
    \lim_{k \to \infty} \int_{\Omega} f_k d \mu = \int_{\Omega} f d \mu
  \end{align*}
\end{thm}
\begin{proof}
  First note from $\abs{f} = \lim_{k \to \infty} \abs{f_k} \leq g$ $\mu$-a.e. it follows
  \begin{align*}
    \abs{f_k - f} \leq \abs{f_k} + \abs{f} \leq 2g
  \end{align*}
  so $f,\abs{f_k - k}$ are $\mu$-summable and
  \begin{align*}
    \liminf_{k \to \infty} 2g - \abs{f_k - f} = 2g \quad \text{$\mu$-a.e.}
  \end{align*}
  so with Corollary \ref{cor:same-integral}, Fatou's Lemma and linearity:
  \begin{align*}
    \int_{\Omega} 2g d \mu
    &=
    \int_{\Omega} \liminf_{k \to \infty} (2g - \abs{f_k - f}) d \mu
    \\
    &\stackrel{\mathrm{Fatou}}{\leq}
    \liminf_{k \to \infty}
    \int_{\Omega} (2g - \abs{f_k - f}) d \mu
    \\
    &=
    \int_{\Omega} 2g d \mu - \limsup_{k \to \infty} \int_{\Omega} \abs{f_k - f} d \mu
  \end{align*}
  And since $\abs{f_k -f} \geq 0$, we have 
  \begin{align*}
    \lim_{k \to \infty} \int_{\Omega} \abs{f_k -f} d \mu = 0
  \end{align*}
  the second statement follows from linearity and the triangle inequality
  \begin{align*}
    0 \leq \abs{\int_{\Omega} f_k d \mu - \int_{\Omega} f d \mu} = \abs{\int_{\Omega} f_k - f d \mu} \leq \int_{\Omega} \abs{f_k -f} d \mu \stackrel{k \to \infty}{\to} 0
  \end{align*}
\end{proof}


We can also get a second proof of \ref{cor:reverse-convergence} 

\begin{proof}[Second Proof]
  \texttt{Skipped}
\end{proof}




\subsection{Absolute Continuity of Integrals}
Let $\mu$ be a Radon measure on $\R^{n}$ and $\Omega$ be $\mu$-measurable.

For any $\mu$-summable function $f: \Omega \to \overline{\R}$, we can define
\begin{align*}
  \nu: \Sigma_{\mu} \to \mathbb{R}, \quad
  A \mapsto \nu(A) := \int_{A} f d \mu
\end{align*}
where $\Sigma_{\mu}$ is the $\sigma$-algebra of $\mu$-measurable sets.

\begin{rem}[]
By Corollary \ref{cor:restriction-to-zero}, we have
\begin{align*}
  \mu(A) = 0 \implies \nu(A) = 0
\end{align*}
Moreover, if $f \geq 0$ and $\mu$ is a Radon measure, then $\nu$ is $\sigma$-additive and again a Radon measure.

We then write
\begin{align*}
  \nu := \mu \mres f
\end{align*}
this notation coincides with the restriction of measures to subsets we saw in Section \ref{sec:radon}, as
\begin{align*}
  \mu \mres \chi_{A} = \mu \mres A
\end{align*}
\end{rem}


\begin{dfn}[]
  Let $\Sigma_{\mu}, \Sigma_{\nu}$ denote the $\sigma$-algebra of $\mu$-(resp. $\nu$)-measurable subsets.
  A measure $\nu$ such that $\Sigma_{\mu} \subseteq \Sigma_{\nu}$ with property
  \begin{align*}
    \mu(A) = 0 \implies \nu(A) = 0
  \end{align*}
  is called \textbf{absolutely continuous} with respect to $\mu$ and we write $\nu \ll \mu$.
\end{dfn}

The name of the definition should be reminiscent of a definition from Analysis.
Let's see why:

\begin{thm}[] \label{thm:absolute-continuity}
Let $f: \Omega \to \overline{\R}$ be $\mu$-summable. Then $\forall \epsilon > 0\  \exists \delta > 0$ such that
\begin{align*}
  \forall A \in \Sigma_{\mu}: \quad \mu(A) < \delta \implies \int_{A} \abs{f} d \mu < \epsilon
\end{align*}
\end{thm}
\begin{proof}[Proof by contradiction]
  Assume that there exists an $\epsilon > 0$ and a sequence of $\mu$-measurable subsets $A_k \subseteq \Omega$ with $\mu(A_k) < 2^{-k}$ that satisfy
  \begin{align*}
    \int_{A_k} \abs{f} d \mu \geq \epsilon \quad \forall k \in \N
  \end{align*}
  for $m \geq 1$ we define
  \begin{align*}
    B_m := \bigcup_{k=m}^{\infty}A_k
  \end{align*}
  This forms a decreasing sequence and by subadditivity: $\mu(B_m) \leq \sum_{k=m}^{\infty} < 2^{1-m}$ which means $\mu(B_1) < \infty$ and
  \begin{align*}
    \int_{B_m} \abs{f} d \mu \geq \int_{A_k} \abs{f} d \mu \geq \epsilon, \forall  k \geq m
  \end{align*}

  By continuity from above, for $B := \bigcup_{l=m}^{\infty}B_m$ we have $\mu(B) = \lim_{m \to \infty} \mu(B_m) = 0$.
  And 
  \begin{align*}
    f_m := \abs{f} \chi_{B_m} \implies \lim_{m \to \infty} f_m = \abs{f}\chi_{B}
  \end{align*}
  Since $\abs{f_k} \leq \abs{f}$ and $\abs{f}$ is $\mu$-summable, we can use the dominated convergence theorem to show
  \begin{align*}
    \epsilon \geq \lim_{m \to \infty} \int_{B_m} \abs{f} d \mu = \lim_{m \to \infty} \int_{\Omega} f_m d \mu \stackrel{\text{DCT}}{=} \int_{\Omega} \lim_{m \to \infty} f_m d \mu = \int_{A} \abs{f} d \mu = 0 \lightning
  \end{align*}
\end{proof}


