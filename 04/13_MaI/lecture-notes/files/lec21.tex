
\subsection{Vitali's Theorem}
Lebesgue's Theorem gives us a sufficient condition to pass exchange limits and integrals.

In this chapter, we will improve it and give a necessary condition to exchange limits and integrals.

In this section, let $f, f_k: \Omega \to \overline{\R}$ be $\mu$-summable.


\begin{dfn}[]
  The family $\{f_k\}_{k \in \N}$ is called \textbf{uniformly $\bm{\mu}$-summable}, if $\forall \epsilon > 0\ \exists \delta > 0: \forall k \in \N, \forall A \subseteq \Omega$ $\mu$-measurable:
  \begin{align*}
    \mu(A) < \delta \implies \int_{A} \abs{f_k} d \mu < \epsilon
  \end{align*}
\end{dfn}

% Consider reformulating it to say that (b) implies (a) always, and if $\mu(\Omega) < \infty$, the converse also holds.

\begin{thm}[Vitali's Theorem]
  Let $f,(f_k)_{k \in \N}: \Omega \to \overline{\R}$.
  If $\mu(\Omega) < \infty$, the following are equivalent:
  \begin{enumerate}
    \item $\{f_k\}_{k \in \N}$ is uniformly $\mu$-summable and $f_k \stackrel{\mu}{\to} f$.
    \item $\lim_{k \to \infty} \int_{\Omega} \abs{f_k -f} d \mu = 0$
  \end{enumerate}
\end{thm}
\begin{proof}
  \phantom{a}
\begin{itemize}
  \item[(b) $\implies$ (a)]
    If $\int_{\Omega} \abs{f_k -f} d \mu \to 0$, by Corollary \ref{cor:reverse-convergence} it follows $f_k \stackrel{\mu}{\to} f$.
    To show that $\{f_k\}_{k \in \N}$ is uniformly $\mu$-summable, let $\epsilon > 0$.

    By our assumption (b), there exist a $k_0$ such that
    \begin{align*}
      \int_{\Omega} \abs{f_k - f} d \mu < \epsilon \quad \forall k \geq k_0
    \end{align*}
    by the previous theorem (Theorem \ref{thm:absolute-continuity}), there exists a $\delta > 0$ such that $\forall A \subseteq \Omega$ $\mu$-measurable
    \begin{align*}
      \mu(A) < \delta \implies \int_{A} \abs{f} d \mu < \epsilon \quad \text{and} \quad \max_{1 \leq k \leq k_0}\int_{A} \abs{f_k} d \mu < \epsilon
    \end{align*}
    and for the remaining $k \geq k_0$, if $\mu(A) < \delta$, we get
    \begin{align*}
      \int_{A} \abs{f_k} d \mu \leq \int_{A}  \abs{f} d \mu + \int_{A} \abs{f_k - f} d \mu \leq 2 \epsilon
    \end{align*}
  which shows that $\{f_k\}_{k \in \N}$ is uniformly $\mu$-summable.

  \item[(a) $\implies$ (b)]
    We first show that (b) holds for some subsequence $(f_{k_n})_{n \in \N}$, and then show it for the entire sequence.

    By Theorem \ref{thm:subsequence-muae}, $f_k \stackrel{\mu}{\to} f$ means that there exists a subsequence $(f_{k_n})_{n \in \N}$ that converges to $f$ $\mu$-a.e.

    By assumption (a), for all $\epsilon > 0$ there exists a $\delta > 0$ such that 
    \begin{align*}
      \mu(A) < \delta \implies \int_{A} \abs{f} d \mu < \epsilon \quad \text{and} \quad \int_{A} \abs{f_k} d \mu < \epsilon, \quad \forall k \in \N
    \end{align*}
    Feeding $\delta$ into Egoroff's Theorem, we get a $B \subseteq \Omega$ with
    \begin{align*}
      \mu(\Omega \setminus B) < \delta \quad \text{and} \quad \sup_{x \in B} \abs{f_{k_n}(x) - f(x)} \to 0 \text{ as } n \to \infty
    \end{align*}
    Now we can bound the integral $\int_{\Omega} \abs{f_{k_n} - f} d \mu$.
    Because the above limit approaches limit, set $n_0 \in \N$ such that
    \begin{align*}
      \sup_{x \in B} \abs{f_{k_n}(x) - f(x)} < \frac{\epsilon}{\mu(\Omega)}
    \end{align*}
    Thus, for all $n \geq n_0$, we have
    \begin{align*}
      \int_{\Omega} \abs{f_{k_n}-f} d \mu
      &=
      \int_{\Omega \setminus B} \abs{f_{k_n}-f} d \mu
      +
      \int_{B} \abs{f_{k_n}-f} d \mu
      \\
      &\leq
      \int_{\Omega \setminus B} \abs{f_{k_n}} + \abs{f} d \mu + 
      \int_{B} \sup_{x \in B} \abs{f_{k_n} - f} d \mu
      \\
      &< 2 \epsilon + \frac{\epsilon}{\mu(\Omega)} \mu(B) \leq 3 \epsilon
    \end{align*}
    Letting $\epsilon \to 0$, we find
    $
      \lim_{n \to \infty} \int_{\Omega} \abs{f_{k_n}-f} d \mu = 0
    $.

    We will prove by contradiction that it also holds for the entire sequence.

    Suppose $\limsup_{k \to \infty}\int_{\Omega} \abs{f_k -f} d \mu > 0$.
    Then there exists a subsequence $(f_{k_n})_{n \in \N}$ such that
    \begin{align*}
      \lim_{n \to \infty} \int_{\Omega} \abs{f_{k_n} - f} d \mu = \limsup_{k \to \infty} \int_{\Omega} \abs{f_k - f} d \mu > 0
    \end{align*}
    But nonetheless, $\{f_{k_n}\}_{n \in \N}$ is still uniformly $\mu$-summable and $f_{k_n} \stackrel{\mu}{\to} f$ as $n \to \infty$, repeating the argument from before, there exists a sub-subsequence $(f_{k_{n'}})_{n' \in \N}$ such that
    \begin{align*}
      \lim_{n' \to \infty} \int_{\Omega} \abs{f_{k_{n'}}-f} d \mu = 0
    \end{align*}
    which contradicts the previous equation.
\end{itemize}
\end{proof}

\begin{rem}[] 
  Since the proof in (a) $\implies$ (b) relied on Egoroff's Theorem, the condition $\mu(\Omega) < \infty$ is necessary.
  The other implication always holds.

  Consider the sequence given by $f_k = \frac{1}{k} \chi_{[0,k]}$. 
  Clearly, $f_k \stackrel{\mu}{\to} \bm{0}$, but
  \begin{align*}
    \int_{\R} \abs{f_k - \bm{0}} d \mu = \int_{[0,k]} \frac{1}{k} d \mu = 1 \neq 0
  \end{align*}
\end{rem}


As promised earlier, we can now improve Lebesgue's Theorem.

\begin{center}
\texttt{Missing 30 minutes}
\end{center}
\begin{thm}[] \label{thm:dct-improved}
\end{thm}





\subsection{$L^{p}(\Omega,\mu)$ spaces}

Again, let $\mu$ be a Radon measure on $\R^{n}$ and $\Omega$ $\mu$-measurable.


Recall that the supremum/infimum of a set are defined in terms of upper/lower bounds.
\begin{align*}
  \sup X := \min\{a \in \R \big\vert a \geq x, \forall x \in X\}
\end{align*}
When talking about measure spaces, we want to ignore measure-zero sets:
\begin{dfn}[]
Let $X \subseteq \R$. We say that $a \in \R$ is a \textbf{$\bm{\mu}$-essential upper bound} of $X$, if
\begin{align*}
  \mu\left(
    \{x \in X \big\vert x > a\}
  \right)
  = 0
\end{align*}
Similar to the defintion of $\sup$, define the \textbf{$\bm{\mu}$-essential supremum} to be smallest $\mu$-essential upper bound
\begin{align*}
  \mu-\esssup X := \inf \{a \in \R \big\vert \mu \left(\{x \in X \big\vert x > a\}\right) = 0\}
\end{align*}
  Analogously, one can define the $\mu$-essential infimum.
\end{dfn}






\begin{dfn}[]
  Let $f: \Omega \to \overline{\R}$ be $\mu$-measurable.
  For $1 \leq p < \infty$, define their \textbf{$\bm{p}$-norm}
  \begin{empheq}[box=\bluebase]{align*}
    \|f\|_{L^{p}(\Omega,\mu)} := \left(
      \int_{\Omega} \abs{f}^{p} d \mu
    \right)^{1/p}
    \leq \infty
  \end{empheq}
  and for $p = \infty$:
  \begin{empheq}[box=\bluebase]{align*}
    \|f\|_{L^{\infty}(\Omega,\mu)} :=
    \mu-\esssup_{x \in \Omega} \abs{f(x)}
    =
    \inf \left\{
      c\in [0,\infty] \big\vert \abs{f} \leq c \text{$\mu$-a.e.}
    \right\}
  \end{empheq}

  We call the space of functions, for which their $p$-norm is finite the $\bm{\mathcal{L}^{p}}$-space
  \begin{empheq}[box=\bluebase]{align*}
    \mathcal{L}^{p}(\Omega,\mu) := \{f: \Omega \to \overline{\R} \big\vert f \text{$\mu$-measurable, } \|f\|_{L^{p}(\Omega,\mu)} < \infty\}
  \end{empheq}
\end{dfn}

If $\Omega$ and $\mu$ are clear from context, we will write
$\|f\|_{L^{p}}$ or even $\|f\|_p$\footnote{The lecturer never used this, but I will.}instead of $\|f\|_{L^{p}(\Omega,\mu)}$.

We call a function $f$ \textbf{$p$integrable}, if $\|f\|_p < \infty$.\footnote{This was not used by the lecturer, but this was used in MMP I, for example.}


\begin{rem}[] \label{rem:abs-leq-infty-norm}
  For $f \in \Leb^{p}(\Omega,\mu)$ we have
  $\abs{f(x)} \leq \|f\|_{\infty}$ for $\mu$-almost all $x \in \Omega$
\end{rem}
\begin{proof}[]
\texttt{Missing}
\end{proof}


\begin{rem}[]
  The space $\Leb^{p}(\Omega,\mu)$ is not necessarily closed under multiplication.
  Set $p = 1$, $\Omega = (0,1]$ and $\mu= \Leb^{1}$ and
  \begin{align*}
    f(x) = g(x) = \frac{1}{\sqrt{x}} \in \Leb^{1}(\Omega,\mu)
  \end{align*}
  then their product is $(fg)(x) = \frac{1}{x}$, which is not $\Leb^{1}$-integrable on $(0,1]$.


\end{rem}
Moreover, $\|\cdot\|_p$ is not a norm on $\Leb^{p}(\Omega,\mu)$ as it is not positive definite.

To remedy this, we consider equivalence classes of functions in $\Leb^{p}(\Omega,\mu)$ where two functions are considered equivalent if and only if they are equal $\mu$-a.e..

\begin{thm}[]\label{thm:lp-is-banach-space}
  Let $L^{p}(\Omega,\mu)$ denote the quotient space $\Leb^{p}(\Omega,\mu)/\sim$, where
  \begin{align*}
    f \sim g \iff f=g \text{ $\mu$-a.e.}
  \end{align*}

  Then $L^{p}$ with norm $\|\cdot\|_p$ is a Banach space. That is: a complete, and normed vector space.
\end{thm}
For now, we will only prove that prove positive homogeneity.

The rest will be split into multiple Lemmas and Theorems.

\begin{proof}[Proof]
  \texttt{Missing}
\end{proof}



\begin{lem}[Young Inequality]
  Let $1 < p,q < \infty$ such that $\frac{1}{p} + \frac{1}{q} = 1$. (We say that $p,q$ are \textbf{conjugated}).
  Then
  \begin{align*}
    \forall  a,b \geq 0 \quad ab \leq \frac{a^{p}}{p} + \frac{b^{q}}{q}
  \end{align*}
\end{lem}
\begin{proof}
If $b$ is zero, it's trivial.
Fix $b > 0$ and
consider the map
\begin{align*}
  f: [0,\infty) \to \R, \quad a \mapsto ab - \frac{a^{p}}{p}
\end{align*}
Because $\lim_{a \to \infty} f(a) = -\infty$, $f$ is bounded from above and must have a maximum at $a = a^{\ast}$ determined by $f'(a^{\ast}) = b- a^{p-1} = 0$.

After checking the second derivative, 
one finds that $a^{\ast} = b^{\frac{1}{p-1}}$ is indeed a maximum of $f$. So $\forall a \geq 0$
\begin{align*}
  ab - \frac{a^{p}}{p} = f(a) \leq f(a^{\ast}) = b^{\frac{1}{p-1}}b - \frac{b^{\frac{p}{p-1}}}{p} = \left(
    1 - \frac{1}{p}
  \right)
  b^{\frac{p}{p-1}}
  = 
  \frac{b^{q}}{q}
\end{align*}
\end{proof}


\begin{cor}[Hölder Inequality]
  Let $1\leq p,q \leq \infty$ be conjugate, $f \in L^{p}(\Omega,\mu), g \in L^{q}(\Omega,\mu)$.
  Then $fg \in L^{1}(\Omega,\mu)$ and
  \begin{align*}
    \|fg\|_1 \leq \|f\|_p \|g\|_q
  \end{align*}
\end{cor}
\begin{proof}
WLOG we can assume $p \leq q$.
\begin{itemize}
  \item Case $p=1,q=\infty$. By Remark \ref{rem:abs-leq-infty-norm}, we have
    $\abs{fg} \leq \abs{f} \|g\|_{\infty}$ $\mu$-a.e., so by monotonicity of the integral
    \begin{align*}
    \int_{\Omega} \abs{fg} d \mu \leq \int_{\Omega} \abs{f} \|g\|_{\infty} d \mu \leq \|g\|_{\infty} \underbrace{\int_{\Omega} \abs{f} d \mu}_{=\|f\|_1}
    \end{align*}
  \item Case $1 < p,q < \infty$.
    If $\|f\|_p=0$ or $\|g\|_q=0$, we can use Proposition \ref{prop:integral-and-zero} and we get zeros on both sides of the equality.
    So assume $\|f\|_p, \|g\|_q > 0$.

    Set $\tilde{f} := \frac{\abs{f}}{\|f\|_p}$ and $\tilde{g} := \frac{\abs{g}}{\|g\|_q}$
    Notice that
    \begin{align*}
      \int_{\Omega} \tilde{f}^{p} d \mu  = \frac{1}{(\|f\|_p)^{p}}\int_{\Omega} \abs{f}^{p}d \mu = \frac{(\|f\|_p)^{p}}{(\|f\|_{p})^{p}} = 1
    \end{align*}
    and likewise $\int_{\Omega} \tilde{g}^{q} d \mu = 1$.
    Apply the Young inequality to $\tilde{f}(x)$ and $\tilde{g}(x)$. We get
    \begin{align*}
      \tilde{f} \tilde{g} \leq \frac{\tilde{f}^{p}}{p} + \frac{\tilde{g}^{q}}{q}
    \end{align*}
    taking the integal on both sides:
    \begin{align*}
      \frac{1}{\|f\|_p \|g\|_q} \underbrace{\int_{\Omega} \abs{fg} d \mu}_{=\|fg\|_1}
      =
      \int_{\Omega}
      \tilde{f} \tilde{g} d \mu \leq \frac{1}{p} \int_{\Omega} \tilde{f}^{p} d \mu + \frac{1}{q} \int_{\Omega} \tilde{g}^{q} d \mu
      = \frac{1}{p} + \frac{1}{q} = 1
    \end{align*}
    and the proof follows.
\end{itemize}
\end{proof}

Say we know that a function $f: \Omega \to \overline{\R}$ lives in some $L^{p}$-space.
It would be nice to know if $f\in L^{r}$ for some other $1 \leq r \leq \infty$.

The next Corollary of the Hölder inequality shows that the different $L^{p}$-spaces are nested inside eachother.

\begin{cor}[] \label{cor:stronger-hoelder}
  \phantom{a}
  \begin{enumerate}
    \item Let $f \in L^{p}, g \in L^{q}$ with $\frac{1}{r} = \frac{1}{p} + \frac{1}{q} \leq 1$.
      Then $fg \in L^{r}$ and $\|fg\|_r \leq \|f\|_p \|g\|_q$.
    \item If $\mu(\Omega) < \infty$, then
      \begin{align*}
        1 \leq r \leq s \leq \infty \implies L^{s}(\Omega,\mu) \subseteq L^{r}(\Omega,\mu)
      \end{align*}
  \end{enumerate}
\end{cor}
\begin{proof}
  \phantom{a}
  \begin{enumerate}
    \item 
  If $p=q=r = \infty$, it's clear. If $r < \infty$, apply the Hölder inequality to $\abs{f}^{r} \in L^{p/r}$ and $\abs{g}^{r} \in L^{q/r}$.
  We can do this because $\frac{1}{p/r} + \frac{1}{q/r} = 1$.

    \item 
      If $\mu(\Omega) < \infty$, the function $\bm{1}$ is $p$-integrable for any $1 \leq p \in \infty$ as it has integral $\mu(\Omega)$.
      Then note that $\frac{1}{r} = \frac{1}{s} + \frac{s-r}{rs}$. So appying (a), we get
      \begin{align*}
        f \in L^{s}(\Omega,\mu) 
        \implies 
        \|f\|_r = \| f\cdot \bm{1}\|_r
        \stackrel{(a)}{\leq}\|f\|_s \cdot \underbrace{\|\bm{1}\|_{\frac{rs}{s-r}}}_{= \mu(\Omega)} < \infty
      \end{align*}
      which shows $f \in L^{r}(\Omega,\mu)$
  \end{enumerate}
\end{proof}

\begin{rem}[]
  In general, the inclusion in (b) is strict in the sense that there are functions in $L^{r}$ that do not belong to $L^{s}$ with $r < s$.

  A simple example is
  \begin{align*}
    \Omega = (0,1), \quad f(x) = \log \frac{1}{x} \in L^{p}, f \notin L^{\infty}
  \end{align*}
\end{rem}


In our quest to prove $L^{p}(\Omega,\mu)$ is a Banach Space, we still need to show the Triangle inequality and closure under addition:
\begin{cor}[Minkowski Inequality]
  Let $1 \leq p \leq \infty$ and $f,g \in L^{p}(\Omega,\mu)$. Then $f + g \in L^{p}$ and 
  \begin{align*}
    \|f+g\|_{p} \leq \|f\|_p + \|g\|_p
  \end{align*}
\end{cor}
\begin{proof}
The cases $p = 1$ and $p = \infty$ are easy:
\begin{align*}
  \abs{(f+g)(x)} \leq \abs{f(x)} + \abs{g(x)} \leq \|f\|_{\infty} + \|g\|_{\infty}
\end{align*}
For $1 < p < \infty$, we use that the function $x \mapsto x^{p}$ is convex. In particular:
\begin{align*}
  \left(\frac{a+b}{2}\right)^{p}
  \leq \frac{a^{p} + b^{p}}{2}
\end{align*}
Setting $a = 2f(x)$ and $b = 2g(x)$, we get the estimate
\begin{align*}
  \abs{(f+g)(x)}^{p} \leq 2^{p-1} \left(
    \abs{f(x)}^{p} + \abs{g(x)}^{p}
  \right), \quad \forall x \in \Omega
\end{align*}
This shows that $f+g \in L^{p}$ and $\abs{f+g}^{p-1} \in L^{\frac{p}{p-1}}$.
\end{proof}


\begin{center}
\texttt{Missing until rest of chapter}
\end{center}
