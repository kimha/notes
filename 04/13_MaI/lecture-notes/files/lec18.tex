\begin{lem}[]\label{lem:linearity-simple-integral}
  This Lemma is taken from Exercise Sheet 09.
  \begin{enumerate}
    \item For two $\mu$-integrable simple functions $f,g$,
      there exist sequences
      $\left\{a_n\right\}_{n \in \N}, \left\{b_n\right\}_{n \in \N} \subseteq \overline{\R}$,
      with
      $\mu$-measurable, mutually disjoint sets
      $(C_n)_{n \in \N}$
      such that
      \begin{align*}
        f = \sum_{n \in \N}a_n \chi_{C_n}, \quad g = \sum_{n \in \N}b_n \chi_{C_n}
      \end{align*}
    \item For a $\mu$-integrable (simple) function of the form $f = \sum_{n \in \N} a_n \chi_{A_n}$ for $A_{n}$ pairwise disjoint and $\mu$-measurable, it holds
      \begin{align*}
        \int_{\Omega} f d \mu = \sum_{n \in \N} a_n \mu(A_n) 
      \end{align*}
    %%%
    \item Let $f,g: \Omega \to [-\infty,\infty]$ be $\mu$-summable simple functions, $a,b \in \R$. Then
      $af + bg$ is a $\mu$-summable simple function and
      \begin{align*}
        \int_{\Omega} (af + bg) d \mu = a\int_{\Omega} f d \mu + b\int_{\Omega} g d \mu
      \end{align*}
  \end{enumerate}
\end{lem}

\begin{proof}[Proof Sketch]
  For a detailed proof, see Master solution of Exercise Sheet 09.
  \begin{enumerate}
    \item Since the images of $f$ and $g$ are countable, let $(a_n)_{n \in \N}$, $(b_n)_{n \in  \N}$ be their values.
      Set $A_n := f^{-1}\{a_n\}, B_n := g^{-1}\{b_n\}$. 
      By taking all intersections of the form $C_{ij} := A_i \cap B_j$ and reindexing them (for example, with Cantor's Diagonal map) we get a sequence  $(C_m)_{m \in \N}$ of disjoint, $\mu$-measurable sets.
    \item Although the values $(a_n)_{n \in \N}$ might not be necessarily be different, we can take the union of all $A_m$ corresponding to a value $c_m \in \overline{\R}$ to write
      \begin{align*}
        f^{-1}\{c_m\} = \bigcup_{n \in \N: a_n = c_m} A_n
      \end{align*}
      using the fact that the $A_n$ are $\mu$-measurable and disjoint 
      and by definition of the integral for simple functions, we have
      \begin{align*}
        \int_{\Omega} f d \mu
        =
        \sum_{m \in \N} c_m \mu\left(
          f^{-1}\{c_m\}
        \right)
        =
        \sum_{m \in \N} c_m \mu\left(
          \bigcup_{n: a_n = c_m}A_n
        \right)
        =
        \sum_{m \in \N} c_m \sum_{n\in \N: a_n = c_m} \mu(A_n) 
        = 
        \sum_{n \in \N} a_n \mu(A_n)
      \end{align*}
  %%%
    \item From part (a), we can find $\mu$-measurable subsets $C_n$ and sequences $a_n,b_n$ such that
      \begin{align*}
        f = \sum_{n \in \N}a_n \chi_{C_n}, \quad g = \sum_{n \in \N} b_n \chi_{C_n}
      \end{align*}
      Since the $C_n$ are mutually disjoint,
      the function $af + bg$, can be written as
      \begin{align*}
        af + bg = \sum_{n \in \N} (aa_n + bb_n)\chi_{C_n}
      \end{align*}
      which shows that $a f + bg$ is a simple function because the $C_n$ are disjoint.
      By (b), we get
      \begin{align*}
        \int_{\Omega} (af + bg) d \mu = \sum_{n \in \N} (aa_n + bb_n) \mu(C_n) = a\sum_{n \in \N}a_n \mu(C_n) + b\sum_{n \in \N} b_n \mu(C_n) = a\int_{\Omega} f d \mu + b\int_{\Omega} g d \mu
      \end{align*}

      To show that $a f + bg$ is $\mu$-summable, apply additivity to $\abs{f},\abs{g}$ and use the triangle inequality:
      \begin{align*}
        \int_{\Omega} \abs{f+g} d \mu
        \leq
        \int_{\Omega} \abs{f} + \abs{g} d \mu
        = \int_{\Omega} \abs{f} d \mu + \int_{\Omega} \abs{g} d \mu < \infty
      \end{align*}
  \end{enumerate}
\end{proof}
\begin{rem}[]
  We can actually weaken the condition on $f,g$.
  The reason we used that $f,g$ are $\mu$-summable in the first place was so that the right hand side of the equation
  \begin{align*}
    \int_{\Omega} (af + bg) d \mu = a \int_{\Omega} f d \mu + b \int_{\Omega} g d \mu
  \end{align*}
  is well defined. This becomes more clear when we write out 
      \begin{align*}
        a \int_{\Omega} f d \mu + b \int_{\Omega} g d \mu
        =
        a\left[
          \int_{\Omega} f^{+} d \mu
          - \int_{\Omega} f^{-} d \mu
        \right]
        + b \left[
          \int_{\Omega} g^{+} d \mu
          - \int_{\Omega} g^{-} d \mu
        \right]
      \end{align*}
      We can see that it is enough to require that either
      \begin{align*}
        \int_{\Omega} f^{+} d \mu < \infty \quad &\text{and} \quad  
        \int_{\Omega} g^{+} d \mu < \infty\\
        \text{or}\quad
        \int_{\Omega} f^{-} d \mu < \infty \quad &\text{and} \quad  
        \int_{\Omega} g^{-} d \mu < \infty
      \end{align*}
      which is the case when $f,g$ are $\mu$-summable.
      Of course, that means that $af + bg$ may no longer be $\mu$-summable.
\end{rem}


\begin{thm}[] \label{thm:linearity-integral}
  Let $f,g: \Omega\to \overline{\R}$ be $\mu$-summable, $\lambda \in \R$. Then
  \begin{enumerate}
    \item $f+g$ is $\mu$-summable and
  \begin{align*}
    \int_{\Omega}(f + g) d \mu 
    =
    \int_{\Omega} f d \mu + \int_{\Omega} g d \mu 
  \end{align*}
  \item $\lambda f$ is $\mu$-summable and
      
  \begin{align*}
    \int_{\Omega} \lambda f d \mu
    =
    \lambda \int_{\Omega} f d \mu
  \end{align*}
\end{enumerate}
\end{thm}
\begin{proof}
  \begin{enumerate}
    \item 
  Let $f,g$ as above.
  %From the last chapter (Theorem \ref{thm:measurable-operation}), we know that $f+g$ is $\mu$-measurable.
  Then for any $\epsilon > 0$, we can choose simple $\mu$-integrable functions $f_{\epsilon},f^{\epsilon},g_{\epsilon},g^{\epsilon}$ such that
  $f_{\epsilon} \leq f\leq f^{\epsilon}, g_{\epsilon} \leq g \leq g^{\epsilon}$ $\mu$-a.e.
  and
  \begin{align*}
    \int_{\Omega} f^{\epsilon} d \mu - \int_{\Omega} f d \mu &< \epsilon
    \quad
    \int_{\Omega} f d \mu - \int_{\Omega} f_{\epsilon} d \mu < \epsilon\\
    \int_{\Omega} g^{\epsilon} d \mu - \int_{\Omega} g d \mu &< \epsilon
    \quad
    \int_{\Omega} g d \mu - \int_{\Omega} g_{\epsilon} d \mu < \epsilon
  \end{align*}
  %%%
  and since $f,g$ are $\mu$-summable, and $f \leq f^{\epsilon} \implies (f^{\epsilon})^{-} \leq f^{-}$, we get
  \begin{align*}
    \int_{\Omega} (f_{\epsilon})^{+} \leq \int_{\Omega} f^{+} d \mu \leq \int_{\Omega} \abs{f} d \mu < \infty\\
    \int_{\Omega} (f^{\epsilon})^{-} \leq \int_{\Omega} f^{-}d \mu \leq \int_{\Omega} \abs{f} d \mu < \infty
  \end{align*}
  Ditto for $(g_{\epsilon})^{+}$ and $(g^{\epsilon})^{-}$.
  By the previous Lemma (using the weakened condition), it follows that $f_{\epsilon}+g_{\epsilon}$ and $f^{\epsilon} + g^{\epsilon}$ are $\mu$-integrable and 
  \begin{align*}
    \int_{\Omega} (f^{\epsilon} + g^{\epsilon}) d \mu 
    &= 
    \int_{\Omega} f^{\epsilon} d \mu + \int_{\Omega} g^{\epsilon} d \mu\\
    \int_{\Omega} (f^{\epsilon} + g^{\epsilon}) d \mu 
    &= 
    \int_{\Omega} f_{\epsilon} d \mu + \int_{\Omega} g_{\epsilon} d \mu
  \end{align*}
  which gives us the estimate
  \begin{align*}
    \int_{\Omega} f d \mu + \int_{\Omega} g d \mu - 2 \epsilon 
    &\leq
    \int_{\Omega} f_{\epsilon} d \mu + \int_{\Omega} g_{\epsilon} d \mu
    =
    \int_{\Omega} (f_{\epsilon} + g_{\epsilon}) d \mu
    \leq 
    \lowint_{\Omega} (f + g) d \mu
    \\
    &\leq
    \upint_{\Omega} (f + g) d \mu
    \leq 
    \int_{\Omega} (f^{\epsilon} + g^{\epsilon}) d \mu
    =
    \int_{\Omega} f^{\epsilon} d \mu + \int_{\Omega} g^{\epsilon} d \mu
    \\
    &\leq
    \int_{\Omega} f d \mu + \int_{\Omega} g d \mu + 2 \epsilon
  \end{align*}
  Letting $\epsilon \to 0$, it follows that $(f + g)$ is $\mu$-integrable with
  \begin{align*}
    \int_{\Omega} (f + g) d \mu = \int_{\Omega} f d \mu + \int_{\Omega} g d \mu
  \end{align*}

  To show that $(f+g)$ is $\mu$-summable, we apply the previous result to $\abs{f}$ and $\abs{g}$ with the triangle inequality:
  \begin{align*}
    \int_{\Omega} \abs{f+g} d \mu
    \leq
    \int_{\Omega} \abs{f} + \abs{g} d \mu
    \leq
    = \int_{\Omega} \abs{f} d \mu
    + \int_{\Omega} \abs{g} d \mu
    < \infty
  \end{align*}

  % lambda f 
\item Let $\lambda \in \R$. For all $\epsilon > 0$, we can again find $\mu$-integrable simple functions $f_{\epsilon} \leq f \leq f^{\epsilon}$ with
  \begin{align*}
    \int_{\Omega} f^{\epsilon} d \mu - \int_{\Omega} f d \mu < \epsilon \quad \text{and} \quad
    \int_{\Omega} f d \mu - \int_{\Omega} f_{\epsilon} d \mu < \epsilon
  \end{align*}
  \begin{itemize}
    \item If $\lambda = 0$, then it's trivial.

    \item If $\lambda \geq 0$, then
      \begin{align*}
        \lambda f_{\epsilon} \leq \lambda f \leq \lambda f^{\epsilon}
      \end{align*}
      and using the previous Lemma, we can make the estimates
      \begin{align*}
        \lambda \int_{\Omega} f d \mu - \lambda \epsilon 
        &\leq 
        \lambda \int_{\Omega} f_{\epsilon} d \mu 
        \leq 
        \lowint_{\Omega} (\lambda f) d \mu\\
        &\leq 
        \upint_{\Omega} (\lambda f) d \mu
        \leq 
        \int_{\Omega} \lambda f^{\epsilon} d \mu
        =
        \lambda \int_{\Omega} f^{\epsilon} d \mu
        \\
        &\leq
        \lambda \int_{\Omega} f d \mu + \lambda \epsilon
      \end{align*}
      and thus $\lambda f$ is $\mu$-integrable with
      \begin{align*}
        \int_{\Omega} (\lambda f) d \mu = \lambda \int_{\Omega} f d \mu
      \end{align*}
      and applying this result to $\abs{\lambda f}$, we get
      \begin{align*}
        \int_{\Omega} \abs{\lambda f} d \mu = \int_{\Omega} \abs{\lambda} \abs{f} d \mu = \abs{\lambda} \int_{\Omega} \abs{f} d \mu < \infty
      \end{align*}
      which shows that $\lambda f$ is $\mu$-summable.
    \item If $\lambda < 0$, then we have
      $
        \lambda f_{\epsilon} \geq \lambda f \leq \lambda f^{\epsilon} 
      $
      and the proof is analogous to the case $\lambda > 0$.
  \end{itemize}
\end{enumerate}
\end{proof}


\begin{cor}[Continuous triangle inequality]
Let $f: \Omega \to \overline{\R}$ be $\mu$-summable. Then
\begin{align*}
  \abs{\int_{\Omega} f d \mu} \leq \int_{\Omega} \abs{f} d \mu
\end{align*}
\end{cor}
\begin{proof}
  From the previous theorem and monotonicity, it follows from $- \abs{f} \leq f \leq \abs{f}$ that
  \begin{align*}
    - \int_{\Omega} \abs{f} d \mu = \int_{\Omega} - \abs{f} d \mu \leq \int_{\Omega} f d \mu \leq \int_{\Omega} \abs{f} d \mu
  \end{align*}
\end{proof}


Given a function $f: \Omega \to \overline{\R}$, there are two ways to restrict a function to a subset $A \subseteq \Omega$.


