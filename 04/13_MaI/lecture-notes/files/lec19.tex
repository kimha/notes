
\begin{lem}[] \label{lem:restriction-integral}
Let $f: \Omega \to \overline{\R}$ be $\mu$-summable and $A \subseteq \Omega$ $\mu$-measurable.

Then $f|_{A}$ and $f \chi_{A}$ are $\mu$-summable (on $A$ and $\Omega$) and
\begin{align*}
  \int_{A} f|_{A} d \mu = \int_{\Omega} f \chi_{A} d \mu
\end{align*}
\end{lem}
\begin{proof}
  We first show this for simple functions and then generalize from there.
  \begin{itemize}
    \item Let $g: \Omega \to \overline{\R}$ be a $\mu$-summable \emph{simple} function.
      By Lemma \ref{lem:linearity-simple-integral}, there exist mutually disjoint, $\mu$-measurable sets $(A_i)_{i \in I}$ with values $(a_i)_{i \in \N}$ such that $g = \sum_{i \in \N} a_i \chi_{A_i}$.
      Moreover, we know (by the same Lemma) that the integrals of $g|_{A}$ and $g \chi_{A}$ are
      \begin{align*}
        g|_{A} = \sum_{i \in \N} a_i \chi_{A_i \cap A}
        \quad
        \implies \int_{A} g|_{A} d \mu &= \sum_{i \in \N} a_i \mu(A_i \cap A)
        \\
        g \chi_{A}  = \sum_{i \in \N} a_i \chi_{A_i} \chi_{A} = \sum_{i \in \N} a_i \chi_{A_i \cap A}
        \quad
        \implies \int_{\Omega} g \chi_{A} d \mu &= \sum_{i \in \N} a_i \mu(A_i \cap A)
      \end{align*}

      And they are $\mu$-summable 
      because $\mu(A_i \cap A) \leq \mu(A_i)$ and $\abs{g \chi_{A}} \leq \abs{g}$.
    \item Let $f: \Omega \to  \overline{\R}$ $\mu$-summable.
      For all $\epsilon > 0$ we can find simple $\mu$-integrable functions $g,h$ such that $g \leq f \leq h$ $\mu$-a.e. (and thus also $g|_{A} \leq f|_{A} \leq h|_{A}$ $\mu$-a.e.)
      such that
      \begin{align*}
        \int_{\Omega} h d \mu - \epsilon \leq
        \int_{\Omega} f d \mu \leq \int_{\Omega} g d \mu + \epsilon
      \end{align*}
      To show that $f|_{A}$ is $\mu$-integrable, we can check (using linearity of the integral) that
      \begin{align*}
        0
        &\leq
        \upint_{A} f|_{A} d \mu - \lowint_{A} f|_A d \mu 
        \leq 
        \int_{A} h|_A d \mu - \int_{A} g|_A d \mu
        \\
        &=
        \int_{\Omega} h \chi_A d \mu - \int_{\Omega} g \chi_A d \mu
        =
        \int_{\Omega} (h-g) \chi_A d \mu
        \leq
        \int_{\Omega} (h-g) d \mu
        \\
        &=
        \int_{\Omega} h d \mu - \int_{\Omega} g d \mu 
        \leq 
        \int_{\Omega} f d \mu - \int_{\Omega} f d \mu + 2 \epsilon 
        = 
        2 \epsilon
      \end{align*}
      Letting $\epsilon \to 0$ shows that $f|_A$ is $\mu$-integrable.

      To show that $f \chi_A$ is $\mu$-integrable, check
      \begin{align*}
        0
        &\leq
        \upint_{\Omega} f \chi_A d \mu - \lowint_{\Omega} f \chi_A d \mu
        \leq
        \int_{\Omega} h \chi_A d \mu - \int_{\Omega} g \chi_A d \mu
        =
        \int_{\Omega} (h-g) \chi_A d \mu
        \\
        &\leq
        \int_{\Omega} (h-g) d \mu
        =
        \int_{\Omega} h d \mu - \int_{\Omega} g d \mu
        \leq 2 \epsilon
      \end{align*}

      Now we only need to show that their integrals are equal. 
      On the one hand, we have
      \begin{align*}
        \int_{A} f|_A d \mu - \int_{\Omega} f \chi_A d \mu
        \leq
        \int_{A} h|_A d \mu - \int_{\Omega} g \chi_A d \mu
        =
        \int_{\Omega} h \chi_A d \mu - \int_{\Omega} g \chi_A d \mu
        = \int_{\Omega} (h-g) \chi_A d \mu 
        \leq
        2 \epsilon
      \end{align*}
      and on the other hand, we have
      \begin{align*}
        \int_{A} f|_A d \mu - \int_{\Omega} f \chi_A d \mu
        \geq \int_{A} g|_A d \mu - \int_{\Omega} h \chi_A d \mu
        =
        \int_{\Omega} g \chi_A d \mu - \int_{\Omega} h \chi_A d \mu
        = \int_{\Omega} (g-h) \chi_A d \mu
        \geq - 2 \epsilon
      \end{align*}
      Which means
      \begin{align*}
        \abs{\int_{A} f|_A d \mu - \int_{\Omega} f \chi_A d \mu} \leq 2 \epsilon
      \end{align*}
  \end{itemize}
\end{proof}

Since we have just showed that both ways of restricting a $\mu$-summable function yield the same integral, we will from now on write
\begin{align*}
  \int_{A} f d \mu := \int_{A} f|_A d \mu = \int_{\Omega} f \chi_A d \mu
\end{align*}


\begin{cor}[]\label{cor:restriction-to-zero}
  Let $f: \Omega \to \overline{\R}$ be a $\mu$-summable function and $A \subseteq \Omega$ with $\mu(A) = 0$. Then
  \begin{align*}
    \int_{A} f d \mu = 0
  \end{align*}
\end{cor}
\begin{proof}
  Since $\mu(A) = 0$ it is $\mu$-measurable and $f \chi_A= 0$ $\mu$-a.e.. 
  By Proposition \ref{prop:integral-and-zero} we have $\int_{\Omega} f \chi_A d \mu = 0$ and from the previous Lemma, the result follows.
\end{proof}


\begin{prop}[]\label{prop:split-integral}
Let $f: \Omega \to \overline{\R}$ be $\mu$-summable, $\Omega = A \cup B$, for $A,B \subseteq \Omega$ $\mu$-measurable.
\begin{enumerate}
  \item If $A \cap B = \emptyset$, then
    \begin{align*}
      \int_{\Omega} f d \mu
      =
      \int_{A} f d \mu
      +
      \int_{B} f d \mu
    \end{align*}
  \item And more generally, for $A \cap B \neq \emptyset$:
    \begin{align*}
      \int_{\Omega} f d \mu
      =
      \int_{A} f d \mu
      +
      \int_{B} f d \mu
      -
      \int_{A \cap B} f d \mu
    \end{align*}
\end{enumerate}

\end{prop}
\begin{proof}
  \begin{enumerate}
    \item 
  Clearly, $f = f \chi_{A} + f \chi_B$, So by linearity and Lemma \ref{lem:restriction-integral} , we have
  \begin{align*}
    \int_{\Omega} f d \mu
    =
    \int_{\Omega} f \chi_A + f \chi_B d \mu
    = 
    \int_{\Omega} f \chi_A d \mu
    + 
    \int_{\Omega} f \chi_B d \mu
    =
    \int_{A} f d \mu
    +
    \int_{B} f d \mu
  \end{align*}
  \item We can partition $\Omega$ into the disjoint sets
    $
    \Omega = (A \setminus B) \sqcup (B \setminus A) \sqcup (A \cap B)
    $
    together with
    \begin{align*}
      \chi_A = \chi_{A \setminus B} + \chi_{A \cap B}
      \quad \text{and} \quad 
      \chi_B = \chi_{B \setminus A} + \chi_{A \cap B}
    \end{align*}
    we can write
    \begin{align*}
      1 = \chi_{\Omega}
      &=
      \chi_{A \setminus B} + \chi_{B \setminus A} +\chi_{A \cap B}
      \\
      &=
      \chi_A - \chi_{A \cap B}
      +
      \chi_B - \chi_{A \cap B}
      +
      \chi_{B \cap A}
      \\
      &=
      \chi_A + \chi_B - \chi_{A \cap B}
    \end{align*}
    So by (a), we have


\end{enumerate}
\end{proof}




\subsection{Comparison between Lebesgue and Riemann-Integral}

\subsubsection{The Riemann Integral}

\begin{dfn} \label{dfn:riemann-integral}


Let $I = [a,b] \subseteq \R$ and $P = \{a=x_0,x_1, \ldots x_n = b\}$ a partition of $I$.

For a bounded function $f: I \to \R$ and $P$ a partition on $I$, we define the \textbf{upper} and \textbf{lower Riemann sums} by
\begin{align*}
  S(P,f) &:= \sum_{i=1}^{n}(x_i - x_{i-1}) \sup_{x \in (x_{i-1},x_i]} f(x)
  \\
  s(P,f) &:= \sum_{i=1}^{n}(x_i - x_{i-1}) \inf_{x \in (x_{i-1},x_i]} f(x)
\end{align*}
For $\mathcal{P}$ the set of all partitions of $I$, define
\begin{align*}
  \mathcal{R} \upint_{a}^{b} f(x) d x &:= \inf \{S(P,f), P \in \mathcal{P}\}\\
  \mathcal{R} \lowint_{a}^{b} f(x) d x &:= \sup \{S(P,f), P \in \mathcal{P}\}
\end{align*}
and we say that a bounded function $f: I \to \R$ is Riemann integrable (or short: $\mathcal{R}$-integrable) if
\begin{align*}
  \mathcal{R} \lowint_{a}^{b}f(x) dx = \mathcal{R} \upint_a^{b} f(x) dx =: \int_a^{b} f(x) dx
\end{align*}

\end{dfn}

The following definition is neither standard nor part of the original lecture.
\begin{dfn}[]
  Let $I \subseteq \R$ be an interval.
  A \textbf{step-function} is a function $\phi: I \to \R$ of the form
  \begin{align*}
    \phi = \sum_{i=1}^{n} c_i \chi_{(x_{i-1},x_i]}
  \end{align*}
  where $P = \{x_1,\ldots,x_n\}$ is a partition of $I$ and $c_{1},\ldots,c_n \in \R$.
\end{dfn}

Since the interval $(x_{i-1},x_i]$ is $\Leb^{1}$-measurable, step-functions are $\Leb^{1}$-integrable and 
\begin{align*}
  \int_{[a,b]} \phi d \Leb^{1} = \sum_{i=1}^{n}c_i (x_i - x_{i-1})
\end{align*}

For any partition $P$, we can express the upper and lower Riemann sums $S(P,f)$ and $s(P,f)$ using the $\Leb^{1}$-integral of step functions.
By defining\footnote{The orginal lecture wrote $\overline{\phi},\underline{\phi}$ instead of $\overline{P},\underline{P}$. That confused me so I changed it.}
\begin{align*}
  \overline{P}(x) &= \sum_{i=1}^{n} \sup_{x \in (x_{i-1},x_i]} f(x) \chi_{(x_{i-1},x_i]}
  \\
  \underline{P}(x) &= \sum_{i=1}^{n} \inf_{x \in (x_{i-1},x_i]} f(x) \chi_{(x_{i-1},x_i]}
\end{align*}
it's easy to see that
\begin{align*}
  S(P,f) = \int_{[a,b]} \overline{P}(x) d \Leb^{1}
  \quad \text{and} \quad 
  s(P,f) = \int_{[a,b]} \underline{P}(x) d \Leb^{1}
\end{align*}

However, we aren't quite satisfied. If you wanted to construct $\overline{P}$ or $\underline{P}$, we would have to calculate $\sup_{x \in (x_{i-1},x_i]}f(x)$ for each interval which isn't so handy.

It's clear that $\underline{P}(x) \leq f(x)  \leq \overline{P}(x)$
and for any other step function $\phi(x)$ to the same partition $P$ will satisfy
\begin{align*}
  \phi \geq f \implies \phi \geq \overline{P} \quad \text{and} \quad \phi \leq f \implies \phi \leq \underline{P}
\end{align*}
which means
\begin{align*}
  S(P,f) &= \inf \left\{\int_{[a,b]} \phi d \Leb^{1} \big\vert \phi: I \to \R \text{ is a step function to the partition }P, \phi \geq f\right\}
  \\
  s(P,f) &= \sup \left\{\int_{[a,b]} \phi d \Leb^{1} \big\vert \phi: I \to \R \text{ is a step function to the partition }P, \phi \leq f\right\}
\end{align*}
so we finally get the alternative characterisation of the Riemann-integral using the Lebesgue integral of step functions:
\begin{empheq}[box=\bluebase]{align*}
  \mathcal{R} \upint_a^{b} f(x) dx 
  &= \inf
  \left\{\int_{[a,b]}\phi d \Leb^{1} \big\vert \phi: I \to \R \text{ is a step-function, }\phi \geq f \right\}
  \\
  \mathcal{R} \lowint_a^{b} f(x) dx 
  &= \sup
  \left\{\int_{[a,b]}\underline{\phi} d \Leb^{1} \big\vert \phi: I \to \R \text{ is a step-function, }\phi \geq f \right\}
\end{empheq}


One can visualize the difference between the Riemann and Lebesgue-integral as follows:
\begin{center}
The Riemann integral considers the area under a curve as made out of vertical rectangles,
whereas the Lebesgue integral considers horizontal slabs under the curve.
\end{center}

\begin{ex}[]
  \begin{itemize}
    \item 
      The Dirichlet function $\chi_{\Q \cap [0,1]}$ is an example of a function that is not Riemann integrable.
      Because for any partition $P$ (which is made up of \emph{finitely} many sections!), one finds that $S(P,f) = 1$ and $s(P,f) = 0$.

    \item Moreover, the Riemann integral doesn't behave as nicely when considering limits of functions.
      For example, for some enumeration $\Q \cap [0,1] = \{r_1,r_2,r_3,\ldots\}$ we can define
      \begin{align*}
        f_n = \chi_{r_1,\ldots,r_n}
      \end{align*}
      It is easy to see that $f_n$ is $\mathcal{R}$-integrable with integral $\mathcal{R} \int_0^{1}f_n(x) dx = 0$.

      But in the limit $n \to \infty$, the sequence of functions converges to the non $\mathcal{R}$-integrable Dirichlet function.
  \end{itemize}

  We will see later in this chapter that the Lebesgue integral behaves much more nicely, 
  because the characteristic functions of $\Leb^{1}$-measurable sets are $\Leb^{1}$-integrable.
\end{ex}



\begin{prop}[]
  Let $f: [a,b] \to \R$ be a bounded $\mathcal{R}$-integrable function. Then it is $\Leb^{1}$-integrable and
  \begin{align*}
    \mathcal{R} \int_a^{b} f(x) dx = \int_{[a,b]}f d \Leb^{1}
  \end{align*}
\end{prop}
\begin{proof}
  As noted earlier, step functions are $\Leb^{1}$-integrable (but not the other way around).
  So comparing the alternative characterisation above with Definition \ref{dfn:integral}, where we are taking the supremum/infimum over a larger set, we have
  \begin{align*}
    \inf
    \left\{
      \int_{[a,b]} \phi d \Leb^{1} \big\vert \phi: I \to \R \text{ a step function, } \phi \geq f
    \right\}
    \\
    \geq
    \inf\left\{
      \int_{[a,b]} g d \Leb^{1} \big\vert g \text{ is a $\Leb^{1}$-integrable simple function, } g \geq f \text{ $\mu$-a.e.}
    \right\}
  \end{align*}
  and similarly:
  $
     \sup \left\{
       \int_{[a,b]} \phi d \Leb^{1}
       \big\vert \ldots
     \right\}
     \leq
     \sup \left\{
       \int_{[a,b]} g d \Leb^{1} \big\vert \ldots
     \right\}
  $
  , so it follows that
  \begin{align*}
    \mathcal{R} \lowint_{a}^{b} f(x) d x
    &=
    \sup \left\{
    \int_{[a,b]} \phi d \Leb^{1} \big\vert \ldots\right\}
    \leq 
    \sup
    \left\{\int_{[a,b]} g d \Leb^{1} \big\vert \ldots \right\}
    \leq 
    \lowint_{[a,b]} f d \mu
    \\
    &\leq
    \upint_{[a,b]} f d \mu
    =
    \inf \left\{\int_{[a,b]} g d \Leb^{1} \big\vert \ldots\right\}
    \leq
    \inf \left\{\int_{[a,b]} \phi d \Leb^{1} \big\vert \ldots \right\}
    = 
    \mathcal{R}
    \upint_{a}^{b} f(x) dx
  \end{align*}
  so $f$ is is $\mathcal{R}$-integrable, the inequalities become equalities and $\lowint_{[a,b]} f d \mu = \upint_{[a,b]} f d \mu$, showing that $f$ is $\Leb^{1}$-integrable and that their integral coincides.
\end{proof}


\subsection{Convergence Results}
In this chapter, we want to see when we can exchange limits with integrals, i.e. when 
\begin{align*}
  \lim_{k \to \infty} \int_{\Omega} f_k d \mu = \int_{\Omega} \lim_{k \to \infty} f_k d \mu
\end{align*}

It is not difficult to find counterexamples, but when this holds, it allows us to calculate many integrals.

For this section, let $\mu$ be a measure on $\R^{n}$ and $\Omega$ $\mu$-measurable.


\begin{thm}[Fatou's Lemma] \label{thm:fatou}
  Let $\mu$ be a Radon measure on $\R^{n}$ and $f_k: \Omega \to [0,\infty]$ be a sequence of $\mu$-measurable functions.
  Then

  \begin{align*}
    \int_{\Omega} \liminf_{k \to \infty} f_k d \mu \leq \liminf_{k \to \infty} \int_{\Omega} f_k d \mu
  \end{align*}
\end{thm}
\begin{proof}
  Since the $f_k$ are $\mu$-integrable, by Theorem \ref{thm:measurable-operation} $f := \liminf_{k \to \infty}f_k$ is aswell.

  We show that for every $\mu$-integrable simple function $g \leq f$:
  \begin{align*}
    \int_{\Omega} g d \mu \leq \liminf_{k \to \infty} \int_{\Omega} f_k d \mu
  \end{align*}
  Since $f_k \geq 0$ and by Lemma \ref{lem:linearity-simple-integral}, we can assume without loss of generality that $g \geq 0$ is of the form $g = \sum_{j=0}^{\infty} a_j \chi_{A_j}$ with $A_j$ $\mu$-measurable and pairwise disjoint and $a_0 = 0, a_i > 0$ for $i > 0$\footnote{We can't have it that all $a_i > 0$, or else the domain of $g$ isn't $\Omega$ anymore. For example with $h = 0$.}.
  
  For any factor $t \in (0,1)$, define
  \begin{align*}
    B_{j,k} := \{x \in A_j \big\vert f_l(x) > t a_j, \forall l \geq k\}
  \end{align*}
  then $A_j = \bigcup_{k=1}^{\infty}B_{j,k}$. 
  Because the series is increasing in $k$, by continuity from below
  \begin{align*}
    \lim_{k \to \infty} \mu(B_{j,k}) = \mu(A_j)
  \end{align*}

  For $J,k \in \N$ fix:
  \begin{align*}
    \int_{\Omega} f_k d \mu \geq \sum_{j=1}^{J} \int_{A_j} f_k d \mu \geq \sum_{j=1}^{J} \int_{B_{j,k}} f_k d \mu \geq t \cdot \sum_{j=1}^{J}a_j \mu(B_{j,k})
  \end{align*}
  Fist, we let $k \to \infty$ and get
  \begin{align*}
    \liminf_{k \to \infty} \int_{\Omega} f_k d \mu \geq \lim_{k \to \infty}t \cdot \sum_{j=1}^{J}a_j \mu(B_{j,k}) = t \cdot \sum_{j=1}^{J}a_j \mu(A_j)
  \end{align*}
  then let $J \to \infty$:
  \begin{align*}
    \liminf_{k \to \infty} \int_{\Omega} f_k d \mu \geq t \sum_{j=1}^{\infty}a_j \mu(A_j) = t \int_{\Omega} g d \mu
  \end{align*}
  and if we let $t \to 1$, we get
  \begin{align*}
    \liminf_{k \to \infty} \int_{\Omega} f_k d \mu \geq \int_{\Omega} g d \mu
  \end{align*}
\end{proof}


\begin{ex}[]
The condition $f_k \geq 0$ in Fatou's Lemma is necessary. 
Take for example $\mu = \Leb^{n}, \Omega = \R^{n}$ and $f_k = - \frac{1}{k^{n}} \chi_{B_k(0)}$.
Since the volume of the ball $B_{k}(0)$ is proportional to $k^{n}$, we have
\begin{align*}
  \liminf_{k \to \infty} \int_{\Omega} f_k d \mu = - C
\end{align*}
for some constant $C$ (that depends on $n$).
But the function sequence converges uniformly to $\liminf_{k \to \infty}f_k = 0$.
\end{ex}


The following theorem is also sometimes known as Beppo Levi's Theorem. We will use the descriptive name Monotone Convergence Theorem, or MCT for short.
\begin{thm}[Monotone Convergence Theorem]
  Let $f_k: \Omega \to [0,\infty]$ be a sequence of $\mu$-measurable functions non-decreasing in $k$ (i.e. $f_1 \leq \ldots \leq f_k \leq f_{k+1} \leq \ldots$).
  Then
  \begin{align*}
    \int_{\Omega} \lim_{k \to \infty}f_k d \mu = \lim_{k \to \infty} \int_{\Omega} f_k d \mu
  \end{align*}
\end{thm}

\begin{proof}
We show inequalities in both directions.
\begin{itemize}
  \item[$\geq$:] Since $f_k$ is non-decreasing, we have for all $j \in \N$ that $\int_{\Omega} f_j d \mu \leq \int_{\Omega} \lim_{k \to \infty} f_kd \mu$. 
    So in the limit
    \begin{align*}
      \lim_{k \to \infty} \int_{\Omega} f_k d \mu \leq \int_{\Omega} \lim_{k \to \infty} f_k d \mu
    \end{align*}

  \item[$\leq$:] By Fatou's Lemma 
    \begin{align*}
      \int_{\Omega} \lim_{k \to \infty}f_k d \mu = \int_{\Omega} \liminf_{k \to \infty}f_k d \mu
      \stackrel{\text{Fatou}}{\leq} \liminf_{k \to \infty} \int_{\Omega} f_k d \mu = \lim_{k \to \infty} \int_{\Omega} f_k d \mu
    \end{align*}
\end{itemize}
\end{proof}


\begin{rem}[]
  We could also prove MCT first and use it to prove Fatou's Lemma.
\end{rem}
\begin{proof}[Proof of Beppo Levi without Fatou's Lemma]

  Set $f := \lim_{k \to \infty} f_k$.
  Since $(f_k)_{k \in \N}$ is non-decreasing, we have $\lim_{k \to \infty} \int_{\Omega} f_k d \mu \leq \int_{\Omega} f d \mu$.

  For any simple function $h$ and $\epsilon > 0$, set
  \begin{align*}
    \Omega_k := \{x \in \Omega \big\vert f_n(x) \geq (1-\epsilon) h(x)\}
  \end{align*}
  this forms an increasing sequence ($\Omega_k \subseteq \Omega_{k+1}$).
  We know that WLOG, $h$ is of the form $h = \sum_{i \in \N}c_i \chi_{A_i}$ with $c_0 = 0, c_i > 0$ for $i > 0$ and $A_i$ mutually disjoint.
  Then we can also show that $\bigcup_{k\in \N} \Omega_k = \Omega$.

  Suppose there exists an $x_0 \in \Omega \setminus \bigcup_{k \in \N}\Omega$.
  This means $\forall k \in \N: f_k(x_0) < (1- \epsilon) h(x_0)$.
  But then
  \begin{align*}
    \lim_{k \to \infty}f_k(x_0) = f(x_0) \leq (1 - \epsilon) h(x_0) < h(x_0) \lightning
  \end{align*}
  Therefore, one finds
  \begin{align*}
    \int_{\Omega} f_k d \mu \geq \int_{\Omega_k} f_k d \mu \geq \int_{\Omega_k} (1-\epsilon)h d \mu
  \end{align*}
  in the limit $k \to  \infty$, it follows
  \begin{align*}
    \lim_{k \to \infty} \int_{\Omega} f_k d \mu \geq \lim_{k \to \infty} \int_{\Omega_k} (1- \epsilon)h d \mu = \int_{\Omega} (1-\epsilon)h d \mu
  \end{align*}
  letting $\epsilon \to 0$ we obtain
  \begin{align*}
    \lim_{k \to \infty} \int_{\Omega} f_k d \mu
    \geq \int_{\Omega} h d \mu
  \end{align*}
  since $h$ was an arbitrary simple function $\leq f$, the proof follows.


\end{proof}

\begin{ex}[]
  A neat application of this is the special case where the limit is sum of non-negative functions:
  Let $(f_k)_{k \in \N}: \Omega \to [0,\infty]$ be a sequence of $\mu$-measurable functions.
  Then $\sum_{k=1}^{\infty}f_k$ is $\mu$-measurable and
  \begin{align*}
    \int_{\Omega} \sum_{k=1}^{\infty}f_k d \mu = \sum_{k=1}^{\infty} \int_{\Omega} f_k d \mu
  \end{align*}
  and to prove this, we apply MCT to the sequence of partial sums $s_n := \sum_{k=1}^{n}f_k$.
\end{ex}



