#+TITLE: Topology PVK - org mode header
#+AUTHOR: Han-Miru Kim
#+STARTUP: overview

#+BEGIN_SRC latex-macros
  \newcommand{\N}{\mathbb{N}}
  \newcommand{\Z}{\mathbb{Z}}
  \newcommand{\Q}{\mathbb{Q}}
  \newcommand{\R}{\mathbb{R}}
  \newcommand{\IS}{\mathbb{S}}
  \newcommand{\ID}{\mathbb{D}}
  \newcommand{\IT}{\mathbb{T}}
  \newcommand{\C}{\mathbb{C}}
  \newcommand{\K}{\mathbb{K}}
  \newcommand{\F}{\mathbb{F}}
  \newcommand{\IP}{\mathbb{P}}

  \DeclareMathOperator{\charac}{char}
  \DeclareMathOperator{\degree}{deg}  

  \newcommand{\del}{\partial}
  \newcommand{\es}{\text{\o}}
  \newcommand{\eqq}{\dot{=}}
  #+END_SRC

