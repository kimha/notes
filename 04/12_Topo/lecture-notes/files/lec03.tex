For sets $X,Y$ let $X \sqcup Y$ denote their disjoint union $X \times \{0\} \cup Y \times \{1\}$ and $X \times Y$ their cartesian product (as sets).
\begin{dfn}[]
  Let $(X,\tau_X)$ and $(Y,\tau_Y)$ be two topological spaces. 
  \begin{itemize}
    \item Their \textbf{coproduct} is the topological space $(X \sqcup Y, \tau_{X \sqcup Y}$, where
      \begin{align*}
        \tau_{X \sqcup Y} := \{U \sqcup V \big\vert U \in \tau_X, V \in \tau_Y\}
      \end{align*}
    \item Their (cartesian) \textbf{product} is the topological space $(X \times Y, \tau_{X \times Y})$, where
      \begin{align*}
        \tau_{X \times Y} := \{W \subseteq X \times Y \big\vert \forall (x,y) \in W \exists U \in \tau_X, \exists V \in \tau_Y: (x,y) \in U \times V \subseteq W \}
      \end{align*}
  \end{itemize}
\end{dfn}
Note that \emph{not} every open subset $W \subseteq X \times Y$ is of the form $W = U \times Y$, for $U \subseteq X$, $V \subseteq Y$ open. For example, the product topology on $\R^{2}$ contains open circles.


\subsection{Basis and Subbasis}
\begin{ex}[A ``round square'']
  Consider the open unit ``square''
  \begin{align*}
    U = (0,1)^{2} = \{(x,y) \in \R^{2} \big\vert 0 < x,y < 1\}
  \end{align*}
  This set can be written as a union of open balls.
  Try to do this by hand and you will see that it is quite difficult to do with only finitely many open balls.
  But it is possible, if we can use infinitely many.
  For every $p = (x,y) \in U$ take $r(p) = \min(x,y)$, so $B(p,r) \subseteq U$ and
  \begin{align*}
    U = \bigcup_{p \in U}B(p,r(p))
  \end{align*}
\end{ex}
More generally, every open set $U \subseteq \R^{n}$ can be written as the union of open balls.
So $U \subseteq \R^{n}$ is open if and only if there exists $(x_i,r_i)_{i \in I}$ such that $U = \bigcup_{i \in I}B_{r_i}(x_i)$.



\begin{ex}[More than just unions]
  Take $\Z$ with the discrete topology (i.e. every subset is open).
  Let $\mathcal{B}$ be the collection of sets of the form $\{n,n+1\}$, for $n \in \Z$, i.e.
  \begin{align*}
    \mathcal{B} = \left\{
      \{n,n+1\} \big\vert n \in \Z
    \right\}
  \end{align*}
  Then we can write singletons $\{n\}$ as the intersection
  \begin{align*}
    \{n\} = \{n-1,n\} \cap \{n,n+1\}
  \end{align*}
  So every open set can be written as a union of sets of the form $U \cap V$, with $U,V \in \mathcal{B}$.

  Note that this is \emph{not} the case if we took $\N$ instead, as there is no way to recover the set $\{0\}$.
\end{ex}


\begin{ex}[The Sorgenfrey Line $\R_{\ell}$]
  What is the smallest topology on the set $\R$, for which intervals of the form $[a,b)$ are open?

  Because an open interval $(a,b)$ can be written as the union of half-open intervals
  \begin{align*}
    (a,b) = \bigcup_{n \in \N} [a + \tfrac{1}{n},b)
  \end{align*}
  every set that is open in the standard topology, must also be open in this new topology.

  So what does the topology look like? Is every subset open?
\end{ex}

\begin{dfn}[]
Let $X$ be a topological space and $\mathcal{B}$ a collection of open sets.
\begin{itemize}
  \item We call $\mathcal{B}$ a \textbf{basis} of the topology, if every open set can be written as a union of elements of $\mathcal{B}$.
  \item $\mathcal{B}$ is called a \textbf{subbasis} of the topology, if every open set can be written as a union of finite intersection of elements of $\mathcal{B}$.
\end{itemize}
\end{dfn}
\begin{rem}[]
  Every basis is a subbasis.
  Every collection $\mathcal{B}$ of open sets is the subbasis of a unique topology, the topology \textbf{generated} by $\mathcal{B}$, which is the smallest topology that contains $\mathcal{B}$.
\end{rem}


