\subsection{Classification of coverings}

\begin{cor}[\hypertarget{thm:uniqueness-theorem}{Uniqueness theorem}]\label{cor:uniqueness-theorem}
  Let $\pi:(Y,y_0) \to  (X,x_0)$ and $\pi':(Y',y_0') \to (X,x_0)$ be coverings with $Y$ and $Y'$ both pathconnected and locally pathconnected.
Then there exists an isomorphism $\phi$ (in $\Top^{\ast}$) such that the following diagram commutes
\begin{center}
\begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
  (Y,y_0) \arrow[]{r}{\phi} \arrow[]{dr}{\pi} & (Y',y_0') \arrow[]{d}{\pi'}\\
                                              & (X,x_0)
\end{tikzcd}
\end{center}
if and only if $G(\pi) = G(\pi')$
\end{cor}
\begin{proof}
\begin{itemize}
  \item[$\implies$] By functoriality of the fundamental group, we have
    \begin{align*}
      G(\pi) = \pi_{\ast}(\pi_1(Y,y_0)) = (\pi' \circ \phi)_{\ast} (\pi_1(Y,y_0)) = \pi_{\ast}'(\phi_{\ast}(\pi_1(Y,y_0)) = \pi_{\ast}'(\pi_1(Y',y_0'))
    \end{align*}
  \item[$\Leftarrow$] Because $G(\pi) = \pi_{\ast}(\pi_1(Y,y_0)) \subseteq G(\pi')$, we can use the lifting criterion to lift $\pi$ over the covering $\pi'$. to get a function
    $\phi: (Y,y_0) \to (Y',y_0')$
    Analogously, we reverse the roles of $\pi'$ and $\pi$ to get a lift $\psi:(Y',y_0') \to  (Y,y_0)$ of $\pi'$ over the covering $\pi$.
    Then we see that $\psi \circ \pi: (Y,y_0) \to (Y,y_0)$ is a lift of the map $\pi$ over the covering $\pi$.

    But the identity $\id_{(Y,y_0)}$ is also a lift of the map $\pi$ over the covering $\pi$.
    By the uniqueness of the lift, we see $\psi \circ \phi = \id_{(Y,y_0)}$. Analogously, we show $(\phi \circ \psi) = \id_{(Y',y_0')}$.
\end{itemize}
\end{proof}
\begin{rem}[]
  If $\pi:(Y,y_0) \to  (X,x_0)$ is a covering with $G(\pi) = \{1\} \subseteq \pi_1(X,x_0)$, $U$ be a uniformly covered neighborhood of $x_0$.

  Then for all loops $\alpha$ in $U$ at $x_0$, there exists a loop $\tilde{\alpha}$ at $y_0 \in \pi^{-1}(U)$ such that $\alpha = \pi \circ \tilde{\alpha}$.

  In particular, $[\alpha] = \pi_{\ast}([\tilde{\alpha}]) \in G(\pi) = \{1\}$.
\end{rem}


\begin{dfn}[]
Let $X$ be a topological space.
\begin{itemize}
  \item We say that $X$ is \textbf{semilocally simply connected}, if $\forall x_0 \in X$ there exists a neighborhood $U$ of $x_0$ such that all loops at $x_0$ in $U$ are homotopic to the constant map $x_0$.
    ($U$ itself need not be simply connected, the homotopy may use regions outside of $U$).
  \item $X$ is called \textbf{sufficiently connected}, if it is pathconnected, locally path connected and semilocally simply connected.
\end{itemize}
\end{dfn}

\begin{xmp}[]
  Open subsets of $\R^{n}$ (or Manifolds) are semilocally simply connected.

  The Hawaiian earring $X$ is path connected, locally pathconnected, but not semilocally simply connected because neighborhoods of the origin contains circles.
  But it's cone $\text{Cone}(X)$ is sufficiently connected.
\end{xmp}


\begin{thm}[\hypertarget{thm:existence-theorem}{Existence Theorem}] \label{thm:existence-theorem}
  Let $X$ be sufficiently connected, $x_0 \in X$ and $G < \pi_1(X,x_0)$ a subgroup.
  
  Then there exists a covering $\pi(Y,y_0) \to  (X,x_0)$ with $Y$ connected and locally pathconnected such that the characteristic group of the covering $G(\pi)$ is $G$.
\end{thm}

Before we prove the theorem, let's consider an example.

For the covering $\pi: (\R,0) \to (\IS^{1},1), x \mapsto  e^{2 \pi i x}$.

Then for any $x \in \R$ there exists a unique homotopy class of loops $[\alpha]$ with $\alpha$ a loop in $\IS^{1}$ such that it's lift satisfies $\tilde{\alpha}(0) = 0$ and $\tilde{\alpha}(1) = x$.


\begin{proof}[Proof Sketch]
  Let $\Omega(X,x_0,x)$ denote the set of paths from $x_0$ to $x \in X$.
  Define
  \begin{align*}
    \Omega(X,x_0) := \bigcup_{x \in X}\Omega(X,x_0,x)
  \end{align*}
  which is well-defined because $X$ is path connected.

  For $\alpha, \beta \in \Omega(X,x_0,x)$ define an eqivalence relation 
  \begin{align*}
    \alpha \sim \beta \iff  [\alpha \beta^{-} \in G
  \end{align*}

  If $G = \{1\}$, then $[\alpha \beta^{-}] = 1$ if and only if $\alpha \sim \beta$ rel endpoints.

  Set $Y := \bigcup_{x \in X}Y_x$ where $Y_x := \Omega(X,x_0,x)/\sim$, and define $y_0$ to be the class of loops which are in the same equivalence class of the constant map $x_0$, i.e. $y_0 := [\text{const}x_0]_{\sim} \in Y_{x_0}$ and define the map
  \begin{align*}
    \pi:Y \to  X, \quad y= [\alpha]_{\sim} \mapsto  x = \alpha(1)
  \end{align*}
  We now want to show that $\pi$ is a covering of $X$ with characteristic subgroup $G(\pi) = G$.

  Since $X$ is path connected, $\pi$ is indeed surjective.

  We then define the topology on $Y$ as follows:

  For $x_0 \in X$, $U$ an open, pathconnected neighborhood of $x$ and $\alpha$ a path from $x_0$ to $x$ (for some $x \in U$) and $y = [\alpha]_{\sim}$, we define the set
  \begin{align*}
    V(U,y) := \{[\alpha \beta]_{\sim}\big\vert \beta \text{ path in $U$ with $\beta(0) = x$}\} \ni y
  \end{align*}
  and use the collection of all such sets
  \begin{align*}
    \mathcal{B} = \{V(U,y) \big\vert y \in Y, U \text{ open path connected, } \pi(y) \subseteq U\}
  \end{align*}
  as our basis for the topology.

  We will show in the exercise sheets that $\pi$ is continuous by checking continuity at every point $y \in Y$.

  $\pi$ is also open because for $V \subseteq Y$, we can write
  \begin{align*}
    V = \bigcup_{U, y} V(U,y) \implies \pi(V) = \bigcup_{U,y} \pi(V(U,y)) = \bigcup_{U,y} U
  \end{align*}
  which is an open subset of $X$.

  Now we show that $\pi$ is a covering.
  Because $X$ is semilocally simply connected, for all $x \in X$, we can chose an open neighborhood $U$ of $x$ that is simply connected, i.e. such that every loop at $x$ in $U$ is homotopic to the constant map $x$.

  We now show that $U$ is uniformly covered by $\pi$.
  This is true because for all $z \in V(U,y)$ we have that $V(U,y) = V(U,z)$, so 
  \begin{align*}
    \pi^{-1}(U) \bigcup_{y \in \pi^{-1}(U)} \{y\} = \bigcup_{y \in \pi^{-1}(U)} V(U,y) = \bigcup_{y \in \pi^{-1}(x)}V(u,y)
  \end{align*}
  but this union is disjiont because for $z \in V(u,y) \cap V(U,y')$ for two classes $y = [\alpha]$ and $y' = [\alpha']$, then we have
  \begin{align*}
    [\alpha \beta]_{\sim} = z = [\alpha' \beta']_{\sim} \iff [(\alpha \beta)({\beta'}^{-}{\alpha'}^{-}] \in G
  \end{align*}
  but because $\beta \beta^{-}$ is homotopic to the constant path $x_0$, we get 
  \begin{align*}
    [\alpha \beta]_{\sim} = [\alpha' \beta']_{\sim} \iff [\alpha {\alpha'}^{-}] \in G \iff [\alpha]_{\sim} = [\alpha']_{\sim} \iff y = y'
  \end{align*}
  and so we get
  \begin{align*}
    \pi^{-1}(U) = \bigsqcup_{y \in \pi^{-1}(x)}V(u,y)
  \end{align*}
  now we show that $\pi|_{V(U,y)}^{U}: V(U,y) \to  U$ is a bijection.
  It is clearly surjective because $U$ is path connected and the map is injective because 
  \begin{align*}
    \pi([\alpha \beta]_{\sim}) = \pi([\alpha \beta']_{\sim}) \implies \beta(1) = \beta'(1) \implies \beta {\beta'}^{-} \simeq \text{ const }x \implies [(\alpha \beta)({\beta'}^{-} \alpha^{-})] = [\alpha \alpha^{-}] = 1 \in G
  \end{align*}
  by continuity and openness of $\pi$, the restriction $\pi|_{V(U,y)}^{U}$ is a homeomorphism.
  Because we have a local homeomorphism, local path connectedness of $X$ also gives us local path connectedness of $Y$. 
  (It also follows that $Y$ is semilocally simply connected).

  We now show that $Y$ is path connected.

  Let $[\alpha]_\sim \in Y$. Then we can find a path from $y_0$ (which is the equivalence class $[\text{const} x_0]_{\sim}$) to $[\alpha]_{\sim}$ by defining the map
  \begin{align*}
    \tilde{\alpha}:[0,1] \to  Y, t \mapsto  [s \mapsto  \alpha(ts)]_{\sim}
  \end{align*}

  Lastly, we show $G(\pi) = G$.
  Let $[\alpha] \in \pi_1(X,x_0)$. Then
  \begin{align*}
    G(\pi) \ni [\alpha] \iff \tilde{\alpha}(1) = y_0 \iff [\alpha]_{\sim} = [\text{const} x_0]_{\sim} \iff [\alpha \text{const} x_0^{-}] \in G \iff [\alpha] \in G
  \end{align*}
\end{proof}

