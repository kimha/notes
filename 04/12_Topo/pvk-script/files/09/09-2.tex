\subsection{Fundamental groups and lifts}

\begin{cor}[]\label{cor:cor2}
  Let $\pi:(Y,y_0) \to (X,x_0)$ be a covering (in $\Top^{\ast}$), then the group homomorphism 
  \begin{align*}
    \pi_{\ast}: \pi_1(Y,y_0) \to \pi_1(X,x_0)
  \end{align*}
  induced by the fundamental group is injective.
\end{cor}
\begin{proof}
  We show that the kernel of $\pi_{\ast}$ is trivial.
  Let $\tilde{\delta}$ be a loop at $y_0$ in $Y$ with $\pi \circ \tilde{\delta} \sim \text{const.} x_0$ rel. $x_0$ (so $[\tilde{\delta}] \in \Ker \pi_{\ast}$).

  Then both $\tilde{\delta}$ and the constant map $y_0$ are lifts of $\pi \circ \tilde{\delta}$ and the constant $x_0$ map, respectively.

  By the corollary, $\tilde{\delta}$ and $\text{const.} y_0$ are homotopic rel endpoints, which means that $[\tilde{\delta}$ is the unit in the fundamental group $\pi_1(Y,y_0)$.
\end{proof}

Now that we know the kernel, can we say something about image of $\pi_{\ast}$?
\begin{dfn}[]
  Let $\pi:(Y,y_0) \to  (X,x_0)$ be a covering. The image of the induced group homomorphism
  \begin{align*}
    G(\pi) := \pi_{\ast}(\pi_1(Y,y_0)) < \pi
  \end{align*}
  is called the \textbf{characteristic subgroup} of the covering $\pi$.
\end{dfn}
Warning, there is a clash of definitions from the ``characteristic subgroup'' we know from Algebra. They are not the same thing.

\begin{xmp}[]
  \begin{itemize}
    \item Looking at the covering $\pi: \R \to \IS, x \mapsto  e^{2 \pi i x}$, we see immediately that $G(\pi) = \{e\}$ is trivial.
    \item For the covering $\pi_n: \IS^{1} \to \IS^{1}, z \mapsto  z^{n}$, the characteristic subgroup is $n\Z < \Z$.
  \end{itemize}
\end{xmp}

\begin{rem}[]
  Let $\pi:(Y,y_0) \to (X,x_0)$ be a covering and $f(Z,z_0) \to (X,x_0)$ continuous and $\tilde{f}:(Z,z_0) \to (Y,y_0)$ a lift such that the following diagram commutes
  \begin{center}
  \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
    & (Y,y_0) \arrow[]{d}{\pi}\\
    (Z,z_0) \arrow[]{ur}{\tilde{f}} \arrow[]{r}{f} & (X,x_0)
  \end{tikzcd}
  \end{center}
  then $f_{\ast}(\pi_1(Z,z_0)) \subseteq G(\pi)$.

  This follows directly from the functoriality of the fundamental group:
  Because of functoriality, the following diagram also commutes:
  \begin{center}
  \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
    & \pi_1(Y,y_0) \arrow[]{d}{\pi_{\ast}}\\
    \pi_1(Z,z_0) \arrow[]{ur}{\tilde{f}_{\ast}} \arrow[]{r}{f_{\ast}} & \pi_1(X,x_0)
  \end{tikzcd}
  \end{center}
  in particular, $f_{\ast} = \pi_{\ast} \circ \tilde{f}_{\ast}$, so 
  \begin{align*}
    f_{\ast}(\pi_1(Z,z_0)) = \pi_{\ast}(\tilde{f}_{\ast}(\pi_1(Z,z_0))) \subseteq \pi_{\ast}(\pi_1(Y,y_0)) = G(\pi)
  \end{align*}
\end{rem}

\begin{dfn}[]
  Warning: There are two common versions of this definition. In the lectures, we used Version 1.

  \textbf{Version 1}:
  A topological space $X$ is called \textbf{locally path connected}(lokal wegzusammenhängend), if for all $x \in X$, every neighborhood of $x$ contains a path connected neighborhood of $x$.

  \textbf{Version 2}:
  A topological space $X$ is called \textbf{locally path connected$^{\ast}$}, if for all $x \in X$, there exists a path connected neighborhood of $x$.
\end{dfn}
Clearly, if $X$ is locally path connected, then it is locally path connected$^{\ast}$.

One can show that if $X$ is Hausdorff, then both deftinions are equivalent.

One can easily show that path connectedness and local path connectedness do not imply eachother.
\begin{xmp}[]
  For example. open subsets of $\R^{n}$ (or manifolds) are locally path connected.
  The Space 
  \begin{align*}
    \text{Cone}(\{0\} \cup \{\frac{1}{n}|n \in \N\}
  \end{align*}
  is path connected, but \emph{not} locally path connected.
\end{xmp}


\begin{rem}[]
  Let $\pi: Y \to X$ be a covering. If $X$ is locally path connected, then so is $Y$.
\end{rem}
\begin{proof}
  Let $y \in Y$. Since $\pi$ is a covering, there exists a neighborhood $U$ of $\pi(y)$ such that
  $$
  \pi|_{\pi^{-1}(U)}: \pi^{-1}(U) \to U
  $$
  is a trivial fiber. Which means that there exist a discrete space $F$ such that
  $
  \pi^{-1}(U) \cong X \times F
  $
  Since $X$ is locally path connected, there exists path connected neighborhood $V \subseteq U$ of $\pi(y)$.

  In the leaf with index $f \in F$ such that $y \in X \times \{f\}$, we se that $V \times \{f\}$ is a neighorhood of $y$ which is homeomorphic to $V$ and thus also locally path connected.
\end{proof}


\begin{thm}[\hypertarget{thm:lifting-criterion}{Lifting criterion}]\label{thm:lifting-criterion}
  Let $\pi:(Y,y_0) \to  (X,x_0)$ be a covering and $Z$ a path connected and locally patch connected topological space and $f:(Z,z_0) \to  (X,x_0)$ continuous.

  Then, there exists a unique lift $\tilde{f}: (Z,z_0) \to   (Y,z_0)$ if and only if
  \begin{align*}
    f_{\ast}(\pi_1(Z,z_0)) \subseteq G(\pi):= \pi_{\ast}(\pi_1(Y,y_0))
  \end{align*}
  In particular, if $Z$ is simply connected, then a lift exists.
\end{thm}
\begin{proof}
\begin{itemize}
  \item[$\implies$] See previous remark.
  \item[$\Leftarrow$] If $f_{\ast}(\pi_1(Z,z_0)) \subseteq G(\pi)$, then for all $z \in \Z$
    chose a path $\alpha$ from $z_0$ to $z$, compose it with $f$ to get $f \circ \alpha$ and lift it to a path $\tilde{\alpha}$ and set $\tilde{f}(z) = \tilde{f \circ \alpha}(1)$.


    This map is well defined and independent on the choice of $\alpha$, because if $\beta$ is another path from $z_0 \to z$, then we consider the loop $\alpha \beta^{-}$ at $z_0$.

    Then $\gamma: f \circ (\alpha \beta^{-}) = (f \circ \alpha)(f \circ \beta)$ is a loop at $x_0$.
    By assumption, we have $[\gamma] = f_{\ast}([\alpha \beta^{-}]) \in G(\pi) = \pi_{\ast}(\pi_1(Y,y_0))$.

    Since this lies in the image, there is a loop $\tilde{\gamma}$ in $(Y,y_0)$ with $\gamma \sim \pi \circ \tilde{\delta}$ rel. endpoints.

    By the Monodromy lemma, $\tilde{\gamma}$ is a lift of $\gamma$ at $y_0$ with 
    \begin{align*}
      \tilde{\gamma} = (\tilde{f \circ \alpha})(\tilde{f \circ \beta^{-}})
    \end{align*}
    and with $(\tilde{f \circ \beta^{-1}})^{-1} = \tilde{f \circ \beta}$ we get
    \begin{align*}
      \tilde{f \circ \beta}(1) = \tilde{f \circ \beta^{-}}(0) = \tilde{f \circ \alpha}81)
    \end{align*}
    which shows well-defined ness.

    We also have $\pi \circ \tilde{f} = f$, since $\pi(\tilde{f}(z)) = \pi(\tilde{f \circ \alpha})(z) = f(\alpha(1)) = f(z)$.

    For continuity, let $z \in Z$ and $V \subseteq Y$ open with $\tilde{f}(z) \in V$.
    Because $\pi$ is a covering, we can assume without loss of generality, we can assume that $\pi(V) = U$ is open and $\pi|_V^{U}:V \to  U$ is  homeomorphism.

    By local path connectedness, we can chose a path connected neighborhood $W \subseteq Z$ of $z$ with $f(W) \subseteq U$.

    Then, for any $w \in W$, let $\alpha$ be a path from $z_0$ to $z$ and $\beta$ a path from $z \to w$ in $W$.

    Taking lifts of $f \circ \alpha$ and $f \circ \beta$ and chaining them together, we get 
    \begin{align*}
      \tilde{f}(w) = \left(
        (\tilde{f \circ \alpha})(\tilde{f \circ \beta})
      \right)(1) = \tilde{f \circ \beta}(1) \in V
    \end{align*}
    which shows $\tilde{f}(W) \subseteq V$ and thus continuity.

    Uniqueness also follows from \ref{lem:path-lift}.
\end{itemize}
\end{proof}


