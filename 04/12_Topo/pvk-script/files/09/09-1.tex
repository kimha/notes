\subsection{Lifting of paths}
Given a covering $\pi: Y \to X$ and a continuous map $f: Z \to  X$, we would like to ``lift'' $f$ to a function $\tilde{f}: Z \to  Y$. such that the following diagram commutes.
\begin{center}
\begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
  & Y \arrow[]{d}{\pi}\\
  Z \arrow[]{r}{f}\arrow[dotted]{ur}{\tilde{f}} & X
\end{tikzcd}
\end{center}

\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{./img/lift-example.eps}
\caption{Example of a lift of a path}
\end{figure}


\begin{dfn}[]
  Let $\pi: Y \to X$ be a covering and $\alpha:[0,1] \to  X$ a path and $y_0 \in \pi^{-1}(\alpha(0))$.
  A path $\tilde{\alpha}:[0,1] \to Y$ is called a \textbf{lift} of $\alpha$ to the starting point $y_0$ if $\pi \circ \tilde{\alpha} = \alpha$, i.e.\ the following diagram commutes:
\begin{center}
\begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
  & Y \arrow[]{d}{\pi}\\
  {[a,b]} \arrow[]{r}{\alpha}\arrow[dotted]{ur}{\tilde{\alpha}} & X
\end{tikzcd}
\end{center}
\end{dfn}

\begin{xmp}[]
  Let $\R \stackrel{\pi}{\to} \IS^{1}$ be the covering $\pi(x) = e^{2 \pi i x}$.
  For the path that goes around the circle $k$ times
\begin{align*}
  \alpha_k: [0,1] \to \IS^{1}, \quad t \mapsto e^{2 \pi i k t}
\end{align*}
a lift of it is the path
\begin{align*}
  \tilde{\alpha}_k: [0,1] \to \R,\quad t \mapsto kt
\end{align*}
starting at the origin and moving with speed $k$.
Another lift with different starting point would be $\tilde{\alpha}_k'(x) = kt + 1$.
\end{xmp}

\begin{lem}[Lifting of Paths]\label{lem:path-lift}
  \hypertarget{lem:lifting-of-paths}{Let} $\pi: Y \to X$ a covering, $\alpha:[0,1] \to X$ a path and $y_0 \in \pi^{-1}(\alpha(0))$.
  
  Then there exists a \hypertarget{lem:unique-lift-of-path}{unique lift} of $\alpha$ to $y_0$.
\end{lem}
\begin{proof}
  %First note that for any $U \subseteq X$ open and uniformly covered:
  %any path $\beta:[0,1] \to U$, has a unique lift of $\beta$ to a $y_j \in \pi^{-1}(\beta(0))$.
  %This is because the lift is uniquely determined by
  %\begin{align*}
    %\tilde{\beta}_j := \left(
      %\pi|_{U_j}^{U}
    %\right)^{-1} \circ \beta
  %\end{align*}
  %\begin{align*}
    %\xymatrix{
      %[0,1]
      %\ar[r]^{\beta}
      %&
      %U
      %&
      %U_j
      %\ar[l]_{\iso \pi|_{U_j}^{U}}
    %}
  %\end{align*}
  %In particular, the images of $\tilde{\beta}_j$ and $\tilde{\beta}_k$ are disjoint for $j \neq k \in I$.
  \textbf{Uniqueness:} 
  $\tilde{\alpha}, \hat{\alpha}: [0,1] \to Y$ be two lifts of $\alpha$ to the point $y_0$.
  Set
  \begin{align*}
    T := \{t \in [0,1] \big\vert \tilde{\alpha}(t) = \hat{\alpha}(t)\} \subseteq [0,1]
  \end{align*}
  Because $\tilde{\alpha}$ and $\hat{\alpha}$ start at the same point $y_0$, $T$ is non-empty.

  $T$ is also open, because for any $t_0 \in T$ chose an uniformly covered open neighborhood $U \subseteq X$ of $\alpha(t_0)$.
  Then for all $a < b$ such that $\alpha([a,b]) \subseteq U$ we have either
  \begin{align*}
    a < t_0 < b
    ,\quad \text{ or } 
    a = 0 = t_0 < b
    , \quad \text{ or } 
    a < t_0 = b = 1
  \end{align*}
  in any case, we noted that $\tilde{\alpha}$ and $\hat{\alpha}$ have to agree on $[a,b]$. 

  With the same argument, we can show that $T$ is also closed, because for some $t \in [0,1] \setminus T$, we chose $U,a,b$ like before and show that $\tilde{\alpha}(t) \neq \hat{\alpha}(t)$ for all $t \in [0,1]$.

  \textbf{Existence:} set
  \begin{align*}
    T := \{t \in [0,1] \big\vert \alpha|_{[0,t]} \text{ has a lift to $y_0$}\}
  \end{align*}
  Tor $t_0 := \sup_{t \in T}$ we either have $T = [0,t_0)$ or $T=[0,t_0]$.

  Chosing $U$ uniformly covered with $\alpha(t_0) \in U$ and $a<b$ as before, we can chose a lift $\tilde{\beta}:[a,b] \to  Y$ of $\alpha|_{[a,b]}$ with $\tilde{\beta}(a) = \tilde{\alpha}(a)$
  Then set set
  \begin{align*}
    \tilde{\alpha}:[0,b] \to  Y, \quad \tilde{\alpha}(t) := \left\{\begin{array}{ll}
        \tilde{\alpha}|_{[0,a](t)} & t \leq a\\
        \tilde{\beta}(t) &  t \in [a,b]
    \end{array} \right.
  \end{align*}
  then $\tilde{\alpha}$ is a lift of $\alpha|_{[0,b]}$.

  Which shows that if $t_0<1$, this is a contradiction that $t_0$ is the supremum, so we must be in the case, where $a < t_0 = b = 1$.
\end{proof}

This next example shows that lifting of paths don't always behave very nice.

\begin{xmp}[Monodromy]
  In the covering $\R \to \IS^{1}$, we can consider two paths in $\IS^{1}$. Both starting at $1$, one going clockwise ($\alpha$) and the other one going counter-clockwise ($\beta$).

  We know that we could lift these to paths to $\tilde{\alpha},\tilde{\beta}:[0,1] \to \R$, but even if both start at the same point, say $\tilde{\alpha}(0) = \tilde{\beta}(0) = 0$,
  the path $\tilde{\alpha}$ will end and the point $-1$ and $\tilde{\beta}$ at $+1$.
  \vspace{1\baselineskip} 
  Similar things happen with the complex logarithm.
  It would be nice to have an inverse to the exponential function $\exp: \C \to \C$.
  But since it is not injective 
  The logarithm , which when we continuously move the argument around the origin, wants to end up at a value shifted by $2 \pi i$.
\end{xmp}

However, one can show that if two loops $\alpha,\beta$ go around the circle the same number of times, their lifts $\tilde{\alpha},\tilde{\beta}$ will end up at the same place (provided they are lifted to the same basepoint).
In fact, a more general statement is true:

\begin{lem}[]\label{lem:homotopy-lift}
  Let $\pi: Y \to X$ be a covering, and $h: Z \times [0,1] \to  X$ continuous and let
  \begin{align*}
    \iota: Z \hookrightarrow Z \times [0,1]
    , \quad
    z \mapsto (z,0)
  \end{align*}
  If we can lift $h_0 = h \circ \iota: Z \to X$ to a continuous function $\tilde{h}_0: Z \to Y$, 
  then there exists a unique lift $\tilde{h}:Z \times [0,1] \to Y$ such that the following diagram commutes
  \begin{center}
  \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
    Z \arrow[]{r}{\tilde{h}_0} \arrow[swap]{d}{\iota}& Y \arrow[]{d}{\pi}\\
    Z \times [0,1] \arrow[]{r}{h}\arrow[dotted]{ur}{\exists!\tilde{h}} & X
  \end{tikzcd}
  \end{center}
  In other words: if the outer square of the diagram commutes, then there exists an $\tilde{h}$ completing the diagram.
\end{lem}
\begin{proof}
  Let $h,\tilde{h}_0$ as above.
  We define $\tilde{h}$ as follows:
  For all $z \in \Z$, the function $h$ induces a path $\alpha_z = h(z,-): [0,1] \to X$.
  By the previous Lemma, for any point $y_0 \in \pi^{-1}(h(z,0))$ we can lift this to a path $\tilde{\alpha}_z: [0,1] \to Y$ 
  with
  $(\pi \circ \tilde{\alpha}_z) = \alpha_z$ and $\tilde{\alpha}_z(0) = y$.
  In particular, we can chose $y_0 = \tilde{h}_0(z)$.
  Then define $\tilde{h}$ as
  \begin{align*}
    \tilde{h}: Z \times [0,1] \to Y, \quad (z,t) \mapsto
    \tilde{\alpha}_z(t)
  \end{align*}
  To show that the diagram commutes in the lower-right and upper-left triangle, we have to show
  \begin{align*}
    \pi \circ \tilde{h} = h \quad \text{and} \quad \tilde{h} \circ \iota = \tilde{h}_0
  \end{align*}
  Indeed, these follow directly from construction:
  \begin{align*}
    \pi(\tilde{h}(z,t))
    =
    \pi(\tilde{a}_z(t)) 
    = 
    a_z(t) 
    = 
    h(z,t) 
    \quad \text{and} \quad 
    \tilde{h}(\iota(z))
    =
    \tilde{h}(z,0)
    =
    \tilde{\alpha}_z(0)
    = 
    \tilde{h}_0(z)
  \end{align*}
  
  For uniqueness, let $\tilde{h}$ and $\tilde{h}'$ be two such functions. Then for all $z \in \Z$ the paths
  \begin{align*}
    \tilde{\alpha}' := \tilde{h}'(z,-) \text{ and } \tilde{\alpha} := \tilde{h}(z,-)
  \end{align*}
  would be lifts of the path $\alpha = h(z,-)$ at $y_0 = \tilde{h}_0(z)$.
  But by the previous Lemma, these would be identical:
  \begin{align*}
    \tilde{\alpha}'(t) = \tilde{\alpha}(t) \forall t \in [0,1] \implies \tilde{h}'(z,t) = \tilde{h}(z,t) \forall (z,t) \in Z \times [0,1]
  \end{align*}
\end{proof}

We will use this lemma to show that lifts of homotopic paths are homotopic, assuming that they are lifted to the same basepoint.


\begin{cor}[\hypertarget{cor:monodromy-lemma}{Monodromy Lemma}]\label{cor:monodromy}
  Let $\pi: Y \to X$ be a covering, $y_0 \in Y$ and $\alpha,\beta:[0,1] \to X$ be paths that are homotopic rel. endpoints.

  If $\tilde{\alpha},\tilde{\beta}:[0,1] \to Y$ are lifts of $\alpha$ and $\beta$, respectively at $y_0$, then $\tilde{\alpha}, \tilde{\beta}$ are also homotopic rel. endpoints.

  In particular, $\tilde{\alpha}(1) = \tilde{\beta}(1)$.
\end{cor}
\begin{proof}
  This follows directly from the previous Lemma.

Since $\alpha,\beta$ are homotopic rel. endpoints $x_0,x_1$, there exists a homotopy
$h: [0,1] \times [0,1] \to X$ with
\begin{align*}
  h(0,t) = \alpha(t), 
  \quad
  h(1,t) = \beta(t), 
  \quad
  h(s,0) = \alpha(0) = \beta(0)
  = x_0, 
  \quad
  h(s,1) = \alpha(1) = \beta(1) = x_1
\end{align*}
Then the constant map $\tilde{h}_0(s) := y_0$ is a lift of $h_0(s) = h(s,0) = x_0$.

By the Lemma, there exists a (unique) lift
\begin{align*}
  \tilde{h}:[0,1] \times[0,1] \to Y \quad \text{with} \quad 
  \tilde{h}(s,0) = \tilde{h}_0(s) \quad \text{and} \quad \pi(\tilde{h}(s,t)) = h(s,t)
\end{align*}
The first equality means $\tilde{h}(s,0) =  \tilde{h}_0(s) = y_0$.
The second means that $\tilde{h}(0,-)$ is a lift of $h(0,-) = \alpha$ and $\tilde{h}(1,-)$ is a lift of $\beta$.

But the lift of paths is unique, and since $\tilde{\alpha},\tilde{\beta}$ are already lifts, we have
\begin{align*}
  \tilde{\alpha}(t) = \tilde{h}(0,t) \quad \text{and} \quad \tilde{\beta}(t) = \tilde{h}(1,t)
\end{align*}

To show that the endpoints are fixed, consider 
the continuous map $\tilde{h}(-,1): [0,1] \to Y$. 
Since it is a lift of $h$, we have $\tilde{h}(s,1) \in \pi^{-1}(h(s,1)) = \pi^{-1}(x_0)\subseteq Y$.
But this is a discrete set (see \ref{dfn:covering}), and so the only way for $\tilde{h}(1,-)$ to be continous is if it is constant.
\end{proof}


\begin{rem}[]
The previous Lemma and the Lifting of Paths Lemma are closely related.
In fact you can translate between their statements by simply exchanging some words:
\begin{itemize}
  \item The ``Lifting of Paths'' Lemma says that 
    for a given a path $\alpha$ and some lift $y_0$ of the basis point $\alpha(0)$, there exists a unique lift $\tilde{\alpha}$ of $\alpha$ to $y_0$.
  \item With the replacements
    \begin{align*}
      \alpha \mapsto h, y_0 \mapsto \tilde{h}_0, \alpha(0) \mapsto h_0, \tilde{\alpha} \mapsto \tilde{h}
    \end{align*}
    we get that for a given $h$ and some lift $\tilde{h}_0$ of the basis function $h_0$, there exists a unique lift $\tilde{h}$ of $h$ to $\tilde{h}_0$.

    So we exchanged points $\alpha(0), y_0$ with functions $h_0, \tilde{h}_0$ and we exchanged paths $\alpha, \tilde{\alpha}$ with homotopies $h,\tilde{h}$.
\end{itemize}
As an exercise, find out what result we would get if we replaced functions $h_0, \tilde{h}_0$ with homotopies $H_0, \tilde{H}_0$ and replaced homotopies $h, \tilde{h}$ with homotopies of homotopies $H, \tilde{H}$.
\end{rem}
%The next exercise should be a test if you understand the contents of this section. 
%If it takes you more than a few moments to solve, go through more examples where we can appply 

\vfill
You should now \ldots
\begin{itemize}
  \item For a covering $\pi: Y \to X$, we can always lift paths in $X$ to paths in $Y$.

  \item The lift of homotopic paths are homotopic.
\end{itemize}
