\subsection{The covering space}
\begin{dfn}[] \label{dfn:covering}
  A continuous surjective map $\pi: Y \to  X$ is called a \textbf{covering} (\textbf{Überlagerung}) of $X$, if $\pi$ is is a fiber bundle, such that the fibers $\pi^{-1}(x)$ are discrete for all $x \in X$.
\end{dfn}


\begin{xmp}[]
The map $\pi: \R \to \IS^{1}, x \mapsto e^{2 \pi i x}$ is a covering.

To see this, we can draw $\R$ not in a straight line, but rather as a spiral $\subseteq \R^{3}$ along the $z$ axis, where $\pi$ quotients out the $z$ dimension.

More precisely, for $z_0 \in \IS^{1}$, set $U = \IS^{1}\setminus \{-z_0\}$.
Then for any $x_0 \in \R$ with $e^{2 \pi i x_0} = z_0$ we can write the inverse image as
\begin{align*}
  x_0 \in 
  \pi^{-1}(U) = \R \setminus \{k + x_0 - \tfrac{1}{2} \big\vert k \in \Z\}
\end{align*}
Here the fibers for all $z \in \IS^{1}$ are $F_z= \Z$ with the discrete topology and the local homeomorphism is the map
\begin{align*}
  \phi: \pi^{-1}(U) \to  U \times \Z, \quad x \mapsto  (e^{2 \pi i x}, \floor{x - x_0 + \tfrac{1}{2}})
\end{align*}
with continuous inverse
\begin{align*}
  \phi^{-1}: U \times \Z \to  \pi^{-1}(U), \quad (x,k) \mapsto  \frac{\log(z)}{2 \pi i} + k
\end{align*}
which let the following diagram commute
\begin{center}
\begin{tikzcd}[column sep=0.8em] %\arrow[bend right,swap]{dr}{F}
  \pi^{-1}(U) \arrow[]{rr}{\phi} \arrow[swap]{dr}{\pi} & & U \times \Z \arrow[]{dl}{\tilde{\pi}}\\
                                 & U
\end{tikzcd}
\end{center}
\end{xmp}


\begin{lem}[Alternative Definition Covering]\label{lem:alt-covering}
Let $\pi: Y \to X$ continuous and surjective. 
Then $\pi$ is a covering if and only if:

For all $x \in X$ there exists an open neighborhoood $U \subseteq X$ of $x$ and disjoint open subsets $(U_i)_{i\in I}$ of $Y$ such that
\begin{align*}
  \pi^{-1}(U) = \bigsqcup_{i \in I}U_i \quad \text{and} \quad \pi|_{U_i}^{U}: U_i \to U \text{ is a homeomorphism}
\end{align*}
\end{lem}
%\textbf{Warning:} The size of the set $I$ may depend on the choice of $x \in X$.

\begin{proof}
  \textbf{Old def. $\bm{\implies}$ Alt.\ def.} 
  Let $x \in X$ and $U$ be a neighborhood of $x$ such that
  \begin{align*}
    \pi|_{\pi^{-1}(U)}: \pi^{-1}(U) \to U
  \end{align*}
  is isomorphic over $X$ to some
  \begin{align*}
    \tilde{\pi}: X \times F, \quad (x,i) \mapsto  x
  \end{align*}
  This means that there exist homeomorphisms $\Phi,\Psi$ such that the following diagram commutes:
  \begin{align*}
    \xymatrix{
      Y
      \ar@/^2ex/[rr]^\Phi
      \ar[dr]_{\pi}
      &
      &
      X \times F
      \ar[dl]^{\tilde{\pi}}
      \ar@/^2ex/[ll]^\Psi
      \\
      & X
    }
  \end{align*}
  Set $U_i := \Psi(\tilde{\pi}^{-1}(U \times \{i\})) \subseteq Y$ for all $i \in F$.
    By construction, they are open and disjoint and the projection $\pi|_{U_i}^{U}: U_i \to U$ is the composition of homeomorphisms and thus also a homeomorphism.
    \begin{align*}
      \pi|_{U_i}^{U}= (\tilde{\pi} \circ \Phi)|_{}
      : U_i \stackrel{\iso}{\to} \tilde{\pi}^{-1}(U) \times \{i\} \stackrel{\iso}{\to} U
    \end{align*}
    The decomposition $\pi^{-1}(U) = \bigsqcup_{i \in F}^{U_i}$ also follows from the construction.

    \textbf{Alt.\ def. $\bm{\implies}$ Old def.} 
    Let $x \in X$ and $U$ as in the lemma. Then set $F := \pi^{-1}(x) \subseteq Y$ with the subspace topology.
  Because $F \cap U_j = \{y_j\}$, we know that $F$ is discrete.
  Then for all $y_i \in \pi^{-1}(x)$ we define the map
  \begin{align*}
    \phi: \pi^{-1}(U) = \bigsqcup_{i \in I}U_i \to  U \times F, \quad y \mapsto (\pi(y),j)
  \end{align*}
  which is a homeomorphism.
\end{proof}

\begin{dfn}[]
A function $\pi: Y \to  X$ is called \textbf{local homeomorphism}, if
$\forall y \in Y$ there exists an open neighborhood $V \subseteq Y$ of $y$ and an open subset $U \subseteq X$ such that $\pi|_{V}^{U}: V \to  U$ is a homeomorphism.
\end{dfn}
If $\abs{\pi^{-1}(x)} = 1$ for all $x \in X$. Then this is just a homeomorphism.

Local homeomorphisms are not the same as coverings!
\begin{xmp}[]
  Set $X = \R$ and $Y = \R \times \{0\} \cup (0,\infty) \times \{1\}$ with the map
  \begin{align*}
    \pi: Y \to  X, (a,b) \mapsto  a
  \end{align*}
  is a local homeomorphism, but not a covering because the fibers do not vary continuously at $a = 0$.
\end{xmp}

\begin{dfn}[]
Let $\pi: Y \to  X$ be a covering.
\begin{itemize}
  \item The neighborhood $U \subseteq X$ as in Lemma \ref{lem:alt-covering} is said to be \textbf{uniformly covered} by $\pi$ and the $U_i$ are called \textbf{leaves} of $\pi$ over $U$.
  \item A covering $\pi$ is called a covering \textbf{of $n$ leaves}, if $\abs{\pi^{-1}(x)} = n$ for all $x \in X$.
\end{itemize}
\end{dfn}

\begin{xmp}[]
Let $d \in \N$. The map
\begin{align*}
  \pi: \IS^{d} \to \R\IP^{d} = \IS^{d}/\sim, \quad v \sim - v, \quad v \mapsto [v]
\end{align*}
is a covering of $2$ leaves.

For the covering $\pi: \R \to \IS^{1}$, the subset $U = \{e^{2 \pi i t}, t \in [0,\frac{1}{2}\}$ is uniformly covered, but the subset $V = X$ is not.
\end{xmp}


\vfill

You should now know \ldots
\begin{itemize}
  \item the two equivalent definitions of a covering.
    They (especially the alternative one) will be used implicitly throughout Chapter~\ref{sec:lifts}.
\end{itemize}
