\subsection{Collapsing of subspaces to a point}

\begin{dfn}[]
Let $X$ be a topological space.
\begin{itemize}
  \item For $A \subseteq X$ a non-empty subset, define $\faktor{X}{A} := \faktor{X}{\sim}$, where
  \begin{align*}
    x \sim y \iff (x = y) \text{ or } x,y \in A
  \end{align*}
  \item For $A_{1}, \ldots, A_{n} \subseteq X$ non-empty and pairwise disjoint, define $\faktor{X}{A_{1}, \ldots, A_{n}} := \faktor{X}{\sim}$, where
  \begin{align*}
    x \sim y \iff x = y \text{ or } \exists i: x,y \in A_i
  \end{align*}
\end{itemize}
\end{dfn}

We have already seen this type of spaces when discussing quotient topologies.
For example for $X = \mathbb{D}^{2}$ and $A = \del X = \mathbb{S}^{1}$, we got $X/A \iso \mathbb{S}^{2}$

Note that if $X$ is metrizable (or $T_2,T_4$) and if the $A_i$ are closed, then the quotient space is $T_2$. (We will prove this later when discussing $T_4$ spaces).

\begin{xmp}[The cone]
  Given a topological space $X$, the quotient space 
  \begin{align*}
    \text{Cone}(X) :=
    \faktor{X \times [0,1]}{X \times \{1\}}
  \end{align*}
  is called the \textbf{cone} over $X$.
  Draw $\text{Cone}(X)$ for the following spaces
  \begin{itemize}
    \item $X = \IS^{1}$. Draw your favourite ice cream flavour into $\text{Cone}(\IS^{1})$.
    \item $X = [0,1] \sqcup  [2,3]$
    \item $X = \{0\}$
    \item $X = \text{Cone}(\{0\})$
    \item $X = \text{Cone}(\text{Cone}(\{0\}))$
  \end{itemize}
\end{xmp}

\begin{xmp}[Suspension]
  For a topological space $X$, the \textbf{suspension} of $X$ is the space
  \begin{align*}
    \Sigma(X) := \faktor{X \times [-1,1]}{X \times \{-1\}, X \times \{1\}}
  \end{align*}
  which can also be obtained by taking two cones and ``glueing'' them together at the base $X \times \{0\}$. 
  (What glueing is will be defined later but it should make intuitive sense).
  Draw $\Sigma(X)$ for the following spaces:
  \begin{itemize}
    \item $X = \{0\}$
    \item $X = \Sigma(\{0\})$.
    \item $X = \Sigma(\Sigma(\{0\}))$
  \end{itemize}
\end{xmp}

\begin{xmp}[]
For a subset $A \subseteq X$ of a topological space, the \textbf{cone over $A$} is the space
\begin{align*}
  C_A(X) := \faktor{X \times [0,1]}{A \times \{1\}}
\end{align*}
Draw $C_A(X)$ for the following spaces:
\begin{itemize}
  \item $X = [0,1], A = \{0,1\}$.
  \item $X = \IS^{1}, A = \{\pm 1\}$. Find a pair of clothing that looks like it.
\end{itemize}
\end{xmp}

\begin{dfn}[Wedge \& Smash]
For $(X,x_0), (Y,y_0)$ pointed topological spaces, define
\begin{align*}
  \text{The \textbf{wedge} } X \vee Y &:= X \times \{y_0\} \cup \{x_0\} \times Y \subseteq X \times Y\\
  \text{The \textbf{smash} } X \wedge Y &:= \faktor{X \times Y}{X \vee Y}
\end{align*}
\end{dfn}

\begin{xmp}
      The smash product is very difficult to explain in just words, but if you make some drawings it should be very sipmle. (I haven't had the time to enter the illustrations in this document, so please refer to the drawings from the hand-written notes in class).
  \begin{itemize}
    \item For $(X,x_0) = (Y,y_0) = ([0,1],\tfrac{1}{2})$ their smash product looks like a four-leaf clover (without the stem).
    %\item For $X = \IS^{2}$ and $Y = [0,1]$, their smash product looks like an empty hourglass without lids.
    \item 
      Take $X = Y = \IS^{1}$. We know already that $\IS^{1} \times \IS^{1} = \mathbb{T}^{2}$ is the torus.
      The set $X \times \{y_0\} \cup \{x_0\} \times Y$ consists of two perpendicular circles along the equators of the torus.

      To see what the smash product $\IS^{1} \wedge \IS^{1}$ looks like, imagine you take these two circles and tighten them together until they become a point. You will end up with a (slightly deformed) sphere.
      Another way to show this is to draw the torus as the quotient of a rectangle where we identify opposite sides.
      The wedge is then the boundary of the rectangle. Identifying the boundary gives us the $2$-sphere $\IS^{2}$.
    \item The smash product $\IS^{1} \wedge ([0,1], \tfrac{1}{2})$ looks like two disks glued together at a point on their boundary.
    %\item The smash product $([0,1]^{2},(\tfrac{1}{2},\tfrac{1}{2})) \wedge ([0,1],\tfrac{1}{2})$ looks like two pancakes 
  \end{itemize}
\end{xmp}
The fact that $\IS^{1} \wedge \IS^{1} \iso \IS^{2}$ is not a coincidence. In fact:
\begin{lem}[]
  For all $n,m \in \N$:
  \begin{align*}
    \IS^{n} \wedge \IS^{m} \iso \IS^{n+m}
  \end{align*}
\end{lem}
\begin{proof}
Note that $\mathbb{S}^{n} = \R^{n} \cup \{\infty\}$ is the one-point-compactification of $\R^{n}$ and define the mapping \begin{align*}
  g: \R^{n} \cup \{\infty\} \times \R^{m} \cup \{\infty\} \to \R^{n+m} \cup \{\infty\}
\end{align*}
given by
\begin{align*}
  g(x,y) =
  \left\{\begin{array}{ll}
      (x,y)  & \text{ if } x \in \R^{n} \text{\ and } y \in \R^{m}\\
     \infty& \text{ otherwise}
  \end{array} \right.
\end{align*}
Since $g^{-1}(\infty) = X \vee Y$, the result follows.

\end{proof}

