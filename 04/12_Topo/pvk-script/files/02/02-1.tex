\subsection{Definitions}
Let $X$ be a set and $\sim$ an equivalence relation on $X$ (so $\sim \subseteq X^{2}$ is reflexive, symmetric and transitive).
For $x \in X$, we denote its equivalence class by
\begin{align*}
  [x] = \{y \in X \big\vert y \sim x\} \subseteq X
\end{align*}
and the set of equivalence classes by
\begin{align*}
  \faktor{X}{\sim} := \{[x] \big\vert x \in X\} = \left\{\{y \in X \big\vert y \sim x\}\big\vert x \in X\right\}
\end{align*}
There is a canonical map $\pi_{\sim}$ called the \textbf{quotient map} (usually just denoted $\pi$) which is given by
\begin{align*}
  \pi: X \to \faktor{X}{\sim}, \quad x \mapsto [x]
\end{align*}

When $X$ is a topological space, how (and when) can we define a reasonable topology on $\faktor{X}{\sim}$?

We know for vector spaces that in order for an equivalence class to give rise to another vector space, we must require that
\begin{align*}
  u \sim v \iff u - v \in W \text{ for some vector space } W
\end{align*}
For topological spaces, we don't need that and we can always turn $\faktor{X}{\sim}$ into a topological space.

\begin{dfn}[]
Let $X$ be a topological space and $\sim$ an equivalence relation on $X$ and $\pi: X \to \faktor{X}{\sim}$ the quotient map.
We call a subset $U \subseteq \faktor{X}{\sim}$ open in the \textbf{quotient topology}, if $\pi^{-1}(U)$ is open in $X$.
We call $\faktor{X}{\sim}$ with this topology the \textbf{quotient space} of $X$ under $\sim$.
\end{dfn}

\begin{xmp}[]
\begin{enumerate}
  \item Let $X = \R^{2}$ and define the equivalence relation
    \begin{align*}
      (x,y) \sim (x,y'), \quad \forall x,y,y' \in \R
    \end{align*}
    Here, the equivalence classes are all the vertical lines. The resulting quotient space is homeomorphic to $\R$.
    Unlike linear algebra, one could even let the equivalence classes consist of curved lines. For example define the equivalence relation 
    \begin{align*}
      (x,y) \sim (x',y') \iff x-x' = \sin(y-y')
    \end{align*}
    this also results in a space homeomorphic to $\R$.
  \item For $X = [0,1]$ let $x \sim y \iff (x<1 \land y<1)$ or $x=y=1$.
      Then $\faktor{X}{\sim}$ consists of only two equivalence classes:
      That of $[0]$ and $[1]$. The induced topology is then the \textbf{Sierpinsky topology}
      \begin{align*}
        \tau_{X/\sim} = \left\{
          \emptyset, \{[0],[1]\}, \{[0]\}
      \right\}
      \end{align*}
    \item For $X = [0,1]$ we \emph{glue} together the endpoints with the equivalence class
      \begin{align*}
        x \sim y \iff x = y \text{ or } (x,y) = (0,1)
      \end{align*}
      We will later see that the resulting space is homeomorphic to the circle space $\mathbb{S}^{1}$
\end{enumerate}
\end{xmp}
How does this compare to other possible topologies on $X/\sim$?

We know that compactness and connectedness are preserved under continous maps, so it follows that the quotient space of a compact/connected space is again compact/connected.

%\begin{rem}[For the minimalists*]\label{rem:minimal-quotient}
%  The definition of the quotient topology made use of the fact that topological spaces are made out of a \emph{set}, or else we couldn't define an equivalence relation on it.
%  It would be nice if we could consider ``spaces'' that are not made out of sets, but arbitrary things that relate to each other in the form of a general \emph{category} (See Chapter~\ref{sec:category-theory}).
%
%  Let $X$ be a topological space and $Y$ a set.
%  A function $f: X \to Y$ is called a \textbf{quotient map}, if it is surjective.
%  The quotient topology on $Y$ is the topology
%  \begin{align*}
%    \tau_Y = \left\{
%      U \subseteq Y \big\vert f^{-1}(U) \text{\ is open in $X$}
%    \right\}
%  \end{align*}
%\end{rem}


