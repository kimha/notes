\subsection{Duality*}
\textbf{This section is not exam relevant.} 
{\color{black!65!white}It gives some examples of how we can use the language of category theory to show that various phenomena in different contexts are fundamentally the same.

\begin{dfn}[]
Let $\textsf{C}$ be a category. The \textbf{opposite category} $\textsf{C}^{\text{op}}$ has the same objects as $\textsf{C}$, but for every morphism $X \stackrel{f}{\to} Y$ in $\textsf{C}$, we have a morphism $Y \stackrel{f^{\text{op}}}{\to} X$ in $\textsf{C}^{op}$.
More precisely, $\text{Obj}(\textsf{C}^{\text{op}}) = \text{Obj}(\textsf{C})$ and $\Hom_{\textsf{C}^{\text{op}}}(X,Y) = \Hom_{\textsf{C}}(Y,X)$.
\end{dfn}
Verify that this forms a valid category by defining composition yourself and checking associativity as well as the left/right unit relations for the identity morphisms.

%\begin{dfn}[]
%  In the following, let $c$ be an object in a Category $\textsf{C}$.
%  \begin{itemize}
%    \item There is a category $c/\textsf{C}$ called the \textbf{slice category} of $\textsf{C}$ \textbf{under} $c$, where the objects are morphisms $f: c \to X$ from $\textsf{C}$, 
%      and a morphism between objects $c \stackrel{f}{\to}$ and $c \stackrel{g}{\to}Y$ is a morphism $h: X \to Y$ from $\textsf{C}$ such that the following diagram commutes:
%      \begin{center}
%        \begin{tikzcd}[column sep=0.8em] %\arrow[bend right,swap]{dr}{F}
%         &  c \arrow[swap]{dl}{f} \arrow[]{dr}{g}\\
%          X \arrow[]{rr}{h} & & Y
%        \end{tikzcd}
%      \end{center}
%      i.e. so that $h \circ f = g$ .
%    \item There is a category $\textsf{C}/c$ called the \textbf{slice category} of $\textsf{C}$ \textbf{over} $c$, where the objects are morphisms $f: X \to c$ and a morphism between objects $X \stackrel{f}{\to}c$, $Y \stackrel{g}{\to}c$ is a morphism $h: X \to Y$ such that the following diagram commutes:
%      \begin{center}
%        \begin{tikzcd}[column sep=0.8em] %\arrow[bend right,swap]{dr}{F}
%          X \arrow[swap]{dr}{f}\arrow[]{rr}{h} & & Y \arrow[]{dl}{g}\\
%         &  c 
%        \end{tikzcd}
%      \end{center}
%  \end{itemize}
%  Note how the two examples above are \emph{dual} to eachother. We used one definition, reversed the arrows and obtained another definition of a category.
%\end{dfn}

The definition of injective/surjective maps between sets we have seen in introductory set theory makes use of the word ``element'' (A function $f: X \to Y$ is injective, if for all $x,y \in X$ such that \ldots).
In category theory however, cannot talk about ``elements'' or what is ``inside'' an object.
We wish to understand objects not by analyzing its internal properties, but rather through its relation to other objects from that category.

\begin{dfn}[]
A morphism $f: X \to Y$ in a category is said to be
\begin{itemize}
  \item a \textbf{monomorphism} (or \textbf{monic}), if for any pair of parallel morphisms $g,h: W \to X$ we have the implication $f \circ g =f \circ h \implies g = h$.
    \begin{center}
      \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
        W \arrow[transform canvas={yshift=0.4ex}]{r}{g}
        \arrow[swap,transform canvas={yshift=-0.4ex}]{r}{h}
        & X \arrow[]{r}{f} & Y
      \end{tikzcd}
    \end{center}

  \item an \textbf{epimorphism} (or \textbf{epic}), if for any pair of parallel morphisms $g,h: Y \to Z$ we have the implication $g \circ f =h \circ f \implies g = h$.
    \begin{center}
      \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
        Z  &
        Y  
        \arrow[swap,transform canvas={yshift=0.4ex}]{l}{g}
        \arrow[transform canvas={yshift=-0.4ex}]{l}{h}
           & X \arrow[swap]{l}{f}
      \end{tikzcd}
    \end{center}
\end{itemize}
\end{dfn}

In the categories $\Set,\Top$ and some others, monomorphisms/epimorphisms are excactly the injective/surjective maps.
Just like how in topology, a bijective continuous function need not be a homeomorphism, a morphism in a category that is both monic and epic need not be iso.

Another counter example is the canonical embedding $\Z \hookrightarrow \Q$ in the category $\Ring$, which is both monic and epic, but clearly not an isomorphism.

\begin{rem}[]
Alternatively, we can define epimorphic maps using the opposite category.
A morphism $f: X \to Y$ is an epimorphism, if $f^{\text{op}}$ is a monomorphism in $\textsf{C}^{\text{op}}$.
\end{rem}

The fact that these definitions of monomorphisms and epimorphisms are dual to each other is useful because if we can prove something for monic maps, then the same holds true in the opposite category for epic maps, so we only have to prove ``half as much''.

\begin{lem}[]\label{lem:mono-epi}
  \begin{enumerate}
    \item If $f: X \mono Y$ and $g: Y \mono Z$ are monomorphisms, then so is $g \circ f: X \mono Z$
    \item If $f: X \to Y$ and $g: X \to Z$ are morphisms such that $g \circ f$ is monic, then $f$ is monic.
  \end{enumerate}
  And dually:
  \begin{enumerate}
    \item[(a')] If $f: X \epi Y$ and $g: Y \epi Z$ are epimorphisms, then so is $g \circ f: X \epi Z$.
    \item[(b')] If $f: X \to Y$ and $g: X \to Z$ are morphisms such that $g \circ f$ is epic, then $g$ is epic.
  \end{enumerate}
\end{lem}
We only need to prove (a) and (b) here. (a') and (b') follow by duality.

\begin{dfn}[]
If $A \stackrel{s}{\to} X \stackrel{r}{\to} A$ are morphisms such that $r \circ s = \id_A$, then we call $s$ a \textbf{section} or \textbf{right inverse} to $r$, while $r$ defines a \textbf{retraction} or \textbf{left inverse} to $s$.
We call $A$ a \textbf{retract} of the object $X$.
\end{dfn}
Applying this definition in the category $\Top$, we recover the definition of rectraction: 
A retraction (in the sense of Definition \ref{dfn:retraction}) is a left inverse to the inclusion map $\iota$ and also a deformation retract, if it is a right inverse to the inclusion map $\iota$: $X \stackrel{\rho}{\to} A \stackrel{\iota}{\hookrightarrow} X$ in the category $\Htpy$.

Many objects (such as Quotient Spaces, Tensor product, Product topology, Polynomial rings etc.) we studied in Linear Algebra and Topology could be understood as a construction, where we directly defined the object through its elements and its structure.
It turns out that we can understand them through their relationship with other objects.
The special relationships usually came in the form of \textbf{universal properties}. 

\begin{dfn}[]
	Let $\textsf{C}$ be a category.
	\begin{itemize}
		\item An \textbf{initial object} of the category, if it exists, is an object $\emptyset$ with the \textbf{universal property} that, for any other object $X$ in $\textsf{C}$, there exists a \emph{unique} morpshism $\emptyset \to X$.
		\item A \textbf{terminal object}, if it exists, is an object $1$ with the universal property that, for any other object $X$ in $\textsf{C}$, there exists a unique morphism $X \to 1$.
	\end{itemize}
\end{dfn}
\begin{xmp}[]
The notation used for $\emptyset$ and $1$ are suggestive, but can be misleading depending on the category.
\begin{itemize}
	\item In the category $\Set$ (and in $\Top$), the initial object is the empty set $\emptyset$ and the terminal object is the singleton set $\{\ast\}$.
	\item In the category $\Grp$, the trivial group $\{e\}$ is both an initial and terminal object. In this case, we also call it a \textbf{zero object}.
	\item In the category $\Ring$, the initial object is the ring of integers $\Z$. 
		Depending on whether we use the axiom $0 \neq 1$ in the definition of a ring, the category may or may not have a terminal object $\{1\}$.
\end{itemize}
\end{xmp}
Notice that we wrote ``the'' initial object or ``the'' terminal object even though for example, there are multiple sets with a single element. 
That is because in category theory, if two things are isomorphic (in the sense of definition \ref{dfn:cat-iso}) they can be thought of as being essentially the same in that category.
\begin{lem}[] \label{lem:unique-iso}
	In any category, the inital and terminal objects, \emph{if they exist}, are unique up to isomorphism.
\end{lem}
\begin{proof}
  As in Lemma~\ref{lem:mono-epi}, note that the terminal object is the initial object in the opposite category, so it suffices to prove it for one of them.
	Suppose $X,Y$ are initial objects with identity morphisms $\id_X$ and $\id_Y$, respectively.
	By their universal properties there exist (unique) morphisms $f: X \to Y$ and $g: Y \to X$. 
	Considering their composition, we get a morphism $g \circ f: X \to X$.
	But by the universal propery, there can only be one unique morphism $X \to X$, so $g \circ f = \id_X$.
	Likewise for $Y$, we get $f \circ g = \id_Y$.
\end{proof}

We can use the notion of universal property in a more general way by using a meta-defintion.
\begin{dfn}[Informal definition]
  An object is said to have a \textbf{universal property}, if it is the inital or terminal object in the category of objects with this property.
\end{dfn}

Let's see how the meta-definition can be used to describe universal properties of some known objects.


\begin{xmp}[Product]
  In the category $\Set$, the (cartesian) product $X \times Y$ of two sets $X,Y$ is equipped with \textbf{projection mappings}
  \begin{align*}
    \pi_X: X \times Y \to X, \quad (x,y) \mapsto x, 
    \quad \text{and} \quad
    \pi_Y: X \times Y \to Y, \quad (x,y) \mapsto y
  \end{align*}
  The cartesian product has the universal property that, for any other set $Z$ with functions $f: Z \to X, g: Z \to Y$, there exists a \emph{unique} function $\phi:Z \to X \times Y$ such that the following diagram commutes.
  \begin{center}
  \begin{tikzcd}[column sep=1em] %\arrow[bend right,swap]{dr}{F}
    Z \arrow[bend left]{drr}{f} \arrow[bend right]{ddr}{g}\arrow[dashed]{dr}{!\exists\phi}\\
    & X \times Y \arrow[]{r}{\pi_X} \arrow[]{d}{\pi_Y}& X\\
    & Y
  \end{tikzcd}
  \end{center}
  Where the map $\phi$ is given by $\phi: Z \to X \times Y, z \mapsto (f(z),g(z))$.
  
  If we view this in the category where the objects are sets with functions to $X$ and $Y$, and where a morphism between objects 
  $\left\{Z' \stackrel{f'}{\to}X, Z' \stackrel{g'}{\to}Y\right\}$
  and
  $\left\{Z \stackrel{f}{\to}X, Z \stackrel{g}{\to}Y\right\}$
  is a function $\phi: Z'  \to Z$ such that $f \circ \phi = f'$ and $g \circ \phi = g'$, 
  then the cartesian product $X \times Y$ is the terminal object of this category.

 In the category $\Top$, the cartesian product with the product topology has the same universal property of being the terminal object of such a category.

 In the category $\textsf{k-Vec}$, the direct product $V \oplus W$ has the same universal property.
\end{xmp}

This leads to our definition of a Product in a general category:

\begin{dfn}[]
  Let $(X_i)_{i \in I}$ be a collection of objects in a category. The (cartesian) \hypertarget{dfn:categorial-product}{\textbf{product}} of these objects (if it exists), is an object $Z$ with morphisms 
  $\left\{Z \stackrel{\pi_i}{\to}X\right\}_{i \in I}$ 
  such that for any other object $Z'$ with morphisms
  $\big\{Z' \stackrel{\pi_i'}{\to}X\big\}_{i \in I}$ there exists a \emph{unique} morphism $\phi: Z' \to Z$ such that for all $i \in I$ the following diagram commutes
  \begin{center}
  \begin{tikzcd}[column sep=0.8em] %\arrow[bend right,swap]{dr}{F}
    & Z' \arrow[dashed,swap]{dl}{!\exists \phi} \arrow[]{dr}{\pi_i'}\\
    Z \arrow[]{rr}{\pi_i} & & X_i
  \end{tikzcd}
  \end{center}
\end{dfn}
Again, it follows from the universal proparty (and more specifically, Lemma \ref{lem:unique-iso}) that the product is unique up to isomorphism.

The dual notion of a product is the \emph{coproduct}. 
We could just say that the coproduct of objects $(X_i)_{i \in I}$ is the product in the opposite category and call it a day, but it's a little easier with some examples
\begin{xmp}[Coproduct]
In the category $\Set$, the \textbf{disjoint union} $X \sqcup Y$ of sets $X,Y$ is equipped with canonical \textbf{embeddings}
\begin{align*}
  \iota_X: X \to X \sqcup Y, x \mapsto (x,0)
  \quad \text{and} \quad 
  \iota_Y: Y \to X \sqcup Y, y \mapsto (y,1)
\end{align*}
This embedding is universal in that for any other set $Z$ with maps $f: X \to Z, g: Y \to Z$, there exists a unique function $\phi: X \sqcup Y \to Z$ such that the following diagram commutes
  \begin{center}
  \begin{tikzcd}[column sep=1em] %\arrow[bend right,swap]{dr}{F}
    Z 
    \\
    & X \sqcup Y 
    \arrow[dashed]{ul}{!\exists\phi}
    &
    X
    \arrow[bend right]{ull}{f}
    \arrow[swap]{l}{\iota_X} 
    \\
    & Y
    \arrow[bend left]{uul}{g} 
    \arrow[]{u}{\iota_Y}
  \end{tikzcd}
  \end{center}
\end{xmp}

We can generalise the notion of products and coproducts to respect morphisms between the components $X_i$.
We skip the category theoretical definition of diagrams for now\footnote{This requires the notion of a functor, which maps objects and morphisms from one category to another in a structure-preserving way.} and think of a diagram as a directed graph over an indexed set of vertices.

\begin{dfn}[]
Let $I$ be some ordered index set, $\{X_i\}_{i \in I}$ a collection of objects in a category, together with some morphisms $\{\alpha_{i,j}:X_i \to X_j\}_{i \leq j}$.

A \textbf{cone} over this diagram, is an object $Z$ with morphisms $\{Z \stackrel{f_i}{\to} X_i\}_{i \in I}$ such that for all $i,j$: $\alpha_{i,j} \circ f_i = f_j$ i.e. such that the triangles
\begin{center}
\begin{tikzcd}[column sep=0.8em]
& Z \arrow[swap]{dl}{f_i} \arrow[]{dr}{f_j} &\\
X_i \arrow[]{rr}{\alpha_{i,j}} & & X_j
\end{tikzcd}
\end{center}
commute.

A \textbf{limit} over this diagram (if it exists) is a universal cone.
In other words, if for every cone $Z'$ with morphisms $\{Z' \stackrel{g_i}{\to} X_i\}_{i \in I}$ there exists a unique morphism $\phi: Z' \to Z$ such that $f_i \circ \phi = g_i$.
\begin{center}
\begin{tikzcd}[column sep=0.8em]
& Z' \arrow[dashed]{d}{\phi}\arrow[swap, bend right]{ddl}{g_i} \arrow[bend left]{ddr}{g_j} &\\
& Z \arrow[swap]{dl}{f_i} \arrow[]{dr}{f_j} &\\
X_i \arrow[]{rr}{\alpha_{i,j}} & & X_j
\end{tikzcd}
\end{center}
A \emph{colimit} over this diagram is a limit in the opposite category.
\end{dfn}

\begin{xmp}[Glueing]
  Given two topological spaces $X,Y$, a subset $A \subseteq X$ and a morphism $\phi: A \to Y$, then the glueing of $X$ onto $Y$ by $\phi$ is the colimit of the diagram
  \begin{center}
    \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
      A \arrow[hook]{r}{\iota} \arrow[]{d}{\phi} & X\\
      Y
    \end{tikzcd}
  \end{center}
Equivalently, it is the \textbf{coequalizer} of the parallel morphisms
\begin{center}
\begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
  X_0 \arrow[shift left=0.5ex]{r}{\iota} \arrow[swap,shift right=0.5ex]{r}{\phi} & Y \sqcup X
\end{tikzcd}
\end{center}
where $\iota:A \to X$ is the inclusion mapping.
\end{xmp}

\textbf{More Examples}
\begin{itemize}
  \item The limit of the empty diagram is the terminal object. In $\Set$ this is any singleton set. In $\Top$, this is the one-point space. In $\Grp$ this is the trivial group $\{e\}$. In the category $\N$ with $n \stackrel{k}{\to} m$ if $k \cdot n = m$, it is $0$. In $\textsf{k-Vec}$ it is the zero vector space.
    Dually, the colimit is the initial object.
    \item The product is a limit over a diagram without any morphisms. In the poset $\R$ with $x \to y$ if $x \leq y$, it is the infimum $\inf\{x_i \big\vert i \in I\}$. In $\N$ as above, it is the greatest common divisor. 
    Dually, the colimit over such a diagram is the coproduct.
    \item For the diagram $X \stackrel{f}{\to} Y$ in $\Set$, the limit is the smallest subset of $X \times Y$ such that $[(x,y) \mapsto x \mapsto f(x)] = [(x,y) \mapsto y]$. 
    This is just the graph $\text{Graph}(f) \subseteq X \times Y$.
    \item The limit over a diagram $A \stackrel{f}{\longrightarrow} C \stackrel{g}{\longleftarrow}B$ is called the \emph{pullback}.
    In $\Set$, if $B = \{\ast\}$ is a singleton, then the pullback is the preimage of $g(\ast)$ under $f$. In the Poset cateogry $(\N,\leq)$, it is the greatest common divisor $\gcd(n,m)$. In $\Grp$, if $B = \{e\}$ is the trivial group, then the pullback is the kernel $\Ker f \subseteq A \times \{e\} \cong A$
    \item For a sequence of sets $A_0 \supseteq A_1 \supseteq A_2 \supseteq \ldots$, the limit (in $\Set$) is the intersection $\bigcap_{n \in \N}A_n$.  For a sequence of non-increasing real numbers $x_0 \geq x_1 \geq \ldots$, the limit exists if and only if the sequence is bounded.
    \item The limit over a parallel pair of morphisms $X \overset{f}{\underset{g}{\rightrightarrows}} Y$ is called \emph{equaliser} of $f$ and $g$. 
    If $g:X \to Y$ is a zero morphism (in categories where this makes sense), then the equaliser is the kernel. 
\end{itemize} 
    

\begin{table}[ht]
    \label{tab:limit-examples}
    \centering
\begin{tabular}{l|c|c|c|c}
	& Initial Object & Terminal Object & Product & Coproduct \\ \hline
	$\Set$ & empty set & singleton & cartesian product & disjoint union \\
	$\Grp$ & \multicolumn{2}{c|}{trivial group} 
	& direct product & free product \\
	$\Top$ & empty space & singleton & product topology & disjoint union \\
  $\textsf{k-Vec}$ & \multicolumn{2}{c|}{zero vector space} & cartesian product & direct sum \\
	Field & -- & -- & -- & --\\
  $\Ring$ & $\Z$ & $0$ &  direct product & tensor product
\end{tabular}
    \caption{Examples of limits in some categories.
    Entries marked with $-$ do not always exist.}
\end{table}
}
