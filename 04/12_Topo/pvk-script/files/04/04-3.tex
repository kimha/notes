\subsection{Functors}
If we look at categories where objects are sets with structure and morphisms are structure preserving maps we might ask what the morphisms in the ``category of categories'' are.

The structure of a category would the morphisms, so we can ask what ``morphism-preserving'' morphisms between categories are. 
This gives rise to the definition of a functor.

\begin{dfn}[]\label{dfn:functor}
  Let $\textsf{C},\textsf{D}$ be categories. A {\color{blue}\textbf{covariant}}/{\color{orange}\textbf{contravariant}} \textbf{Functor} $\mathcal{F}$ from $\textsf{C}$ to $\textsf{D}$ (written $\mathcal{F}: \textsf{C} \to  \textsf{D}$) does the following:
  \begin{itemize}
    \item To every object $X \in \text{Obj}(\textsf{C})$ it assigns an object $F(X) \in \text{Obj}(\textsf{D})$.
    \item For all objects $X,Y \in \text{Obj}(\textsf{C})$ and all morphisms $f \in \Hom_{\textsf{C}}(X,Y)$ it assigns a morphism
      \begin{align*}
        F(f) \in {\color{blue}\Hom_{\textsf{D}}(F(X), F(Y))}
        \quad \text{or} \quad 
        F(f) \in
        {\color{orange}\Hom_{\textsf{D}}(F(Y),F(X))}
      \end{align*}
      \begin{enumerate}[{(}i{)}]
        \item $F (\id_X) = \id_{F(X)}$ for all $X \in \text{Obj}(\textsf{C})$
        \item For all composable pairs of morphisms from $\textsf{C}$:
          \begin{align*}
            {\color{blue}F (g \circ f) = F(g) \circ F(f)}
            \quad \text{or} \quad 
            {\color{orange}F (g \circ f) = F(f) \circ F(g)}
          \end{align*}
      \end{enumerate}
      the second condition is equivalent to saying that for any pair of composable morphisms 
      \begin{align*}
        \xymatrix{
          X
          \ar[r]^f
          \ar@(dr,dl)[rr]^{g \circ f}
          &
          Y
          \ar[r]^g
          &
          Z
        }
      \end{align*}
      in $\textsf{C}$, the following diagram commutes in $\textsf{D}$
\begin{center}
  {\color{blue}
\begin{tikzcd}[] %\arrow[bend right,swap]{dr}{F}
  F(X) \arrow[]{r}{F(f)} \arrow[bend right,swap]{rr}{F(g \circ f)} \arrow[bend left]{rr}{F(g) \circ F(f)}&
  F(Y) \arrow[]{r}{F(g)} & 
  F(Z) 
\end{tikzcd}}
\quad \text{or} \quad 
  {\color{orange}
\begin{tikzcd}[] %\arrow[bend right,swap]{dr}{F}
  F(X) 
  &
  F(Y) 
  \arrow[]{l}{F(f)} 
  &
  F(Z) 
  \arrow[]{l}{F(g)} 
  \arrow[bend left]{ll}{F(g \circ f)} 
  \arrow[bend right,swap]{ll}{F(f) \circ F(g)}
\end{tikzcd}}
\end{center}
  \end{itemize}
\end{dfn}
\textbf{Notation:} We will often drop parenthesis for functor application, so we will write $FX$ instead of $F(X)$ or $Ff$ instead of $F(f)$.
Usually, we call covariant functors simply functors. In some literature, both are just called functors\footnote{If you read the previous chapter, this comes from that fact that a contravariant functor $\mathcal{F}: \textsf{C} \to  \textsf{D}$ is the same as a covariant functor $\textsf{C}^{\text{op}} \to \textsf{D}$.}.


\begin{xmp}[]\label{xmp:functors}
  Quite a few concepts that we encountered througout Algebra or Topology are functors.
  \begin{enumerate}
    \item If we have a group $(G,\cdot,e)$, we can forget the group structure and only look at the underlying set $G$.
      Group homomorphisms turn into functions.
      We call this the \textbf{forgetful functor}.
      The same construction works for topological spaces, where we forget the topology of a topological space.
      \begin{align*}
        (G,\cdot,e) \mapsto G, \quad \text{and} \quad 
        \underbrace{(G \stackrel{\phi}{\to} H)}_{\text{group hom.}} \mapsto \underbrace{(G \stackrel{\phi}{\to} H)}_{\text{function}}\\
        (X, \tau) \mapsto X, \quad \text{and} \quad 
        \underbrace{(X \stackrel{f}{\to} Y)}_{\text{cont.\ map}} \mapsto \underbrace{(G \stackrel{\phi}{\to} H)}_{\text{function}}
      \end{align*}
    \item We can turn every field into a group, by removing the $0$. Field homomorphisms then become group homomorphisms.
    \item Given a ring $R$, one can take the group of units $R^{\times}$ to obtain a group.
      This defines a functor $\textsf{Ring} \to \text{Grp}$.
    \item There is a functor $\Top \to \Htpy$ that preserves the objects (topological spaces), but maps continuous maps to their homotopy equivalence classes.
    \item Consider the \textbf{endofunctor} $\textsf{F}: \textsf{Vect}_k \to  \textsf{Vect}_k$ which maps vector spaces to their dual, and linear maps to their dual maps.
      So for vector spaces $V,W$ and a linear map $f \in \Hom(V,W)$ we define
      \begin{align*}
        F V = V^{\ast}, \quad \text{and} \quad Ff := f^{\ast} := (- \circ f) := (\beta \mapsto  \beta \circ f): W^{\ast} \to  V^{\ast}
      \end{align*}
      This is an example of a contravariant functor, as the directon of $Ff$ is opposite to that of $f$.
    \item A directed graph is a tuple $(V,E)$ consisting of a set $V$ of \emph{vertices} and a set $E \subseteq V \times V$ of \emph{edges}.
      Let $I$ be the category with two objects $E,V$ and two (non-identity) morphisms 
      \begin{align*}
        \xymatrix{
          E
          \ar@/^2ex/[r]^{\text{source}}
          \ar@/_2ex/[r]_{\text{target}}
          &
          V
        }
      \end{align*}
      a directed graph is then nothing but a functor $F: I \to \Set$.

      A graph homomorphism between directed graphs $F,G: I \to \Set$ then consists of two functions
      \begin{align*}
        \Phi_{V}: FV \to GV, \quad \Phi_{E}: FE \to GE
      \end{align*}
      such that both these diagrams commute
      \begin{center}
      \begin{tikzcd}[ ] 
        FE
        \arrow[]{r}{F \text{source}}
        \arrow[swap]{d}{\Phi_E}
        &
        FV
        \arrow[]{d}{\Phi_V}
        \\
        GE
        \arrow[swap]{r}{G \text{source}}
        &
        GV
      \end{tikzcd}
      \quad
      \begin{tikzcd}[ ] 
        FE
        \arrow[]{r}{F \text{target}}
        \arrow[swap]{d}{\Phi_E}
        &
        FV
        \arrow[]{d}{\Phi_V}
        \\
        GE
        \arrow[swap]{r}{G \text{target}}
        &
        GV
      \end{tikzcd}
      \end{center}
      This description of a graph homomorphism might seem unnecessarily complicated, but it has a use.
      Note that our definition does not make any use of that fact that $F,G$ are graphs. 
      In fact, this definition can be generalized to give a notion of morphism between two parallel functors $F,G: \textsf{C} \to \textsf{D}$, between arbitrary categories. 
      Such a thing is called a \textbf{natural transformation}.
  \end{enumerate}
\end{xmp}


\begin{lem}[]\label{lem:func-iso}
Let $\textsf{F}: \textsf{C} \to  \textsf{D}$ be a functor.
If $X,Y \in \text{Obj}(\textsf{C})$ and $f \in \Hom(X,Y)$ is an isomorphism, then $Ff \in \Hom(FX,FY)$ (or $\Hom(FY,FX)$ for contravariant functors) is also an isomorphism.
\end{lem}
\begin{proof}
  Since $f$ is an isomorphism, there exists a morphism $g \in \Hom(Y,X)$ such that $g \circ f \stackrel{\dagger}{=}\id_X$ and $f \circ g \stackrel{\ast}{=} \id_Y$.

  \textbf{Covariant case:} 
  By properties (i) and (ii) from Definition~\ref{dfn:functor}, $Ff$ and $Fg$ satisfy
  \begin{align*}
    Fg \circ Ff \stackrel{(ii)}{=} F(g \circ f) \stackrel{\dagger}{=} F(\id_X) \stackrel{(i)}{=} \id_{FX} \quad \text{and} \quad Ff \circ Fg \stackrel{(ii)}{=} (f \circ g) \stackrel{\ast}{=} F \id_Y \stackrel{(i)}{=} \id_{FY}
  \end{align*}
  \begin{align*}
    \xymatrix{
      FX
      \ar@/^2ex/[r]^{Ff}
      \ar@(dl,ul)[]^{\id_{FX}}
      &
      FY
      \ar@/^2ex/[l]^{Fg}
      \ar@(ur,dr)[]^{\id_{FY}}
    }
  \end{align*}
  \textbf{Contravariant case:} The proof is analogous. We have
  \begin{align*}
    Fg \circ Ff = F(f \circ g) = F \id_Y = \id_{F Y} \quad \text{and} \quad Ff \circ Fg = F(g \circ f) = F \id_X = \id_{F X}
  \end{align*}
  \begin{align*}
    \xymatrix{
      FX
      \ar@/_2ex/[r]_{Fg}
      \ar@(ul,dl)[]_{\id_{FX}}
      &
      FY
      \ar@/_2ex/[l]_{Ff}
      \ar@(dr,ur)[]_{\id_{FY}}
    }
  \end{align*}
\end{proof}
\begin{cor}
Every homeomorphism is a homotopy equivalence.
\end{cor}
\begin{proof}
  A homeomorphism is an isomorphism in $\Top$. Applying the functor described in Example~\ref{xmp:functors} (d) gives us an isomorphism in $\Htpy$, which is a homotopy equivalences.
\end{proof}

This next definition is not exam relevant.
{\color{black!65!white}
\begin{dfn}[]
  Let $I$ be a small category.
  A diagram of shape $I$ in a category $\textsf{C}$ is a functor 
  \begin{align*}
    X_{\bullet}: I \to \textsf{C}, \quad (i \stackrel{\phi}{\to}j) \mapsto  (X_i \stackrel{X\phi}{\to} X_j)
  \end{align*}
\end{dfn}
}
