\subsection{First and Second Countable spaces}

As a motivating example consider a metric space $(X,d)$ and $x_0 \in X$, then every neighborhood of $x_0$, no matter how small contains an open ball around $x_0$ of radius $\tfrac{1}{n}$ for some $n \in \N$ large enough.

\begin{dfn}[]
  Let $X$ be a topological space, $x_0 \in X$.
  \begin{itemize}
    \item A collection of neighborhoods of $x_0$
      \begin{align*}
        \mathcal{U} \subseteq \{\text{neighborhoods of $x_0$}\} = \{U \subseteq X \big\vert \exists V \subseteq U \text{ open and }x_0 \in V\}
      \end{align*}
      is called a \textbf{neighborhood basis} of $x_0$, if every neighborhood of $x_0$ contains a $U \in \mathcal{U}$.
    \item We say that $X$ is \textbf{first-countable} (\textbf{erfüllt das erste Abzählbarkeitsaxiom} (\textbf{1AA})), if every point has a countable neighborhood basis.
    \item A collection of subsets $\mathcal{U} \subseteq \mathcal{P}(X)$ is called a \textbf{neighborhood basis} of $X$, if $\mathcal{U}$ is a neighborhoood basis for all $x_0 \in X$.
    \item $X$ is said to be \textbf{second-countable} (\textbf{erfüllt das zweite Abzählbarkeitsaxiom} (\textbf{2AA})), if $X$ has a countable neighborhood basis.
  \end{itemize}
\end{dfn}
\begin{xmp}[]
The motivating example shows that
\begin{itemize}
  \item Every metric space is first-countable.
  \item In particular for $\R^{n}$, we can take the countably-many points with rational coordinates (i.e.\ $\Q^{n} \subseteq \R^{n}$) and for each of those points get a countable neighborhood basis.
    The collection of all those forms a countable neighborhood basis of $\R^{n}$, because $\Q \subseteq \R$ is dense.
\end{itemize}
\end{xmp}

\begin{rem}[]
Let $X$ be a topological space. The following are equivalent:
\begin{enumerate}
  \item $X$ is second countable.
  \item $X$ has a countable basis. (As in Definition~\ref{dfn:basis})
\end{enumerate}
\end{rem}
\begin{proof}
  \textbf{(a)$\bm{\implies}$(b):} Let $\mathcal{U} = (U_n)_{n \in \N}$ be a countable neighborhood basis of $X$. We can assume without loss of generality that the $U_n$ are open. 
  Let $V \subseteq X$ be open.
  For all $v \in V$, $V$ is a neighborhood of $v$, so there exists an $n_v \in \N$ with $v \in U_{n_v} \subseteq V$. Then
  $
    V = \bigcup_{v \in V}U_{n_v}
  $ can be written as the union of elements of $\mathcal{U}$. As $V \subseteq X$ was arbitrary, $\mathcal{U}$ is a countable basis of $X$.

  \textbf{(b)$\bm{\implies}$(a):} Let $\mathcal{U} = (U_n)_{n \in \N}$ be a countable basis of $X$.
  Let $x \in X$ and $V$ a (WLOG open) neighborhood of $x$.
  Since $\mathcal{U}$ is a basis of $V$, there exist $(U_i)_{i \in I_x \subseteq \N}$ such that $V = \bigcup_{i \in I_x}U_i$.
  So for any $i \in I_x$, $U_i$ is a neighborhood of $x$ contained in $V$.
  Repeating this for all $x \in X$, we get that for
  \begin{align*}
    I := \bigcup_{x \in X}I_x \subseteq \N 
  \end{align*}
  $(U_i)_{i \in I}$ is a countable neighborhood basis of $X$.
\end{proof}


\begin{rem}[]\label{rem:n-countable}
  Let $X$ be a topological space.
\begin{enumerate}
  \item $X$ second-countable $\implies$ $X$ first-countable
  \item If $Y \subseteq X$ is a subspace, then 
    \begin{align*}
      X \text{ is first-/second countable } \implies Y \text{ is first-/second-countable }
    \end{align*}
  \item If there exists an uncountable and discrete subset $A \subseteq X$, then $X$ cannot be second-countable.
\end{enumerate}
\end{rem}
\begin{proof}
  \begin{enumerate}
    \item For $x_0 \in X$ and take all elements of $\mathcal{U}$ that contain $x_0$. 
    \item Take all elements of $\mathcal{U}$ and intersect them with $Y$.
    \item Let $\mathcal{B}$ be a basis and choose a $U_a \subseteq X$ such that $U_a \cap A = \{a\}$. This is possible because $A$ is discrete.
    Since $\mathcal{B}$ is a Basis, 
    \begin{align*}
     \forall a \in A, \exists O_a \in \mathcal{B} \text{ with }a  \in O_a \subseteq U_a
    \end{align*}
    Because $O_a \cap A \subseteq U_a \cap A = \{a\}$, this defines an injective map
    $
      A \hookrightarrow \mathcal{B}, a \mapsto O_a 
    $
    and shows that $\mathcal{B}$ is uncountable.
  \end{enumerate}

\end{proof}
\begin{xmp}[]
  The space of continuous and bounded functions with the metric induced by the $\|\cdot\|_{\infty}$-Norm
  \begin{align*}
    C(\R) := \{\phi:  \R \to \R \big\vert \phi \text{ continuous, bounded}\}
  \end{align*}
  is first-countable, but not second-countable.
  To prove this, we construct such an uncountable discrete subset $A$ as in the previous Remark~\ref{rem:n-countable}. Take the set of $(0,1)$ sequences
  \begin{align*}
    \epsilon = \left(\epsilon_{n}\right)_{n \in \N}
  \end{align*}
  using (for example) piecewise linear functions, one can construct a bounded continuous function
  $\phi_{\epsilon}: \R \to \R$ that satisfies $\phi_{\epsilon}(n) = \epsilon_n$.
  for all $n \in \N$.
  Set
  \begin{align*}
    A = \{\phi_{\epsilon} \big\vert \epsilon \in \{0,1\}^{\N}\}
  \end{align*}
  which is clearly uncountable. 
  Moreover, For all $a,b \in A$, $\|a -b\|_{\infty} = 1 - \delta_{ab}$, so $B_1(a) \cap A = \{a\}$ for all $a \in A$ and as such, $A$ is discrete.
\end{xmp}



The following definition and lemma were not covered in the lecture (so they are \textbf{not exam relevant}), but come up in many of the older previous exams, so I added them here if you want to solve them.
{\color{black!60!white}
\begin{dfn}[]
{\color{black!60!white}
  A topological space $X$ is called \textbf{separable}, if it contains a countable dense set.
  That is, there exists a sequence $\left(x_{n}\right)_{n \in \N}$ such that every non-empty open subset of $X$ contains at least one $x_n$.
}
\end{dfn}
\begin{lem}[]
  Every second countable space is separable.
\end{lem}
\begin{proof}
  Let $\mathcal{B} = \{B_i\}_{i \in I}$ be a countable Basis of $X$.
  For $i \in I$ chose some $x_i \in B_i$. 
  We claim that $A := \bigcup_{i \in I} \{x_i\}$ is a countable dense subset, i.e. that $\overline{A} = X$.

  Assume that there exists a $x \in X \setminus \overline{A}$. 
  Since $\overline{A}$ is closed, $X \setminus \overline{A}$ is open, which means it can be written as a union of elements of $\mathcal{B}$, meaning there exists an $i \in I$ such that $B_i \subset X \setminus \overline{A}$.

  But $B_i$ contains $x_i \in A$, which gives
  \begin{align*}
    \emptyset = B_i \cap \overline{A} \supseteq B_i \cap A \supseteq \{x_i\} \quad \lightning
  \end{align*}
\end{proof}
}

\vfill

You should by now feel comfortable working with neighborhoods and what kind of things we can assume WLOG when proving results.
