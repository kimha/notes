\subsection{The role of the countability axioms}

\begin{dfn}[]
  A function $f: X \to  Y$ between two topological spaces is called \textbf{sequentially continuous}, if for all sequences $\{x_{n}\}_{n \in \N}$ \hyperlink{dfn:convergence}{converging to} $a$ in $X$, the sequence of their images $\{f(x_{n})\}_{n \in \N}$ converges to $f(a)$.
\end{dfn}

\begin{lem}[]
Let $f: X \to Y$ be a function between two continuous spaces.
\begin{enumerate}
  \item $f$ is continuous $\implies$ $f$ sequentially continuous.
  \item If $X$ is first-countable, then $f$ continuous $\iff$ $f$ is sequentially continuous.
\end{enumerate}
\end{lem}
\begin{proof}
  \begin{enumerate}
    \item 
For a neighborhood $V$ of $f(a)$, there exists a neighborhood $U$ of $x$ such that $f(U) \subseteq V$.
And because the sequence converges, there exists an $N \in \N$ such that $n \geq N \implies x_n \in U \implies f(x_n) \in V$.

\item
We show contraposition:
Assume that $f$ is not continuous, so there exists a point $a \in X$ and a neighborhood $V$ of $f(a)$ such that all neighborhoods $U$ of $a$ do not satisfy $f(U) \subseteq V$.

Let $\mathcal{U} = \{U_1,U_2\ldots\}$ be countable neighborhood basis of $a$.
Then $\forall n$, chose an $x_n \in U_1 \cap \ldots \cap U_n$ with $f(x_n) \notin V$.
Clearly, this sequence converges to $a$ since for any neighborhood $U$ of $a$ there exists an $N \in \N$ such that $U$ is a neighborhood of $x_n$ which contains an element of $\mathcal{U}$.
Also,$f(x_n)$ clearly does not converge since $f(a) \notin V$.
\end{enumerate}
\end{proof}



\begin{xmp}[]
  Let $X = \Hom_{\Top}([0,1],[0,1])$ be the space of continuous functions with the subspace topology of $\Hom_{\Set}([0,1],[0,1]) = [0,1]^{[0,1]}$ with the product topology.

  We can show in the exercise classes that 
  \begin{align*}
    \lim_{n \to \infty} \phi_n = \phi \iff \lim_{n \to \infty}\phi_n(s) = \phi(s) \forall s \in [0,1]
  \end{align*}

  If chose the same space $X$, but with the topology $\tau_d$ induced by the $L$-metric 
  \begin{align*}
    d(\phi, \psi) = \int_0^{1} \abs{\phi(s) - \psi(s)}ds
  \end{align*}
  then the map
  \begin{align*}
    f: (X,\tau_{\text{prod}}) \to (Y,\tau_{d}), \quad \phi \mapsto  \phi
  \end{align*}
  is sequentially continuous but not continuous.

  By using the majorised converges theorem (and the majorant $1$), we get
  \begin{align*}
    \lim_{n \to \infty}\phi_n(s) = \phi(s) \implies \lim_{n \to \infty} \int_0^1 \abs{\phi_n(s) - \phi(s)} ds = 0
  \end{align*}

  The map $f$ is discontinuous at for example the constant function $\phi = 0$.

  Let $0 < \epsilon < 1$ and $V = B_{\epsilon}(f(\phi_0)) \subseteq (X,\tau_{d})$.

  We now show that all neighborhoods $U \subseteq (X,\tau_{\text{prod}})$ of $\phi$, $f(U) \nsubseteq V$.

  Let $U$ be such a neighborhood of $\phi$. Then there exists $s_1, \ldots, s_m \in [0,1]$ and $U_i \subseteq [0,1]$ open with $00in U_i$ such that
  \begin{align*}
    U 
    &\supseteq \pi_{s_1}^{-1}(U_1) \cap \ldots \cap \pi_{s_m}^{-1}(U_m)\\
    &= \{\psi: [0,1] \to [0,1] \big\vert \phi_i(s_i) \in U_i, \phi \text{ continuous}\}
  \end{align*}
  but because the condition only covers finitely any $i$ we can construct a function $\psi$ which is $0$ at each $s_i$, but almost everywhere else has value $1$.

  So $\psi \in U$ but clearly
  \begin{align*}
    d(\phi,\psi) = \int_0^1 \abs{\psi(s)} ds > \epsilon
  \end{align*}
\end{xmp}


\begin{dfn}[]
A topological space $X$ is called \textbf{sequentially compact} if every sequence has a converging subsequence.
\end{dfn}
Generally speaking, compactness and sequential compactness do not imply eachother.
But some implications are true

\begin{lem}[]
  If $X$ is first-countable, then
\begin{enumerate}
  \item $X$ compact $\implies$ $X$ sequentially compact
  \item If $X$ is (additionally) a metric space, then $X$ sequentially compact $\iff X$ compact.
\end{enumerate}
\end{lem}
\begin{proof}
Let $X$ be compact.
\begin{enumerate}
  \item Let $\left(x_{n}\right)_{n \in \N}$ be a sequence in $X$.
    We want to find an $a \in X$ such that for all neighborhoods $U$ of $a$, $\forall n \in \N \exists m \geq n: x_m \in U$.

    Such an $a$ exists because if $\forall a \in X$ there exists an neighborhood $U_a$ and a number $N_a$ such that
    \begin{align*}
      n \geq N_a \implies x_n \notin U_a
    \end{align*}
    then by compactness we could cover $X$ with finitely many $U_a$ and take the maximum of the $N_a$. What this would give us is that $x_n \notin X$ for $n \geq \max \{N_{a_1}, N_{a_2}, \ldots, N_{a_m}\}$, which does not make sense.

    By 1AA, let $\mathcal{U} = \{U_1, \ldots\}$ be a countable neighborhood basis of $a$.
    Just like in the previous proof we inductivley chose $n_1 \leq n_2 \leq \ldots \leq n_l \leq \ldots$ such that
    \begin{align*}
      x_{n_l} \in U_1 \cap \ldots \cap U_l
    \end{align*}
    which means $\lim_{l \to \infty} x_{n_l} = a$.

  \item  See Analysis II
\end{enumerate}
\end{proof}


\begin{xmp}[Counterexamples]
  The space $[0,1]^{[0,1]}$ is compact, but not sequentially compact.

  The \hyperlink{xmp:long-line}{\textbf{long line}} is first-countable and sequentially compact, but not compact.

  $\R$ with the co-finite topology is not first-countable.

\end{xmp}

\vfill

The main takeaways from this section are the implications between continous/sequentially continuous and compact/sequentially compact and how being first-countable or a metric space changes this.

Have some counterexamples of each implication in mind.
