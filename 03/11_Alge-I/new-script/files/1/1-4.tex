\subsection{Polynomial Rings}
The main difficulty is understanding the notation/construction.

Still, here are some examples.
Over $R = \Z/6\Z$, we have
\begin{align*}
  p &= X^3 + 2X^2 + 3, \quad q = 3X^3 + 2X \in R[X]
  \\
  p + q &= 4X^3 + 2X^2 + 2X + 3\\
  pq &= (X^3 + 2X^2 + 3)(3X^3 + 2X) = 3X^6 + 2X^4 + X^3
\end{align*}
(note that the terms $6X^5$ and $6X$ vanish).
Using the notation from the construction in the lecture, one would write
\begin{align*}
  N = \{\lambda\}, \quad \underline{X} = (X_{\lambda}) \text{\ a $1$-tuple},
\end{align*}
the set $I_N$ of functions $\underline{i}: N \to \Z^{\geq 0}$ is in bijection with $\Z^{\geq 0}$, through
\begin{align*}
  I_N &\stackrel{\sim}{\to} \Z^{\geq 0}\\
  \underline{i} &\mapsto \underline{i}(\lambda) \in \Z^{\geq 0}\\
  (\lambda \mapsto n) &\mapsfrom n \in \Z^{\geq 0}
\end{align*}
viewing $p,q$ as sequences in $R$ using $(I_N \to R) \iso (\Z^{\geq 0} \to R)$ we, have
\begin{align*}
  p \leftrightsquigarrow (3, 0, 2, 1, 0, 0, \ldots)
  \quad \text{and} \quad 
  q \leftrightsquigarrow
  (0, 2, 0, 3, 0, 0, \ldots)
\end{align*}


As another simple example, we have a polynomial in two variables
\begin{align*}
  p = X_1^2 + X_2^2 + 2X_1X_2 + 1 \in \Z[X_1,X_2]
\end{align*}
which in our notation becomes
\begin{align*}
  N = \{1,2\}, \quad 
  \underline{X} = (X_1,X_2)
  \implies
  I_N = \left\{
    \underline{i}: \{1,2\} \to \Z^{\geq 0}
  \right\}
\end{align*}
here, the set $I_N$ is in correspondence with tuples $(\Z^{\geq 0})^{2}$.
\begin{align*}
  I_n &\stackrel{\sim}{\to} (\Z^{\geq 0})^{2}
  \\
  \underline{i}
      &\mapsto (\underline{i}(1),\underline{i}(2))
      \\
      (1 \mapsto a, 2 \mapsto b)
      &\mapsfrom (a,b)
\end{align*}
This means that $R[\underline{X}]$ is the set of maps $I_n \to R$ whose elements correspond to ``infinite matrices'' with coefficients in $R$.
\begin{align*}
  R[\underline{X}] \ni a_{\bullet} = (\underline{i} \mapsto a_{\underline{i}})_{\underline{i} \in I_n}
  &\leftrightsquigarrow
  (a_{\underline{i}(1),\underline{i}(2)})_{\underline{i} \in I_n}
\end{align*}

For our polynomial from before, we have
\begin{align*}
  p : I_n \to R,
  \quad
  p(\underline{i}) = 
  \left\{\begin{array}{lll}
      1,& \text{if }\underline{i}(1) = 2 \land \underline{i}(2) = 0 & (\text{corresponds to }X_1^2)\\
      1,& \text{if }\underline{i}(1) = 0 \land \underline{i}(2) = 2 & (\text{corresponds to }X_2^2)\\
      2,& \text{if }\underline{i}(1) = 1 \land \underline{i}(2) = 1 & (\text{corresponds to }2X_{1}X_2)\\
      3,& \text{if }\underline{i}(1) = 0 \land \underline{i}(2) = 0 & (\text{corresponds to }3)\\ 
     0, & \text{else}
  \end{array} \right.
\end{align*}

As a non-example consider the polynomial ring $R = \C[X_1,X_2,X_3,\ldots]$ with infinitely many indeterminates.
This is allowed because we can set $N = \{1,2,3,\ldots\}$ and $\underline{X} = (X_n)_{n \in N}$.
However, both
\begin{align*}
  p = 15X_1X_2X_3X_4X_5X_6X_7 \cdots
  \quad \text{and} \quad 
  q = 1 + X_1 + X_1^2 + X_1^{3} + \ldots
\end{align*}
are \emph{not} polynomials in this ring.
$p$ isn't because the corresponding index $\underline{i} = (\nu \mapsto 1)$ does not have compact support, and $q$ isn't because 
the assignment
\begin{align*}
  q : I_n \to R,
  \quad
  q(\underline{i})
  =
  \left\{\begin{array}{ll}
    1, & \text{if }\underline{i}(1) = 1 \land \underline{n} = 0 \forall n \neq 1\\
    0, & \text{otherwise}
  \end{array} \right.
\end{align*}
does not have compact support either.

Identifying $I_N$ with sequences in $\Z^{\geq 0}$, one would write
\begin{align*}
  X_1X_3^{4}X_5^2 = \underline{X}^{\underline{i}} \quad \text{for} \quad \underline{i} = (1,0,4,2,0,0,\ldots)
\end{align*}

\begin{exr}[Universal property of polynomial ring]
  We can define the polynomial ring using the universal property by showing that any other ring with such a property must be isomorphic to the one we constructed.

  Let $R$ be a ring, $R[X,Y]$ the as above constructed polynomial ring over $R$ in two variables and
  let $T$ be another ring with a ring homomorphism $j: R \to T$ and elements $t_1,t_2$ with the property that:
  For any other ring $S$ with fixed ring homomorphism $\phi: R \to S$ and every two elements $s,s' \in S$, there exists a unique ring homomorphism $\overline{\phi}_{s_1,s_2}: T \to S$ with $\overline{\phi}_{s_1,s_2} \circ j = \phi$ and 
  $\overline{\phi}_{s_1,s_2}(t_1) = s_1, \overline{\phi}_{s_1,s_2}(t_2) = s_2$.

  Show that $R[X,Y]$ and $T$ are isomorphic.
\end{exr}
\begin{sol}
  The proof is very similar to the proof that the tensor product is unique.

  From the viewpoint of $R[X,Y]$, it sees the homomorphism $j: R \to T$ and by its universal property there exists a ring homomorphism
  \begin{align*}
    \phi_{t_1,t_2} : R[X,Y] \to T
    \quad \text{with} \quad 
    \phi_{t_1,t_2} \circ \iota = j
  \end{align*}
  From the viewpoint of $T$, it sees the homomorphism $\iota$ and provides a ring homomorphism
  \begin{align*}
    \overline{\phi}_{X,Y}: T \to R[X,Y]
    \quad \text{with} \quad 
    \overline{\phi}_{X,Y} \circ j = \iota
  \end{align*}
  We thus get a diagram
  \begin{center}
  \begin{tikzcd}[ ] 
    R
    \arrow[]{r}{\iota}
    \arrow[]{d}{\iota}
    \arrow[]{dr}{j}
    &
    R[X,Y]
    \\
    R[X,Y]
    \arrow[]{r}{\phi_{t_1,t_2}}
    &
    T
    \arrow[]{u}{\overline{\phi}_{X,Y}}
  \end{tikzcd}
  \end{center}
  But from the perspective of $R[X,Y]$ (the bottom left one), it sees a ring homomorphism $\iota: R \to R[X,Y]$ and thus there should be a \emph{unique} homomorphism
  \begin{align*}
    \phi_{X,Y}: R[X,Y] \to R[X,Y]
    \quad \text{with} \quad 
    \phi_{X,Y} \circ \iota = \iota
    \quad \text{and} \quad 
    \phi_{X,Y}(X) = X, \phi_{X,Y}(Y) = Y
  \end{align*}
  we already have two candidates, namely the identity $\id_{R[X,Y]}$, but also $\overline{\phi}_{X,Y} \circ \phi_{t_1,t_2}$.

  Since this morhpism must be unique, we have
  \begin{align*}
    \overline{\phi}_{X,Y} \circ \phi_{t_1,t_2} = \id_{R[X,Y]}
  \end{align*}
  similarly, one finds
  \begin{align*}
    \phi_{t_1,t_2} \circ \overline{\phi}_{X,Y} = \id_{T}
  \end{align*}
  so $\phi_{t_1,t_2}$ and $\overline{\phi}_{X,Y}$ are inverses of eachother and $T \iso R[X,Y]$.
\end{sol}


\begin{xmp}[Functoriality]
  In the second chapter, we are interested in finding polynomials $f \in \Z[X]$ with special properties.
  Testing if $f$ satisfies these properties directly is often difficult because $\Z$ is infinite and $f$ is complicated to factorize.

  We now consider the ring homomorphism
  \begin{align*}
    \phi: \Z \to \Z/p\Z, \quad n \mapsto \overline{n} := n \text{ mod } p
  \end{align*}
  By functoriality, there exists a unique ring homomorphism
  \begin{align*}
    \tilde{\phi}: \Z[X] \to (\Z/p\Z)[X],
    \quad
    {\sum_{n = 0}^{d}}a_n X^{n} \mapsto {\sum_{n=0}^{d}} \overline{a_n}X^{n}
  \end{align*}
  By choosing the prime $p$ well, one can do the simpler calculations with $f_{\text{mod} p} := \tilde{\phi}(f)$ and obtain new information about the original polynomial $f$.

  See Chapter 2.7 for examples.
  

\end{xmp}

\begin{exr}[Functor Laws]
  \begin{enumerate}
    \item Show that for $\phi = \id_R: R \to R$ the identity, the unique ring homomorphism $\tilde{\phi}: R[X] \to R[X]$ is the identity on $R[X]$.
    \item If we have composable ring homomorphisms
  \begin{align*}
    R \stackrel{\phi}{\to} S \stackrel{\psi}{\to} T
    \quad \text{inducing ring homomorphisms} \quad 
    R[X] \stackrel{\tilde{\phi}}{\to} S[X] \stackrel{\tilde{\psi}}{\to} T[X]
  \end{align*}
  also the composition
  \begin{align*}
    R \stackrel{\psi \circ \phi}{\to} T
    \quad \text{induces a ring homomorphism} \quad 
    R[X] \stackrel{\widetilde{(\psi \circ \phi)}}{\to} T[X]
  \end{align*}
  show that $\widetilde{(\psi \circ \phi)} = \tilde{\psi} \circ \tilde{\phi}$.
  \end{enumerate}
\end{exr}



\begin{exr}[Polynomial Evaluation]
  Given some polynomial $p \in R[X]$, there should exist an obvious function
  \begin{align*}
    \overline{p}: R \to R, \quad x \mapsto p(x)
  \end{align*}
  this however is using abuse of notation, since $p(x)$ doesn't mean anything (yet).

  Use the universal property of the polynomial ring to define an evaluation map
  \begin{align*}
    \ev: R[X] \to R^{R} = \{\text{function }f: R \to R\}
  \end{align*}

\end{exr}
\begin{sol}
  Let $S = R^{R}$ be the ring of functions (with pointwise addition and multiplication) and consider the ring homomorphism
  \begin{align*}
    \phi: R \to S, \quad x \mapsto (y \mapsto x) =: \text{const}_x
  \end{align*}
  Fix $N = \{\nu\}$ a one-element set.
  For each $x \in R$, consider the system $\underline{x} = (y \mapsto x) \in S^{N} = S$, where we identify one-tuples in $S$ with elements of $S$.

  By the universal property of the polynomial ring, there exists a unique ring homomorphism
  \begin{align*}
    \phi_{\underline{x}}: R[X] \to S
    \quad \text{with} \quad 
    \phi_{\underline{x}} \circ \iota,
    \quad
    \phi_{\underline{x}}(X) = (y \mapsto x)
  \end{align*}

  In particular we see that $\phi_{\underline{x}}$ maps polynomials $p \in R[X]$ to a function
  \begin{align*}
    \phi_{\underline{x}}(p) : R \to R,
    \quad
  \end{align*}

\end{sol}
