#+TITLE: Algebra I - README
#+AUTHOR: Han-Miru Kim
#+STARTUP: OVERVIEW

* About
The notes were taken during Prof. Einsiedler's lecture "Algebra I" from Winter Semester 2020/2021.

The exercise-class is a nodejs project containing the slides made for the exercise class for Prof. Pink's "Algebra I" lecture in Winter Semester 2022/2023.


* How-To
Read this if you want to make slides for your exercise classes using reveal.js and KaTeX.

From https://revealjs.com/installation/
The slides will be written in external files in markdown and we will use a local KaTeX installation for math typsetting.

#+BEGIN_SRC shell
  git clone https://github.com/hakimel/reveal.js.git exercise-class
  cd exercise-class && npm install
  npm install katex
#+END_SRC

The markdown files will be stored in a separate directory
#+BEGIN_SRC shell
  mkdir md
  touch md/file1.md
#+END_SRC

Edit index.html as follows for a basic setup

#+BEGIN_SRC html
  <!doctype html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

      <title>Insert Title Here</title>

      <link rel="stylesheet" href="dist/reset.css">
      <link rel="stylesheet" href="dist/reveal.css">
      <link rel="stylesheet" href="dist/theme/black.css">

      <!-- Theme used for syntax highlighted code -->
      <link rel="stylesheet" href="plugin/highlight/monokai.css">
    </head>

    <body>
      <div class="reveal">
        <div class="slides">
          <section data-markdown="./md/file1.md"
                   data-separator="^\n\n\n"
                   data-separator-vertical="^\n\n"
                   data-separator-notes="^Note:"
                   data-charset="utf-8">
          </section>
        </div>
      </div>

      <script src="dist/reveal.js"></script>
      <script src="plugin/notes/notes.js"></script>,
      <script src="plugin/markdown/markdown.js"></script>
      <script src="plugin/highlight/highlight.js"></script>
      <script src="plugin/math/math.js"></script>
      <script>
        Reveal.initialize({
          hash: true,
          katex: {
            local: 'node_modules/katex',
          },
          plugins: [ RevealMarkdown, RevealHighlight, RevealNotes, RevealMath.KaTeX ],
        });
      </script>
    </body>
  </html>
#+END_SRC

If you want to add more markdown files, copy the section and replace the path in the data-markdown attribute.

I personally prefer a bit smaller text. To adjust the font sizes, change make a copy of ~./css/theme/source/black.scss~ and edit
~mainFontSize~ and other options such as setting ~headingTextTransform~ to ~none~.
#+BEGIN_SRC shell
  cd ./css/theme/source
  cp black.scss mytheme.scss
#+END_SRC

edit ~index.html~ to refer to the new theme



