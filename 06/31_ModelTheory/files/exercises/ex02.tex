\begin{exr}[]
Consider $\R$ as an $\mathcal{L}_{\Ring}$-structure.
\begin{enumerate}
  \item Show that the relation $B = \left\{
      (x,y) \in \R^{2} \big\vert x < y
  \right\}$ is $0$-definable.

  \item Is $B = \{\sqrt{2}\}$ $0$-definable? 
  \item Show that there exists an $r \in \R$ such that $\{r\}$ is not $0$-definable.
  \item Show that $\N$ is not $0$-definable.
\end{enumerate}
\emph{Hint: Use the compactness theorem for (d)}
\end{exr}
\begin{sol}
  \begin{enumerate}
    \item The trick is finding some way to describe the $x < y$ relation.
      Since $z^{2}$ is always non-negative, we take the formula
      \begin{align*}
        \phi(x,y) = [(\exists z\ x + z \cdot z \doteq y) \land \lnot (x \doteq y)]
      \end{align*}
      Note that this does not work for $\Q$.
    \item Yes, using $\phi(x,y)$ from (a), we can set
      \begin{align*}
        \psi(x) = [(x \cdot x \doteq  \underline{1} + \underline{1}) \land \phi(0,x)] 
      \end{align*}
    \item It is a simple argument of size.
      There are only $\aleph_0$ many $\mathcal{L}_{\Ring}$-formulae, but the cardinality of $\R$ is strictly bigger.
    \item The proof is pretty much the same the used in Exercise 1.3.
      Assume there exists a $\phi$ such that
      \begin{align*}
        \N = \{\underline{0}^{\R}, \underline{1}^{\R},\ldots\}
        =
        \left\{
          a \in \R \big\vert \R \models \phi(a)
        \right\}
      \end{align*}
      Then set $L' = L_{\Ring} \cup \{a\}$, where $a$ is a new constant.
      Then consider the formula
      \begin{align*}
        \phi_a(n) = [\phi(a) \land (a > \underbrace{\underline{1} + \cdots + \underline{1}}_{n \text{\ times}})]
      \end{align*}
  \end{enumerate}
\end{sol}


\begin{exr}[]
Write down the axioms for the theory of strong total orders in the language $\mathcal{L}_{\text{Ord}} = \{<\}$.
Is this theory consistent?
Is this theory complete?
\end{exr}


\begin{exr}[]
Show the following
\begin{enumerate}
  \item If two $\mathcal{L}$-structures $\mathcal{A}$ and $\mathcal{B}$ are isomorphic, then they are elementarily equivalent $\mathcal{A} \equiv \mathcal{B}$.
  \item Let $\mathcal{L}$ be a finite language, $\mathcal{A}$ a finite $\mathcal{L}$-structure and $\mathcal{B} \equiv \mathcal{A}$ elementarily equivalent.
    Then $\mathcal{A}$ and $\mathcal{B}$ are isomorphic.
\end{enumerate}
\end{exr}
\begin{sol}
  We start by enumerating the elements in the universe
  \begin{align*}
    z: A \to \{1,2,\ldots,\abs{A}\}
  \end{align*}
  For $c \in \mathcal{L}$ a constant, let
  \begin{align*}
    \phi_c = [c \doteq V_{z(c^{\mathcal{A}})}]
  \end{align*}
  for $f \in \mathcal{L}$ a function, let
  \begin{align*}
    \psi_f(v_{i_1}, \ldots, v_{i_n})
    =
    [
    f(v_{i_1}, \ldots, v_{i_n})
    \doteq 
    V_{z(f^{\mathcal{A}}(z^{-1}(i_1),\ldots,z^{-1}(i_n)))}
    ]
  \end{align*}
  and for $R \in \mathcal{L}$ a relation, set
  \begin{align*}
    v_R = R(v_{i_1},\ldots,v_{i_n})
  \end{align*}
  then, define the giga formula
  \begin{align*}
    \alpha(\alpha_{1}, \ldots, \alpha_{\abs{A}})
    &=
    \forall v_0 \biglor_{i=1}^{\abs{A}} v_0 \doteq v_i
    \land \bigland_{i \neq j} \lnot v_i \doteq v_0
    \land
    \bigland_{c \in \mathcal{L}} \phi_c
    \land
    \bigland_{f \in \mathcal{L}, i_{1}, \ldots, i_{n}}
    \psi_f(v_{i_1}, \ldots, v_{i_n})
    \land
    \bigland_{\text{\parbox{3em}{$R \in \mathcal{L},i_{1}, \ldots, i_{n}, R^{\mathcal{A}}(z^{-1}(i_1),\ldots,z^{-1}(i_n))$}}} v_R(v_{i_1}, \ldots, v_{i_n})
  \end{align*}
  Clearly, $\mathcal{A} \models \exists v_{1}, \ldots, v_{\abs{A}}: \alpha(v_{1}, \ldots, v_{\abs{A}})$, 
  since $v_i^{\mathcal{A}} = z^{-1}(i) \in A$.
  But $\mathcal{A} \equiv \mathcal{B}$ so we also have
  $\mathcal{A} \models \exists v_{1}, \ldots, v_{\abs{A}}: \alpha(v_{1}, \ldots, v_{\abs{A}})$, so let $b_{1}, \ldots, b_{\abs{A}}$ be such that $\mathcal{B} \models \alpha(b_{1}, \ldots, b_{\abs{A}})$.

  Define
  \begin{align*}
    h: \mathcal{A} \to \mathcal{B}, \quad z^{-1}(i) = v_i^{\mathcal{A}} \mapsto  v_i^{\mathcal{B}} = b_i
  \end{align*}
  Then we check that $h$ sends constants to the right constants
  \begin{align*}
    h(c^{\mathcal{A}}) \stackrel{\phi_c}{=} h(v_{z(c^{\mathcal{A}})}^{\mathcal{A}})
    =
    v_{z(c^{\mathcal{B}})}^{\mathcal{B}} \stackrel{\phi_c}{=} c^{\mathcal{B}}
  \end{align*}
  likewise, for an $n$-ary function $f$ we have
  \begin{align*}
    f^{\mathcal{B}}(h(a_1),\ldots,h(a_n))
    &\stackrel{\psi_f}{=}
    f^{\mathcal{B}}\left(
      h(v_{z(a_1)}^{\mathcal{A}}),\ldots,
      h(v_{z(a_n)}^{\mathcal{A}})
    \right)
    \\
    &=
    \ldots
    \\
    &= h(f^{\mathcal{A}}(a_{1}, \ldots, a_{n}))
  \end{align*}
  and similarly for relations.


\end{sol}

\begin{edfn}[]
  Two structures are called \textbf{partially isomorphic}, if there is a non-empty set $\mathcal{I}$ of isomorphisms of substructures of $\mathcal{A}$ and $\mathcal{B}$, that satisfy the \textbf{back-and-forth} properties:
  \begin{enumerate}
    \item For every $f \in \mathcal{I}$ and $a \in A$ there exists an extension $f_a \in \mathcal{I}$ of $f$ that contains $a$ in its domain.
    \item For every $\mathcal{I}$ and $b \in B$ there exists an extension $f_b \in \mathcal{I}$ that contains $b$ in its domain.
  \end{enumerate}
\end{edfn}

\begin{exr}[]
  Show that partially isomorphic structures are elementarily equivalent.

  \emph{Hint: Show by induction over the complexity of the formula $\phi$ that for all $f \in \mathcal{I}$ and $\overline{a} = (a_{1}, \ldots, a_{n})$ with $a_i$ in its domain of $f$}
  \begin{align*}
    \mathcal{A} \models \phi(\overline{a}) \iff \mathcal{B} \models \phi(f(\overline{a}))
  \end{align*}
\end{exr}
\begin{sol}
\end{sol}


\begin{exr}[]
Let $\mathcal{L}_G = \{E\}$ be the language of graphs.
Show that there does not exist a $\mathcal{L}_G$ theory such that the models of the theory are precisely the connected graphs.
\end{exr}
\begin{sol}
Assume there is an $\mathcal{L}_{\text{Graph}}$-theory $T$ of connected graphs.
Set $\mathcal{L} = \mathcal{L} \cup \{a,b\}$, for $a,b$ constants.
Define
\begin{align*}
  \psi_n = \text{``$a$ and $b$ are at least $n$ edges apart''}
\end{align*}
Then set $T' = T \cup \left\{\psi_n \big\vert n \in \N\right\}$.
For $\Delta \subseteq T'$ finite, one easily finds models for $\Delta$, one example being a long line where we have one vertex per integer and neighboring integers are connected.

By the compactness theorem, there must be a model for $T'$, which means that it is a connected graph that has two vertices which are infintely far apart.
\end{sol}
