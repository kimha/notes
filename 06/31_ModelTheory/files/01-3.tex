\subsection{Theories}
Let $\mathcal{L}$ be an $\mathcal{L}$-formula.

\begin{dfn}[]\label{dfn:theory}
A formula without free variables is called an $\mathcal{L}$-\textbf{proposition}.
\begin{itemize}
  \item If $\phi$ is a proposition and $A \models \phi$ (which means there exists an assignment $\vec{b}$ such that $\mathcal{A} \models \phi[\vec{b}]$), we say that $\phi$ \textbf{holds} in $\mathcal{A}$ and that $\mathcal{A}$ is a \textbf{model} for $\phi$.
  \item An \textbf{$\mathcal{L}$-theory} is a set of $\mathcal{L}$-propositions.
  \item If $T$ is an $\mathcal{L}$-theory, we say that $\mathcal{A}$ is a \textbf{model} for $T$, if $\mathcal{A} \models \phi$, for every $\phi \in T$.
  \item A theory is \textbf{consistent} (as an $\mathcal{L}$-theory), if it has a model.\footnote{In the lecture ``Die Gödelschen Sätz'', a different (and eventually proven to be equivalent) definition was given.}
  \item A set of $\mathcal{L}$-formulae $\Sigma$ is \textbf{consistent}, if there exists an $\mathcal{L}$-structure $\mathcal{A}$ and an assignment $\vec{b}$, such that $\mathcal{A} \models \phi[\vec{b}]$ for all $\phi \in \Sigma$.
  \item $\Sigma$ is \textbf{consistent with} $T$, if $\Sigma \cup T$ is consistent.
\end{itemize}
\end{dfn}


\begin{xmp}[]
We can talk about the Theory of abelian groups.
Let $\mathcal{L} = \{+,-,\underline{0}\}$.
We introduce the formulae
\begin{itemize}
  \item $\forall  x,y,z\ (x + y) + z = x + (y + z)$
  \item $\forall x\ \underline{0} + x \doteq x$.
  \item $\forall x\ (-x) + x = \underline{0}$
  \item $\forall x,y\ x + y \doteq y + x$.
\end{itemize}
The theory of groups is the collection of the first three propositions.
The theory of abelian is the set of all four propositions.
Both are consistent, as they have models.
The theory consisting of the formula 
\begin{align*}
  \psi = [\forall  x\ x \doteq x\ \land \ \lnot(x \doteq  x)]
\end{align*}
is \emph{not} consistent.
\end{xmp}

\begin{lem}[]\label{lem:consistent-extension}
Let $T$ be an $\mathcal{L}$-theory, $\mathcal{L}' \supseteq \mathcal{L}$ an extension.
Then $T$ is consistent as an $\mathcal{L}$-theory if and only if $T$ is consistent as an $\mathcal{L}'$-theory.
\end{lem}
\begin{proof}
First check that the statement is not non-sensical.
Since $T$ is a set $\mathcal{L}$-propositions, verify for yourself that every $\mathcal{L}$-proposition is also an $\mathcal{L}'$-proposition.
After that, assume $\mathcal{A} = (A,{(z^{\mathcal{A}})}_{z \in \mathcal{L}})$ is an $\mathcal{L}$-structure.
To turn this into an $\mathcal{L}'$-structure, define
\begin{align*}
  A' := A \cup \{\text{constants of }\mathcal{L}'\}
\end{align*}
and if $z \in \mathcal{L}'$ is an $n$-ary function symbol that is not in $\mathcal{L}$, define $z^{\mathcal{L}'}: {A'}^{n} \to A'$ arbitrarily. (For example $z^{\mathcal{A}'}(a_{1}, \ldots, a_{n}) = a_{1}$).
Similarly, for the $n$-ary relations in $\mathcal{L}'$ that are not in $\mathcal{L}$, define them arbitrarily, (for example $z^{\mathcal{A}'} = {A'}^{n}$).

It remains to check that all $\mathcal{L}$-propositions $\phi \in T$ hold in $\mathcal{A}'$.
\end{proof}

\begin{dfn}[]~\label{dfn:equivalence-theories}
  If $\phi$ is a proposition that holds in all models of $T$, we say that $\phi$ \textbf{follows from} $T$ and we write $T \vdash \phi$.
  If $S$ is another theory, we write $T\vdash S$ if all models of $T$ are also models of $S$.
  We write $T \equiv S$, if $T\vdash S$ and $S\vdash T$.
\end{dfn}
\begin{xmp}[]
Theory of abelian groups $\vdash$ Theory of groups.
\end{xmp}


\begin{dfn}[]\label{dfn:completeness}
Let $T$ be a consistent $\mathcal{L}$-theory.
\begin{itemize}
  \item $T$ is \textbf{complete}, if for all $\mathcal{L}$-propositions $\phi$ either $T \vdash \phi$ or $T \vdash \lnot \phi$.
  \item If $T$ is complete, we define $\abs{T} = \max(\abs{L},\aleph_0)$
  \item We define
  \begin{align*}
    \Theory(\mathcal{A}) := \{\phi \big\vert \mathcal{A} \models \phi\}
  \end{align*}
\end{itemize}
\end{dfn}
\begin{xmp}[]
Let $\mathcal{A}$ be an $\mathcal{L}$-structure.
Then trivially $\Theory(\mathcal{A})$
is complete.

For $\mathcal{L} = \mathcal{L}_{\Grp}$, the theory $T_{\Grp}$ consisting of the group axioms is not complete, because there exist both abelian and anabelian groups.
More precisely, for the formula
\begin{align*}
  \phi = \forall x,y\ x \cdot y \doteq y \cdot x
\end{align*}
we do not have $T \vdash \phi$ or $T \vdash \lnot \psi$.
If that were the case, it would mean
\begin{align*}
  \forall \text{\ models $\mathcal{A}$ of $T$, $\mathcal{A}$ is abelian} 
  \quad \text{or} \quad 
  \forall \text{\ models $\mathcal{A}$ of $T$, $\mathcal{A}$ is not abelian} 
\end{align*}
\end{xmp}

\begin{lem}[]
Every consistent theory is complete if and only if it is \emph{maximally consistent}, which means that it is equivalent to every consistent extension.
\end{lem}
\begin{proof}
  \textbf{``$\bm{\implies}$'':} 
  Assume $T$ is a (consistent and) complete theory and assume $T' \supseteq T$ is a consistent extension.

\end{proof}

\begin{dfn}[]\label{dfn:elementarily-equivalent}
Two $\mathcal{L}$-strucutre $\mathcal{A}$, $\mathcal{B}$ are called \textbf{elementarily equivalent} (written $A \equiv B$), if $\text{Th}(\mathcal{A}) = \text{Th}(\mathcal{B})$. 
This means that for all $\mathcal{L}$-propositions $\phi$:
\begin{align*}
  A \models \phi \iff \mathcal{B} \models \phi
\end{align*}
\end{dfn}


\begin{lem}[]
Let $T$ be a consistent $\mathcal{L}$-theory. The following are equivalent:
\begin{enumerate}
  \item $T$ is complete
  \item All models of $T$ are elementarily equivalent.
  \item There exists a structure $\mathcal{A}$ such that $T \equiv \text{Th}(\mathcal{A})$.
\end{enumerate}
\end{lem}
The proof is mostly a check if you understand the definitions correctly.
\begin{proof}
\textbf{(a)$\bm{\implies}$ (c):} 
If $T$ is complete, it is in particular consistent so there exists a model $\mathcal{A}$ of $T$. (See Definition~\ref{dfn:theory}).
We now claim that that $T \equiv \Theory(\mathcal{A})$, which by Definition~\ref{dfn:equivalence-theories} means the following
\begin{align*}
  \text{For any $\mathcal{L}$-structure $\mathcal{B}$:} \quad
  \mathcal{B} \models T \iff \mathcal{B} \models \Theory(\mathcal{A})
\end{align*}
which in turn means by Definition~\ref{dfn:theory} that
\begin{align*}
  \text{For all $\mathcal{L}$-structures $\mathcal{B}$:} \quad
  \forall \phi \in T: \mathcal{B} \models \phi \text{\ if and only if } \forall \phi \in \Theory(\mathcal{B}): \mathcal{B} \models \phi
\end{align*}






So if $\phi$ is an $\mathcal{L}$-proposition, and 
Let $\mathcal{A}$ be a model of $T$ and $\phi$ be an $\mathcal{L}$-proposition.
If $\phi$ holds in $\mathcal{A}$ (first point Definition 1.30), then we cannot have $T \vdash \lnot \phi$.
Since $T$ is complete, $T \vdash \phi$.
It follows $T \equiv \text{Th}(\mathcal{A})$.

\textbf{(c)$\bm{\implies}$ (b):} 
Let $B \models T$ be another model. Then $B \models \text{Th}(\mathcal{A})$, so $B \equiv A$.

\textbf{(b)$\bm{\implies}$ (a):} 
Let $\mathcal{A}$ be a model of $T$ and $\phi$ an $\mathcal{L}$-proposition.
If $\phi$ holds in $\mathcal{A}$, then (by assumption) it holds in all models of $T$, so $T \vdash \phi$.
If $\lnot \phi$ holds in $\mathcal{A}$, then $T \vdash \lnot \phi$.
\end{proof}
