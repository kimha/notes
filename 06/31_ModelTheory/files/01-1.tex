\subsection{Languages, Structures, Homomorphisms}

\begin{dfn}[]
A \textbf{Language} $\mathcal{L}$ is a set of constants, function symbols and relation symbols.

For a language $\mathcal{L}$, an $\mathcal{L}$-structure is a pair $\mathcal{A} = (A,{(z^{\mathcal{A}})}_{z \in \mathcal{L}})$ such that $A$ is a non-empty set and
\begin{itemize}
  \item $z^{\mathcal{A}} \in A$, if $z$ is a constant
  \item $z^{\mathcal{A}}: A^{n} \to A$, if $z$ is an $n$-ary functionsymbol
  \item $z^{\mathcal{A}} \subseteq A^{n}$, if $z$ is an $n$-ary relation symbol.
\end{itemize}

$A$ is called the \textbf{universe} of $\mathcal{A}$ and $z$ is called the $\mathcal{A}$-intepretation of $\mathcal{L}$.

The \textbf{cardinality} $\abs{\mathcal{A}}$ of an $\mathcal{L}$-structure $\mathcal{A}$ is the cardinality of the universe $\abs{A}$.
\end{dfn}


\begin{xmp}[]
  \begin{itemize}
    \item Consider the language $\mathcal{L}_{\Grp} = \{\underline{e},\circ,\cdot^{-1}\}$.

    A group can be realized as an $\mathcal{L}_{\Grp}$-structure. For example set $\mathcal{A} = \GL_n(\R)$, where
      \begin{align*}
        A = \{\text{invertible $n \times n$ matrices over $\R$}\}
      \end{align*}
      $\underline{e}^{\GL_n(\R)} = 1_n \in \GL_n(\R)$.
      \begin{align*}
        \circ^{\GL_n(\R)}: A \times A \to  A, (M,N) \mapsto  MN
      \end{align*}
      \begin{align*}
        {\cdot^{-1}}^{\GL_n(\R)}: A \to A, M \mapsto  M^{-1}
      \end{align*}

      Note that group axioms such as associativity, inverse laws etc.\ are not required to be an $\mathcal{L}_{\Grp}$-structure.
      We will see later how they can be required by adding \emph{formulas} to a language.

    \item The language of rings is $\mathcal{L}_{\Ring} = \{\underline{0},\underline{1},+,-,\cdot\}$, where $\mathcal{L}_{\Ring}$-structures are the rings we know from algebra.
    \item The lanuage of ordered sets $\mathcal{L}_{\text{ord}} = \{<\}$, where an $\mathcal{L}_{\text{ord}}$-structure is an ordered set.
      For example $\mathcal{A} = \R$ with $A = \{\text{real numbers}\}$, with
      \begin{align*}
        <^{\R} = \{(a,b) \in \R^{2} \big\vert a < b\}
      \end{align*}
  \end{itemize}
\end{xmp}
There are also non-algebraic structures that can be described as $\mathcal{L}$-structures.
\begin{xmp}[]
  \begin{itemize}
    \item Graphs can be described using the language $\mathcal{L} = \{E\}$.
      A graph $G = (V,E)$ is realized as the tuple
      \begin{align*}
        \mathcal{A} = (V, E^{\mathcal{A}})
      \end{align*}
    \item If we were to realize a $K$-vector space $V$, we can't define scalar multiplication as a function, because it would need to be of type $K \times V \to V$.
      One way to remedy this is to use the map
      \begin{align*}
        K \to \Hom(V,V), \quad \lambda \mapsto (v \mapsto  \lambda v) =: m_{\lambda}
      \end{align*}
      and to set
      \begin{align*}
        \mathcal{A} = (V,0^{\mathcal{A}},{(m_{\lambda})}_{\lambda \in K})
      \end{align*}
  \end{itemize}
\end{xmp}

There is a natural way to define morphisms between $\mathcal{L}$-structures.

\begin{dfn}[]~\label{dfn:l-structure-homomorphism}
  Let $\mathcal{L}$ be a language.
  Let $\mathcal{A},\mathcal{B}$ be $\mathcal{L}$-structures.

  A \textbf{homomorphism} from $\mathcal{A}$ to $\mathcal{B}$ is a map $h: A \to B$ such that for all $a_{1}, \ldots, a_{n} \in A$:
  \begin{itemize}
    \item $h(c^{\mathcal{A}}) = c^{\mathcal{B}}$ for all constants $c \in \mathcal{L}$.

    \item $h(f^{\mathcal{A}}(a_{1}, \ldots, a_{n})) = f^{\mathcal{B}}(h(a_{1}), \ldots, h(a_{n}))$ for all $n$-ary function symbols $f \in \mathcal{L}$.
      
      In other words, the following diagram commutes
      \begin{center}
      \begin{tikzcd}[ ] 
        {A}^{n}
        \arrow[]{r}{(h,\ldots,h)}
        \arrow[swap]{d}{f^{\mathcal{A}}}
        & B^{n} \arrow[]{d}{f^{\mathcal{B}}}
        \\
        A \arrow[]{r}{h}
        & B
      \end{tikzcd}
      \end{center}

    \item $R^{\mathcal{A}}(a_{1}, \ldots, a_{n}) \implies R^{\mathcal{B}}(h(a_1),\ldots,h(a_n))$ for all $n$-ary relation symbols $R \in \mathcal{L}$.
  \end{itemize}
  we then write $h: \mathcal{A} \to \mathcal{B}$.

  \begin{itemize}
    \item If additionally $h$ is injective and
    $R^{\mathcal{A}}(a_{1}, \ldots, a_{n}) \iff R^{\mathcal{B}}(h(a_1),\ldots,h(a_n))$, we call $h$ an (isomorphic) \textbf{embedding} of $\mathcal{A}$ into $\mathcal{B}$.
  \item If $h$ is a surjective embedding, we call it an \textbf{isomorphism} of $\mathcal{L}$-structures and we write $h: \mathcal{A} \stackrel{\iso}{\to} \mathcal{B}$ and say that $\mathcal{A}$ and $\mathcal{B}$ are isomorphic and write $\mathcal{A} \iso \mathcal{B}$.
  \item An \textbf{automorphism} is an isomorphism $\mathcal{A} \stackrel{\iso}{\to} \mathcal{A}$.
    The set $\Aut(\mathcal{A})$ of automorphisms of $\mathcal{A}$ forms a group under composition.
  \item $\mathcal{A}$ is said to be a \textbf{substructure} of $\mathcal{B}$, if $A \subseteq B$ and the inclusion mapping $\iota: A \hookrightarrow B$ defines an embedding of $\mathcal{A}$ into $\mathcal{B}$.
    We write $\mathcal{A} \subseteq \mathcal{B}$ and say that $\mathcal{B}$ is an \textbf{extension} of $\mathcal{A}$.
  \end{itemize}
\end{dfn}


\begin{rem}[]\label{rem:substructure}
If $\mathcal{B}$ is an $\mathcal{L}$-structure and $B' \subseteq B$, then $B'$ is the universe of a (unique) substructure $\mathcal{B}' \subseteq \mathcal{B}$ if and only if $B'$ contains all constants $c^{\mathcal{B}}$ and it is closed under all operations $f^{\mathcal{B}}$.

In particular, if $h: \mathcal{A} \to \mathcal{B}$ is a homomorphism, then the image $h(A) \subseteq B$ defines a substructure (denoted $h(\mathcal{A})$) of $\mathcal{B}$.
\end{rem}
\begin{proof}
  \textbf{``$\bm{\impliedby}$'':} 
Assume $B' \subseteq B$ contains all constants and is closed under all operations.

Let $\iota: B' \hookrightarrow B'$ denote the inclusion.
Let $b_{1}, \ldots, b_{n} \in B$. Then
\begin{itemize}
    \item $\iota(c^{\mathcal{B}}) = c^{\mathcal{B}}$ for all constants $c \in \mathcal{L}$.

    \item 
      For all $n$-ary function symbols $f \in \mathcal{L}$.
      \begin{align*}
        f^{\mathcal{B}}(\iota(b_{1}), \ldots, \iota(b_{n}))
        =
        \underbrace{f^{\mathcal{B}}(b_{1}, \ldots, b_{n})}_{\in B'}
        =
        \iota(f^{\mathcal{B}}(b_{1}, \ldots, b_{n})) 
      \end{align*}

    \item $R^{\mathcal{A}}(a_{1}, \ldots, a_{n}) \implies R^{\mathcal{B}}(h(a_1),\ldots,h(a_n))$ for all $n$-ary relation symbols $R \in \mathcal{L}$.
    \item For any $n$-ary relation $R^{\mathcal{B}}$, we have
      \begin{align*}
        R^{\mathcal{B}}(\iota(b_1), \ldots, \iota(b_n)) = R^{\mathcal{B}}(b_{1}, \ldots, b_{n}) 
      \end{align*}
\end{itemize}
$\iota$ is also injective and thus is an embedding of $\mathcal{B}'$ into $\mathcal{B}$.

\textbf{``$\bm{\implies}$'':} Assume $B'$ is the universe of a substructure $\mathcal{B}' \subseteq B$.
Since $\mathcal{B}'$ is also an $\mathcal{L}$-strucutre, it must contain all constants and must also be closed under operations $f^{\mathcal{B}'}$, or else the type wouldn't be $f^{\mathcal{B}'}: {B'}^{n} \to B'$.
\end{proof}

\begin{dfn}[]
Let $\mathcal{B}$ be an $\mathcal{L}$-structure and $\emptyset \neq S \subseteq B$.
Then there exists a smallest substructure $\mathcal{A} = \scal{S}^{\mathcal{B}}$ of $\mathcal{B}$ that contains $S$.

We say that $\mathcal{A}$ is \textbf{generated} by $S$.
If $S$ is finite, we say that $\mathcal{A}$ is \textbf{finitely generated}.
\end{dfn}

\begin{lem}[]
If $\mathcal{A}$ is generated by $S$, then for every $\mathcal{L}$-structure $\mathcal{B}$, every homomorphism $h: \mathcal{A} \to \mathcal{B}$ is determined by its values on $S$.
\end{lem}
\begin{proof}
  Let $h,h': \mathcal{A} \to \mathcal{B}$ be homomorphisms agreeing on $S$.
We first claim that
\begin{align*}
  C = \{a \big\vert h(a) = h'(a)\}
\end{align*}
defines (i.e.\ is the universe of) a substructure of $\mathcal{A}$.
By the previous Remark~\ref{rem:substructure}, it suffices to check that $C$ contains all constants and is closed under all operations $f^{\mathcal{A}}$.
\begin{itemize}
  \item If $c^{\mathcal{A}}$ is a constant, $h(c^{\mathcal{A}}) = h'(c^{\mathcal{A}})$ (by the first point in Definition~\ref{dfn:l-structure-homomorphism}), so $c^{\mathcal{A}} \in C$.
  \item Let $c_{1}, \ldots, c_{n} \in C$ and let $f^{\mathcal{A}}: A^{n} \to A$.
    Then
    \begin{align*}
      h(f^{\mathcal{A}}(c_{1}, \ldots, c_{n}))
      &= f^{\mathcal{B}}(h(c_1),\ldots,h(c_n))\\
      &= f^{\mathcal{B}}(h'(c_{1}), \ldots, h'(c_{n})) \\
      &= h'(f^{\mathcal{A}}(c_{1}, \ldots, c_{n}))
    \end{align*}
    so $f^{\mathcal{A}}(c_{1}, \ldots, c_{n}) \in C$.
\end{itemize}
Which proves the claim.
Since $h|_S = h'|_S$, we have $\emptyset \neq S \subseteq C$.
But $\mathcal{A}$ is the smallest substructure containing $S$, so $C = A$, which means $h = h'$.
\end{proof}


\begin{dfn}[]
  A \hyperlink{dfn:partial-order}{partially ordered set} $(I,\leq)$ is called \textbf{directed}, if for all $i,j \in I$, there exists a $k \in I$ with $i \leq k$ and $j \leq k$, or equivalently, if every finite subset has an upper bound.

Let $(I,\leq)$ be a directed partial order.
A family ${(\mathcal{A}_i)}_{i \in I}$ of $\mathcal{L}$-structures is called \textbf{directed}, if
\begin{align*}
  i \leq j \implies \mathcal{A}_i \subseteq \mathcal{A}_j
\end{align*}
\end{dfn}

\begin{lem}[]~\label{lem:directed-colimit}
Let ${(\mathcal{A}_i)}_{i \in I}$ be a directed family of $\mathcal{L}$-structures.
Then $A = \bigcup_{i \in I}A_i$ is the universe of a unique $\mathcal{L}$-structure $\mathcal{A} = \bigcup_{i \in I}\mathcal{A}_i$, that is an extension of all $\mathcal{A}_i$.
\end{lem}
\begin{proof}
We need to define the constants, relations and functions on this universe.
The constants are obvious, 
For the $n$-ary relations $R^{\mathcal{A}}$ and functions $f^{\mathcal{A}}$, let $a_{1}, \ldots, a_{n} \in A$, so there exist $A_{1}, \ldots, A_{n}$ with $a_i \in A_i$.
As the family ${(A_i)}_{i}$ is directed, there exists some $k \in I$ with $k \geq i$ for all $i$ such that $a_i \in A_k$.
We thus define
\begin{align*}
  R^{\mathcal{A}}(a_{1}, \ldots, a_{n}) &:= R^{\mathcal{A}_k}(a_{1}, \ldots, a_{n})
  \\
  f^{\mathcal{A}}(a_{1}, \ldots, a_{n}) &:= f^{\mathcal{A}_k}(a_{1}, \ldots, a_{n})
\end{align*}
One easily checks that $\mathcal{A}_i \subseteq \mathcal{A}$ is an embedding.
\end{proof}

