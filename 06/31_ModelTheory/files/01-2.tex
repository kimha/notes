\subsection{Terms and Formulae}
Let $\mathcal{L}$ be a language.

If we take $\mathcal{L}_{\Ring}$, we may be interested in the element $3$ in the specific ring $\Z$.
One way to refer to it is to use an abstract \emph{term} such assuch as  $\underline{1} + \underline{1} + \underline{1}$ and \emph{evaluate} or \emph{interpret} this in the $\mathcal{L}$-structure $\Z$.


\begin{dfn}[]\label{dfn:l-term}
  Let $v_{0},v_1,\ldots$ be variables (abstract symbols)
  \begin{itemize}
    \item An \textbf{$\mathcal{L}$-term} is a sequence of variables, constants, function symbols from $\mathcal{L}$ \emph{constructed} from the following rules.
      \begin{enumerate}
        \item Every variable $v$ and every constant $c$ is an $\mathcal{L}$-term.
        \item If $f \in \mathcal{L}$ is an $n$-ary function and $t_{1}, \ldots, t_{n}$ are $\mathcal{L}$-terms, then $f\ t_{1}\ \cdots\ t_{n}$ is an $\mathcal{L}$-term.
      \end{enumerate}
    \item The \textbf{complexity} of a term is number of function symbols in it.
  \end{itemize}
\end{dfn}
Note that parenthesis in the ``function application'' are technically not part of the syntax.
So an $\mathcal{L}$-term is merely a sequence of symbols such that every function has correct number of arguments. 

For example, in the language of rings, we would write the expression $(x + y) \cdot (z + w)$ as follows
\begin{align*}
  \cdot\ +\ x\ y\ +\ z\ w
\end{align*}
which is also known \emph{polish notation}. We will however use paranthesis and infix notation when convenient.

If you know some Haskell, one could implement $\mathcal{L}$-terms using algebraic data types:
\begin{lstlisting}
data LTerm = Var v | Const c | Ap f [LTerm]
\end{lstlisting}

What is nice is that we do not need to ``know beforehand'' what the collection of all valid terms and just refer to the constructors.

Also note that an $\mathcal{L}$-term can never be infinitely long, because they cannot be constructed using the above rules.

We see that the $\mathcal{L}$-terms are just syntactical objects. Their semantics are given in $\mathcal{L}$-structures as follows.

%\begin{xmp}[]
%  One can construct the natural numbers as follows:
%  Take $\mathcal{L} = \{\underline{0},\text{succ}\}$, where $\text{succ}$ is a unary function symbol.
%  Without any variables, we can associate to every natural number an $\mathcal{L}$-term as follows:
%  \begin{align*}
%    \underline{0} \mapsto 0, \quad 
%    \text{succ}\ \underline{0} \mapsto 1, \quad
%    \text{succ}\ \text{succ}\ \underline{0} \mapsto 2,\quad
%    \text{succ}\ n \mapsto n+1
%  \end{align*}
% Verify for yourself that these are valid $\mathcal{L}$-terms. 
% In fact, these are the only valid $\mathcal{L}$-terms.
%\end{xmp}

\begin{dfn}[]~\label{dfn:interpretation}
Let $t$ be an $\mathcal{L}$-term, $\mathcal{A} = (A,{(z^{\mathcal{A}})}_{z \in \mathcal{L}})$ be an $\mathcal{L}$-structure and $\vec{b} = (b_0,b_1,\ldots)$ a sequence of elements of $A$.
We define the \textbf{interpretation} $t^{\mathcal{A}}[\vec{b}] \in A$ inductively by
\begin{align*}
  v_i^{\mathcal{A}}[\vec{b}] 
  &= b_i\\
  c^{\mathcal{A}}[\vec{b}] 
  &= c^{\mathcal{A}}\\
  {(f\ t_1 \cdots t_n)}^{\mathcal{A}} 
  &= f^{\mathcal{A}}\left(
    t_1^{\mathcal{A}}[\vec{b}], \ldots, t_n^{\mathcal{A}}[\vec{b}]
  \right)
\end{align*}
we call $\vec{b}$ an \textbf{assignment} of the variables $v_0,v_1,\ldots$.
\end{dfn}
In formally, an assignment replaces every occurence of a variable with a specific value from the universe.

\begin{xmp}[]
Let $\mathcal{L} = \mathcal{L}_{\Ring}$.
We claim that every $\mathcal{L}$-term is a polynomial in $\Z[x_{0}, \ldots]$.
\end{xmp}
\begin{proof}
  We prove this by induction over the complexity.
  First, we check the statement for $\mathcal{L}$-terms of complexity $0$, which are exactly the variables and the constants.
  Clearly, $x_i,\underline{0},\underline{1}$ are polynomials in $\Z[x_{0}, \ldots]$.

  Now, assume that $p,q$ be $\mathcal{L}$-terms of some complexity which are polynomials.
  The only way in which the complexity can grow is when we apply functions.
  So it suffices to check that
  \begin{align*}
    p + q \in \Z[x_0,\ldots], \quad p \cdot q \in \Z[x_0,\ldots], \quad -p \in \Z[x_0,\ldots]
  \end{align*}
  are again polynomials.
\end{proof}

\begin{xmp}[]
Take the $\mathcal{L}_{\Ring}$ structure $\mathcal{A} = \Z$.
For the term $t = (x_0 + x_1) \cdot (x_2 + x_3)$ and $\vec{b} = [3,-5,1,2,1,\ldots]$, the interpretation of the term is
\begin{align*}
  t^{\mathcal{A}}[\vec{b}] = (3 + -5) \cdot (1 + 2) = -6 \in A= \Z
\end{align*}
\end{xmp}


\begin{lem}[]\label{lem:independent-unused-variables}
The interpretation $t^{\mathcal{A}}[\vec{b}]$ is only dependent on $b_i$, if $v_i$ is present in $t$.
\end{lem}
\begin{proof}
  Let $t$ be a term in variables $v_{1}, \ldots, v_{n}$ and $\vec{b}, \vec{b}'$ assignments such that for $1 \leq i \leq n$, $b_i = b_i'$.

  We again prove this by induction on the complexity of the term.
  If $t$ is a constant $c$, then
  \begin{align*}
    t^{\mathcal{A}}[\vec{b}] = c^{\mathcal{A}}[\vec{b}] = c^{\mathcal{A}} = t^{\mathcal{A}}[\vec{b}']
  \end{align*}
  If $t$ is a variable $v_i$, then by assumption $1 \leq i \leq n$, so
  \begin{align*}
    t^{\mathcal{A}}[\vec{b}] = v_i^{\mathcal{A}}[\vec{b}] = b_i = b_i' = t^{\mathcal{A}}[\vec{b}']
  \end{align*}
  And if $t$ is of complexity $n+1$, then $t$ is of the form $f\ t_1 \cdots t_k$, where each $t_i$ is a term of complexity $\leq n$ and only contains the variables $v_1,\ldots,v_n$.
  By induction hypothesis, we may assume that $t_i^{\mathcal{A}}[\vec{b}] = t_i^{\mathcal{A}}[\vec{b}']$, so 
  \begin{align*}
    t^{\mathcal{A}}[\vec{b}] = {(f\ t_1 \cdots t_k)}^{\mathcal{A}} 
    = f^{\mathcal{A}} \left(
      t_1^{\mathcal{A}}[\vec{b}],\ldots,t_k^{\mathcal{A}}[\vec{b}]
    \right)
    = f^{\mathcal{A}} \left(
      t_1^{\mathcal{A}}[\vec{b}'],\ldots,t_k^{\mathcal{A}}[\vec{b}']
    \right)
    = t^{\mathcal{A}}[\vec{b}']
  \end{align*}
\end{proof}

\begin{dfn}[]
If $x_{1}, \ldots, x_{n} \in \{v_0,v_1,\ldots\}$ are pairwise different and no other variables occur in $t$, we write $t = t(x_{1}, \ldots, x_{n})$.

If $\vec{b}$ is an assignment that assigns to $x_i$ the element $a_i \in A$, we write
\begin{align*}
  t^{\mathcal{A}}[a_{1}, \ldots, a_{n}] := t^{\mathcal{A}}[\vec{b}]
\end{align*}
(Which of course is well-defined as it does not depend on the choice of any such $\vec{b}$.)

If $t_{1}, \ldots, t_{n}$ are terms, we can replace them with the variables $x_{1}, \ldots, x_{n}$.
The result is a term we write as $t(t_1,\ldots,t_1)$.
\end{dfn}
\begin{xmp}[]
  In $\mathcal{L}_{\Ring}$, take $t_1 = t_1(x_1) = x_1 \cdot x_1$ and $t_2(x_2) = x_2 + x_2$.
  For $t = t(x_1,x_2) = x_1 + x_2$ we can write
  \begin{align*}
    t(t_1,t_2) = (x_1 \cdot x_1) + x_2 + x_2
  \end{align*}
\end{xmp}

\begin{lem}[]\label{lem:application-interpretation}
\begin{align*}
  {t(t_{1}, \ldots, t_{n})}^{\mathcal{A}}[\vec{b}]
  =
  t^{\mathcal{A}}\left[
    t_1^{\mathcal{A}}[\vec{b}],\ldots,t_n^{\mathcal{A}}[\vec{b}]
  \right]
\end{align*}
%in other words, the following diagram commutes:
%\begin{center}
%\begin{tikzcd}[ ] 
%  \left\{
%    \mathcal{L}-term
%  \right\}
%  \arrow[]{r}{\text{application}}
%  &
%  \{\mathcal{L}-term\}
%  \\
%  &
%\end{tikzcd}
%\end{center}
\end{lem}
\begin{xmp}[]
In the previous example, where $t(x_1,x_2) = x_1 + x_2$, $t_1 = x_1 \cdot x_1$, $t_2 = x_2 + x_2$, take for example $\vec{b} = [0,1,2,\ldots]$. Then
\begin{align*}
  {t(t_1,t_2)}^{\mathcal{A}}[\vec{b}] = 
  t^{\mathcal{A}}\left[
    t_1^{\mathcal{A}}[\vec{b}], t_2^{\mathcal{A}}[\vec{b}]
  \right]
  = t^{\mathcal{A}}\left[
    1 \cdot 1, 2 + 2
  \right]
  = 1 + 4 = 5
\end{align*}
\end{xmp}

\begin{lem}[]
Let $h: \mathcal{A} \to \mathcal{B}$ be a homomorphism and $t(x_{1}, \ldots, x_{n})$ a term.
Then for all $a_{1}, \ldots, a_{n}\in A$:
\begin{align*}
  t^{\mathcal{B}}[h(a_1),\ldots,h(a_n)]
  =
  h(t^{\mathcal{A}}[a_{1}, \ldots, a_{n}])
\end{align*}
\end{lem}
\begin{proof}
  The proof is pretty much similar to the proof of Lemma~\ref{lem:independent-unused-variables}
\end{proof}

Using the language of terms and interpretations, we can give an alternative definition of generated substructures.
Namely, if an element $a \in A$ in the universe is generated by a subset $S \subseteq A$, then there exists a term using variables in $S$ whose interpretation is the element $a$.
\begin{lem}[]
Let $\mathcal{A}$ be an $\mathcal{L}$-structure and $\emptyset \neq S \subseteq A$.
Then
\begin{align*}
  \scal{S}^{\mathcal{A}} = \left\{
    t^{\mathcal{A}}[s_{1}, \ldots, s_{n}]
    \big\vert
    t(x_{1}, \ldots, x_{n})\ \text{is an $\mathcal{L}$-term}, s_{1}, \ldots, s_{n} \in S
  \right\}
\end{align*}
\end{lem}
\begin{proof}
  We can assume that $\mathcal{L}$ contains a constant, or else both sides are trivial.

  \textbf{``$\bm{\supseteq}$'':} 
  By Lemma~\ref{lem:application-interpretation}, the universe is closed under interpretation of terms $t^{\mathcal{A}}[\ldots]$.

  \textbf{``$\bm{\subseteq}$'':} 
  Note that the right hand side is closed under all operations $f^{\mathcal{A}}$ and contains all constants $c^{\mathcal{A}}$.
  Indeed, if $c^{\mathcal{A}} \in A$ is a constant, then take the term $t = c$ in no variables.
  Then $t^{\mathcal{A}}[] = c^{\mathcal{A}}$. 

  Now, let $a_{1}, \ldots, a_{n}$ be elements in the right-hand-side and $f$ an $n$-ary function symbol.
  So there exist terms $t_i(x_{i,1},\ldots,x_{i,_{r_i}})$ and elements $s_{i_1},\ldots,s_{i,r_i} \in S$ such that
  \begin{align*}
    a_i = t_i^{\mathcal{A}}[s_{i,1},\ldots,s_{i,r_i}]
  \end{align*}
  We can re-order the indices for $x_{i,k}$ and $s_{i,k}$ into $x_{1},\ldots,x_r$ and $s_1,\ldots,s_r$ (with $r = \sum_{i=1}^{n}r_{i}$) such that the elements
  Then chose the $\mathcal{L}$-term $t = f\ t_1\ \cdots\ t_n$ to get
  \begin{align*}
    t^{\mathcal{A}}[x_1,\ldots,x_r] 
    &= {(f\ t_1\ \cdots\ t_n)}^{\mathcal{A}}[x_{1}, \ldots, x_r]\\
    &= f^{\mathcal{A}}
    \left(
      t_1^{\mathcal{A}}[x_{1}, \ldots, x_{r}],
      \ldots
      t_n^{\mathcal{A}}[x_{1}, \ldots, x_{r}],
    \right)
    \\
    &= f^{\mathcal{A}}\left(
      a_1,\ldots,a_n
    \right)
  \end{align*}
  It follows that the right hand side is a substructure containing $S$.
  Since $\scal{S}^{\mathcal{A}}$ is the smallest substrucutre containing $S$ equality follows.

\end{proof}

\begin{xmp}[]
Take $\mathcal{L} = \mathcal{L}_{\Grp}$, $\mathcal{A} = G$ for some group $G$ and $S \subseteq G$.
The $\mathcal{L}_{\Grp}$ terms are precisely the words, so this lemma says that the subgroup generated by $S$ is
\begin{align*}
  \scal{S}^{G} = \left\{
    s_{i_1}^{\pm 1} s_{i_2}^{\pm 1} \cdots s_{i_k}^{\pm 1} \big\vert s_{ij} \in S
  \right\}
\end{align*}
What is nice about this, is that we can use this ``template definition'' and use it in many contexts.
\end{xmp}

\begin{cor}[]
  Let $\emptyset \neq S \subseteq A$. Then
\begin{align*}
  \abs{\scal{S}^{\mathcal{A}}} \leq \max \left(
    \abs{S},\abs{\mathcal{L},K,\aleph_0}
  \right)
\end{align*}
where $\aleph_0$ is the cardinality of the natural numbers.
\end{cor}
\begin{proof}
There are at most $\max(\abs{\mathcal{L}},\aleph_0)$ many $\mathcal{L}$-terms, because we may only create terms of countable length. (They are sequences, after all).

For every term $t$, there are most $\max(\abs{S},\aleph_0$) combinations of elements from $S$ for the variables that occur in $t$.
\end{proof}


\begin{dfn}[]~\label{dfn:l-formula}
  Let $\mathcal{L}$ be a language.
  \begin{itemize}
    \item An $\mathcal{L}$-formula is of the form
      \begin{enumerate}
        \item $t_1 \doteq t_2$, where $t_1,t_2$ are $\mathcal{L}$-terms.
        \item $R\ t_1 \cdots t_n$, where $R \in \mathcal{L}$ is an $n$-ary relation symbol and $t_{1}, \ldots, t_{n}$ are $\mathcal{L}$-terms.
        \item $\lnot \psi$, where $\psi$ is an $\mathcal{L}$-formula.
        \item $(\psi_1 \land \psi_2)$, where $\psi_1,\psi_2$ are $\mathcal{L}$-formulae.
        \item $\exists x \psi$, where $\psi$ is an $\mathcal{L}$-formula and $x$ is a variable.
      \end{enumerate}
    \item $\mathcal{L}$-formulae as in (a) and (b) are called \textbf{atomic formulae}.
    \item The \textbf{complexity} of a formula is the number of occurences of $\lnot, \exists$ and $\land$.
  \end{itemize}
  By \emph{convention}, we use the following abbreviations for $\mathcal{L}$-formulae.
  \begin{itemize}
    \item $(\psi_1 \lor \psi_2) := \lnot (\lnot \psi_1 \land \psi_2)$.
    \item $(\psi_1 \to \psi_2) := \lnot(\psi_1 \land \lnot \psi_2)$.
    \item $(\psi_1 \leftrightarrow \psi_2) := ((\psi_1 \to \psi_2) \land (\psi_2 \to \psi_1))$
    \item $\forall x \psi := \lnot \exists x \lnot \psi$
    \item $\exists x_{1}, \ldots, x_{n} := \exists x_1 \exists x_2 \cdots \exists x_n$
    \item $\forall  x_{1}, \ldots, x_{n} := \forall x_1 \cdots \forall x_n$
  \end{itemize}
\end{dfn}
We will add or omit parentheses, to make formulae more readable.


\begin{dfn}[]~\label{dfn:satisfying-formula}
Let $\mathcal{A}$ be an $\mathcal{L}$-structure, $\psi$ an $\mathcal{L}$-formula and $\vec{b}$ an assigment.
We define the relation $\mathcal{A} \models \psi(\vec{b})$ as follows:
\begin{itemize}
  \item $\mathcal{A} \models (t_1 \doteq t_1)[\vec{b}] \iff t_1^{\mathcal{A}}[\vec{b}] = t_2^{\mathcal{A}}[\vec{b}]$ (as elements in the universe $A$)
  \item $\mathcal{A} \models R\ t_1 \cdots t_n[\vec{b}] \iff R^{\mathcal{A}}\left(
    t_1^{\mathcal{A}}[\vec{b}],\ldots,t_n^{\mathcal{A}}[\vec{b}]\right)$
  \item $\mathcal{A} \models \lnot \psi[\vec{b}] \iff \mathcal{A} \not\models \psi[\vec{b}]$.
  \item $\mathcal{A} \models (\psi_1 \land \psi_2)[\vec{b}] \iff \mathcal{A} \models \psi_1[\vec{b}]$ and $\mathcal{A} \models \psi_2[\vec{b}]$
  \item $\mathcal{A} \models \exists x \psi[\vec{b}] \iff$ there exists an $a \in A$ such that $\mathcal{A} \models \psi[(b_0,\ldots,b_{i-1},a,b_{i+1},\ldots)]$ for $x = v_i$.
\end{itemize}
If $\mathcal{A} \models \psi[\vec{b}]$, we say that ``$\psi$ \textbf{is true for} $\vec{b}$'' or ``$\vec{b}$ \textbf{satisfies} $\psi$ in $\mathcal{A}$''.

\end{dfn}

\begin{xmp}[]
For $\mathcal{L} = \mathcal{L}_{\Ring}$, consider the formula $\psi = \left[
  \exists\ x\ (x^{2} \doteq \underline{0}) \land (\not (x \doteq \underline{0}))
\right]$.
Then $\R \not \models \psi$, 
\end{xmp}

Note that the formula $\psi$ does not depend on the variable $x$.
If we were to write $y$ instead of $x$, it would not make a difference at all.
This leads us to the concept of \emph{free variables}.
\begin{dfn}[]
Let $x$ be a variable and $\psi$ a formula.
We say that
\begin{itemize}
  \item 
    $x$ is \textbf{free} in
    $t_1 = t_2 \iff x$ appears in $t_1$ or $t_2$.
  \item $x$ is \textbf{free} in $R\ t_1\ \cdots\ t_n$ $\iff$ $x$ appears in one of the $t_i$.
  \item
    $x$ is \textbf{free} in
    $\lnot \psi \iff x$ is free in $\psi$

  \item 
    $x$ is \textbf{free} in
    $(\psi_1 \land \psi_2) \iff x$ is free in $\psi_1$ or $\psi_2$
  \item 
    $x$ is \textbf{free} in
    $\exists y \psi \iff x \neq y$ and $x$ is free in $\psi$
\end{itemize}
If $x$ appears in $\psi$ and is \emph{not} free, we say that $x$ is a \textbf{bound variable} in $\psi$.
\end{dfn}
\begin{xmp}[]
$x$ is free in the formula $[x^{2} \doteq  \underline{0}]$.
$x$ is bound in $[\exists x\ x^{2} \doteq  \underline{0}]$.
\end{xmp}

\begin{rem}[]
An alternative definition is as follows:

$x$ is bound in $\psi$, if it occurs within a subformula $\psi$ of $\psi$ of the form $\exists x\ \psi$ or $\forall x\ \psi$.
The free variables are those, that are not introduced with $\forall$ or $\exists$.
\end{rem}


\begin{lem}[]
If $\vec{b}$ and $\vec{c}$ agree on all variables that are free in $\psi$, then
\begin{align*}
  \mathcal{A} \models \psi[\vec{v}] \iff \mathcal{A} \models \psi[\vec{c}]
\end{align*}
\end{lem}

\textbf{Notation:} We will write
\begin{align*}
  \psi(x_1,\ldots,x_n) \quad \text{if} \quad x_i \neq x_j \text{\ and all $\psi$ contains no other free variables}
\end{align*}
\begin{align*}
  \mathcal{A} \models \psi[a_{1}, \ldots, a_{n}] \quad \text{if} \quad \mathcal{A} \models \psi[\vec{b}] \text{\ for some assignment $\vec{b}$ with $\vec{b}(x_i) = a_i$}
\end{align*}

\begin{dfn}[]
Let $\psi = \psi(x_{1}, \ldots, x_{n})$ be an $\mathcal{L}$-formula and $\mathcal{A}$ an $\mathcal{L}$-structure.

\begin{itemize}
  \item The set
    \begin{align*}
      \psi(\mathcal{A}) := \left\{
        \overline{a} = (a_{1}, \ldots, a_{n}) \in A^{n} \big\vert \mathcal{A} \models \psi[\overline{a}]
      \right\}
      \subseteq A^{n}
    \end{align*}
    is called the \textbf{realisation} of $\psi$.
  \item A subset $S \subseteq A^{n}$ is called \textbf{$\bm{0}$-definable}, if $S = \psi(\mathcal{A})$ for some formula $\psi(x_{1}, \ldots, x_{n})$.
  \item If $B \subseteq A$, then we get a new language $\mathcal{L}(b) = \mathcal{L} \cup B$, by treating every element of $B$ as a new constant.
  In this view, $\mathcal{A}$ is also an $\mathcal{L} (B)$-structure which we denote by $\mathcal{A}_B = {(\mathcal{A},b)}_{b \in B}$.
\item Let $B \subseteq A$. A subset $S \subseteq A^{n}$ is called \textbf{$B$-definable}, if $S = \psi(\mathcal{A})$ for an $\mathcal{L}(B)$-formul $\psi(x)$.
\item $S \subseteq A^{n}$ is called \textbf{definable}, if it is $B$-definable for some $B \subseteq A$.
\item Two formulae $\phi,\psi$ are \hypertarget{dfn:equivalent-formulae}{\textbf{equivalent}}, if for \emph{every} $\mathcal{L}$-structure $\mathcal{A}$ it holds $\phi(\mathcal{A}) = \psi(\mathcal{A})$.
\end{itemize}
\end{dfn}
\begin{xmp}[]
For $\mathcal{L} = \mathcal{L}_{\Ring}$, $\phi(\overline{x}) = [P(\overline{x}) \doteq 0]$, where $P$ is some polynomial.
Then for a ring $R$ (i.e.\ an $\mathcal{L}$-structure), $\phi(R)$ is the set of roots of $P$ in $R$.

For the ring $\R$, the set $\{\pm \sqrt{2}\} \subseteq \R$ is $0$-definable via $\phi(x) = [x^{2} \doteq 2]$.
However, (even for $R = \C$), the set $\{\pm \pi\}$ is \emph{not} $0$-definable.

For $B = \{\pi\} \subseteq \R$, it is $B$-definable, for $\phi(x) = [x^{2} \doteq \pi^{2}]$.

\end{xmp}
