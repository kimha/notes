\subsection{Elementary substructures}
\begin{dfn}[]
Let $\mathcal{A},\mathcal{B}$ be $\mathcal{L}$-structures with universes $A,B$.
\begin{itemize}
  \item 
A map $h:A \to B$ is called \textbf{elementary}, if it preserves validity of formulae.
Explicitly:

For all $\mathcal{L}$-formulae $\phi(x_{1}, \ldots, x_{n})$ and $a_{1}, \ldots, a_{n} \in A$
\begin{align*}
  \mathcal{A} \models \phi[a_{1}, \ldots, a_{n}]
  \iff
  \mathcal{B} \models \phi[h(a_{1}), \ldots, h(a_{n})]
\end{align*}
  \item 
    A substructure $\mathcal{A}$ of $\mathcal{B}$ is called \textbf{elementary}, if the inclusion map is elementary.
    We write $\mathcal{A} \prec \mathcal{B}$ and say that $\mathcal{B}$ is an \textbf{elementary extension} of $\mathcal{A}$.
\end{itemize}
\end{dfn}

\begin{rem}[]
Note that if $h: A \to B$ is elementary, then $h$ contains quantor-free formulae and thus is also an embedding (as in Definition~\ref{dfn:l-structure-homomorphism}).
\end{rem}
For $A$ a set, we denote by $\mathcal{L}(A)$ the language, obtained by taking $\mathcal{L}$ and adding all elements of $S$ as constants.

\begin{thm}[Tarskis Test]
  Let $\mathcal{B}$ a an $\mathcal{L}$-structure and $A \subseteq B$. 
  Then $A$ is the universe of an elementary substructure if and only if every $\mathcal{L}(A)$-formula $\phi(x)$ that is satisfiable in $\mathcal{B}$ is also satisfiable by an element in $A$.
\end{thm}
\begin{proof}
\textbf{``$\bm{\implies}$'':} 
Assume $\mathcal{A} \prec \mathcal{B}$ and $\mathcal{B} \models \exists x \phi(x)$, then $A \models \exists x f(x)$, so there is an $a \in A$ such that $\mathcal{A} \models \phi(a)$.
Thus $\mathcal{B} \models \phi(a)$.

\textbf{``$\bm{\impliedby}$'':} 
Assume every $\mathcal{L}(A)$ formula that is satisfiable in $\mathcal{B}$ is also satisfiable in $A$.

We show again that $A$ is the universe of a substructure $\mathcal{A}$.

By Remark~\ref{rem:substructure}, it suffices to show that $A$ contains all constants and is closed under all operations.

If $c \in \mathcal{L}(A)$ is a constant, then $\exists x\ x \doteq c$ is satisfiable in $\mathcal{B}$ and by assumption also in $A$.
So $A$ contains all constants.

Now if $f \in \mathcal{L}$ is an $n$-ary function symbol and $a_{1}, \ldots, a_{n} \in A$, then the formula
$\phi(x) = [f(a_{1}, \ldots, a_{n}) \doteq  x]$
is satisfiable in $\mathcal{B}$ (or else $\mathcal{B}$ would be a valid $\mathcal{L}$-strucutre).
Since $\phi(x)$ is also satisfiable in $A$, it follows that $A$ is closed under all operations and thus the universe of a substructure $\mathcal{A} \subseteq \mathcal{B}$.

We have to show that the inclusion map $A \to B$ is elementary. 
We proceed by induction on the constructors of $\mathcal{L}$-formulae, i.e.\ on the complexity of $\phi$. (See Definition~\ref{dfn:l-formula}).

For atomic formulae and formulae of the form $\lnot \psi$ or ($\psi_1 \land \psi_2$), this is trivial.

So assume that $\phi = \exists x \psi(x)$.
If $\phi$ holds in $\mathcal{A}$, there exists an $a \in A$ such that $\mathcal{A} \models \psi(x)$.
By induction ($\psi$ has lesser complexity than $\phi$), $\mathcal{B} \models \psi(a)$, so $\mathcal{B} \models \phi$.

On the other hand, assume $\mathcal{B} \models \phi$.
Then $\psi(x)$ is satisfiable in $\mathcal{B}$, and by assumption satisfiable for some $a \in A$.
Then $\mathcal{B} \models \psi(a)$ and by induction we have $\mathcal{A} \models \psi(a)$, so $\mathcal{A} \models \phi$.
\end{proof}

\begin{cor}[]
Let $\mathcal{B}$ be an $\mathcal{L}$-structure and $S \subseteq B$.
Then $\mathcal{B}$ contains an elementary substructure $\mathcal{A}$ that contains $S$ and
\begin{align*}
  \abs{\mathcal{A}} \leq \max(\abs{S},\abs{\mathcal{L}},\aleph_0)
\end{align*}
\end{cor}
\begin{proof}
  We construct $A$ as a union of an increasing sequence $S_0 \subseteq S_1 \subseteq \ldots$ of subsets of $B$.
  Set $S_0 := S$ and define inductively $S_{i+1}$ as follows:
  For each $\mathcal{L}(S_i)$-formula $\phi(x)$ that is satisfiable in $\mathcal{B}$, we chose an element $a_{\phi} \in B$ that satisfies $\phi$ (which means $\mathcal{B} \models \phi(a)$)
  \begin{align*}
    S_{i+1} := S_i \cup \left\{
      a_{\phi} \big\vert \text{\parbox{10em}{$\phi$ is an $\mathcal{L}(S_i)$-formula satisfiable in $\mathcal{B}$.}}
    \right\}
  \end{align*}
  Then set $A := \bigcup_{i \geq 0}S_i$.
  Note that every $\mathcal{L}(A)$ formula $\phi$ that is satisfiable in $\mathcal{B}$ is an $\mathcal{L}(S_i)$-formula satisfiable in $\mathcal{B}$ for some $i$ large enough.
  And by construction, $\phi$ is satisfiable by an element in $A$, so 
  by Tarksis test, $A$ is the universe of an elementary substrucutre of $\mathcal{B}$.

  It remains to show the upper bound for $\abs{\mathcal{A}}$.

  An $\mathcal{L}'$-fomrula is a finite sequence of symbols of $\mathcal{L}'$, quantors and the symbols $\land, \lnot$.
  In particular, there are at most 
  $\abs{\mathcal{L}'} + \aleph_0 = \max (\abs{\mathcal{L}'},\aleph_0)$ many such symbols.

  It follows that there at most $\max \left\{
    \abs{S}, \abs{\mathcal{L}}, \aleph_0
  \right\}$ many $\mathcal{L}(S)$ formula, so $\abs{S_1} \leq 
  \max \left\{
    \abs{S}, \abs{\mathcal{L}}, \aleph_0
  \right\}$.
  Inductively we show this bound for all $\abs{S_i}$.
\end{proof}

\begin{dfn}[]
A directed family ${(\mathcal{A}_{i})}_{i \in I}$ is of $\mathcal{L}$-structures is called \textbf{elementary}, if $\mathcal{A}_i \prec \mathcal{A}_j$ for $i \leq j$.
\end{dfn}

\begin{thm}[Tarskis Chain Lemma]~\label{thm:chain-lemma}
  The union of an elementary directed family ${(\mathcal{A}_{i})}_{i \in I}$ is an elementary extension of all $\mathcal{A}_i$.
\end{thm}
\begin{proof}
  Set $\mathcal{A} := \bigcup_{i \in I}\mathcal{A}_i$.
  We prove by induction over the complexity of $\phi(\overline{x})$ that for all $i \in I, \overline{a} \in \mathcal{A}_i$
  \begin{align*}
    \mathcal{A}_i \models \phi(\overline{a}) 
    \iff
    \mathcal{A} \models \phi(\overline{a}) 
  \end{align*}
  For the atomic formula, this is obvious.
  We now go over the constructors.

  \textbf{Case $\bm{\phi \lnot \psi}$:} We have
  \begin{align*}
    &\mathcal{A}_i \models \phi(\overline{a}) 
    \iff
    \mathcal{A}_i \models \lnot \psi(\overline{a})
    \stackrel{\text{Def.}~\ref{dfn:}}{\iff}
    \mathcal{A}_i \not \models \psi(\overline{a})
    \\
    \stackrel{\text{ind.}}{\iff}
    &\mathcal{A} \not \models \psi(\overline{a})
    \iff
    \mathcal{A} \models \lnot \psi(\overline{a})
    \mathcal{A} \models \phi(\overline{a}) 
  \end{align*}
  The proof is again similar for the constructor $\phi = \psi_1 \land \psi_2$.

  \textbf{Case $\bm{\phi(\overline{x}) = \exists y \psi(\overline{x},y)}$:} 
  

\end{proof}
Compare this with the example from Exercise~\ref{exr:union-directed-family} and check that this does not form a contradiction.

The reason this fails is that for the elementary embedding $\mathcal{A}_i \prec \mathcal{A}_{j}$, we \emph{have to} use the inclusion map $h: \tfrac{a}{2^{i}}\mapsto \tfrac{2^{j-i}a}{2^{j}}$ and we cannot use the isomorphism $h_{ij}$ as in the exercise.

So for the formula
\begin{align*}
  \phi(x) := [\exists y\ y + y \doteq x]
\end{align*}
we have $\mathcal{A}_1 \not \models \phi(1)$, but $\mathcal{A}_2 \models \phi(h(1))$.
So we do not have an elemenentary directed family and the theorem does not apply.




