\subsection{Compactness theorem}

Let $T$ be an $\mathcal{L}$-theory.


\begin{dfn}[]
$T$ is called \textbf{finitely satisfiable}, if any finite subset $\Delta \subseteq T$ is consistent.
\end{dfn}

\begin{thm}[Compactness theorem]
Every finitely satisfiable theory is consistent.
\end{thm}
\begin{rem}[]
  We will not give a proof now because this was given in the lecture ``Die Gödelschen Sätze''.
  Nonetheless, this is an important theorem, so please try to understand it well.

  The name comes from topology, where the theorem says something about compactness of a Stone space.
\end{rem}

\begin{cor}[]~\label{cor:consequence}
$T \vdash \phi$ if and only if there exists a finite subset $\Delta \subseteq T$ with $\Delta \vdash \phi$.
\end{cor}
\begin{proof}
  $T \vdash \phi$ if and only if $T \cup \{\lnot \phi\}$ is inconsistent.
\end{proof}

\begin{cor}[]
A set of formula $\Sigma(x_{1}, \ldots, x_{n})$ is consistent with $T$ if and only if every finite subset of $\Sigma$ is consistent with $T$.
\end{cor}
\begin{proof}
Extend $\mathcal{L}$ by adding new constants $c_{1}, \ldots, c_{n}$.
By Lemma~\ref{lem:consistent-extension}, then $\Sigma$ is constent with $T$ if and only if $T \cup \Sigma(c_{1}, \ldots, c_{n})$ is consistent.

By the compactness theorem, this is the case if and only if every finite subset of $T \cup \Sigma(c_{1}, \ldots, c_{n})$ is consistent.
\end{proof}


\begin{dfn}[]
Let $\mathcal{A}$ be an $\mathcal{L}$-structure, $\mathcal{B} \subseteq \mathcal{A}$ and $\Sigma(x)$ a set of $\mathcal{L}(B)$-formulae with at least one free variable.


\begin{itemize}
  \item $a \in A$ is said to \textbf{realize} (or \textbf{satisfy}) $\Sigma(x)$, if $a$ realizes all formulae $\phi(x)$ of $\Sigma(x)$ (i.e.\  $\mathcal{A} \models \phi(a)$).

    If such an $a$ exists, we write $\mathcal{A} \models \Sigma(a)$.
  \item $\Sigma(x)$ is called \textbf{finitely satisfiable} in $\mathcal{A}$, if every finite subset of $\Sigma$ is realizable in $\mathcal{A}$.
\end{itemize}
We extend this notation to write $\mathcal{A} \models \Sigma(\overline{a})$ if $\Sigma(\overline{x})$ has multiple free variables.
\end{dfn}

\begin{lem}[]\label{lem:finitely-satisfiable-iff-realized-elementary-extension}
$\Sigma(x)$ is finitely satisfiable in $\mathcal{A}$ if and only if there exists an elementary extension of $\mathcal{A}$ where $\Sigma(x)$ is realizd.
\end{lem}
\begin{proof}
  \textbf{``$\bm{\impliedby}$'':} 
  Let $\mathcal{A} \prec \mathcal{B}$ such that $\mathcal{B} \models \Sigma(\vec{b})$.
  For $\phi_1(\overline{x}), \ldots, \phi_n(\overline{x}) \in \Sigma(\overline{x})$ we have
  \begin{align*}
    \mathcal{B} \models \exists \overline{x} \phi_1(\overline{x}) \land \ldots \land \phi_n(\overline{x})
  \end{align*}
  Since $A \prec \mathcal{B}$, we also have $\mathcal{A} \models \exists \overline{x} \phi_1(\overline{x}) \land \ldots \land \phi_n(\overline{x})$.

  \textbf{``$\bm{\implies}$'':} Let $\Sigma(\overline{x})$ be finitely satisfiable.
  For $\phi_{1}(\overline{x}), \ldots, \phi_{n}(\overline{x}) \in \Sigma(\overline{x})$, we have
  \begin{align*}
    \mathcal{A}_A \models \exists \overline{x} \phi_1(\overline{x}) \land \ldots \land \phi_n(\overline{x})
  \end{align*}
  so every finite subset of $\Sigma(\overline{x})$ is consistent with $\Theory(\mathcal{A}_A)$.
  %By Corollary~\ref{cor:}
  \begin{center}
  Missing 5 min
  \end{center}

\end{proof}

Recall that $\mathcal{A}_A$ is an $\mathcal{L}(A)$-structure, where elements of $A$ are additional constants.
\begin{lem}[]
The models of $\Theory(\mathcal{A}_A)$ are precisely the structures of the form ${(\mathcal{B},h(a))}_{a \in A}$ for elementary embeddings $h: \mathcal{A} \stackrel{\prec}{\to}\mathcal{B}$.
\end{lem}


\begin{dfn}[]
Let $\mathcal{A}$ be an $\mathcal{L}$-structure and $B \subseteq A$.

\begin{itemize}
  \item A set $p(x)$ of $\mathcal{L}(B)$-formula is a \textbf{$1$-type over $B$}, if $p(x)$ is maximally finitely satisfiable.

    Maximally here means that adding another $\mathcal{L}(B)$-formula to $p(x)$ would make it no longer finitely satisfiable.
  \item We denote by $S(B) = S^{\mathcal{A}}(B)$ the set of all $1$-types over $B$.
  \item An \textbf{$n$-type} $p(x_{1}, \ldots, x_{n})$ over $B$ is a set of $\mathcal{L}$-formulae in $n$ variables that is maximally finitely satisfiable in $\mathcal{A}$.
  \item We denote by $S_n(B) = S_n^{\mathcal{A}}(B)$ the set of all $n$-types over $B$.
  \item Every element $\overline{a} \in A^{n}$ determines an $n$-type
    \begin{align*}
      \text{tp}(\overline{a}/B) 
      := \text{tp}^{\mathcal{A}}(\overline{a}/B)
      :=
      \left\{
        \phi(\overline{x})
        \big\vert \phi \text{\ is an $\mathcal{L}(B)$-formula such that } \mathcal{A} \models \phi(\overline{a})
      \right\}
    \end{align*}
  \item We write $\text{tp}(\overline{a}) = \text{tp}(\overline{a}/\emptyset)$.
\end{itemize}
\end{dfn}

\begin{rem}[]
\begin{itemize}
  \item $a$ realizes $p \in S(B)$ if and only if $p = \text{tp}(a/B)$.
    The inclusion $p \subseteq \text{tp}(a/B)$ is always true.
  \item If $\mathcal{A} \prec \mathcal{A}$, then $S^{\mathcal{A}}(B) = S^{\mathcal{A}'}(B)$ and $\text{tp}^{\mathcal{A}}(a/B) = \text{tp}^{\mathcal{A}'}(a/B)$.
    So the notation is well-defined.
\end{itemize}
\end{rem}

\begin{cor}[]
Every $\mathcal{L}$-structure $\mathcal{A}$ has an elementary extension in which all types over $A$ are realized.
\end{cor}
\begin{proof}
By the well-ordering theorem, there exists an ordinal $\lambda$ and an ordering ${(p_{\alpha})}_{\alpha \leq \lambda}$ of $S(A)$.

We construct the elementary extension as an elementary chain
\begin{align*}
  \mathcal{A} = \mathcal{A}_0 \prec \mathcal{A}_1 \prec \ldots \prec \mathcal{A}_{\beta} \prec \ldots \quad \text{for all} \quad \beta \leq \lambda
\end{align*}
such that $p_{\alpha}$ is realized in $\mathcal{A}_{\alpha + 1}$.
Then every type over $\mathcal{A}$ s realized in $\mathcal{B} = \mathcal{A}_{\lambda}$.

\textbf{Constructing the chain:} 
Assume for an ordinal $\beta$, we have already defined ${(\mathcal{A}_{\alpha})}_{\alpha < \beta}$.
If $\beta$ is a limit ordinal, define
\begin{align*}
  \mathcal{A}_{\beta} := \bigcup_{\alpha < \beta} \mathcal{A}_{\alpha}
\end{align*}
By Tarski's chain lemma (Lemma~\ref{thm:chain-lemma}), ${(\mathcal{A}_{\alpha})}_{\alpha \leq \beta}$ is elementary.

If $\beta = \beta' + 1$ is a successor ordinal, $p_{\alpha}$ is finitely satisfiable.
By Lemma~{lem:finitely-satisfiable-iff-realized-elementary-extension}, there dxists an elementary extension $\mathcal{A}_{\beta}$ in which $p_{\alpha}$ is satisfied.


\end{proof}
