We consider an electron in a potential pot and we would like to see if we can excite this electron into a higher energy state.

The system is described by a perturbed Hamiltonian 
\begin{align*}
  \hat{H} = \hat{H}_0 + \hat{H}_I
\end{align*}
where the perturbed part is given by
\begin{align*}
  \hat{H}_I = - \frac{e}{2 mc} (\hat{\bm{p}} \cdot \bm{A}(\hat{\bm{r}}) 
  + \bm{A}(\hat{\bm{r}}) \hat{\bm{p}}) + \mathcal{O}(\bm{A})
\end{align*}
The current is defined as
\begin{align*}
  \hat{\bm{j}}(\bm{r})
  :=
  \frac{1}{2m}
  \left(
    \hat{\bm{p}}
    \delta\left(
      \bm{r} - \hat{\bm{r}}
    \right)
    + \delta(\bm{r} - \hat{\bm{r}})
    \hat{\bm{p}}
  \right)
\end{align*}
which is an operator and takes a wave function and looks if there is something there at $\bm{r}$ and applies the momentum operator.
We divide by two because we do this twice, once in each ordering of $\hat{\bm{p}}$ and $\delta(\bm{r} - \hat{\bm{r}})$.
Since in the integration, we exchange operators with the corresponding vectors, we can switch the ordering of $\hat{\bm{p}}$ with $\bm{A}$ and just write $\hat{\bm{j}} \cdot \bm{A}$, as $\bm{A}$ only acts on $\bm{r}$.
With this, we can write the perturbative Hamiltonian as
\begin{align*}
  \hat{H}_I
  =
  - \frac{e}{c}
  \int d \vec{r}
  \hat{\bm{j}}(\bm{r}) \cdot \bm{A}(\bm{r})
\end{align*}
which makes sense because we are integrating over a delta function, this transforms the vector $\bm{r}$ into the corresponding operator $\hat{\bm{r}}$.

We found that $\bm{A}(\bm{r})$ was a linear combination of plane waves:
\begin{align*}
  \bm{A}(\bm{r})
  =
  \frac{1}{\sqrt{L^{3}}}
  \sum_{\bm{k},\lambda}
  a(\bm{k},l) \bm{e}(\bm{k},\lambda) e^{i(\bm{k} \cdot \bm{r} - \omega_{\bm{k}} t)}
  + \text{compl.\ conj.}
\end{align*}
where $\bm{k} \in \frac{2 \pi}{L} \Z^{3}$ and $\bm{e} \in \C^{3}$ is the polarisation of the light.

So the Hamiltonian becomes
\begin{align*}
  \hat{H}_I
  =
  \sum_{\bm{k}, \lambda}
  \left(
    - \frac{e}{c \sqrt{L^{3}}}
  \right)
  a(\bm{k},\lambda)
  \hat{\bm{j}}(-\bm{k})
  \bm{e}(\bm{k},\lambda) e^{-i \omega_{\bm{k}} t}
  + \text{compl.\ conj.}
\end{align*}
where $\bm{j}(\bm{k})$ is the fourier transform of $\bm{j}(\bm{k})$.
\begin{align*}
  \hat{\bm{j}}(\bm{k})
  =
  \int d \bm{r} e^{-i \bm{k} \cdot \bm{r}} \hat{\bm{j}}(\bm{r})
  = \frac{1}{2m} \left(
    \hat{\bm{p}} e^{i \bm{k} \hat{\bm{r}}} + e^{i \bm{k} \hat{\bm{r}}} \hat{\bm{p}}
  \right)
\end{align*}

Writing $H_I$ in the form
\begin{align*}
  H_I = \Theta(t - t_0) \hat{V} e^{-i \omega t} \quad \text{for} \quad \hat{V}_{\bm{k},\lambda} = \left(
    - \frac{e}{c \sqrt{L^{3}}} a(\bm{k},\lambda) \hat{\bm{j}} \bm{e}(\bm{k},\lambda)
  \right)
\end{align*}

Fermi's golden rule
\begin{align*}
  \Gamma = \frac{2 \pi}{\hbar} \abs{\braket{n|\hat{V}|0}}^{2} \delta(E_n - E_0 - \omega)
\end{align*}
then gives us that the transition rate is given by
\begin{align*}
  \Gamma_{0 \to  n;\bm{k},\lambda}
  = 
  \frac{2 \pi}{\hbar}
  \delta(E_n - E_0 - \hbar \omega_{\bm{k}})
  \abs{\braket{n|\hat{V}_{\bm{k},\lambda}|0}}^{2}
\end{align*}
Now we assume that the individual plane waves act independently on the electron.
Under this assumption, we can sum over all waves:
\begin{align*}
  \Gamma_{0 \to n}
  =
  \sum_{\bm{k},\lambda}
  \Gamma_{0 \to n; \bm{k},\lambda}
\end{align*}

In the limit $L \to \infty$, the possibles values for $\bm{k}$ (which are $\frac{2 \pi}{L} \Z^{3}$) form a dense subset of $\R^{3}$, so we can replace the sum by an integral
\begin{align*}
  \Gamma_{0 \to n}
  &=
  \sum_{\lambda}
  \frac{L^{3}}{{(2 \pi)}^{3}}
  \int d \bm{k} 
  \Gamma_{0 \to n; \bm{k},\lambda}
  \\
  &\stackrel{\text{spheric. coord.}}{=}
  \sum_{\lambda} \frac{L}{{(2 \pi)}^{3}}
  \int dk \int d \Omega k^{3} \Gamma_{0 \to n; k, \Omega,\lambda}
  \\
  &=
  \sum_{\lambda} \frac{L}{{(2 \pi c)}^{3}}
  \int \frac{d (\hbar \omega)}{\hbar} \omega^{2}
  \int d \Omega_{\bm{k}}
  \Gamma_{0 \to n; \omega, \Omega,\lambda}
\end{align*}
By fixing an angle and not carrying out the integration, we can find the infinitesimal contribution for the transition amplitudes in terms of $\bm{k}$.
THe integration over the delta function picks out the frequency $\omega_{n,0} = \frac{E_n - E_0}{\hbar}$.
So we get
\begin{align*}
  d \Gamma_{0 \to n}(\bm{k})
  &= \frac{2 \pi e^{2}}{\hbar^{2} c^{2}}
  \frac{\omega_{n,0}^{2}}{{(2 \pi c)}^{3}}
  \sum_{\lambda}
  \abs{a(\bm{k},\lambda)}^{2}
  \abs{\braket{n|\hat{\bm{j}}(- \bm{k}) \bm{e}(\bm{k},\lambda)}|0}^{2}
  d \Omega_k
\end{align*}

Since we always have the complex conjugate, and the complex conjugate and its phase different is irrelevant in the absolute value, we find that
\begin{align*}
  \Gamma_{0 \to n; \bm{k},\lambda} = \Gamma_{n \to 0; \bm{k},\lambda}
\end{align*}
but this result is in general not true because here we treated the electromagnetic wave classically.
In the quantum treatment, the electromagnetic field has vacuum fluctuations, which cause \emph{spontaneous decay}.


By expanding the exponential in the current, we can make the approximation that $\exp(x) = 1$, so
\begin{align*}
  \hat{\bm{j}}(-\bm{k})
  &= \frac{1}{2}
  \left(
    \frac{\hat{p}}{m} e^{+ i \bm{k}\cdot \hat{\bm{r}}}
    + e^{+i \bm{k} \cdot \hat{\bm{r}}}
    \frac{\hat{p}}{m}
  \right)
  \\
  &\simeq \frac{\hat{p}}{m}
  + \mathcal{O}(k)
\end{align*}
This is called the \textbf{dipole approximation} and can be justified by saying that for a single atom, the wavelenght is usually much larger, so the wave looks flat.
Using the trick
\begin{align*}
  [\hat{\bm{r}}, \hat{H}_0] =
  \frac{1}{2m} [\hat{\bm{r}}, \hat{\bm{p}}^{2}]
  = \underbrace{[\hat{\bm{r}}, \hat{\bm{p}} \hat{\bm{p}}]}_{i \hbar} \hat{\bm{p}}
  + \hat{\bm{p}} \underbrace{[\hat{\bm{r}}, \hat{\bm{p}}]}_{i \hbar}
  = \frac{i \hbar}{m} \hat{\bm{p}}
\end{align*}
so we can simplify the complicated matrix element in the expression for $\Gamma_{0 \to n; \bm{k},\lambda}$ as follows
\begin{align*}
  \frac{1}{m}
  \braket{n|\hat{\bm{p}} \cdot \bm{e}(\bm{k},\lambda)|0}
  &=
  \frac{1}{i \hbar}
  \braket{n|[\hat{\bm{r}},\hat{H}] \cdot \bm{e}(\bm{k},\lambda)|0}
  \\
  &=
  \underbrace{\frac{1}{i \hbar} (E_0 - E_n)}_{i \omega_{n,0}}
  \braket{n|\hat{\bm{r}} \cdot \bm{e}(\bm{k},\lambda)|0}
\end{align*}

\begin{center}
``If we have a vector operator, we can treat it as a vector and as an operator. Of course, because it is both.''
- R. Renner 07.04.2022
\end{center}
What we expect is that both its properties should be independent of each other.
\begin{center}
``If we have a vector and rotate it, we have a rotated vector''
- R. Renner 07.04.2022
\end{center}
So let $\hat{\bm{V}}$ be a vector operator and $R \in \SO(3)$. Then $\hat{\bm{V}}$ transforms as follows
\begin{align*}
  \hat{\bm{V}} \stackrel{R}{\mapsto} R \hat{\bm{V}} =: \hat{\bm{V}}' \quad \text{with} \quad 
  {(\hat{\bm{V}}')}_i = R_{ij} \hat{\bm{V}}_j
\end{align*}
on the other hand if we treat it as an operator, then for $U(R) \in \SU(2)$, we get
\begin{align*}
  \hat{\bm{V}} \mapsto \hat{\bm{V}}' = {U(R)}^{\ast} \hat{\bm{V}} U(R)
\end{align*}
so we expect that in either case, both should lead to the same result.


