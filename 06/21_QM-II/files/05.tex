\subsection{Commutator relations}
It is a postulate of quantum mechanics that the position and momentum relation satisfy $[\hat{q},\hat{p}] = i \hbar$.
Compare this with the classical the poisson bracket $\{q,p\} = 1$.
Although they seem related, the commutator relation does not follow from the poisson relation.

We saw that using the matrix element 
\begin{align*}
  \braket{q',t'|\hat{A}(t),q_i,t_i}
  \int_{(t_i,q_i) \to (q',t')} D_{\gamma} A(\gamma(t)) e^{\tfrac{i}{\hbar}S[\gamma]}
\end{align*}
we can compute the probability that a particle starts at $(q_i,t_i)$, we make a measurement using the observable $\hat{A}$ and that it ends up at $(q',t')$, where the probability is given by the square of its absolute value.

Without any observables, we expressed the matrix element of an intesimal change using the propagation kernel
\begin{align*}
  \braket{q',t+ \epsilon|q,t} = K(\epsilon,q',q)
\end{align*}
We expect that in the limit $\epsilon \to 0$ it holds that a change is only possible if and only if the end state $q'$ is identical to the initial state $q$
\begin{align*}
  \lim_{\epsilon \to 0} K(\epsilon,q',q) = \delta(q'-q)
\end{align*}

Recall from that in the single discretization step, one has
\begin{align*}
  \lim_{\epsilon \to 0} \braket{q',t + \epsilon|q,t} 
  &= \lim_{\epsilon \to 0} K(\epsilon,q',q)\\
  &= \lim_{\epsilon \to 0} \int_{(t,q) \to (t+ \epsilon,q')} D_{\gamma} e^{\tfrac{i}{\hbar}S[\gamma]}
  \\
  &= \lim_{\epsilon \to 0} C_{\epsilon} e^{\tfrac{i}{\hbar}L_{\epsilon}(q',q)}
\end{align*}
for $L_{\epsilon} = \frac{m}{2} {\left(
  \frac{q'-q}{\epsilon}
\right)}^{2} - V(q')$ and $C_{\epsilon} = \sqrt{\frac{m}{i \epsilon 2 \pi \hbar}}$.

However, this is not so nice to work with because it is not a function.
It would be nicer to work with smooth functions.

To do this, take the fourier transform of the function, which equals the right hand side for $V(q') = 0$.
\begin{align*}
  \phi_{\epsilon}(q') = C_{\epsilon} e^{\tfrac{i}{\hbar}L_{\epsilon}(q',q)}
\end{align*}
using the change of variables $\tilde{\theta} = q' -q$, we get
\begin{align*}
  \hat{\phi_{\epsilon}}(p') = \int d \tilde{q} e^{- \tfrac{i}{\hbar}q'p'} \quad \text{and} \quad \phi_{\epsilon}(q') = \int d \tilde{q} e^{- \tfrac{i}{\hbar}q p'} e^{- \tfrac{i}{\hbar}\tilde{q}p'} C_{\epsilon} e^{- \tfrac{m}{\hbar 2 \epsilon i}\tilde{q}^{2}}
\end{align*}

Using Lemma~\ref{lem:integral} again for $a = \frac{m}{\hbar \epsilon i}$ and $b = \tfrac{i}{\hbar}p'$, we get
that this equals
\begin{align*}
  \hat{\phi_{\epsilon}(p')} = 
  e^{- \tfrac{i}{\hbar}qp'} \underbrace{C_{\epsilon}\sqrt{\frac{2 \pi \hbar \epsilon i}{m}}}_{=1} e^{- {p'}^{2} \frac{\hbar \epsilon i}{2 \hbar m}}
  = \hat{\delta}(q'-q)
\end{align*}
so we find $\lim_{\epsilon \to 0}\phi_{\epsilon} = \delta(q'-q)$.

Now we give an actual definition of the momentum operator
\begin{empheq}[box=\bluebase]{align*}
  p_{\gamma}(t) := \lim_{\epsilon \to 0} m \frac{\gamma(t + \epsilon) - \gamma(t)}{\epsilon}
\end{empheq}
which is identical to the classical definition of momentum.

Now, without using the Heisenberg picture but just using the path integral formalism, we would like to show that
\begin{align*}
  \braket{q_f,t_f|[\hat{q}(t),\hat{p}(t)]|q_i,t_i} 
  &= i \hbar \braket{q_f,t_f|q_i,t_i}
\end{align*}
which would prove the commutator relation
Using the path integral formula, the left hand side becomes
\begin{align*}
  &m \int D_{\gamma}
  \left[
    \gamma(t + \epsilon) \frac{\gamma(t + \epsilon) - \gamma(t)}{\epsilon} - \gamma(t) \frac{\gamma(t + \epsilon) - \gamma(t)}{\epsilon}
  \right]
  e^{\tfrac{i}{\hbar}S[\gamma]}\\
  =
  &\frac{m}{\epsilon} \int D_{\gamma} {\left(
    \gamma(t + \epsilon) - \gamma(t) 
  \right)}^{2}
  e^{\tfrac{i}{\hbar}S[\gamma]}\\
  = &\frac{m}{\epsilon}(q'-q)^{2} e^{\tfrac{i}{\hbar}\epsilon L_{\epsilon}}\\
  \stackrel{\epsilon \to 0}{=}
    &(q'-q)^{2} e^{\tfrac{i}{\hbar} \tfrac{m}{2 \epsilon}(q'-q)^{2}}
\end{align*}

