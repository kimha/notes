In our last step to solve the approximate Lippmann-Schwinger equation, we assume that the potential is spherically symmetric, so $V(\bm{x}) = V(r)$.
Fix $\bm{k}$, so drop the subscript $\psi := \psi_{\bm{k}}$.

To trick is to use ONB of spherical harmonics and expand $\psi$ in terms of this basis:
\begin{align*}
  \psi(\bm{x})
  =
  \sum_{l,m} c_{lm} \frac{u_l(r)}{r} Y_{l,m}(\theta,q)
\end{align*}
If we chose $\theta = 0$, this corresponds to the direction $\bm{k}$.
We know from MMP-I, that
\begin{align*}
  Y_{l,0}(\theta,\phi) = \sqrt{\frac{2l + 1}{4 \pi}} P_l(\cos \theta)
\end{align*}
, where $P_l(z)$ is the Legendre polynomial and which satisfy the orthogonality relation
\begin{align*}
  \int dz P_l^{\ast}(z) P_{l'}(z) = \frac{2 \delta_{l,l'}}{2l + 1}
\end{align*}
The plane wave can be decomposed as
\begin{align*}
  e^{i \bm{k} \cdot \bm{x}}
  =
  \sum_{l=0}^{\infty}
  i^{l} (2l + 1) j_l(kr) P_l(\cos \theta)
\end{align*}
, where $j_l$ is the spherical Bessel function, which for $r \to \infty$, can be described as
\begin{align*}
  j_l(kr) \simeq \frac{1}{kr} \sin(kr - \frac{\pi}{2}l)
\end{align*}

As we have an incoming beam, this breaks spherical symmetry, but we still have rotational symmetry with respect ot the incoming beam axis $\bm{k}$, so there should not be any dependence on $\phi$.
Therefore, we assume that in the expansion of $\psi(\bm{x})$ in spherical harmonics, all $m\neq 0$ terms are zero and put this Ansatz into the Schrödinger equation.

From MMP-I and Electrodynamics, we know that $u_l$ satisfies
\begin{align*}
  - \frac{\hbar^{2}}{2m} \frac{d }{d r^{2}} u_l(r) + V_{\text{eff}}(r) u_l(r) = E_{\bm{k}}u_l(r)
\end{align*}
where $V_{\text{eff}}$ is the effective potential
\begin{align*}
  V_{\text{eff}} = V(r) + \frac{\hbar^{2} l(l+1)}{2 m r^{2}}
\end{align*}
For $r \to \infty$, the general solution for $u_l(r)$ is
\begin{align*}
  u_l(r)
  \simeq A \sin(kr - \frac{\pi}{2}l  + \delta_l(k))
\end{align*}
And ror $r \to 0$, we have the approximation
\begin{align*}
  u_l(r) = r^{l+1} \quad \text{or} \quad u_l(r) = r^{-l}
\end{align*}
Puttin everything together, we obtain
\begin{align*}
  \psi_{\bm{k}}(\bm{x}) 
  &\simeq
  e^{i \bm{k} \cdot \bm{x}}
  \\
  \sum_{l=0}^{\infty} c_l \frac{1}{kr} \sin(kr - \tfrac{\pi}{2} + \delta_l(k)) P_l(\cos \theta)
  \simeq& 
  \sum_{l=0}^{\infty} i^{l}(2l+1) \frac{1}{kr} \sin(kr - \tfrac{\pi}{2}l) P_l(\cos \theta)
  + \frac{e^{ikr}}{r}  f(\bm{k},\bm{k'})
\end{align*}
so we can find an expression for $f$ by comparing the coefficients on both sides and requiring that they are equal.

After some lengthy expression (not done in the lecture), one finds that
\begin{empheq}[box=\bluebase]{align*}
  f(\bm{k},\bm{k'}) 
  = \frac{1}{k} \sum_{l=0}^{\infty} a_l(k) (2l+1) P_l(\cos \theta)
  \quad \text{where} \quad a_l(k) = e^{i \delta_l(k)} \sin \delta_l(k)
\end{empheq}
What does this tell us?
Since the differential cross section is related to $f$, it gives us
\begin{align*}
  \frac{d \sigma}{d \Omega}
  = \abs{f(\bm{k},\bm{k'})}^{2}
  = \frac{1}{k^{2}}
  \abs{\sum_{l=0}^{\infty}a_l(2l + 1) P_l(\cos \theta)}^{2}
\end{align*}
So in order to get a real result, we only need to find the \textbf{scattering phase shifts} $\delta_l(k)$ by solving the Schrödinger equation for
\begin{align*}
  u_l(r) = A \sin(kr - \tfrac{\pi}{2}l + \delta_l(k))
\end{align*}
As a sanity check, let's check what happens if the potential is zero.
As $f$ is just the convolution of the potential, it must also be zero, so we see that all $\delta_l(k)$ must also be zero.


As another sanity check, we should check the total cross section
\begin{align*}
  \sigma(k) 
  &= \int \frac{d \sigma}{d \Omega} d \Omega
  = \int d \phi \int d \theta
  \sin \theta \abs{f(\bm{k},\theta)}^{2}
  \\
  &= 2 \pi \int dz \frac{1}{k^{2}} \abs{\sum_{l} a_l(2l+ 1)P_l(z)}^{2}
  \\
  &= \frac{2 \pi}{k^{2}}
  \sum_{l,l'}
  (2l+1)(2l'+1)
  a_{l'}^{\ast}
  a_l
  \underbrace{\int dz P_{l'}(z) P_l(z)}_{\frac{2 \delta_{l,l'}}{2 l+1}}\\
  &= \frac{4 \pi}{k^{2}}
  \sum_{l=0}^{\infty}
  (2l+1) \underbrace{a_l^{\ast} a_l}_{\sin^{2} \delta_{l(k)}}
\end{align*}
Now consider the imaginary part of $f(\bm{k},\bm{k'})$ for $\bm{k'} = \bm{k}$.
Then the angle $\theta$ is zero, so
\begin{align*}
  \image f(\bm{k},\theta = 0) 
  &= \frac{1}{k} \sum_{l=0}^{\infty}  \image(a_l)
  (2l + 1)
  \underbrace{P_l(\cos 0)}_{P_l(1) = 1}\\
  &= \frac{1}{k} \sum_{l=0}^{\infty}(2l+1) \sin^{2}(\delta_l(k))
\end{align*}
if we compare this with the total cross section, we obtain
\begin{empheq}[box=\bluebase]{align*}
  \sigma(k) = \frac{4 \pi}{k} \image f(\bm{k},\theta=0)
\end{empheq}
this result is called the \textbf{optical theorem}.
Which says that the total amount of stuff that gets scattered is determined by the amount of stuff that does \emph{not} get scattered.

Our next goal is to describe light-matter interaction with quantum mechanics.
It turns out that it suffices to give the quantum treatment to just the matter and treat light classically.
If we want to treat light quantum mechanically, we have to wait for the Quantum Field Theory Lecture next semester.

\section{Interaction between matter and Electromagnetic radiation}
We will use the Gaussian units throughout.
Recall the maxwell equations
\begin{align*}
  &(h1) \quad \nabla \cdot \bm{B} = 0\\
  &(h2) \quad \nabla \wedge \bm{E} + \frac{1}{c} \frac{\del }{\del t} \bm{B} = 0\\
  &(i1) \quad \nabla \cdot \bm{E} = 4 \pi \rho\\
  &(i2) \quad \nabla \wedge \bm{B} - \frac{1}{c} \frac{\del }{\del t}\bm{E} = \frac{4 \pi}{c} j
\end{align*}
The homogeneous equations have equivalent formulations
\begin{align*}
  &(h1) \iff
  \exists \bm{A}: \nabla \wedge \bm{A} = \bm{B}\\
  &(h2) \iff
  \nabla \wedge (\bm{E} + \frac{1}{c} \frac{\del }{\del t} \bm{A}) = 0\\
  &(h2) \iff \exists \varphi: \bm{E} + \frac{1}{c} \frac{\del }{\del t} \bm{A} = - \nabla \varphi
\end{align*} 
and the inhomogeneous equations also have equivalent formulations:
\begin{align*}
  &(i1) \iff \nabla( - \nabla \varphi - \frac{1}{c} \frac{\del }{\del t} \bm{A}) = 4 \pi \rho\\
  &(i2) \iff \underbrace{\nabla \wedge (\nabla \wedge \bm{A})}_{\nabla (\nabla \cdot \bm{A}) - \nabla^{2} \bm{A}}
  + \frac{1}{c} \frac{\del }{\del t}
  \left(
    \nabla \varphi + \frac{1}{c} \frac{\del }{\del t} \bm{A}
  \right)
  = \frac{4 \pi}{c} \bm{j}
\end{align*}

Since we have some degrees of freedom in chosing the potential $\bm{A}$, we can chose the \textbf{Coulomb gauge}, where $\nabla \cdot \bm{A} = 0$.
In this gauge, we have the following equivalences:
\begin{align*}
  &(i1) \iff - \nabla^{2} \varphi = 4 \pi \rho = 0 \implies \varphi(\bm{r},t) = \int d^{3} \bm{x} \frac{\rho(\bm{x},t)}{\abs{\bm{r} - \bm{x}}}
  \\
  &(i2) \iff \underbrace{\frac{1}{c^{2}} \frac{\del^{2}}{\del t^{2}} \bm{A} - \nabla^{2} \bm{A}}_{\square \bm{A}} = \frac{4 \pi}{c} \bm{j} - \frac{1}{c} \frac{\del }{\del t} \varphi = 0
\end{align*}
