The fact that both formalisms are equivalent is made clear by the fact that there is an invertible transformation between the Hamiltonian and Langrangian, known as the \textbf{Legendre transform}

\begin{empheq}[box=\bluebase]{align*}
  H(q,p) &= \min_{\dot{q}} p \dot{q} - L(q,\dot{q}) \quad \text{for} \quad p = \frac{\del L}{\del \dot{q}}\\
  L(q,\dot{q}) &= \sup_{p} \dot{q}p - H(q,p) \quad \text{for} \quad \dot{q} = \frac{\del H}{\del p}
\end{empheq}


Now, we are ready to compare the two formalisms and make them ready for use in quantum systems.

In the classical quantisation, the state-space at a time $t$ is given by an element $\ket{\psi(t)} \in \mathcal{H}$ in an hilbert space.
The dynamics of the system is governed by the Hamiltonian operator $\hat{H}: \mathcal{H} \to \mathcal{H}$, where the laws of motion are determined by the \textbf{Schrödinger equation}
\begin{align*}
  i \hbar \frac{d }{d t} \ket{\psi(t)} = \hat{H} \ket{\psi(t)}
\end{align*}

Of course, in the classical realm we do not have a discrepancy from the state space (consisting of position and momentum) and the observables. We just look at a particle and measure them.

In the quantum treatment however, observables are described as postive, self-adjoint operators $\hat{E}$ on $\mathcal{H}$.

The probability to observe an event $\hat{E}$ is given by the \textbf{Born rule}
\begin{align*}
  \IP[\hat{E}] = 
  \braket{\psi(t)|\hat{E}|\psi(t)}
\end{align*}
or for more general operators $\hat{A}$, we obtain the expected value
\begin{align*}
  \scal{\hat{A}}_{\psi(t)} = 
  \braket{\psi(t)|\hat{E}|\psi(t)}
\end{align*}



As a first example, we consider a particle on a one-dimension space, where the Hamiltonian is given by
\begin{align*}
  \hat{H} = \frac{1}{2m} \hat{p}^{2} + V(\hat{q})
\end{align*}
or, more generally we just make the assumption that the Hamiltonian does not contain mixed terms.
\begin{align*}
  \hat{H} = f(\hat{p}) + g(\hat{q})
\end{align*}

To solve this, we introduce the \textbf{propagator} with the defining property
\begin{align*}
  \ket{\psi(t')} = U(t',t) \ket{\psi(t)} 
\end{align*}

If the Hamiltonian $\hat{H}$ does not depend on $t$, we can shift the problem around in time, so we have
\begin{align*}
  U(t',t) = U(t'-t,0) =: U(t' - t)
\end{align*}
in this case, the Schrödinger equation simplifies to the first order ODE problem
\begin{align*}
  i \hbar \frac{d }{d t}U(t) = \hat{H}U(t) \quad \text{with} \quad U(0) = \id
\end{align*}
which has the solution $U(t) = e^{-\tfrac{i}{\hbar}H t}$.

Now, if we have a basis ${\{\ket{q}\}}_q$ of $\mathcal{H}$ with the property
\begin{align*}
  \braket{q'|q} = \delta(q-q') \quad \text{for} \quad \delta \text{\ the delta distribution}
\end{align*}
we define the \textbf{propagator kernel}\footnote{Like the Dirichlet or Fejér kernel from MMP-I} of time evolution by
\begin{empheq}[box=\bluebase]{align*}
  K(t',q',t,q) = \braket{q'|U(t',t)|q}
\end{empheq}
Using $\int dq' \ket{q'} \bra{q'} = \id$, we can write
\begin{align*}
  K(t'',q'',t,q) 
  &= \braket{q''|U(t'',t) \id U(t',t)|q}\\
  &= \int dq' \braket{q''|U(t'',t')|q} \braket{q'|U(t',t)|q}\\
  &= \int dq' K(t'',q'',t',q')K(t',q',t,q)
\end{align*}
The interpretation of $K(t',q',t,q)$ is that its square of the absolute value it is the probability that a wave in state $\ket{q(t)}$ will end up at the state $\ket{q'(t')}$.
What the equation
\begin{align*}
  K(t'',q'',t,q) = \int dq' K(t'',q'',t',q')K(t',q',t,q)
\end{align*}
tell us is that in order to compute the probaility that we go from state $(q,t)$ to $(q'',t'')$ is the ``sum'', or rather an integral of probabilities all paths that have an intermediary state $(q',t')$.


In the simplest example, we consider the two-slit experiment.
A particle can go from position $q_0$ to either the hole $q_{1,1}$ or the hole $q_{1,2}$ and then reach $q_{2}$.
We would find that the probabilty to reach the position $q_{2}$ is
\begin{align*}
  K(t_2,q_2,t_0,q_0) = \sum_{i=1}^{2} K(t_2,q_2,t_{1,i},q_{1,i})K(t_{1,i},q_{1,i},t_0,q_0)
\end{align*}
which generalizes to more holes and more layers.
If we have $N$ layers with holes $q_i$, we get
\begin{align*}
  K(t_N,q_N,t_0,q_0) = \sum_{q_{N-1}}\ldots\sum_{q_1} 
  K(t_N,q_N,t_{N-1},q_{N-1})
  \cdots K(t_2,q_2,t_{1},q_{1})K(t_{1,i},q_{1,i},t_0,q_0)
\end{align*}

For $\hat{H}$ time-independent, we have
\begin{align*}
  K(t',q',t,q) = K(t-t,q',0,q) =: K(t'-t,q',q)
\end{align*}

and in the case of infinitely many slits with one layer, we recover our equation
\begin{align*}
  K(t''-t,q'',q) = \int dq' K(t''-t',q'',q') \cdot K(t'-t,q',q)
\end{align*}

And if we have $N$ layers, for fixed $T$, set $\epsilon = \frac{T}{N}$, we get
\begin{align} \label{eq:k-factor-integral}
  K(T,q_N,q_0) = \int dq_1 \cdots \int d q_{N-1} K(\epsilon,q_{N},q_{N-1}) \cdots K(\epsilon,q_1,q_0)
\end{align}

Now, what if we have infinitely many layers?
Then the ways to go from point $q_0$ to $q_{\infty}$, is a continuous path $\gamma: [0,1] \to \R^{n}$.
The formula would get something where integrate over infinitely many $q_i$. For this we define
\begin{align*}
  \lim_{N \to \infty} \int d q_1 \cdots \int d q_{N-1} =: \int D_{\gamma}
\end{align*}
which represents an integral over all possible paths from $q_0$ to the final state $q_f$.
Now, this looks much more like the Lagrangian formalism of classical mechanics, as we somehow are considering all possible paths from an initial state to a final one.

But why do we only treat time-independent cases? 
It turns out that time-dependent systems, although more general are just ``special cases'' of time-independent ones.
The reason is that the fundamental laws of physics do not care about what time it is.
If we have a machine that generates a time-dependent electric field, we can describe the physics of the machine using the fundamental time-independent laws and thus see that we are still in the time-independent case.

\begin{center}
``A clock is a time-independent object''
- R. Renner 24.02.2022
\end{center}


Recall the Baker-Campbell-Hausdorff formula
\begin{align*}
  e^{\epsilon A} e^{\epsilon B} 
  &= (1 + \epsilon A + \mathcal{O}(\epsilon^{2}))(1 + \epsilon B + \mathcal{O}(\epsilon^{2}))\\
  &= 1 + \epsilon(A + B) + \mathcal{O}(\epsilon^{2}) =
  e^{\epsilon(A + B)} + \mathcal{O}(\epsilon^{2})
\end{align*}
So we get
\begin{align*}
  K(\epsilon,q',q) 
  &= \braket{q'|e^{- \tfrac{i}{\hbar}\hat{H}\epsilon}|q}\\
  &= \braket{q'|e^{-\tfrac{i}{\hbar}f(\hat{p}) \epsilon - \tfrac{i}{\hbar} g(\hat{q}) \epsilon}|q}\\
  &= \braket{q'|e^{-\tfrac{i}{\hbar}f(\hat{p}) \epsilon} e^{\tfrac{i}{\hbar} g(\hat{q}) \epsilon}|q} + \mathcal{O}(\epsilon^{2})
\end{align*}
Note that we have $N$ such terms, so because $\epsilon = \tfrac{T}{N}$, our error will be of order
\begin{align*}
  N \cdot \mathcal{O}(\epsilon^{2}) = \mathcal{O}(\tfrac{1}{N})
\end{align*}
so if $N \to \infty$, we can ignore the error.

Since $\ket{p}$ can be obtained from taking the fourier transform of $\ket{q}$
\begin{align*}
  \ket{p} := \int e^{\tfrac{i}{\hbar}p q} \ket{q} \implies \frac{1}{2 \pi \hbar}\int dp \ket{p} \bra{p} = \id
\end{align*}
so we insert this identity into our formula from before and get
\begin{align*}
  K(\epsilon,q',q)
  &= \frac{1}{2 \pi \hbar} \int dp \braket{q'|e^{- \tfrac{i}{\hbar} f(\hat{p})\epsilon}} \braket{p|e^{- \tfrac{i}{\hbar} g(\hat{q})\epsilon}}
\end{align*}
Since $\ket{p}, \ket{q}$ are eigenvectors of the operators $\hat{p},\hat{q}$,  we have
\begin{align*}
  F(\hat{p}) \ket{p} = F(p) \ket{p} \quad \text{and} \quad \bra{q} g(\hat{q}) = \bra{q} g(q)
\end{align*}
so we can replace the operators with their eigenvalues, which commute
\begin{align*}
  K(\epsilon,q',q)
  &= \frac{1}{2 \pi \hbar} \int dp \braket{q'|e^{- \tfrac{i}{\hbar} f(p)\epsilon}} \braket{p|e^{- \tfrac{i}{\hbar} g(q)\epsilon}}\\
  &= \frac{1}{2 \pi \hbar} \int dp e^{- \epsilon\tfrac{i}{\hbar}(f(p) + g(q))}
  \underbrace{\braket{q'|p}}_{e^{\tfrac{i}{\hbar}pq'}}
  \underbrace{\braket{p|q}}_{e^{- \tfrac{i}{\hbar}pq}}
\end{align*}

And in the case where $\hat{H} = \frac{1}{2m} \hat{p}^{2} + V(\hat{q})$, we get
\begin{align*}
  K(\epsilon,q',q)
  &= \frac{1}{2 \pi \hbar} \int dp e^{- \epsilon\tfrac{i}{\hbar}p^{2} \epsilon}
  e^{\tfrac{i}{\hbar}p(q'-q)}
  e^{- \tfrac{i}{\hbar}V(q') \epsilon}
\end{align*}
If we evaluate the integral, we will see that we recover the Lagrangian.
