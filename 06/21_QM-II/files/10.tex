\begin{center}
``We have to suffer through that. Sometimes physics is like that.''
- R. Renner 24.03.2022
\end{center}

Before we try to solve the scattering problem, we first state our assumptions.
The first one is that the system is described by the hamiltonian
\begin{align*}
  \hat{H} = \frac{1}{2m} \hat{p}^{2} + V(\hat{x})
\end{align*}
that looks like the Hamiltonian for just one particle, but we can work around it by considering the \emph{relative} position and momentum.
The second assumption is that the potential is \emph{short ranged}, so $\lim_{\abs{x} \to \infty} \abs{x} V(x) = 0$.
We also assume that the incident wave far away from the center is a planar wave of the form $e^{i \vec{k} \cdot \vec{x} - i \omega t}$.

For this problem, it is convenient to look at the Time-Independent Schrödinger Equation (TISE)
\begin{align*}
  \hat{H} \psi_{\vec{k}}(\vec{x}) = E_{\vec{k}} \psi_{\vec{k}}(\vec{x})
\end{align*}
which, by writing $\hat{H} = - \tfrac{\hbar^{2}}{2m} \nabla_x^{2} + \hat{V}$, is equivalent to
\begin{align*}
  \underbrace{\left(
    \frac{\hbar^{2}}{2m} \nabla_{\vec{x}}^{2} + E_{\vec{k}}\right)}_{=:L}
\underbrace{\psi_{\vec{k}}(\vec{x})}_{=: w}
  = \underbrace{V(\vec{x})\psi_{\vec{k}}(\vec{x})}_{=:u}
\end{align*}
From linear algebra, we know that a linear equation of the form $Lw = u$ has solutions of the form $w = Gu + v$, for $v \in \kernel L$ and $G$ such that $L \circ G = \id$.

Computing the kernel is easy. One has
\begin{align*}
  v = \phi_{\vec{k}} = e^{i \vec{k} \cdot \vec{x}}
  \quad \text{with} \quad E_{\vec{k}} = \frac{k^{2} \hbar^{2}}{2m}  
\end{align*}
the Green's function has to satisfy the equation
\begin{align}\label{eq:greens}
  \left(
    \frac{\hbar^{2}}{2m} \nabla_{\vec{x}}^{2} + E_{\vec{k}}\right)
    G_{\vec{k}}(\vec{x},\vec{y})
    = \delta(\vec{x} - \vec{y})
\end{align}
Since the left hand operator is invariant under shifts of $\vec{x}$ and $\vec{y}$, we can additionally assume that $G(\vec{x},\vec{y})$ is the form $G(\vec{x} - \vec{y})$.

Once we have found the green's function, the solution is then
\begin{empheq}[box=\bluebase]{align*}
  \psi_{\vec{k}}(\vec{x}) =
  \phi_{\vec{k}}(\vec{x}) +
  \int G_{\vec{k}}(\vec{x} - \vec{y})
  V(\vec{y}) \psi_k(\vec{y}) dy
\end{empheq}
which is called the \textbf{Lippmann-Schwinger} equation.

When looking at the equation, we can interpret it as follows:
When scattering, we integrate over all possibel places $\vec{y}$, where we can scatter and the Green's function tells us the contribution to the scattering at that position.

In order to find the Green's function, we compute its fourier transform $\tilde{G}_{\vec{k}}(\vec{q})$, so they are related by
\begin{align*}
  G_{\vec{k}}(\vec{x}) = \frac{1}{{(2 \pi)}^{3}}
  \int d \vec{q} \tilde{G}_{\vec{k}}(\vec{q}) e^{i \vec{q} \vec{x}}
\end{align*}
Setting $\vec{y} = 0$ in Equation~\ref{eq:greens}, we get
\begin{align*}
  \frac{1}{{(2 \pi)}^{3}}
  \int d \vec{q}
  \left(
    - \frac{\hbar^{2}}{2m}q^{2}
    + E_{\vec{k}}
  \right)
  \tilde{G}_{\vec{k}}(\vec{q})
  e^{i \vec{q}\cdot \vec{x}}
  = \delta(\vec{x})
\end{align*}
but the fourier transform of the delta function is the identity, so we obtain the following equation for $\tilde{G}_{\vec{k}}$.
\begin{align*}
  \tilde{G}_{\vec{k}}(\vec{q}) = \frac{1}{E_{\vec{k}} - \frac{hb^{2}q^{2}}{2m}}
\end{align*}
so applying the reverse transformation to $\tilde{G}_{\vec{k}}(\vec{q})$, and inserting the equation for $E_{k} = \frac{k^{2}\hbar^{2}}{2m}$ we found before, we obtain the Greens function
\begin{align*}
  G_{\vec{k}}(\vec{x})
  =
  \frac{2m}{{(2 \pi)}^{3} \hbar^{2}}
  \int \frac{e^{i \vec{q}\cdot \vec{x}}}{k^{2} - q^{2}}
\end{align*}
It may appear we are stuck, but instead of integrating $d \vec{q}$ over $\R^{3}$, we integrate over spherical coordinates.
Using a change of variables $z = \cos(\theta), \frac{d z}{d \theta} = - \sin \theta$, and writing $r = \abs{\vec{x}}$, it follows
\begin{align*}
  G_{\vec{k}}(\vec{x})
  &=
  \frac{2m}{{(2 \pi)}^{3} \hbar^{2}}
  \int_0^{\infty}dq q^{2}
  \int_0^{\pi} d \theta \int_0^{2 \pi} d \phi
  \frac{e^{i qr \cos \theta}
  }{k^{2} - q^{2}}
  \sin \theta\\
  &=
  \frac{2m 2 \pi}{{(2 \pi)}^{3} hb^{2}}
  \int_0^{\infty}
  dq q^{2}
  \int_{-1}^{1} 
  dz
  \frac{e^{iqrz}}{k^{2} - q^{2}}
  \\
  &=
  \frac{m}{2 \pi^{2} \hbar^{2} i r}
  \int_{- \infty}^{\infty}dq q \frac{e^{iqr}}{k^{2} - q^{2}}
\end{align*}  
We first note that the integrand has singularities at $\pm k$. To compute this integral, we use the residue theorem and instead integrate over the following parts:
\begin{align*}
  &(-R, -k - \epsilon) \sqcup 
  \left\{
    -k + \epsilon e^{i\pi t} | t \in [0,1]
  \right\}
  \sqcup
  (-k + \epsilon, k- \epsilon)
  \sqcup
  \\
  &\left\{
    k + \epsilon e^{i(\pi t + \pi)} | t \in [0,1]
  \right\}
  \sqcup
  (k+\epsilon,R)
  \sqcup
  \left\{
    Re^{i \pi t} \big\vert t \in [0,1]
  \right\}
\end{align*}
So a semi-circle in the upper complex plane, and half circles that exclude the singularity $-k$ and one that includes the singularity at $+k$.

The residue at $+k$ of the integration is $- \frac{e^{ikr}k}{2k}$, so by the reside theorem, the integral is $2 \pi i$ times the residue.

So, we finally get to write the Green's function:
\begin{align*}
  G_{\vec{k}}(\vec{x})
  =
  \frac{m}{2 \pi^{2} \hbar^{3} ir}
  \left(
    - \frac{2 \pi i e^{i k r}k}{2k}
  \right)
  = - \frac{m}{2 \pi \hbar^{2}} \frac{e^{ikr}}{r}
\end{align*}


%In fact, when we approximate the delta function with very tight gaussians of width $\delta$, a slightly more complicated, but similar computation can be done.
%There, the final integral becomes
%\begin{align*}
  %\int_{- \infty}^{\infty}
  %d q
  %\frac{e^{i qr}}{k^{2}- (q + i \delta)^{2}}
%\end{align*}
%which has singularites at $\pm k - i \delta$.

Now we insert this into the Lippmann-Schwinger equation and get
\begin{align*}
  \psi_{\vec{k}}(\vec{x})
  =
  e^{i \vec{k} \cdot \vec{x}}
  - \frac{m}{2 \pi \hbar^{2}}
  \int d \vec{y} \frac{e^{ik \abs{\vec{x} - \vec{y}}}}{\abs{x - y}}
  V(\vec{y})  \psi_{\vec{k}}(\vec{y})
\end{align*}
We now assume that the potential is local, so there is an $R > 0$ such that $V(\vec{y}) \simeq 0$ for $\abs{y} > R$.
Since the detectors are far away from the potential, we may also assume $r =\abs{x} \gg R$, this gives us the approximation
\begin{align*}
  k \abs{\vec{x} - \vec{y}}
  &= kr
  \sqrt{1 + \frac{y^{2}}{r^{2}} - \frac{2}{r}\vec{x}\cdot\vec{y}}
  \\
  &\simeq
  kr - \frac{k}{r} \vec{x} \cdot \vec{y}
\end{align*}
which is called the \textbf{Born approximation}.

Setting $\vec{k}' = \frac{k}{r} \vec{x}$, we get the \textbf{approximate Lippmann Schwinger equation}
\begin{empheq}[box=\bluebase]{align*}
  \psi_{\vec{k}}(\vec{x}) \simeq e^{i \vec{k} \cdot \vec{x}}
  + \frac{e^{ikr}}{r}
  f(\vec{k},\vec{k}')
  \quad \text{for} \quad 
  f(\vec{k},\vec{k}')
  =
  - \frac{m}{2 \pi \hbar^{2}}
  \int d \vec{y} e^{-i \vec{k}' \vec{y}}
  V(\vec{y}) \psi_{\vec{k}}(\vec{y})
\end{empheq}
which is good for $r \gg R$, where $R$ is the range of the scattering potential.


