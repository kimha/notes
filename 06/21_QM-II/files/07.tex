With the current development of physical theories, we do not have a full quantum description of gravity.
For example, we do not know the hilbert space of a black hole.
What we can do however is is calculate path integrals, so we see that this formalism is really important, even though some simpler calculations are more tedious than in the Hamiltonian formalism.


\subsection{Euclidean path integrals}

We want to have the means to calculate expectation values of observables.
Usually, we calculate matrix elements like $\braket{q'|\hat{A}|q}$, using the position eigenstates.

However, having position eigenstates is not so natural as they do not have well defined energies.
More often than not, we know more about the energy eigenstates of a system. 
For example, in the harmonic oscillator, we know alot about the ground state, so we want to work with energy eignestates instead to compute matrix elements for observables.

The picture to have in mind is that we have some machine that takes in a state $\ket{\phi}$ and outputs the state after some evolving in an eigenstate $\ket{q}$.

We know from thermodynamics that if the maschine is closed, (i.e.\ there is no exchange of energy with the surrounding environment), then the time evolution is simply $U = e^{- \tfrac{i}{\hbar} \hat{H}}$ (and is unitary).

Now assume that the input state $\ket{\phi}$ is an unknown state from a set of $N$ possible mutually orthogonal state.

Since $U$ is orthogonal, then it is not possible to obtain the eigenstate $\ket{q}$, or else it would not be reversible.

Another reason this would fail is that if the input state is unkonwn and the final state is precisely known, the entropy would decrease.


What we need to do is to put the energy somewhere else.
We do this by connecting the machine to a thermal reservoir and putting in work to move the entropy in the form of heat into the reservoir wit

The work $W$ needed to move entropy $S$ into a thermal reservor with temperature $T$ is
\begin{align*}
  W = T k_B \frac{\log N}{S}
\end{align*}

Now if we want to treat the entire system, we need to prepare the input state of the thermal reservoir and use the path integral formalism to figure out how the system evolves.

Let $\hat{H}_R = \hat{H}$ be the Hamilton operator of a system (the $R$ is for reservoir).
In order to describe the state of the reservoir, let
\begin{align*}
  \hat{H} = \sum_{k}E_k \ket{E_k} \bra{E_k}
\end{align*}
be a spectral decomposition of the Hamiltonian in terms of the (possibly degenerate) energy Eigenstates.


At temperature $T$, each state $\ket{E_k}$ occurs wit propability
\begin{align*}
  p_k(\beta) = \frac{1}{Z(\beta)} e^{- \beta E_k} \quad \text{where} \quad \beta = \frac{1}{k_B T}, \quad \text{and} \quad Z(\beta) = \sum_{k} e^{- \beta E_k}
\end{align*}
$\beta$ is often called the \emph{inverse temperature}, with $[\beta] = \text{Energy}^{-1}$ and $Z(\beta)$ is the \textbf{partition function} and is often simply written $Z$ (without the argument).

Because $\hat{H} \ket{E_k} = E_k \hat{E_k}$, the expectation value of an observable $\hat{A}$ is given by
\begin{align*}
  \scal{\hat{A}}_{\beta} 
  &= \sum_{k} p_k \braket{E_k|\hat{A}|E_k}
  \\
  &= \frac{1}{Z} \sum_{k} e^{- \beta E_k} \braket{E_k|\hat{A}|E_k}
  \\
  &= \frac{1}{Z} \sum_{k} \braket{E_k|e^{- \tfrac{\beta}{2} \hat{H}} \hat{A} e^{- \tfrac{\beta}{2}\hat{H}}|E_k}
\end{align*}
Recall that for $X$ an operator on a Hilbert space $\mathcal{H}$ with ONB $\{\ket{e_j}\}$, the trace of $X$ is
\begin{align*}
 \trace(X) := \sum_{j} \braket{e_j|X|e_j} 
\end{align*}
which is well-defined and thus doesn't depend on the choice of the basis.

So, we see that the expectation value of $\hat{A}$ can be expressed as the trace, then switch to the position basis.
\begin{align*}
  \scal{\hat{A}}_{\beta} 
  &=
  \frac{1}{Z}\trace(e^{- \tfrac{\beta}{2} \hat{H}} \hat{A} e^{- \tfrac{\beta}{2} \hat{H}})
  \\
  &=
  \frac{1}{Z}
  \int dq \braket{q|e^{- \tfrac{\beta}{2} \hat{H}} \hat{A} e^{- \tfrac{\beta}{2} \hat{H}}|q}
\end{align*}
This looks like the expression for the matrix element in the schrödinger picture
\begin{align*}
  \braket{q_f,t_f|\hat{A}(t)|q_i,t_i}
  &=
  \braket{q_f|e^{- \tfrac{i}{\hbar} \hat{H}(t_f - t)} \hat{A} e^{- \tfrac{i}{\hbar}\hat{H}(t - t_i)}q_i}
  \\
  &=
  \int D_{\gamma} A(\gamma(t)) e^{\tfrac{i}{\hbar} S[\gamma]}
\end{align*}
If we check the derivation, we never used the fact that time is real, so we can replace $t$ with any value in the above equation.
\begin{center}
``Imagine that time is imaginary''
- R. Renner 14.03.2022
\end{center}

There is only a small critical step, where we integrated over a small timestep $\epsilon$, and used the gaussian integral formula (Lemma~\ref{lem:integral}) where we need to be careful, as the parameter may only allow positive real values.
To circumvent this, we 
use the transformation $t \mapsto  - i \tau$, we get $dt = - i d \tau$, so our epsilon will become $\epsilon \mapsto  - i d \tau$.
And then the parameters will have the required properties and we can use the integral formula again.

So with the replacements
\begin{align*}
  t \mapsto  - i \tau, \quad (t_f - t) \mapsto  -i \tfrac{\beta}{2} \hbar, \quad (t - t_i) \mapsto  -i \tfrac{\beta}{2}  \hbar
\end{align*}
we get
\begin{align*}
  \scal{\hat{A}}_{\beta}
  &=
  \frac{1}{Z} \sum_{k} dq \int D_{\gamma} A(\gamma(t)) e^{\tfrac{i}{\hbar} S[\gamma]}
\end{align*}
Whereas the path used to go along the real line from $t_i$ to $t_f$, we now have to go from $\frac{i \beta}{2 \hbar}$ to $- \frac{i \beta}{2 \hbar}$. 
This transformation is called the \textbf{Wick rotation}.

