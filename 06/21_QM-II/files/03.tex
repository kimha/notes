For this, we need a lemma 
\begin{lem}[]\label{lem:integral}
Let $a,b \in \C$, with $\text{Re}(a) = 0$ and $\image(a) \neq 0$ and $\text{Re}(b) = 0$.
\begin{align*}
  \int_{\R} dx e^{- \tfrac{a}{2}x^{2} + bx} = \sqrt{\frac{2 \pi}{a}} e^{\frac{b^{2}}{a}}
\end{align*}
\end{lem}
If $\text{Re}(a) > 0$, it means that the expression $e^{-\tfrac{a}{2}x^{2}}$ would look like the Gaussian function, so there is exponential decay as $\abs{x} \to \infty$.
This means that we can consider the particle as a local phenomenon. 
The reach of the particle is limited and we can cut the function of for large values of $\abs{x}$ and get a good result.

Since this is true for all $\text{Re}(a) > 0$, we can take the limit $\text{Re}(a) \to 0$ and still make the approximation.

For example, if we consider a point-particle, then its energy density would be infinite at that point.
This can be handeled by considering instead a particle, that is ever so slightly spread out in space and then take the limit to get a physical result.

Using this Lemma for $x = p, a = \frac{i \epsilon}{\hbar m}, b = \frac{1}{\hbar}(q'-q)$, we get
\begin{align}
  K(\epsilon,q',q) 
  &= 
    \frac{1}{2 \pi \hbar}
    \sqrt{\frac{2 \pi \hbar}{i \epsilon}} 
  e^{\frac{i m}{2 \epsilon \hbar}{(q' - q)}^{2}}
  e^{- \tfrac{i}{\hbar}V(q') \epsilon}\\
  &=
  \underbrace{\sqrt{\frac{m}{i \epsilon 2 \pi \hbar}}}_{=: C_{\epsilon}}
  e^{\epsilon\frac{i m}{2 \epsilon \hbar}{\tfrac{q' - q}{\epsilon}}^{2}}
  e^{- \tfrac{i}{\hbar}V(q) \epsilon}\\
  &= C_{\epsilon} e^{\epsilon \tfrac{i}{\hbar}L_{\epsilon}}, \quad \text{for} \quad L_{\epsilon} = \frac{m}{2}{\left(\frac{q'-q}{\epsilon}\right)}^{2} - V(q')
  \label{eq:c-epsilon}
\end{align}
Note that in the limit where the steps get infinitely small, we have
\begin{align}
  \label{eq:lagrangian-limit}
  \lim_{N \to \infty,\epsilon \to 0} L_{\epsilon} 
  = \frac{m}{2}\dot{q}^{2}(t) - V(q(t))
\end{align}
which is exactly the Lagrangian of a moving particle.


Recall the Legendre Transformation of the Lagrangian in terms of the Hamiltonian:
\begin{align*}
  L(q,\dot{q}) = \sup_{p} \dot{q} p - H(q,p) \quad \text{for} \quad \dot{q} = \frac{\del H}{\del p} \implies p = m \dot{q}
\end{align*}
applying this to the Hamiltonian $H(p,q) = \frac{1}{2m}p^{2} + V(q)$, we obtain the same result as before:
\begin{align*}
  L(q,\dot{q}) = m \dot{q}^{2} - \frac{{(m \dot{q})}^{2}}{2m} - V(q) = \frac{m}{2} \dot{q}^{2} - V(q)
\end{align*}
From equations~\ref{eq:lagrangian-limit},~\ref{eq:c-epsilon}, and~\ref{eq:k-factor-integral}, we obtain
\begin{align*}
  \int dq_1 \cdots dq_{N-1} K(\epsilon,q_N,q_{N-1}) \cdots K(\epsilon,q_1,q_0)
  &=
  C_{\epsilon}^{N} e^{\frac{i \epsilon}{\hbar}L_{\epsilon}(q_{N},q_{N-1})}
  \cdots
  e^{\frac{i \epsilon}{\hbar} L_{\epsilon}(q_1,q_0)}\\
  &= C_{\epsilon}^{N} e^{\frac{i}{\hbar}\sum_{k=0}^{N-1}\epsilon L_{\epsilon}(q_{k+1},q_k)}
\end{align*}
In the limit, the sum in the exponent becomes an integral
\begin{align*}
  \lim_{N \to \infty,\epsilon \to 0} \sum_{k=0}^{N-1} \epsilon L_{\epsilon}(q_{k+1},q_k)
  = \int_{t=0}^{T}dt L(q(t),\dot{q}(t)) =: S[q]
\end{align*}
which is known as the \textbf{classical action} we know from Lagrangian mechanics.
The $N$ integrals become an integral over all paths.

\vspace{2\baselineskip} 

To summarize, the \textbf{propagator kernel} is given by
\begin{empheq}[box=\bluebase]{align*}~\label{eq:propagator-kernel}
  K(t_{\text{f}}- t_{\text{i}},q_{\text{f}},q_{\text{i}}) := \braket{q_{\text{f}}|U(t_{\text{f}},t_{\text{i}})|q_{\text{i}}} = \int D_{\gamma} e^{\tfrac{i}{\hbar}S[\gamma]}
\end{empheq}
where the \textbf{action} $S[q]$, the \textbf{path integral}\footnote{Not to be confused with the \emph{line integral} $\int_{\gamma} f(z) dz$} $\int D_{\gamma}$ and the \textbf{propatagor} $U(t'-t')$ are defined as
\begin{empheq}[box=\bluebase]{align*}
  S[\gamma] 
  &= \int_{t_{\text{i}}}^{t_{\text{f}}} dt L(q(t),\dot{q}(t))\\
  \int D_{\gamma} 
  &= \lim_{N \to \infty} \int d q_1 \cdots \int d q_{N-1}\\
  U(t'-t) 
  &= e^{- \tfrac{i}{\hbar} (t'-t) H}
\end{empheq}
where $\gamma$ is any path that goes from $q_{\text{i}}$ to $q_{\text{f}}$ and the $q_k$ are the points that lie on $\gamma$ at time $t_k = k \cdot\tfrac{t_{\text{f}} - t_{\text{i}}}{N}$.
Now, we are integrating over the collection of all possible paths, which seems absurd.
But we will see that in practice (see Exercises), this integration can be done rather efficiently.

This formalism is invariant under Gallileo transformations (and in relativistic treatment Lorentz transformations).

\begin{exr}[]
In the classical case, the path chosen are the one where the action $S[\gamma]$ is extremal, i.e.\ its derivative is zero.

Show that for a smooth real function $f$ with critical point\footnote{points, where $f'(x_c) = 0$} $x_c$ weh have
\begin{align*}
  \frac{1}{\sqrt{2 \pi i \hbar}} \int dx e^{\tfrac{i}{\hbar}f(x)} = \frac{1}{\sqrt{f''(x_c)}} e^{\tfrac{i}{\hbar}f(x_c)} \left[
    1 + \mathcal{O}(\hbar)
  \right]
\end{align*}
and conclude that the semi-classical limit $\hbar \to 0$ agrees with the classical case.
\end{exr}
The intuition behind it is that as $\hbar \to 0$, the integrand oscillates faster and faster, until only the critical points remain relevant.
\begin{sol}

\end{sol}



