\subsection{Heisenberg formalism}

We can thus describe the behaviour of single particles using either the Hamiltonian formalism using the propagator, or using the path integral formalism using the action.

But we would like to make measurements.
So consider a particle that starts at position $q_{\text{i}}$ and end up at $q_{\text{f}}$ at times $t_{\text{i}}$ and $t_{\text{f}}$.
Now, we measure the velocity at time $t$.

What we do is that we introduce operators into the equations for the propagator kernel.

Recall that in the Heisenberg picture, we take out the time dependence of the states and put it into the operators.
So we define the state where the particle is at position $q$ at time $t$ as
\begin{align*}
  \ket{q,t} := U(0,t) \ket{q}
\end{align*}
where $\ket{q}$ is the state where the particle is exactly at position $q$ without any indication of time.
This makes sense because such delta states spread out ove time, so they only exist at specific points in time.

We can read $\ket{q,t}$ as being the state $\ket{q(t)}$ at time $t_0$, such that when evolving it forwards towards time $t$, it becomes the state $\ket{q,t}$, i.e.
\begin{align*}
  U(t,0) \ket{q,t} = \ket{q}
\end{align*}

We can do the same for operators. Set
\begin{align*}
  \hat{A}_H(t) = U(0,t) \hat{A} U(t,0)
\end{align*}
under the assumption that $\hat{A}$ is diagonalizable, i.e.\ of the form
\begin{align*}
  \hat{A} = \int dq A(q) \ket{q} \bra{q}
\end{align*}
we define
\begin{align*}
  \hat{A}_H(t) 
  &= \int dq A(q) U(0,t) \ket{q} \bra{q} U(t,0)\\
  &= \int dq A(q) \ket{q,t} \bra{q,t}
\end{align*}
Using the notation $U(t,0) =: U$ and $U(0,t) = U^{\ast}$, the Heisenberg operator of motion is
\begin{align*}
  i \hbar \frac{d }{d t} \hat{A}_H(t) 
  &=
  (i \hbar \frac{d }{d t} U^{\ast})
  \hat{A}U
  +
  U^{\ast}\hat{A}(i \hbar \frac{d}{d t} U)\\
  &= 
  {(- \hat{H}U)}^{\ast} \hat{A} U + U^{\ast}\hat{A} (\hat{H}U)\\
  &= - \underbrace{U^{\ast}\hat{H}U}_{\hat{H}_H(t)} \underbrace{U^{\ast}\hat{A} U}_{\hat{A}_H(t)} + \underbrace{U^{\ast}\hat{A} U}_{\hat{A}_H(t)} \underbrace{U^{\ast}\hat{H}U}_{\hat{H}_H(t)}\\
  &= [\hat{A}_H,\hat{H}_H]
\end{align*}
where we used the fact that $i \hbar \frac{d }{d t}U = \hat{H}U$, and $\hat{H}$ is hermitian.

If we assume that $\hat{H}$ is time independent, then $U$ is of the form $U = e^{- \tfrac{i}{\hbar}\hat{H}t}$ which means $[\hat{H},\hat{U}] = 0$, so $\hat{H}_H = \hat{H}$.
So our formula for the propagator kernel can be written as
\begin{align*}
  K(t_{\text{f}} - t_{\text{i}}, q_{\text{f}},q_{\text{i}})
  &= \braket{q'|U(t_{\text{f}},0) U(0,t_{\text{i}})|q}\\
  &= \braket{q',t_{\text{f}}|q,t_{\text{i}}}
\end{align*}

We define the \textbf{matrix element} in the Heisenberg picture as
\begin{align*}
  \braket{q',t_{\text{f}}|\hat{A}_H(t)|q,t_{\text{i}}}
\end{align*}
which corresponds to the probability that a particle starts at configuration $q$ at time $t_{\text{i}}$, we measure some observable $\hat{A}_H(t)$ at time $t$ and then goes to configuration $q'$ at time $t_{\text{f}}$.

In the Schrödinger picture, we can write it as
\begin{align*}
  \braket{q',t_{\text{f}}|\hat{A}_H(t)|q,t_{\text{i}}}
  &=
  \braket{q'|U(t_{\text{f}},0) U(0,t) \hat{A} U(t,0) U(0,t_{\text{i}})|q}\\
  &=
  \braket{q'|U(t_{\text{f}},t)\hat{A}U(t,t_{\text{i}})|q}
\end{align*}
which expresses the same story.

Again, under the assumption that $\hat{A}$ is diagonalizable, we can also write it as
\begin{align*}
  \braket{q',t_{\text{f}}|\hat{A}_H(t)|q,t_{\text{i}}}
  &=
  \int d \tilde{q} A(\tilde{q}) \underbrace{\braket{q'|U(t_{\text{i}},t)|\tilde{q}}}_{K(t_{\text{f}}-t,q',\tilde{q})}
  \underbrace{\braket{\tilde{q}|U(t,t_{\text{i}})|q}}_{K(t-t_{\text{i}},\tilde{q},q)}
  \\
  &= \int d \tilde{q} A(\tilde{q}) \int_{\gamma_1(t) = \tilde{q},\gamma_1(t_{\text{f}}) = q'} D_{\gamma_1} e^{\tfrac{i}{\hbar}S[\gamma_1]}
  \int_{\gamma_2(t) = \tilde{q}, \gamma_2(t_{\text{i}}) = q} D_{\gamma_2} e^{\tfrac{i}{\hbar}S[\gamma_2]}\\
  &=
  \int_{\gamma(t_{\text{i}}) = q, \gamma(t_{\text{f}}) = q'} D_{\gamma} A(\gamma(t)) e^{\tfrac{i}{\hbar}S[\gamma]}
\end{align*}
In the last step, we say that as we are integrating over all intermediary points $\tilde{q}$ and considering paths that join at the point $\tilde{q}$, this is the same as integrating over all paths from $q$ to $q'$.
We also used the fact that the action of two coinjointed paths is the sum of the individual actions $(S[\gamma_2 \cdot \gamma_1] = S[\gamma_1] + S[\gamma_2])$

Since we often have to write $\int_{\gamma(t_{\text{i}})=q, \gamma(t_{\text{f}}) = q'}$, we will write it simply as $\int_{(t_{\text{i}},q) \to (t_{\text{f}},q')}$ instead.

It may seem that the integral formulation is very tedious.
And for some cases, like the hydrogen atom, it is.
But for scattering problems, this is much easier to solve.

\subsection{Feyman diagrams}

Feynman realized that we can take the set of all paths $(t,q) \to (t',q')$ and partition it into types of ``diagrams'' that describe all kinds of interactions that may happen.

What happens if we make multiple measurements?

Let $\hat{A} = \int dq A(q) \ket{q} \bra{q}$, $\hat{B} = \int dq B(q) \ket{q} \bra{q}$ be two diagonalisable operators.

We want to consider the event where the particle goes from $(t_i,q_i)$ to $(t_f,q_f)$ via two intermediary steps where we make a measurement $A$ at time $t_1$ and a measurement $B$ at time $t_2$.

\begin{align*}
  &\int_{(t_i,q_i) \to (t_2,q_f)} D_{\gamma} e^{\tfrac{i}{\hbar}S[\gamma]} A(\gamma(t_1)) B(\gamma(t_2))\\
  =
  &\int dq_1 dq_2 A(q_1) B(q_1)
  \int_{(t_i,q_i) \to (t_1,q_1)} D_{\gamma_1}
  \int_{(t_1,q_1) \to (t_2,q_2)} D_{\gamma_2}
  \int_{(t_2,q_2) \to (t_f,q_f)} D_{\gamma_3} e^{\tfrac{i}{\hbar}\left(
    S[\gamma_1] + S[\gamma_2] + S[\gamma_3]
  \right)}\\
  =
  &\int dq_1 dq_2 A(q_1) B(q_2)
  K(t_f-t_2,q_f,q_2) K(t_2-t_1,q_2,q_2) K(t_1 - t_i,q_1,q_i)\\
  =
  &\int dq_1 dq_2 A(q_1)B(q_2) \braket{q_f,t_f|q_2,t_2}
  \braket{q_2,t_2|q_1,t_1} \braket{q_1,t_1|q_i,t_i}
  \\
  =
  &\braket{q_f,t_f|\hat{B}_H(t_2) \hat{A}_H(t_1)|q_i,t_i} \text{\ for $t_2 \geq t_1$}
\end{align*}
Since in quantum mechanics, the operators do not commutes, it makes a difference whether $t_2 \geq t_1$ or not.

If we had it the other way around, the sandwiched operator would be $\hat{A}_H(t_1) \hat{B}_H(t_2)$.

This can be solved using the \textbf{time ordering operator} $\mathcal{T}$.
So if we want to write it in the Heisenberg picture, one would simply write
\begin{align*}
  \braket{q_f,t_f|\mathcal{T}\left[
    \hat{A}_H(t_1) \hat{B}_H(t_2)
  \right]|q_i,t_i}
  \quad \text{for} \quad 
  \mathcal{T}[\hat{A}_H(t_1) \hat{B}_H(t_2)]
  = \left\{\begin{array}{ll}
    \hat{B}_H(t_2) \hat{A}_H(t_1) & t_1 \leq t_2\\
    \hat{A}_H(t_1) \hat{B}_H(t_2) & t_2 \leq t_1
  \end{array} \right.
\end{align*}

Next time, we will show that
\begin{align*}
  \braket{q_f,t_f|\underbrace{\hat{q}(t) \hat{p}(t) - \hat{p}(t) \hat{q}(t)}_{[\hat{q},\hat{p}] = \hat{\id} \hbar i}|q_i,t_i}
\end{align*}

