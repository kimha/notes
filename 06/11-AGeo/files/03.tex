\begin{dfn}[]
A topological space is called \textbf{noetherian}, if every descending chain of embedded closed subsets eventually stabilizes, or equivalently, if every ascending chain of open subsets stabilizes.
\end{dfn}
Any algebraic set is noetherian with the Zariski topology.

\begin{thm}[]
Let $X$ be a Noetherian space and $Y \subseteq X$ closed. Then we can find a decomposition
\begin{align*}
  Y = Y_1 \cup \ldots \cup Y_r, \quad Y_i \not\subseteq \bigcup_{j \neq i}Y_j
\end{align*}
with $Y_i$ irreducible (called the irreducible components).
This decomposition is unique up to reordering.
\end{thm}
\begin{proof}
\textbf{Existence:} Since $X$ is Noetherian, every non-empty family $\Sigma$ of closed subsets has a minimal member, (which means none of its subsets are in $\Sigma$).

The following gives an example of \emph{Noetherian induction}.
Let $\Sigma$ be the family of closed subsets of $Y$ that do not have such a decomposition.
If $M \in \Sigma$, is such a minimal member and $M = M_1 \cup M_2$ for $M_i$ closed proper subsets and $M$ has no decomposition, then $M_1$ or $M_2$ has no decomposition.
This would mean that $M$ is not minimal $\lightning$.
\end{proof}

\begin{xmp}[]
Hypersurfaces $X = Z(f)$ are the simplest examples.

If $f = f_1^{a_1} \cdots f_r^{a_r}$ for $f_i$ irreducible, then the decomposition is
\begin{align*}
  X = Z(f_1) \cup \ldots \cup Z(f_r)
\end{align*}

For $X = Z(x^{2} - yz, xz - x)$, we have the decomposition
\begin{align*}
  X = \underbrace{Z(x,y)}_{\text{line along $z$}} \cup \underbrace{Z(x,z)}_{\text{line along $y$}} \cup \underbrace{Z(y-x^{2},z-1)}_{\text{parabola}}
\end{align*}
\end{xmp}

\begin{dfn}[]
  Let $Y$ be a topological space. The \textbf{dimension} of $Y$ is
  \begin{align*}
    \dim Y := \sup \left\{
      r \in \N \big\vert
      Y_0 \subsetneq Y_1 \subsetneq \ldots \subsetneq Y_r, Y_i \text{\ are closed non-empty irreducible subsets.}
    \right\}
  \end{align*}
\end{dfn}

This definition agrees with the Krull dimension from Commutative algebra:
\begin{prop}[]
  For $X$ an algebraic set, it holds $\dim X = \dim k[X]$.
\end{prop}
\begin{proof}
  By Hilbert's Nullstellensatz, we get an order reversing bijection
  \begin{align*}
    \{X_0 \subsetneq \ldots \subsetneq X_r \text{\ irreducible subsets}\}
    \leftrightarrow 
    \left\{
      I(X_r) \subsetneq \ldots \subsetneq I(X_0) \text{\ prime ideals}
    \right\}
  \end{align*}
\end{proof}
\begin{xmp}[]
Take $X = Z(zx,zy) \subseteq \A^{3}$ which has decomposition
\begin{align*}
  X = \underbrace{Z(z)}_{\text{plane at $z=0$}} \cup \underbrace{Z(x,y)}_{\text{line with $x = y = 0$}}
\end{align*}
so its dimension is $2$.
But in terms of prime ideals, we have the chain
\begin{align*}
  0 \subsetneq (x) \subsetneq (x,y) \subsetneq \faktor{k[x,y,z]}{z} \iso k[x,y] 
\end{align*}
which gives a chain of length $2$, which gives the Krull dimension.
\end{xmp}


\begin{prop}[]
Let $Y$ be a topological space, $Y' \subseteq Y$ a closed subspace.
Then $\dim Y' \subseteq Y$.
Moreover, if $Y,Y'$ are irreducible, then $\dim Y' = \dim Y \iff Y' = Y$.
\end{prop}
\begin{cor}[]
Let $X \subseteq \A^{n}$ be an algebraic set. Then $\dim X \leq n$.
\end{cor}

\begin{prop}[]
Let $Y$ be an irreducible psace, $U \subseteq Y$ an open subset.
Then $\dim U \leq \dim Y$.
\end{prop}
\begin{proof}
Assume $\dim U = r$, so consider a chain $U_0 \subsetneq \ldots \subsetneq U_{r}$.
Then their closures $\overline{U_i} \subseteq Y$ are also irreducible and the chain
$\overline{U_0} \subsetneq \ldots \subsetneq \overline{U}_r$ is a strictly ascending chain in $Y$ because $\overline{U_i} \cap U = U_i$.
\end{proof}

Usually, we have equality, but the following gives an example, where that is not the case.
\begin{xmp}[]
Take $Y = \{y,\eta\}$ the Sierpinsky space, with $y$ closed and $\eta$ open.
For $U = \{\eta\}$, $\dim U = 0$.
But $\dim Y = 1$ thanks to the chain $\{y\} \subsetneq Y$.
\end{xmp}


\subsection{Polynomial maps}

The following is a generalisation of linear maps.
\begin{dfn}[]
Let $X \subseteq \A^{n}, Y \subseteq \A^{m}$ be algebraic sets.
A map $\phi=(q_1,\ldots,q_m): X \to Y$ is a \textbf{polynomial map} if all $q_i$ are polynomial functions on $X_i$, i.e.\ $\phi$ is induced by a polynomial map $\A^{n} \to \A^{m}$.
We denote the set of all these maps
$\Hom_{\Aff}(X,Y)$.
\end{dfn}

\begin{xmp}[]
Consider $\phi: \A^{2} \to \A^{2}, (x,y) \mapsto (x,xy)$.
Then
\begin{align*}
  \phi^{-1}(u,v) =
  \left\{\begin{array}{ll}
      (u,u^{-1}v) & u \neq 0\\
      y\text{-axis}& (u,v) = (0,0)\\
      \emptyset & u=0, v \neq 0
  \end{array} \right.
\end{align*}
And the image of $\phi$ consists of all non-verticales lines through the origin.
\end{xmp}

\textbf{Construction:} A polynomial map $\phi: X \to Y$ induces a $k$-algera homomorphism via pullback
\begin{align*}
  \phi^{\ast}: k[Y] \to k[X], f \mapsto \phi \circ f
\end{align*}
\begin{center}
\begin{tikzcd}[column sep=0.8em] 
  X \arrow[]{rr}{\phi} \arrow[]{dr}{\phi^{\ast}f}
  && Y \arrow[]{dl}{f}
  \\
  &\A^{1}
\end{tikzcd}
\end{center}

\begin{thm}[]
There is a contravariant equivalence, (i.e.\ a fully faithful contravariant fucntor)
\begin{align*}
  \parbox{20ex}{algebraic sets and polynomial maps}
  &\to 
  \parbox{30ex}{finitely generated, reduced $k$-algebras and homomorphisms}\\
  X &\mapsto k[X]\\
  \Hom_{\Aff}(X,Y) \ni \phi &\mapsto \phi^{\ast} \Hom_{k-\Alg}(k[Y],k[X])
\end{align*}
\end{thm}
\begin{proof}
  \textbf{Bijection on objects:} 
  For all algebraic sets $X$, $k[X]$, is a finitely generated reduced $k$-algebra. 
  For all finitely generated algebras $A = k[x_{1}, \ldots, x_{n}]/I$, if $A$ is reduced, then $I$ is radical, so we can define $X = Z(I) \subseteq \A^{n}$ and $A = k[X]$.
  \textbf{Bijection on morphisms:} 
  First we check that if $\phi^{\ast} = \psi^{\ast}$ in $\Hom_{k-\Alg}(k[Y],k[X])$, then $\phi = \psi$ in $\Hom_{\Aff}(X,Y)$
  Since $\phi^{\ast},\psi^{\ast}$ can be extended to a map $k[y_{1}, \ldots, y_{m}] \to k[x_{1}, \ldots, x_{n}]$ by $y_i \mapsto q_i(x_{1}, \ldots, x_{n})$, if they are equal, then $\phi,\psi$ have the same components, so $\phi = \psi$.

  For surjectivity, we want that given an $\alpha: k[Y] \to k[X]$, define $\phi: X \to Y$ by defining its components by $q_i = \alpha(\overline{y}_i)$.
  If $\alpha = 0$ on $I(Y)$, then $\phi(X) \subseteq Y$, so it is well-defined.

  By construction, $\phi^{\ast} =\alpha$, which is what we wanted.
\end{proof}

For example, we have the correspondence
\begin{align*}
  \A^{1} \stackrel{\phi}{\to} \A^{1}
  &\qquad k[t] \from k[t]\\
  t \mapsto t^{2} 
  &\qquad
  f(t^{2}) \mapsfrom f(t)
\end{align*}

\begin{prop}[]\label{prop:polynomial-continuous}
Polynomial maps are continuous with respect to the Zariski topology.
\end{prop}
\begin{proof}
Let $\phi: X \to Y$ be a polynomial map.
If $\alpha \in k[Y]$, then
\begin{align*}
  \phi^{-1}(Z(\alpha)) = Z(\phi^{\ast} \alpha) \text{\ is closed}
\end{align*}
And if $\alpha_{1}, \ldots, \alpha_{n} \in k[Y]$, then
\begin{align*}
  \phi^{-1}(Z(\alpha_{1}, \ldots, \alpha_{n})) = \phi^{-1}(Z(\alpha_1) \cap \ldots \cap Z(\alpha_n)) = \bigcap_{i} \phi^{-1}(Z(\alpha_i))
\end{align*}
which is closed.
\end{proof}
