\section{Dimension revisited}
\subsection{Motivation}
For $X$ a topological space, we defined its dimension as
\begin{align*}
  \dim X = \sup \left\{
    r \big\vert x_0 \subsetneq \ldots \subsetneq X_r \text{\ irreducible closed subsets of }X
  \right\}
\end{align*}
We also saw how this definition of dimension went hand in hand for the krull dimension of a ring.
In particular, we saw that for $X$ an affine variety $\dim X = \dim k[X]$.

Now, we would like to know how we can compute the dimension of a variety.

\begin{prop}[]
Let $X$ be a topological space and $X = \bigcup_{i \in I}U_i$ an open cover.
Then $\dim X = \sup_{i \in I} \dim U_i$.
\end{prop}
\begin{proof}
We have already proved the inequality $\dim X \geq \dim U_i$ before.

\textbf{$\bm{\dim X \leq \sup \dim U_i}$:} Pick a chain $Z_0 \subsetneq \ldots \subsetneq Z_d$ of irreducible closed subsets in $X$.
Chose a $U = U_i$ such that $U_i \cap Z_0 \neq \emptyset$.
We claim that
\begin{align*}
  Z_0 \cap U \subseteq \ldots \subseteq Z_d \cap U
\end{align*}
gives a chain in $U$.
Indeed, $Z_j \cap U \subseteq Z_j$ is non-empty and open, so it is dense and irreducible.
So $\overline{Z_j \cap U} = Z_j$, which means that $Z_j \cap U \neq Z_{j-1} \cap U$.
\end{proof}
So in order to compute the dimension, we can take an open cover and compute the dimension of each part.

\begin{prop}[]
Let $\phi: X \to Y$ be a morphism of affine varieties. Then
\begin{align*}
  \phi \text{\ is dominant } \iff \phi^{\ast}: k[Y] \to k[X] \text{\ is injective.}
\end{align*}
\end{prop}
\begin{proof}
  \textbf{``$\bm{\implies}$'':}  Let $f \in k[Y], f \neq 0$.
  Since $\phi$ is dominant, $\phi(X) \cap D(f) \neq \emptyset$, so $f \circ \phi \neq \emptyset$ on $\phi^{-1}(D(f)) \neq \emptyset$, so $\phi^{\ast} f \neq 0$.

  \textbf{``$\bm{\impliedby}$'':} Suppose that $\overline{\phi(X)} = A \subsetneq Y$.
  As $Y$ is affine, the closed set $A$ is $Z(I)$, for some ideal $I \subseteq k[Y]$. Pick $f \in I, f \neq 0$.
  As $f$ vanishes along $A$, it would follow that $\phi^{\ast}f = 0$ on $X$, so $\phi^{\ast}$ is no injective.
\end{proof}


\begin{dfn}
A morphism of affine variaties $\phi: X \to Y$ is \textbf{finite}, if $k[X]$ is a \textbf{finitely generated} module over $k[Y]$ via $\phi^{\ast}$.

Let $\phi: X \to Y$ be a morphism of varieties.
\begin{itemize}
  \item $\phi$ is \textbf{affine}, if $\forall$ affine $V \subseteq Y: \phi^{-1}(V) \subseteq X$ is affine.
  \item $\phi$ is \textbf{finite}, if it is affine and $\forall y \in Y: \exists$ affine $V \ni y$ such that $\phi: \phi^{-1}V \to V$ is finite.
\end{itemize}
\end{dfn}

\begin{xmp}[]
  \begin{itemize}
    \item The open embedding $\phi: \A^{1} - \{0\} \to \A^{1}$, induces $\phi^{\ast}[t] \hookrightarrow k[t,t^{-1}]$, which is not finite.
      Note: closed embeddings are finite, but open embeddings are not.
    \item Let $X = Z(x^{3} - y^{2}) \subseteq \A^{2}$ the cuspidal cubic.
      Let $\phi: X \to \A^{1}$ be the projection onto the $x$-axis.
      The induced map is
      \begin{align*}
        \phi^{\ast}: k[x] \hookrightarrow \faktor{k[x,y]}{(x^{3} - y^{2})} \iso k[x] \oplus k[x] \cdot y
      \end{align*}
      the image is a rank $2$-module which relates to the fact that almost all fibers are 2 points.
  \end{itemize}
\end{xmp}

\begin{exr}[]
Show that if $\phi: X \to  Y$ is finite, then all fibers are finite.
Show that the converse is not true.
\end{exr}



\begin{lem}[]
Let $\phi: X \to Y$ be affine, $x \in X, y \in Y$. Then
\begin{align*}
  \phi(x) = y \iff \phi^{\ast}m_y \subseteq m_x \text{\ in }k[X]
\end{align*}
where $m_x$ consists of functions vanishing at $x$.
\end{lem}
\begin{proof}
  \begin{align*}
  \phi^{-1}(y) = \{x \in X \big\vert \phi(x) = y\}
  = \{x \in X \big\vert f(\phi(x)) = 0 \forall f \in m_y \}
  = Z(\phi^{\ast}m_x)
\end{align*}
\end{proof}

\begin{prop}[]\label{prop:finite-closed-dominant-surjective}
Let $\phi: X \to Y$ be a morphism of varieties.
Then
\begin{itemize}
  \item If $\phi$ is finite, it is also closed. 
  \item If $\phi$ is finite and dominant, then $\phi$ is surjective.
\end{itemize}
\end{prop}
\begin{proof}
  (b) follows immediately from (a).

  If $\phi$ is dominant, $\phi^{\ast} k[Y] \hookrightarrow k[X]$ is injective.
  If $\exists y \in Y$ such that $y \notin \phi(x)$, then
  \begin{align*}
    \phi^{\ast} m_y = m_y k[X] = k[X]
  \end{align*}
  but $k[X]$ is a finitely generated $k[Y]$-module, so by Nakayama's Lemma, $\exists a \in m_y: (1-a) k[X] =  0$.
  This however would imply that
  \begin{align*}
    0 = (1-a) \cdot 1 = \phi^{\ast}(1-a) \stackrel{\phi \text{\ inj.}}{\implies} 1-a = 0 \implies a = 1 \implies 1 \in m_y
  \end{align*}
  which is absurd, because then $m_Y$ would no longer be a proper ideal.


  For (a), Let $Z \subseteq X$ be closed. 
  If $Z$ is irreducible, then $W := \overline{\phi(Z)} \subseteq Y$ is closed irreducible and $\phi|_Z: Z \to W$ is dominant and finite.
  If $x_{1}, \ldots, x_{d}$ are generates for $k[X]$ over $k[Y]$, then $\overline{x_1},\ldots,\overline{x_d}$ generate $k[Z]$ over $k[W]$.

  Hence, $\phi|_Z$ is surjective and by the previous part, $\phi(Z) = W$ is closed.

  If $Z$ is not irreducible, consider its irreducible components $Z = Z_1 \cup \ldots Z_r$. Then $\phi(Z) = \phi(Z_1) \cup \ldots \cup \phi(Z_r)$ is closed.
\end{proof}

\begin{prop}[Going Up]
Let $\phi:X \to Y$ be finite dominant.
If $Z \subseteq Y$ is a closed irreducible subset, then there exists a closed irreducible subset $W \subseteq X$ with $\phi(W) =Z$.
\end{prop}
\begin{proof}
By the previous proposition, $\phi$ is surjective, so $\phi^{-1}(Z) \subseteq X$ is closed non-empty.
By splitting it into its irreducible components $\phi^{-1}(Z) = W_1 \cup \ldots \cup W_r$, we get $Z = \phi(W_1) \cup \ldots \cup \phi(W_r)$.
And since $Z$ is irreducible, $Z = \phi(W_i)$ for some $i$.
\end{proof}


\begin{lem}[]
Let $\phi: X \to Y$ be finite dominant.
If $Z \subsetneq X$ is a closed subset, then $\phi(Z) \subsetneq Y$.
\end{lem}
\begin{proof}
We can assume $X,Y$ are affine.
Suppose $\phi(Z) = Y$, let $f \in I(Z)$, $f \neq 0$.
Since $\phi$ is finite, $k[X]$ is a finitely generated $k[Y]$-module, so there exists a relation
\begin{align*}
  f^{n} + \phi^{\ast}(a_{n-1}) f^{n-1} + \ldots + \phi^{\ast}a_0 = 0 \quad \text{for some} \quad a_{0}, \ldots, a_{n} \in k[Y]
\end{align*}
with $n > 0$ minimal. Since $f(x) = 0$ for all $x \in Z$, $\phi^{\ast}(a_0)(x) = 0 \forall x \in Z$, so $a_0$ vanishes along $\phi(Z) = Y$.

By injectivity of $\phi^{\ast}$, it follows that $a_0 = 0$, so $n$ was not minimal $\lightning$.
\end{proof}


\begin{thm}[]
  Let $\phi: X \to Y$ be a finite morphism. Then $\dim X \leq \dim Y$.
  If $\phi$ is also dominant, then $\dim X = \dim Y$.
\end{thm}
\begin{proof}
By substituting $\overline{\phi(X)}$ for $Y$, it suffices to prove the second part.
\textbf{$\bm{\dim X \leq \dim Y}$:} Let $W_0 \subsetneq \ldots \subsetneq W_r$ be a chain of closed irreducibles in $X$.
Then $\phi(W_0) \subseteq \ldots \subseteq \phi(W_r)$ is a chain in $Y$.
They are irreducible, and because $\phi$ is finite they are closed (by Proposition~\ref{prop:finite-closed-dominant-surjective}) and they are distinct because of the last lemma.

\textbf{$\bm{\dim Y \leq \dim X}$:} Let $Z_0 \subsetneq \ldots \subsetneq Z_r$ be a chain on $Y$.
Applyin Going-up on the last element $Z_r$, and then successively down until $Z_0$ be searching for $W_{i-1}$ inside $W_{i}$, we find a chain $W_0 \subseteq \ldots \subseteq W_r$ in $X$ such that $Z_i = \phi(W_i)$.
\end{proof}


For $X$ an affine variety, we defined its coordinate ring, for which there is no good analogue for varieties in general.
There is however an alternative definition for the function field.
Namely, for $X$ an irreducible variety, we have
\begin{align*}
  k(X) \iso \left\{
    (U,f)\big\vert U \subseteq X \text{\ non-empty open}, f \in \mathcal{O}_X(U)
  \right\}/\sim
\end{align*}



