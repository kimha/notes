\section{Sheaves and varieties}

When we have some irreducible algebraic sets, we want to be able to glue them together to form algebraic varieties.
But the algebraic sets carry with them some extra information in the form of the Zariski topology.
So not only do we want to glue them together, we want this glueing to respect the information.

This will lead to the idea of locally ringed spaces.

The book ``Foundations of Algebraic Geometry'' by Ravi Vakil has a really good chapter on sheaves.

Another nice resource I found was this nlab article \url{https://ncatlab.org/nlab/show/motivation+for+sheaves,+cohomology+and+higher+stacks}

\subsection{Sheaves}
The story starts with something that does not respect the gluing of data.
\begin{dfn}[]
A \textbf{presheaf} of a category $\textsf{D}$ on a category $\textsf{D}$ is a functor $F: \textsf{C}^{\op} \to \textsf{D}$.

For $X$ a topological spcae, a \textbf{presheaf on $X$} is a presheaf $R: \text{Open}(X) \to D$, where the objects of $\text{Open}(X)$ are the open sets of $X$ and the morhpism are the inclusion maps $V \stackrel{\subseteq}{\hookrightarrow} U$.
We call $R(U) \stackrel{\rho_{UV}}{\to} R(V)$ the \textbf{restriction} map.
We denote $\rho_{UV}(f)$ as $f|_V$.
If $\textsf{D}$ is a set (or a set-like object), then elements of $R(U)$ are called \textbf{sections}, and elements of $R(X)$ \textbf{global sections}.
\end{dfn}
Usually, we will chose the category $\textsf{D}$ to be either $\Set,\Grp,\Ring,\Top$.


\begin{xmp}[]
  Let $X,Y$ be topological spaces.
  Consider the set $h_Y(X)$ of $Y$-valued continuous functions on $X$.
  If $X \stackrel{f}{\to}Y \in h_Y(X)$, and $U$ is an open set, we can \emph{restrict} $f$ to $U$ and obtain another continous function $U \stackrel{f|_U}{\to}Y \in h_Y(U)$.
  This defines the restriction map
  \begin{align*}
    h_Y(X) \stackrel{\rho_{XU}}{\to} h_Y(U)
  \end{align*}

  Moreover, if we have open sets $U \subseteq V \subseteq X$, then it doesn't matter if we restrict $f$ to $V$ and then to $U$, or do the restriction directly.
  In other words: the following diagram commutes
  \begin{center}
    \begin{tikzcd}[column sep=0.8em] 
      h_Y(X) \arrow[]{rr}{\rho{XV}} \arrow[]{dr}{\rho_{XU}}&& h_Y(V) \arrow[]{dl}{\rho{VU}}\\
             & h_Y(U)
    \end{tikzcd}
  \end{center}
  This shows that the functor $h_Y: \text{Open}(X) \to \Set$ is a presheaf on $X$.

  More generally, for any locally small category $\textsf{C}$, the Yoneda Lemma shows that for any object $A \in \textsf{C}$, the functor $h_A = \Hom_{\textsf{C}}(-,A)$ is a presheaf (called \textbf{representable} presheaf).
\end{xmp}

As further motivation for the definition of sheaves, recall the following fact from basic Topology
\begin{lem}[Gluing Lemma]
  Let ${(U_i)}_{i \in I}$ be a collection of open subsets of $X$ and assume we have continuous maps ${(U_i \stackrel{f_i}{\to}Y)}_{i \in I}$ such that for all $x \in U_i \cap U_j$ we have $f_i(x) = f_j(x)$.

  Then there exists a unique map $f: \bigcup_{i \in I}U \to Y$ such that $f|_{U_i} = f_i$.
\end{lem}
\begin{proof}[Proof Sketch]
  Define $f(x) = f_i(x)$, if $x \in U_i$.
  For $V \subseteq Y$ open, $f_i^{-1}(V) \subseteq X$ is open.
  Then $f^{-1}(V) = \bigcup_{i\in I} f_i^{-1}(V)$ is a union of open sets and thus open.
  Uniqueness folllows from the fact that $f|_{U_i} = f_i$.
\end{proof}


\begin{dfn}[]
  Let $X$ be a topological space.
A \textbf{sheaf} $R$ on $X$ is a presheaf on $X$ that satisfies
\begin{enumerate}
  \item For any open cover $U = \bigcup_{i \in I}U_i \subseteq X$ and any sections $s,t \in R(U)$ we have
    \begin{align*}
      s|_{U_i} = t|_{U_i} \forall i \implies s|_U = t|_U
    \end{align*}
    so if two sections agree locally, they must agree globally.
  \item If $U = \bigcup_{i} U_i$ is an open cover and for each $i$, we have a section $s_i \in R(U_i)$ such that $\forall i,j: s_i|_{U_i \cap U_j} = s_j|_{U_i \cap U_j}$, then there exists a section $s \in R(U)$ with $s|_{U_i} = s_i \forall i$.
\end{enumerate}
\end{dfn}

\begin{xmp}[]
  \begin{itemize}
    \item  The gluing lemma says that the presheaf of $Y$-valued functions on $X$ is a sheaf.
    \item  Similarly, the presheaf of $C^{\infty}$-functions on a smooth manifold is a sheaf. Same for holomorphic functions etc.

    \item The continuous $k$-valued functions form a sheaf of $k$-algebras in the following way:
      \begin{align*}
        R_X(U) := \left\{
        \parbox{14em}{$f: U \to \A_k^{1}$ continuous, i.e.\quad $\forall a \in k: f^{-1} (a) \subseteq U$ is closed}
        \right\}
      \end{align*}
      Locality comes from the fact that if $f: U \to \A_k^{1}$ continuous has $f|_{U_i} = 0$ for all $i$, then $f = 0$.
      The gluing follows from the glueing lemma.
      The restriction map $\rho_{UV}$ is also a $k$-algebra morphism.
  \end{itemize}
  
  Here are some non-examples:

  \begin{itemize}
    \item Bounded functions do not form a sheaf, as beging bounded is not a local condition.
      You cannot glue together bounded functions and preserve boundedness.
    \item For  $A = \Z \in \Ring$, the constant presheaf $A_X$ on a non-connected space $X$ given by $A_X(U) = A$, $\rho_{UV} = \id_A$ is not a sheaf.
      If $X = U_1 \sqcup U_2$, then take $s_1 = 3 \in A_X(U_1) = \Z$ and $s_2 = 5$.
      There cannot be a global section $s \in \Z$ that restricts to $3$ and $5$, since the restriction map must is the identity on $\Z$
  \end{itemize}
  The last problem can be solved by defining the \textbf{constant sheaf}
  \begin{align*}
    A_X(U) := \{\text{locally constant\ } U \to  A\}
  \end{align*}
  which, if $U$ is locally connected, is isomorphic to $\prod_{\pi_0(U)}A$.
  this is an example of a more general canonical procedure called \textbf{sheafification}.
\end{xmp}


\begin{xmp}[Pullaback of functions]
  Let $\Phi: X \to Y$ be a continuous map of topological spaces.
  This defines a morphism of sheaves from $R_Y$ to $R_X$ by collection of $k$-algebra morphisms
  \begin{align*}
    \phi^{\ast}: R_Y(U) \to R_X(\phi^{-1}(U)), \quad f \mapsto \phi^{\ast}f := f \circ \phi
  \end{align*}
  \begin{center}
  \begin{tikzcd}[column sep=0.8em] 
    \phi^{-1}U 
    \arrow[]{rr}{\phi}
    \arrow[]{dr}{\phi^{\ast}f}
    && U \arrow[]{dl}{f}
    \\
    & k
  \end{tikzcd}
  \end{center}
\end{xmp}

\begin{dfn}[]
A \textbf{subsheaf} $R' \subseteq R$ of a (set-valued) sheaf is a subfunctor of $R$ that is a sheaf itself.
In other words it is a sheaf such that $R'(A) \subseteq R(A)$ and if $A \stackrel{f}{\to} B$ is a morphism, $R'f$ is the retriction of $Rf$ on $R(A)$.
We write $R' \subseteq R$.
\end{dfn}

\begin{dfn}[]
A ($k$)-\textbf{ringed space} is a pair $(X,R)$, where $X$ is a topological space and $R \subseteq R_X$ is a $k$-algebra subsheaf of the sheaf of continuous $k$-valued functions on $X$.

A morphism of ringed spaces $(X,R) \stackrel{\phi}{\to} (Y,S)$ is a continuous map $\phi: X \to Y$ such that
\begin{align*}
  \phi^{\ast}f \in R(\phi^{-1}U) \quad \forall f \in S(U)
\end{align*}
\end{dfn}

\begin{rem}[]
More generally, any topological space with a sheaf of rings is also a ringed space and the definitions of morphisms is more abstract.
In practice, we'll be using these specific kinds of sheaves so we stick to the more concrete dfinitions.

From now on, ``sheaf'' will usually mean a $k$-algebra subsheaf of $R_X$.
\end{rem}

\begin{xmp}[]
\begin{itemize}
  \item A smooth manifold is a ringed space $(X,R)$ that is locally isomorphic to $(\R^{n},C^{\infty})$.
  \item A complex manifold is a ringed space $(X,R)$ is a ringed spcae $(X,R)$ that is locally isomorphic to $(\C^{n},A_{\C^{n}})$, where $A_{\C^{n}}$ is the sheaf of analytic functions.
\end{itemize}
\end{xmp}

\subsection{Regular functions}

\begin{dfn}[]
Let $X$ be an algebraic set, $U \subseteq X$ open, $f: U \to k$.
\begin{itemize}
  \item We say that $f$ is \textbf{regular at $p\in U$} if one can write in a open $V \in p$ that $f = \frac{g}{h}$ in $V$, for $g,h \in k[X]$.
    In particular, $h(p) \neq 0$.
  \item $f$ is \textbf{regular}, if it's regular for all $p \in U$.
\end{itemize}
\end{dfn}
\begin{xmp}[]
If $\phi: X \to Y$ is a polynomial map of algebraic sets, then if $f$ is regular on $U \subseteq Y$, then $\phi^{\ast}f$ is regular on $\phi^{-1}U \subseteq X$.
\end{xmp}


\begin{dfn}[]
Let $X \subseteq \A^{n}$ be a closed subset.
The \textbf{structure sheaf} $\mathcal{O}_X$ of $X$ is defined by
\begin{align*}
  \mathcal{O}_X(U) := \left\{
    f: U \to k \big\vert f \text{\ regular on }U
  \right\}
\end{align*}
it's a $k$-algebra sheaf for the same reason as $R_X$ is.
\end{dfn}
\begin{prop}[]
Every regular function is continuous.
In particular, $\mathcal{O}_X \subseteq R_X$ is a $k$-algebra subsheaf.
\end{prop}
\begin{proof}
  It suffices to check continuity for functions of the forn $f = \tfrac{g}{h}: U \to k$ for $g,h \in k[X]$ with $h \neq 0$.
  As $g,h$ are polynomial maps, they are continuous (See Proposition~\ref{prop:polynomial-continuous}).
  We need to check that for all $a \in k$, ${(\tfrac{g}{h})}^{-1}(a)$ is closed in the distinguished open set $D(h) = X - Z(h)$. But this set is exactly
  \begin{align*}
    Z(g - a \cdot h) \cap D(h) \subseteq D(h)
  \end{align*}
  which is the intersection of two closed sets.
\end{proof}

Now we would like to know how regular functions $f: U \to k$ are related to polynommial functions on $X$.

We will see next time that regular functions with domain $X$ are precisely the polynomial functions on $X$.

We will also see an explicit description of regular functions on $D(h)$.
There is however no explicit description on any open $U \subseteq X$.




