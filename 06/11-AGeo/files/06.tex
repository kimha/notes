\subsection{Stalks}
\begin{dfn}[]
Let $(X,\mathcal{O}_X)$ be a $k$-ringed space.

A \textbf{germ} of a function at $p \in X$ is an equivalence class of pairs $(U,f)$, where $U \ni p$ and $f\in \mathcal{O}_X(U)$ ($f$ is a regular function on $U$), and $(U,f) \sim (U',f')$ if theree exists an open neighborhod $p \in V \subseteq U \cap U'$ such that $f|_V = f|_{V'}$.


The \textbf{stalk} of $\mathcal{O}_X$ at $p$ is
\begin{align*}
  \mathcal{O}_{X,p} = \left\{
  \text{\parbox{10em}{Germs of functions at $p$}}
\right\}
= \colim_{p \ni U \subseteq X} \mathcal{O}_X(U)
\end{align*}
\end{dfn}

\begin{prop}[]
\begin{itemize}
  \item Any stalk $\mathcal{O}_{X,p}$ is a $k$-algebra, where
    \begin{align*}
      (U,f) + (U',f') = (U \cap U',f+f')
    \end{align*}
  \item If $U \subseteq X$ open, $p \in U$ then $\mathcal{O}_{U,p} \iso \mathcal{O}_{X,p}$.

  \item If $X$ is an affine variety, $p \in X$ we have
    \begin{align*}
      \mathcal{O}_{X,p} \iso {k[X]}_{m_p} \quad \text{where} \quad m_p = \{g \in k[X] \big\vert g(p) = 0\}
    \end{align*}
    where the right side of the isomorphism is the local ring with maximal ideal $m_p$.
    $\mathcal{O}_{X,p}$ is called the \textbf{local ring at $p$}
\end{itemize}
\end{prop}
We only prove the third point
\begin{proof}
  By the previous theorem, we have
\begin{align*}
 \mathcal{O}_{X,p} = 
 {\left\{(U \ni p, f \in \mathcal{O}_X(U))\right\}}_{\sim}
 \iso
 \left\{
   (D(h) \ni p, f \in \mathcal{O}_X(D(h)))
 \right\}
 _{\sim}
 \iso 
 {\left\{
   (U \ni p,f \in {k[X]}_h)
 \right\}}_{\sim}
 \iso
 {\left\{f \in {k[X]}_h \text{\ such that } h \notin m_p\right\}}_{\sim}
 \iso
 {k[X]}_{m_p}
\end{align*}
where in the last step, we used the property of localisation.

\end{proof}
For $X$ affine, we get from (c) that there is a bijection
\begin{align*}
    \text{\parbox{8em}{prime ideals inside $\mathcal{O}_{X,p}$}}
  \iso
    \text{\parbox{10em}{prime ideals of $k[X]$ contained in $m_p$}}
\end{align*}
so with the algebra-geometric dicionary, there is a bijection
\begin{align*}
    \text{prime ideals of $\mathcal{O}_{X,p}$}
  &\iso 
    \text{\parbox{10em}{Irreducible closed subsets passsing through $p$}}
  \\
  \left\{
    (U,f) \big\vert f|_{U \cap Z} = 0
  \right\}
  &\mapsfrom
  Z \ni p
\end{align*}
in particular, we also have
\begin{align*}
    \text{minimal prime ideals of $\mathcal{O}_{X,p}$}
  \iso
    \text{irreducible components containing $p$}
\end{align*}

So $\mathcal{O}_{X,p}$ is an integral domain if and only if $p$ lies in a single irreducible component.


\begin{dfn}[Construction]
  Let $\phi: (X,\mathcal{O}_X) \to (Y,\mathcal{O}_Y)$ be a morphism of $k$-ringed spaces.

  For any open $U \subseteq Y$ we have the pullback $\phi^{\ast} : \mathcal{O}_Y(U) \to \mathcal{O}_X(\phi^{-1}(U))$.
  In particular, for all $p \in X$, this induces a morphism
  \begin{align*}
    \phi^{\ast}: \mathcal{O}_{Y,\phi(p)} &\to \mathcal{O}_{X,p}\\
    [(U,f)] &\mapsto [\phi^{-1}(U),f \circ \phi]
  \end{align*}
  by construction, $\phi^{\ast}$ is a \emph{local homomorphism} i.e.\ $\phi^{\ast}(m_{\phi(p)}) = m_p$.
\end{dfn}


\begin{center}
``Life is easy if you're irredicuble!''
- M. Lakerson
11.08.22
\end{center}

Recalll that
\begin{align*}
  \text{$X$ is an irreducible algebraic set}
  \iff
  \text{$I(X)$ is prime}
  \iff
  \text{$k[X]$ is an integral domain}
\end{align*}

\begin{dfn}[]
Let $X$ be an irreducible algebraic set.
The \textbf{function field} of $X$ of $X$ is
\begin{align*}
  k(X) := \text{Frac}(k[X])
\end{align*}
whose elements are called \textbf{rational functions}.

\end{dfn}

\begin{xmp}[]
As a non-example, we would not want to use the above definition on the non-irreducible set $X = Z(x \cdot y) \subseteq \A^{2}$, as we would get into trouble with the origin.
\end{xmp}


Every rational function defines a regular function on a dense open in $X$.

\textbf{Claim:}
For irreudible $X$, all the rings attached to $X$ as above are \textbf{subrings} of $k(X)$.

For example
\begin{itemize}
  \item $\mathcal{O}_{X,p} \iso
      \left\{
        \tfrac{g}{h} \in k(X) \big\vert h(p) \neq 0
      \right\}$
  \item $\mathcal{O}_X(D(h))
      \iso
      \left\{
        \tfrac{g}{h^{n}} \in k(X) \big\vert g \in k[X], n \in \N
      \right\}$
  \item $\mathcal{O}_X(U) \iso \bigcap_{p \in U}\mathcal{O}_{X,p}$
\end{itemize}


\subsection{Morphisms into affine varieties}

The next lemma says that being a morphism is a local property
\begin{lem}[]
Let $X,Y$ be algebraic varieties, $\phi: X \to Y$ continuous.
Assume there are open covers $X = \bigcup_{i}U_i$, $Y = \bigcup_{i}V_i$ such that $\phi(U_i) = V_i$ and that $\phi|_{U_i}: U_i \to V_i$ is a morphism.
Then $\phi$ is a morphism.
\end{lem}
This follows trivially the fact that being a regular function is a local property and from the sheaf structure.


Let $X$ be an algebraic variety and $f_{1}, \ldots, f_{m}$ regular functions on $X$.
This defines a continuous map 
\begin{align*}
  \phi: X \to \A^{m}, x \mapsto (f_1(x),\ldots,f_m(x))
\end{align*}
We claim that $\phi$ is a morphism, which means we need to show that for all $f \in \mathcal{O}_{\A^{m}}(U)$, we have $\phi^{\ast}f \in \mathcal{O}_X(\phi^{-1}(U))$.

By the lemma, one can assume that $U = D(g)$ and so $f = \frac{a}{g^{r}}, r \geq 0$, (by Theorem~\ref{thm:localisation-distinguisehd}).
Then
\begin{align*}
  \phi^{\ast}f = f \circ \phi = \frac{a(f_{1}, \ldots, f_{m})}{g^{r}(f_{1}, \ldots, f_{m})}
\end{align*}
which is regular on $\phi^{-1}(D(g))$, since $g(f_{1}(x), \ldots, f_{m}(x)) \neq 0$.



\begin{prop}[]
Let $X$ be an algebraic varety. There exists a bijection
\begin{align*}
    \text{morphisms $X \stackrel{\phi}{\to} \A^{m}$}
  \leftrightsquigarrow
    \text{\parbox{14.5em}{$k$-algebra homomorphisms $k[y_{1}, \ldots, y_m] \stackrel{\phi^{\ast}}{\to} \mathcal{O}_X(X)$, $y_i \mapsto  f_i$.}}
\end{align*}
\end{prop}
We can generalize
\begin{cor}[]
Let $X$ be a variety, $Y$ an \emph{affine} variety.
There is bijection
\begin{align*}
  \text{\parbox{5em}{morphisms $X \stackrel{\phi}{\to}Y$}}
  \leftrightsquigarrow
  \text{\parbox{10em}{$k$-algebra homomorphisms
  $\mathcal{O}_Y(Y) \stackrel{\phi^{\ast}}{\to} \mathcal{O}_X(X)$}}
\end{align*}
\end{cor}
\begin{proof}
  Since $Y$ is affine, we can assume that it is a closed subset $Y \subseteq \A^{m}$.
  Then the diagram
  \begin{center}
    \begin{tikzcd}[ ] 
      X \arrow[]{r}{\phi} \arrow[dashed]{dr}{} 
      & \A^{m}
      \\
      & Y \arrow[]{u}{\iota}
    \end{tikzcd}
  \end{center}
  corresponds to the diagram
  \begin{center}
  \begin{tikzcd}[ ] 
    \mathcal{O}_X(X)
    &
    k[y_{1}, \ldots, y_{m}]
    \arrow[]{l}{\phi^{\ast}}
    \arrow[]{d}{\pi}
    \\
    & \faktor{k[y_{1}, \ldots, y_{m}]}{I(Y)}
    \iso \mathcal{O}_Y(Y)
    \arrow[dashed]{ul}{}
  \end{tikzcd}
  \end{center}
\end{proof}



\begin{thm}[Main theorem of affine varieties]
There exists a contravariant equivalence between the category of affine $k$-varieties and the category of reduced finitely generated $k$-algebras
\begin{align*}
  \Aff_k^{\op} 
  &\stackrel{\iso}{\to} \text{RedFinGenAlg}_k\\
  X &\mapsto k[X]\\
  (\phi: X \to Y) &\mapsto (\phi^{\ast}: k[Y] \to k[X])
\end{align*}
\end{thm}

\begin{cor}[]
For $X,Y$ affine, $X \iso Y$ if and only if $k[X] \sim k[Y]$.
In particular, $k[X]$ does not depend on the choice of embedding $X \subseteq \A^{n}$ or $X \subseteq \A^{n'}$.
\end{cor}

\begin{xmp}[]
\begin{itemize}
  \item $\A^{n} - 0, n\geq 2$ is \emph{not} an affine variety, because $\A^{n} - 0 \stackrel{\iota}{\hookrightarrow} \A^{n}$ is not an isomorphism, but $\iota^{\ast}$ is.
  \item The twisted cubic 
    \begin{align*}
      \phi: \A^{1} \to C = Z(Y-X^{2},Z-X^{3}) \subseteq \A^{3}, \quad
      t \mapsto  (t,t^{2},t^{3})
    \end{align*}
    is an affine variety and the induced map 
    \begin{align*}
      \phi^{\ast}: \faktor{k[x,y,z]}{(y-x^{2},z-x^{3})}
      \iso k[t] \quad (x \mapsto  t, y \mapsto t^{2}, z \mapsto t^{3})
    \end{align*}
    is an isomorphism. Therefore $\phi$ is an isomorphism.
  \item
    The cusp
    \begin{align*}
      \phi: \A^{1} \to \A^{2}, \quad t \mapsto (t^{2},t^{3})
    \end{align*}
    is a bijection, but not an isomorphism because the induced map
    \begin{align*}
      \phi^{\ast}: \faktor{k[x,y]}{(y^{2}-x^{3})} \to k[t], \quad (x \mapsto t^{2}, y \mapsto t^{3})
    \end{align*}
    is not surjective.
\end{itemize}
\end{xmp}

\begin{center}
``Teaching has alot in common with standup comedy''
- M. Lakerson 11.03.2022
\end{center}


\subsection{Products of varieties}


\begin{thm}[]
\begin{itemize}
  \item The category $\Aff_k$ has products and
    \begin{align*}
      k[X \times Y] \iso k[X] \otimes_k k[Y]
    \end{align*}
  \item The category $\text{Var}_k$ also has products and are obtained by glueing together the products of taffine varieties.
    (``The glueing of the product is the product of the glueing'')
\end{itemize}
\end{thm}
We prove it for the affine varieties.
\begin{proof}
  Let $X \subseteq \A^{n}, Y \subseteq \A^{m}$ and $I(X) = (f_{1}, \ldots, f_{r}), I(Y) = (g_{1}, \ldots, g_{s})$.
  Set
  \begin{align*}
     I = (f_{1}, \ldots, f_{r},g_{1}, \ldots, g_{s}) \subseteq K[x_{1}, \ldots, x_{n},y_{1}, \ldots, y_{m}]
  \end{align*}
  we know from commutative Algebra that since $k$ is algebraically closed 
  \begin{align*}
    \text{$I(X),I(Y)$ radical/prime $\implies$ $I$ radical/prime}
  \end{align*}
  Define $W = Z(I) \subseteq \A^{n+m}$. Then, since $I$ is radical, we know from Hilberts Nullstellensatz that
  \begin{align*}
    k[W] \iso \faktor{k[x_{1}, \ldots, x_{n},y_{1}, \ldots, y_{m}]}{I}
    \iso k[X] \otimes_k k[Y]
  \end{align*}
  Lastly, we check that $W$ is the product of $X$ and $Y$ in $\Aff_k$.
  The projection maps come from restricting the projection maps
  \begin{center}
  \begin{tikzcd}[ ] 
    \A^{n+m} \arrow[]{r}{}
    \arrow[]{d}{}
    & \A^{m}
    \\
    \A^{n} 
  \end{tikzcd}
  \end{center}
  to $W$ to obtain maps $W \stackrel{\pi_X}{\to} X, W \stackrel{\pi_Y}{\to}Y$.

  If $Z$ is another affine variety with maps
  $Z \stackrel{\phi_X}{\to}X, Z \stackrel{\phi_Y}{\to}Y$, we define the map
  \begin{align*}
    \phi: Z \to  W \subseteq \A^{n+m}, \quad z \mapsto (\phi_X(z),\phi_Y(Z))
  \end{align*}
  which satisfies $\pi_X \circ \phi = \phi_X$ and $\pi_Y \circ \phi = \phi_Y$

\end{proof}
\begin{rem}[]
  Since $I(X), I(Y)$ prime $\implies$ $I$ prime, it follows that if $X,Y$ are irreducible, then so is $X \times Y$.

  As a set $X \times Y$ is the cartesian product, but it has a different topology than the product topology.

  One can show that $\A^{n} \iso \A^{1} \times \ldots \A^{1}$, where $\times$ is the product in $\Aff_k$.
\end{rem}
