\subsection{Affine Varieties}

\begin{dfn}[]
An \textbf{affine variety} over $k$ is a $k$-ringed space $(W,F)$, which is isomorphic\footnote{In the sense that there are continuous maps $\phi: X \to Y$, $\psi:Y \to X$ such that $\phi^{\ast}f \in R(\phi^{-1}(U)) \forall f \in S(U)$ and $\psi^{\ast}g \in R(\psi^{-1}(V))$ for all $g \in S(V)$ that are inverses to each other.} to $(X,\mathcal{O}_X)$, for some algebraic set $X \subseteq \A^{n}$ and $\mathcal{O}_X$ is the structure sheaf of $X$.
\end{dfn}

\begin{rem}[]
Some sources use the word \emph{variety} in a more restrictive sense, which we call \emph{irreducible varieties}.
\end{rem}

\begin{prop}[]
Let $X$ be an affine variety and $Z \subseteq X$. Then $Z$ is also an affine variety.
\end{prop}
\begin{proof}
Because $X$ is an affine variety, there exists an algebraic set $W \subseteq \A^{n}$ such that $(X,\mathcal{O}_X) \iso (W,\mathcal{O}_W)$, where $\mathcal{O}_W$ is its structure sheaf.
Restricting the structure sheaf $\mathcal{O}_X$ to $W$ induces an isormorphism
\begin{align*}
  (Z,\mathcal{O}_X|_{W}) \iso (\rho(Z), \mathcal{O}_{\rho(Z)})
\end{align*}
For $\rho(Z) \subseteq \A^{n}$ an algebraic set.
\end{proof}

\begin{prop}[]
Let $X$ an affine variety and $f \in k[X]$.
Then $(D(f),\mathcal{O}_X|_{D(f)})$ is an affine variety.
\end{prop}
\begin{proof}
Assume $X = Z(a) \subseteq \A$.
Let $W = Z(a,x_{n+1} \cdot f -1) \subseteq \A^{n+1}$ which ensures that $f$ is not zero.
Using the projection map
\begin{align*}
  p: W \to D(f) \subseteq X, (x_{1}, \ldots, x_{n+1}) \mapsto (d_{1}, \ldots, x_{n})
\end{align*}
which is a polynomial map and moreover a morphism of $k$-ringed spaces.

Now, we want to find an inverse map.
We define 
\begin{align*}
  \alpha: D(f) \to W, \quad (x_{1}, \ldots, x_{n}) \mapsto \left(x_{1}, \ldots, x_{n}, \frac{1}{f(x_{1}, \ldots, x_{n})}\right)
\end{align*}
which is clearly an inverse to $p$.
For continuity, let $Z(g) \subseteq W$ be any closed set.
Then
\begin{align*}
  \alpha^{-1}(Z(g)) = \left\{
    (x_{1}, \ldots, x_{n}) \in D(f) \big\vert
    g(x_1,\ldots,x_{n},\tfrac{1}{f(x_{1}, \ldots, x_{n})}) = 0
  \right\}
\end{align*}
the equation on the right holds if and only if
\begin{align*}
  \frac{g'(x_{1}, \ldots, x_{n})}{f'(x_{1}, \ldots, x_{n})} = 0 \iff g'(x_{1}, \ldots, x_{n}) = 0
\end{align*}
for some $g',f'$ polynomial maps.
so $\alpha^{-1}(Z(g)) = Z(g')$ is closed, so $\alpha$ is continuous.

It remains to check that $\alpha$ is a map of $k$-ringed spaces i.e.\ that for $\phi \in \mathcal{O}_W(U)$ then $\alpha^{\ast}\phi \in \mathcal{O}_{D(f)}(\alpha^{-1}(U))$.
Since $\phi$ is regular, we may assume that $\phi = \frac{s}{t}$ (at least locally), in which case we get
\begin{align*}
  \alpha^{\ast} \phi = \alpha^{\ast} \tfrac{s}{t} = \frac{s(x_{1}, \ldots, x_{n},\tfrac{1}{f(x_{1}, \ldots, x_{n})})}{t(x_{1}, \ldots, x_{n},\tfrac{1}{f(x_{1}, \ldots, x_{n})})}
  = \frac{g'(x_{1}, \ldots, x_{n})}{f'(x_{1}, \ldots, x_{n})}
\end{align*}
which is regular in $\phi^{-1}U$.
\end{proof}


In algebraic geometry, we say that a space is \textbf{quasicompact} if and only its compact, but not Hausdorff.
\begin{lem}[]
Let $X$ be a noetherian space (for example, an algebraic varty) and $U \subseteq X$ open.
Then $U$ is compact.
\end{lem}
\begin{proof}
Take an infinite open cover $U = \bigcup_{i \in I}U_i$.
If there is no finite subcover, then we can pick $U_0,U_1,\ldots$ distinct open sets such that $U_{n+1} \not \subseteq \bigcup_{i=0}^{n}U_i$.
Thus we get an open chain $V_0 \subsetneq V_1 \subsetneq V_2 \ldots$ that does not stabilize $\lightning$.
\end{proof}

SO, an algebraic variety is the glueing-together of affine varieties, but we do not want to glue together too many of them.

\begin{dfn}[]
An \textbf{(algebraic) variety} $(X,\mathcal{O}_X)$ is a $k$-ringed space which is locally isomorphic to an affine variety and which is quasicompact. 
So we can write
\begin{align*}
  X = \bigcup_{i=1}^{n}U_i \quad \text{such that} \quad (U_i,\mathcal{O}_{X}|_{U_i}) \iso (X_i,\mathcal{O}_{X_i})
\end{align*}
for algebraic sets $X_i$ with structure sheaves $\mathcal{O}_{X_i}$.

We call $\mathcal{O}_X$ the \textbf{structure sheaf} of $X$.
Sections of $\mathcal{O}_X$ are called \textbf{regular functions} on $X$.
Morphisms of varieties are called \textbf{regular maps}.
\end{dfn}

\begin{rem}[]
Some sources require that varieties must be separated.
This is to exclude examples like the line with two origins:
\begin{align*}
  X = \R \sqcup \R/\sim \quad \text{where} \quad (x,1) \sim (x,2) \text{ if } x \neq 0
\end{align*}
We will however not deal with such spaces.
\end{rem}

\begin{prop}[]
Let $(X,\mathcal{O}_X)$ be an algebraic variety, $U \subseteq X$ open.
Then $(U,\mathcal{O}_U)$ is also an algebraic variety.
\end{prop}
\begin{proof}
Since we can write $X = \bigcup_{i=1}^{n}V_i$ such that $(V_i,\mathcal{O}_X|_{V_i})$ are \emph{affine} varieties.
It is enough to show that $(U \cap V_i,\mathcal{O}_{U \cap V_i})$ is an algebraic variety for all $i$.
Hence we may assume that WLOG $X$ is affine.
By the previous Lemma, $U$ is quasicompact and $\{D(g)_{g \in k[X]}\}$ forms a basis, so we can write $U = \bigcup_{i=1}^{n}D(g_i)$.

Then we use the fact that $(D(g),\mathcal{O}_{D(g)})$ is an affine variety.
\end{proof}

\textbf{Note:} If $X$ is an algebraic variety and $Z \subseteq X$ is a closed subset, it is not that obvious how we should equip $Z$ with the structure of an algebraic variety.


\begin{thm}[]
Let $(X,\mathcal{O}_X)$ be an algebraic variety, $Z \subseteq X$ a closed subset.
Then there exists a $k$-algebra subsheaf $\mathcal{O}_Z \subseteq R_Z$ such that $(Z,\mathcal{O}_Z)$ is an algebraic variety and $\iota: Z \hookrightarrow X$ is a morphism.
\end{thm}
\begin{proof}[Construction]
  For all $U \subseteq Z$ open, define
  \begin{align*}
    \mathcal{O}_Z(U) := 
    \left\{
    \text{
      \parbox{10em}{Continuous functions $f: U \to k$ which locally are restrictions of regular maps on $X$}
    }
  \right\}
    \subseteq R_Z(U)
  \end{align*}
  The rest of the proof is left as an exercise
\end{proof}

\subsection{Regular functions on open sets}

Recall from Commutative Algebra that for $R$ a ring, $f \neq 0 \in R$, the \textbf{localisation} is the universal $R$-algebra where $f$ is invertible.
Which can be written as the set
\begin{align*}
  R_f = \left\{\tfrac{a}{f^{m}} \big\vert a \in R, m \in \N\right\}_{/ \sim} \quad \text{where} \quad \tfrac{a}{f^{m}} \sim \tfrac{b}{f^{n}} \text{\ if } a f^{n} = b f^{m}
\end{align*}
It comes with the structure map $R \stackrel{\phi}{\to} R_f: a \mapsto \tfrac{a}{1}$ and it is universal in the sense that such that for any $R$-algebra $A$, there exists a bijection
\begin{align*}
  \Hom_{R\Alg}(R_f,A) &\stackrel{\sim}{\to} \{g \in \Hom_{R\Alg}(R,A) \big\vert g(f) \subseteq A^{\times}\}\\
  q &\mapsto q \circ \phi_S
\end{align*}
\begin{center}
\begin{tikzcd}[] 
  R
  \arrow[]{r}{\phi}
  \arrow[swap]{dr}{q \circ \phi}
  &
  R_f
  \arrow[]{d}{q}
  \\
  & A
\end{tikzcd}
\end{center}


\begin{thm}[]\label{thm:localisation-distinguised}
Let $X$ be an affine variety, $h \in K[X]$. Then there is a $k$-algebra isomorphism
\begin{align*}
  k[X]_h 
  &\to \mathcal{O}_X(D(h))\\
  \frac{g}{h^{m}}
  &\mapsto \left(p \mapsto  \frac{g(p)}{h^{m}(p)}\right)
\end{align*}
in particular, setting $h = 1$ gives us $\mathcal{O}_X(X) \iso k[X]$.
So regular functions on $X$ are precisely the polynomial maps.
\end{thm}

\begin{proof}
Well-definedness and injectivity follow from the following claim:
\begin{align*}
  \frac{g}{h^{m}} = 0 \text{\ in } D(h) 
  \iff
  gh = 0 \text{\ in } k[X]
\end{align*}

For surjectivity, let $f \in \mathcal{O}_X(D(h))$.
By definition, there exists an open cover $D(h) = \bigcup_{i}V_i$ such that $f|_{V_i} = \frac{g_i}{h_i}$, for some $g_i,h_i \in k[X]$.

\textbf{Step 1:}  We can assume $V_i = D(h_i)$.
Because $V_i$ is open, we can write $V_i = D(a_i)$ for $a_i \in K[X]$.
Since $D(a_i) \subseteq D(h_i)$, we have $a_i^{N_i} = h_i \cdot g_i'$ for some $N_i \in \N$ and $a_i \in k[X]$.
Hence on $D(a_i)$, it holds $f = \frac{g_i}{h_i} = \frac{g_i g_i'}{h_i g_i'} = \frac{g_i g_i'}{a_i^{N_i}}$. So replacing $g_i$ with $g_ig_i'$, and $h_i$ with $a_I^{N_i}$, we can assume that $V_i = D(h_i)$.

\textbf{Step 2:} Constructing the candidate function.
By Step 1 and since $D(h)$ is open and thus quasicompact, we have $D(h) = \bigcup_{i=1}^{n}D(h_i)$ with $f|_{D(h_i)} = \frac{g_i}{h_i}$.
On $D(h_i) \cap D(h_j)$ we have $\frac{g_i}{h_i} = \frac{g_j}{h_i}$, which by definition of the localisation means that in $k[X]$, we have
\begin{align*}
  h_i h_j(g_ih_j - g_j h_i) = 0 
  \implies
  h_i h_j^{2}g_i = h_i^{2} h_j g_i
\end{align*}
Since $D(h) = \bigcup_{i}D(h_i) = \bigcup_{i}D(h_i^{2})$ we have $Z(h) = Z(h_1^{2},\ldots,h_m^{2})$.
In other words, there exist  $b_i \in k[X], N \in \N$ such that $h^{N} = \sum_{i=1}^{m}b_i h_i^{2}$.

Now, we define the canditate function $\frac{\sum_{i}b_i g_i h_i}{N} \in k[X]$ and check (\textbf{Step 3}) that on $D(h)$, it agrees with $f$.

Pick $p \in D(h)$.
If there exists $j$ with $p \in D(h_j)$ then we have
\begin{align*}
  h_j^{2} \sum_{i}b_i g_i h_i = \sum_{i}b_i g_i h_i^{2} h_j = g_j h_j h^{N} \text{\ in } k[X]
\end{align*}
And on $D(h_j)$ $f = \frac{g_j}{h_j}$, so $f h_j  g_j$ on $D(h_j)$.
Hence on $D(h_j)$, we get 
\begin{align*}
  h_j^{2} \sum_{i}b_ig_ih_i = h_j^{2} f h^{N} \implies \sum_{i}b_i g_i h_i = f h^{N}
\end{align*}
so we get that on $D(h_j)$, $f$ is also equal to $\frac{\sum_{i}b_ig_ih_i}{h^{N}}$
\end{proof}


\begin{xmp}[]
As a non-example, take $U = \A^{2} - \{(0,0)\}$ the plane without a point.
This is not a distinguised open set, as they must at least be missing something of dimension $1$.

Then the inclusion $\iota: U \hookrightarrow \A^{2}$ an isomorphism
\begin{align*}
  k[x,y] \iso \mathcal{O}_{\A^{2}}(\A^{2}) \stackrel{\iota^{\ast}}{\to} \mathcal{O}_{\A^{2}}(U)
\end{align*}
which means we can extend holomorphic functions from $U$ to $\A^{2}$.
\end{xmp}
