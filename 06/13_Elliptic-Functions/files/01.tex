\section*{About}
The Website of the seminar is at \url{https://people.math.ethz.ch/~mschwagen/ellipticfunctions}

We follow the book ``Elliptische Funktionen und Modulformen'' by Koecher-Krieg.
In fact, many parts of the script are just translation of it.

\section{Introduction}
\subsection{Elliptic Integrals}
How did people come up with the theory of elliptic functions?

The first example comes from computing the arc length of an ellipse given by
\begin{align*}
  \frac{x^{2}}{a^{2}} + \frac{y^{2}}{b^{2}} = 1
  \quad \text{for} \quad a,b > 0,\  a \geq b
\end{align*}

For example, for $a = b = r$, this is just a circle, whose arc length ($2 \pi r$) is used to define $\pi$.

Recall that if a curve $c$ is parametrized by a function $\gamma: [a,b] \to \C$, its arc length is
\begin{align*}
  L(c) := \int_{a}^{b} \abs{\dot{\gamma}(t)}dt
\end{align*}

So to compute the arc length of an ellipse, we find a parametrisation
\begin{align*}
  \gamma: [0,2 \pi] \to \C, \quad
  t \mapsto (a \cos t, b \sin t)
\end{align*}
By symmetry, it suffices to compute the arc length for $t \in [0, \tfrac{\pi}{2}]$ and multiply the result by $4$.
The result is thus
\begin{align*}
  L(c) = 4 \cdot \int_{0}^{\tfrac{\pi}{2}} \sqrt{a^{2} \sin^{2}(t) + b^{2} \cos^{2}(t)} dt
\end{align*}
which can be simplified as follows: If we substitute $x = \sin t$, we  get
\begin{align*}
  L(c) = 4 \cdot \int_{0}^{1} \frac{1}{\sqrt{1-x^{2}}}\sqrt{a^{2} x^{2} + b^{2}(1 - x^{x})} dx
\end{align*}
Sadly, we cannot find a closed formula for these integrals.

Similarly, one may define $\pi$ by setting $a=b=1$ resulting in the integral
\begin{align*}
  4 \cdot \int_0^{1} \frac{1}{\sqrt{1 - x^{2}}}dx =: 2\pi
\end{align*}



Another example is that of the \textbf{Lemniscate}, which gives a curve that looks like the shape $\infty$
\begin{align*}
  (x^{2} + y^{2})^{2} - x^{2} - y^{2}
\end{align*}
Its arc length is given by
\begin{align*}
  4 \cdot \int_{0}^{1} \frac{1}{\sqrt{1 - t^{4}}} dt =: 2 \varpi, \quad \text{where} \quad \varpi \simeq 2.622057
\end{align*}

Generalizing, we get Fagnano's integral
\begin{align*}
  F(x) = \int_{0}^{x} \frac{1}{\sqrt{1-t^{4}}} dt, \quad \text{for} \quad x \in [0,1]
\end{align*}
which we cannot express in terms of simpler functions.

\begin{dfn}[]
An \textbf{elliptic integral} is an integral of the form
\begin{align*}
  \int R(t,\sqrt{p(t)}) dt
\end{align*}
where $R(x,y)$ is a rational function, and $p$ is a polynomial of degree $3$ or $4$ without multiple roots.
\end{dfn}

\begin{thm}[Fagnano $\sim$ 1761]
  \begin{align*}
    2 F(x) = F\left(
      2x \frac{\sqrt{1-x^{4}}}{\sqrt{1 + x^{4}}}
    \right)
  \end{align*} 
\end{thm}

People found that it is actually easier to compute not $F(x)$, but its inverse function.
One can show that $F$ is injective and continuous.
So for $\sigma = F(1)$, there exists an inverse function
\begin{align*}
  G:[0,\sigma] \to [0,1], \quad \text{with} \quad F(G(y)) = y,\ G(F(x)) = x
\end{align*}
Taking the derivative on both sides of the identities above, one can show
\begin{align*}
  (G')^{2} = 1- G^{4}, \quad G(0) = 0, \quad G'(0) = 1
\end{align*}

By applying these relations to the Theorem above, we get
\begin{align*}
  G(2u) = \frac{2G(u) G'(u)}{1 + G^{4}(u)}
\end{align*}

People later found out some nice properties of this inverse function
\begin{thm}[Euler 1761]
  The function $G$ satisfies
  \begin{align*}
    G(u+v) = 
    \frac{
      G(u)G'(v) + G(v)G'(u)
    }{
    1 + G^{2}(u)G^{2}(v)
  }
  \end{align*}
\end{thm}
Using the differential equations of $G$, we can extend $G$ to a meromorphic function on $\C$ which satisfies
\begin{align*}
  G(iu) = i G(u)
\end{align*}
moreover, it is \emph{doubly periodic}.
For $v \in \C$ with $G'(v) = 1$, then $G(v) = 0$, in which case we get
\begin{align*}
  G(u + v) = 
    \frac{
      G(u)G'(v) + G(v)G'(u)
    }{
    1 + G^{2}(u)G^{2}(v)
  }
  = G(u)
\end{align*}
aswell as
\begin{align*}
  G(u + iv) = G(u)
\end{align*}

so it is periodic with respect to the vectors $v$ and $iv$.

This motivated mathematicians to study such doubly periodic functions.

\begin{dfn}[]
  A meromorphic function $f$ on $\C$ is called \textbf{elliptic} (or doubly periodic), 
  if there exist periods $\omega_1, \omega_2 \in \C$ which are linearly independent (over $\R$) such that
  \begin{align*}
    f(u + \omega_1) = f(u) 
    =
    f(u + \omega_2) \quad \text{for all} \quad u \in \C
  \end{align*}
\end{dfn}
In particular, such a function satisfies
\begin{align*}
  f(u + \omega) = f(u), \quad \text{for all} \quad w \in \Z \omega_1 + \Z \omega_2
\end{align*}
Such a set $\Z \omega_1 + \Z \omega_2$ is called a lattice.


\section{Periods and lattices}
\begin{dfn}[]
A function $f: \C \to  \C \cup \{\infty\}$ is \textbf{meromorphic} if there exists a closed discrete subset $D_f \subseteq \C$ such that
\begin{enumerate}
  \item $f: \C \setminus D_f \to \C$ is holomorphic
  \item $f$ has poles at the points in $D_f$.
\end{enumerate}
\end{dfn}
Recall some facts from complex analysis.

A subset $D \subseteq \C$ is called discrete, if for every $d \in D$, there exists a neighborhood $U$ of $d$ such that $U \cap D$ is finite. (Since $\C$ is Hausdorff, we can also require that $U \cap D = \{d\}$.)

If $f \neq 0$ is meromorphic on $\C$, then for each point $c \in \C$, there exists a neighborhood $U$ of $c$ such that $f$ has a Laurent-series expansion of the shape
\begin{align*}
 f(z) = \sum_{n = n_0}^{\infty} a_{f,c}(n) {(z - c)}^{n}
\end{align*}
on $U \setminus \{c\}$ with $n_0 \in \Z, a_{f,c}(n) \in \C, a_{f,c}(n_0) \neq 0$.
We call $\ord_c(f) := n_0$ the \textbf{order} of $f$.

If $n_0$ is positive, $f$ has a \textbf{root} of order $n_0$ at $c$.
If $n_0$ is negative, $f$ has a \textbf{pole} of order $\abs{n_0}$ at $c$.
The \textbf{residue} of $f$ at $c$ is defined as
\begin{align*}
  \res_c(f) := a_{f,c}(-1)
\end{align*}

The collection of meromorphic functions form a field.

\begin{dfn}[]
Let $f$ be meromorphic on $\C$.
$\omega \in \C$ is called a \textbf{period} of $f$ if 
\begin{align*}
  f(z + \omega) = f(z) \quad \text{for all} \quad z \in \C
\end{align*}
we denote the set of periods of $f$ with $\Per(f)$.
\end{dfn}
$\Per(f)$ is a subgroup of $(\C,+)$. 

\begin{dfn}[]
If $f$ is non-constant meromorphic, then $\Per(f)$ is a closed and discrete subgroup of $\C$.
\end{dfn}
\begin{proof}
Assume it is not closed or not discrete.
In either case, there is a sequence $\omega_n \in \Per(f)$, such that the limit $\lim_{n \to \infty} \omega_n =: \omega$ exists and $\omega \notin \Per(f)$.

But by continuity of $f$, it would follow
\begin{align*}
  f(z + \omega) 
  = 
  \lim_{n \to \infty} f(z + \omega_n) = \lim_{n \to \infty} f(z) = f(z), \quad \implies \omega \in \Per(f) \lightning
\end{align*}
\end{proof}


\begin{lem}[]
If $f$ is non-constant meromorphic, then precisely one of the three following occurs:

\begin{enumerate}
  \item $\Per(f) = \{0\}$
  \item There exists a (up to sign unique) $\omega_f \in \C \setminus \{0\}$ such that
    \begin{align*}
      \Per(f) = \Z \omega_f = \{n \omega_f \big\vert n \in \Z\}
    \end{align*}
  \item There exist $\omega_1,\omega_2 \in \C \setminus \{0\}$ linearly independent (over $\R$) with
    \begin{align*}
      \Per(f) = \Z \omega_1 + \Z \omega_2
    \end{align*}
    and they can be chosen such that 
    $\tau := \tfrac{\omega_1}{\omega_2}$ satisfies
    \begin{align*}
      \image (\tau) > 0, \quad \abs{\text{Re}(\tau)} \leq \tfrac{1}{2}, \quad \abs{\tau} \geq q
    \end{align*}
\end{enumerate}
\end{lem}
\begin{proof}
  Assume $\Per(f) \neq \{0\}$.
  Since $\Per(f)$ is closed and discrete, there exists a $\omega_f \in \Per(f)$ with
  \begin{align*}
    0 < \abs{\omega_f} = \min \{\abs{\omega} \big\vert \omega \in \Per(f)\}
  \end{align*}
  the minimum is attained, or else we would get that $0 \in \Per(f)$ is an accumulation point.
  
  Then consider the line $\R \omega_f$ from the origin through $\omega_f$. 
  Clearly, $\Z \omega_f = \Per(f) \cap \R \omega_f$.

  If $\Z \omega_f = \Per(f)$, we are in case (b) and we are done.
  If that is not the case, chose $\omega_1 = \omega_f$ and chose $\omega_2 \in \Per(f) \setminus \Z \omega_1$ such that
  \begin{align*}
    0 < \abs{\omega_2} = \min \{\abs{\omega} \big\vert \omega \in \Per(f) \setminus \Z \omega_1\}
  \end{align*}
  
  We then claim that $\omega_1,\omega_2$ generate $\Per(f)$.
  If $\omega$ were not generated by the two, we could shift $\omega$ by multiples of $\omega_1$ and $\omega_2$ until we land somewhere closer to the origin than $\omega_1,\omega_2$, which contradicts the construction of $\omega_1,\omega_2$.
\end{proof}

\begin{xmp}[]
\begin{itemize}
  \item The polynomial $f(z) = z^{5} + 3i z$ is not periodic.
  \item The function $f(z) = e^{2 \pi i z}$  has period $\Per(f) = \Z$.
  \item The function XXX is doubly periodic with $\Per(f) = \Z  +\Z i$.
\end{itemize}
\end{xmp}

