from sage.plot.complex_plot import complex_plot
# See https://doc.sagemath.org/html/en/reference/plotting/sage/plot/complex_plot.html
f = lambda z: exp(z)
# complex_plot(function, xrange, yrange)
complex_plot(f, (-2,2), (-2,2))
