#+TITLE: Homotopy Type Theory
#+AUTHOR: Han-Miru Kim
#+STARTUP: OVERVIEW


* Compilation
To compile the file, either edit the main.tex file directly, or run
#+BEGIN_SRC shell
make all
#+END_SRC
and pick the pdf from the ~output~ directory.
** Dependencies:
- GNU make
- latexmk
- TeX installation
  




