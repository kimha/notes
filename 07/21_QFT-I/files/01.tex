\section{Introduction}
We will follow the book ``Quantum Field Theory'' by Mark Srednicki very closely. Pointers to missing things will refer to this book.

One might ask why quantum mechanics is not enough to describe relativistic particles.
It appears as if nowhere in in the Schrödinger equation do whe make any assumptions that do not hold for relativistic particles.
The Schrödinger equation however uses the Hamiltonian
\begin{align*}
  \hat{H} = \frac{\hat{p}^{2}}{2m}, \quad \text{for} \quad \hat{p} = - i \hbar \frac{\del}{\del x}
\end{align*}
and hidden in the Hamiltonian is the assumption of non-relativity creeps in.
Recall from SRT, that the energy of a relativistic particle is
\begin{align*}
  E = \sqrt{m^{2} c^{4} + p^{2}c^{2}}
\end{align*}
If we were to turn this into a relativistic Hamilton operator $\hat{H}_{\text{rel}}$, one would have to find a way to find the square root of an operator.
One approach would be to use the Taylor-series expansion
\begin{align*}
\sqrt{1+x} = 1 + \frac{1}{2}x + \mathcal{O}(x^2)
\end{align*}
and define
\begin{align*}
  \hat{H}_{\text{rel}} := mc^{2} + \frac{1}{2m}\hat{p}^{2} + \mathcal{O}(\hat{p}^{4})
\end{align*}
and then solve for the relativistic Schrödinger equation
\begin{align*}
  i \hbar \del_t \Psi(x,t) = \hat{H}_{\text{rel}}(x,t)
\end{align*}
However, this approach will not work well.
Because we have arbitrary high powers of $\hat{p}$, we need to know arbitrary high derivatives.
But in order for this to make sense, we may only consider analytic wave functions.
The problem with this is that in order to compute arbitrary high derivatives, we must know what the function looks like globally.
This is very inconvenient because when studying a particle in a lab, we do not wish to necessitate information about its wave function on the opposite side of the earth, on the moon or locations ever farther away.


There is also another reason why quantum mechanics doesn't translate well into the relativistic setting:

Consider two reference frames belonging to Alice and Bob.
For some object $P$, Alice will describe it with coordinates $x^{\rho}(P)$, whereas Bob will use coordinates $\overline{x}^{\rho}(P)$.

To convert from one coordinate to the other, we look at the posion $b^{\rho}$ of Bob relative to Alice, aswell as a Lorentz transformation $\tensor{\Lambda}{^{\sigma}_{\rho}}$ and can write
\begin{align*}
  \overline{x}^{\rho}(P) = b^{\rho} + \tensor{\Lambda}{^{\sigma}_{\rho}} x^{\rho}(P)
\end{align*}

To deal with with wave functions, where the position of a particle is not well defined we instead consider a region $R$ around $P$.
For example, if Alice is certain that the particle is contained in $R$, then Bob must also be certain about that.

We then compute the probability to find the probability that the particle is in some arbitrary region. 
Alice would calculate
\begin{align*}
  \IP[\vec{X} \in R] = \int_R d \vec{x} \abs{\psi(\vec{x})}^{2}
\end{align*}
and whatever Bob would calculate must give the same result 
\begin{align*}
  \IP[\vec{X} \in R] = \int_R d \vec{\overline{x}} \abs{\overline{\psi}(\vec{\overline{x}})}^{2}
\end{align*}
Next, we make the region $R$ arbitrarily small, turning it into an $\epsilon$-Ball.
With some abuse of notation, we can express this fact as follows:
\begin{align*}
  \abs{\psi(\vec{x})} \stackrel{!}{=} \abs{\overline{\psi}(\vec{\overline{x}})} =
  \abs{\overline{\psi}(\vec{b} + \Lambda \vec{x})}
\end{align*}

It is natural to ask if this requirement is fulfilled in quantum mechanics.
Consider an $n$-particle wave function $\ket{\psi^{(n)}}$
\begin{align*}
  \psi^{(n)}(\vec{x}_{1}, \ldots, \vec{x}_{n} ) = \braket{\vec{x}_{1}, \ldots, \vec{x}_{n}| \psi^{(n)}}
\end{align*}
then the probability that each particle $X_i$ is contained in a region $R_i$ is
\begin{align*}
  \IP[\vec{X}_1 \in R_1, \ldots, \vec{X}_n \in R_n]
  =
  \int_{R_1} d \vec{x}_1
  \ldots
  \int_{R_n} d \vec{x}_n
  \abs{\psi^{(n)}(\vec{x}_{1}, \ldots, \vec{x}_{n})}
\end{align*}
Lorentz invariance for $n$-particles would mean that for any Lorentz transformation
\begin{align}\label{eq:lorentz-invariance-multi-particle}
  \abs{\psi^{(n)}(\vec{x}_{1}, \ldots, \vec{x}_{n})}
  =
  \abs{\overline{\psi}^{(n)}(\vec{\overline{x}}_{1}, \ldots, \vec{\overline{x}}_{n})}
  \quad \text{for} \quad 
  \vec{\overline{x}}_i = \vec{b} + \Lambda \vec{x}_i
\end{align}

Another reason why we do not want to use QM for relativistic situations is that the number of particles is not a universally agreed upon number.

Assume we have two processes, where one particle is created and another is annihilated.
Further assume that for Alice, the two processes happen simultaneously.
She then sees that at any time, only one particle exists and can use a one-particle wave-function $\psi^{(1)}(\vec{x}(t))$ to describe this situation.

Bob however has other time slices, and so could see that at some point in time $t = s$, there existed two particles simultaneously.

Bob would then first use a one-particle function $\overline{\psi}^{(1)}(\vec{x},t)$ for $t$ much earlier than $s$, then a two-particle function $\overline{\psi}^{(2)}(\vec{x}_1,\vec{x}_2,t)$ for $t$ around $t=s$ (and then back to a one-particle function again.)

In QM-II, we saw that we can describe the state for Bob as a single state in the Fock space
\begin{align*}
  \ket{\Phi} = c_0 \ket{\psi^{(1)}} + c_1 \ket{\psi^{(1)}} + c_2 \ket{\psi^{(2)}} + \ldots
\end{align*}

The difficulty lies in how one would describe Lorentz invariance in the Fock space.
Instead of having simple relations as in~\ref{eq:lorentz-invariance-multi-particle}, one would have mixed relations between $c_i$ and $\overline{c}_j$ even for $i \neq j$.



How do we remedy this? Instead of considering particles and then quantizing them, we throw out the particles and start from a field, quantize that and then ask questions about particles as an emergent notion from studiying the field.

It is from this viewpoint that quantum \emph{field} theory gets its name.


\begin{center}
``I need to tell you a few things that I forgot about.
First, I need to introduce myself.''

- R. Renner at the end of the first lecture September 22nd, 2022.
\end{center}
