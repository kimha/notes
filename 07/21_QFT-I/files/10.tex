\subsection{Interacting field}
For ease of notation, we will do an implicit integration over the variables, so in the case of the partition function for the free field, we would write
\begin{align*}
  Z_0[J] 
  &= \braket{0|0}_J = \int \mathcal{D}_{\phi} e^{i \int d^{4} x (\mathcal{L}_0 + J \phi)}
  =
  e^{i \tfrac{1}{2} \int d^{4}y \int d^{4}z J(y) \Delta(y-z) J(z)}
  \\
  &=
  e^{i \tfrac{1}{2} J_y \Delta_{yz} J_z}
\end{align*}
Recall that this partition function is useful becaause it allows us to compute the correlation function
\begin{align*}
  \braket{0|T \phi(x_1) \cdots \phi(x_n)|0}
  = \left(
    \tfrac{1}{i} \frac{\delta }{\delta J(x_1)} \cdots \tfrac{1}{i} \frac{\delta }{\delta J(x_n)} Z_0[J]
  \right)|_{J=0}
\end{align*}

Now, we want to generalize the Lagrangian $\mathcal{L}$ for an interacting theory.
\begin{align*}
  \mathcal{L} = \mathcal{L}_0 + \mathcal{L}_1 
  \quad \text{for} \quad \mathcal{L}_1 = \tfrac{g}{3!}\phi
\end{align*}

Later on, we will consider consider the Lagrangian
\begin{align*}
  \mathcal{L} = - \tfrac{1}{2} Z_{\phi} \del_{\mu} \del^{\mu} \phi
  - \tfrac{1}{2} Z_m m^{2} \phi^{2} + \tfrac{1}{3!} Z_g g \phi^{3} + Y \phi
  \quad \text{for} \quad Z_{\phi},Z_m, Z_g, Y \in \R
\end{align*}

The second term will be identified with the spectrum of the free field hamiltonian
\begin{align*}
  H_0 = \int \tilde{dk} \omega a^{\dagger}(\vec{k}) a(\vec{k})
  \quad \text{for} \quad \omega = \sqrt{\vec{k}^{2} + m^{2}}
\end{align*}
which looks like a parabola in the $\vec{k}-E$ diagram at height $E_1 = m$.


If we add a $\tfrac{1}{3!}g \phi^{3}$ (without $Z_g$), then under small disturbances, the Eigenenergies change such that $E_1 - E_0 \neq m$.
By adding a factor $Z_g$ we ensure that $E_1 - E_0 = m$.


Then, we split off the \textbf{counterterm} Lagrangian
\begin{align*}
  \mathcal{L}_{\text{ct}} = - \tfrac{1}{2}(Z_{\phi} - 1) \del^{\mu} \phi \del_{\mu} \phi - \tfrac{1}{2}(Z_m - 1) m^{2} \phi^{2} + Y \phi
\end{align*}
which we will ignore for now, so that our Lagrangian becomes
\begin{align*}
  \mathcal{L} = \mathcal{L}_0 + \mathcal{L}_1 (+ \mathcal{L}_{\text{ct}}) \quad \text{for} \quad \mathcal{L}_1 = \tfrac{1}{!} Z_g g \phi^{3}
\end{align*}
Now, one might protest that the lagrangian has a cubic term, which doesn't usually have a well-defined ground state.
We can motivate this by starting with a more complicated lagrangian that has a ground state, and then we compute its Taylor series up to third order as an approximation.

So now, let's calculate the partition function.
By the path integral formula, we get
\begin{align*}
  Z[J]
  &=
  \braket{\Omega|\Omega}_J
  =
  \int \mathcal{D} \tilde{\phi} \exp\left[
    i \int d^{4} (\mathcal{L} + J \phi)
  \right]
  \\
  &=
  e^{i \int d^{4} x \mathcal{L}_1(\tfrac{1}{i} \frac{\delta }{\delta J(x)}}
\end{align*}

Note that the integral term is proportional to the partition function of the free field $Z_0[J]$ up to some normalisation factor $\mathcal{N}$ that describes the change of the measure $\mathcal{D} \phi \mapsto \mathcal{D} \tilde{\phi}$.

Thus we have
\begin{align*}
  Z[J]
  &=
  \mathcal{N}
  e^{i \int d^{4} x \mathcal{L}_1(\tfrac{1}{i}\frac{\delta }{\delta J(x)})} Z_0[J]
\end{align*}
the normalization constant can be fixed by considering the fac that the transition amplitude of the ground state into itself should be one, i.e.\ $Z[0] = 1$.

Now we evaluate the interacting Lagrangian term and get
\begin{align*}
  \mathcal{L}_1\left(
    \tfrac{1}{i} \frac{\delta }{\delta J(x)}
  \right)
  =
  \frac{\tilde{g}}{3!}
  \left(
    \tfrac{1}{i} \frac{\delta }{\delta J(x)}
  \right)^{3}
\end{align*}
Doing the Taylor series expansion, we get
\begin{align*}
  Z[J]
  &=
  \mathcal{N}
  \sum_{v = 0}^{\infty}
  \tfrac{1}{V!}
  \left[
    \tfrac{i \tilde{g}}{3!}
    \int d^{4}x \left(
      \tfrac{1}{i} \frac{\delta }{\delta J(x)}
    \right)^{3}
  \right]
  \cdot
  \sum_{p=0}^{\infty}
  \tfrac{1}{P!}
  \left[
    \tfrac{i}{2}
  \int d^{4} y \int d^{4}z J(y) \Delta(y-z) J(z)
  \right]^{P}
\end{align*}
with some calculation, this can be simplified into
\begin{align*}
  Z[J]
  &=
  \mathcal{N}
  \sum_{V=0}^{\infty} \sum_{P=0}^{\infty}
  i ^{V - 3V + P}
  \tfrac{1}{V!}
  \tfrac{1}{P!}
  \left[
    \tfrac{\tilde{g}}{3!}
    \delta_x^{3}
  \right]^{V}
  \left[
    \tfrac{1}{2} J_y \Delta_{yz} J_z
  \right]^{P}
\end{align*}
, where $\delta_x$ is shorthand for $\frac{\delta }{\delta J(x)}$.

Consider the term for $V = P = 0$. It will give some constant.

For $P = 0$ and $V > 0$, the term will vanish.
In general, one show sthat we get zero contribution when
\begin{align*}
  E = 2P - 3 V < 0
\end{align*}

Now consider the case $V = 0, P=1$ (so $E = 2$).
We then get a contirbution
\begin{align*}
  \tfrac{1}{2} J_y \Delta_{yz} J_z
\end{align*}
which can be visualized as a source $J_y$, a propagator $\Delta_{yz}$ and another surce $J_z$.

The case $V = 0, P = 2$ (and $E= 4$) gives the term
\begin{align*}
  \text{const.} \cdot
  J_w \Delta_{wu} J_u
  J_y \Delta_{yz} J_z
\end{align*}



Other terms for $V = 0$ should account for the terms that neglect the interacting term $\mathcal{L}_1$, i.e.\ they shouldresult in non-interacting diagrams.

We see that $E$ counts the number of sources, and $p$ the number of propagators.
\begin{center}
Missing drawings
\end{center}




\begin{rem}[]
If we have a functional derivative acting on an integral
\begin{align*}
  \delta_x J_y \Delta_{yz} J_z
  =
  \int d^{4}x \frac{\delta }{\delta J(x)}
  \int d^{4}y d^{4}z
  J(y) \Delta(y-z)
  J(z)
\end{align*}
then the functional derivative will eliminate the integral because it only picks out the single element from the integration region, so
\begin{align*}
  \delta_x J_y \Delta_{yz} J_z
  = \int d^{4} x \int d^{4}z 2 \Delta(x-z) J(z)
\end{align*}
so we get the simplification rule
\begin{empheq}[box=\bluebase]{align*}
  \delta_x J_y \Delta_{yz} J_z
  =
  2 \Delta_{xz} J_z
\end{empheq}
\end{rem}

\begin{dfn}[Diagrammatic Notation]
  For a source $i \int d^{4}y J(y)$, we draw a filled circle and label its edge with $y$.

  For a propagator $\tfrac{1}{i} \Delta(y-z)$, we draw a straight line connecting $y$ and $z$.

  For a vertex $i \tilde{g} \int d^{4} x$, we write a labelled vertex.

  This explains the names $V,P,E$ which stand for Vertex, Propagator and source.
\end{dfn}
