Since we are only using infinitesimal transformations, we can only obtain those that are in the same path connected component of the identity.
This component defines the subgroup of the Lorentz group $O(1,3)$ called the \textbf{proper orthochronous Lorentz group}
\begin{align*}
  SO^{+}(1,3) = \{\Lambda \in O(1,3) \big\vert \det \Lambda = 1, \tensor{\Lambda}{^{0}_0} \geq 1\}
\end{align*}

For an infinitesimal transformation, $1 + \delta \omega$, let $\tensor{M}{^{\mu}^{\nu}}$ be a generator in the Lie algebra.
\begin{align*}
  U(1 + \delta \omega) = I + \frac{i}{2 \hbar}\delta \omega_{\mu \nu}M^{\mu \nu}
\end{align*}
After some calculations (TODO:\ add them here), we obtain the commutation relations
\begin{align*}
  [M^{\mu \nu}, M^{\rho \sigma}] = i \hbar (g^{\mu \rho} M^{\nu \sigma} - (\mu \leftrightarrow \nu))
  - (\rho \leftrightarrow \sigma)
\end{align*}
We now define the angular momentum operator $J$ and the boost operator $K$ via
\begin{align*}
  J := \tfrac{1}{2} \epsilon_{ijk} M^{jk}
  \quad \text{and} \quad 
  K_i := M^{i0}
\end{align*}
From these, we obtain the commutation relations
\begin{align*}
  [J_i,J_j] = i \hbar \epsilon_{ijk} J_k,
  \quad
  [J_i,K_j] = i \hbar \epsilon_{ijk} K_k,
  \quad
  [K_i,K_j] = - i \hbar \epsilon_{ijk}J_k
\end{align*}

\subsubsection*{Translations}
We consider two reference frames with shared orientation.
Suppose Alice's origin has coordinates $\overline{a}$ from Bob's point of view.
If Alice assigns coordinates $x^{\mu}(p)$ for some point $p$, then Bob will assign coordinates
\begin{align*}
  \overline{x}^{\mu}(p) = x^{\mu}(p) + \overline{a}^{\mu}
\end{align*}

If now we have a field $\phi$ that has some strength $\phi(p)$ at the point $p$, then Alice will describe the field in terms of the Alice's Coordinates $\phi = (x \mapsto  \phi(x))$.
Bob will use his coordinates to describe the same field $\overline{\phi} = (\overline{x} \mapsto  \overline{\phi}(\overline{x}))$.
As both are talking about the same field, it must hold $\phi(x(p)) = \overline{\phi}(\overline{x}(p))$. So Bob's field can be described via Alice's field:
\begin{align*}
  \overline{\phi}(\overline{x}) = \phi(\overline{x} - \overline{a})
\end{align*}
If we now move from a classical field to a quantum field (as in~\ref{dfn:quantum-field}) will assign to each point some operator in some algebra.
Assume we instruct both Alice and Bob to go to their origin and measure the corresponding operator.
Alice will measure some operator $W = \phi(x)$, and Bob as another operator $\overline{W} = \overline{\phi}(x)$.
Note how here, we don't put a bar over $x$, because we want to forget the geometric meaning. It is only $\phi$ that obtains a bar here. 
Moreover, unlike in the classical case, the coherence condition isn't that these operators are exactly the same.
Alice and Bob both do field theory, and may use different operators to do so, but all operators appearing in their field theory will carry the same structure, so there will be an algebra isomorphism that translates Alice's operators to Bob's operator.
Since a change in phase describes the same physical situation, their operators might differ by unitary transformations, so we want
\begin{align*}
  \overline{W} = U^{-1}WU \quad \text{for some} \quad U \text{\ unitary}
\end{align*}
It then follows that the fields must satisfy
\begin{align*}
  \overline{\phi}(x) = U^{-1}\phi(x) U
\end{align*}
Of course, this transformation $U$ will depend on the translation parameter $a$, so let's write $U = T(a)$.
Then for some infinitesimal translation $\delta a$, we can write the corresponding transformation using a generator $P^{\rho}$:
\begin{align*}
  T(\delta a) = I - \tfrac{i}{\hbar} \delta a^{\mu} P^{\rho} g_{\mu \nu}
  + \mathcal{O}(\delta^{2})
\end{align*}

We then define the Hamiltonian $H$ and the $3$-momentum $\vec{P}$ via
\begin{align*}
  H = P^{0}, \quad \text{and} \quad \vec{P} = {(P^1,P^2,P^3)}^{T}
\end{align*}

Then we can break down any transformation $T(a)$ into the composition of smaller and smaller transformations
\begin{align*}
T(a) = \lim_{N \to \infty} T\left({\left(\frac{1}{n}-a\right)}^{N}\right) = e^{- \tfrac{i}{\hbar}a^{\mu}P_{\mu}}
\end{align*}

Let's see what happens when we take an infinitesimal translation in time direction: ($\delta a = {(\delta t,0,0,0,)}^{T}$).


Then our coherence condition becomes
\begin{align*}
  \phi(x - \delta a) 
  &= T{(\delta a)}^{-1} \phi(x) T(\delta a)
  \\
  &= (I - \tfrac{i}{\hbar}\delta t H)
  \phi(x)
  (I + \tfrac{i}{\hbar}\delta t H)
  + \mathcal{O}(\delta^{2})
\end{align*}

This coherence condition will be satisfied if and only if
\begin{align*}
  \frac{\phi(x) - \phi(x - \delta a)}{\delta t} = \tfrac{i}{\hbar} H \phi(x) - \tfrac{i}{\hbar} \phi(x) H + \mathcal{O}(\delta^{2})
\end{align*}
which, in the limit $\delta t \to 0$ becomes the Schrödinger equation for quantum fields
\begin{align*}
  \dot{\phi}(x) = \tfrac{i}{\hbar}[H, \phi(x)]
\end{align*}


\section{Canonical Quantization of scalar field}
We can shift from the forces-acting-on-particles viewpoint to the Lagrangian viewpoint by introducing a suitable lagrangian that describes all mechanics by introducing a suitable lagrangian that describes all mechanics of a system.

We will take the Lagrangian density
\begin{align*}
  \mathcal{L} = - \tfrac{1}{2} \del^{\mu} \phi \del_{\mu} \phi - \tfrac{1}{2}m^{2} \phi^{2} + \Omega_0
\end{align*}
and define the action
\begin{align*}
  S = \int d^{4}x \mathcal{L} = \int dx^{0}L
\end{align*}
, where $L = \int d^{3} x \mathcal{L}$ is the lagrangian.

Since spacetime measure $d^{4}x$ is Lorentz invariant, it must be the case that the lagrangian density $\mathcal{L}$ must be a Lorentz scalar: $\mathcal{L}(x) = \overline{\mathcal{L}}(\overline{x})$.

\begin{center}
``You can not ask where the Lagrangian comes from. Well, you can ask where it comes from, it comes from experiments, but you can not ask me'' --- R. Renner, October $6^{\text{th}}$ 2022.
\end{center}

By the principle of extremal action for variations $\delta q$ of the field with $\delta \phi(x) \stackrel{\abs{x}  \to  \infty}{\to} 0$\footnote{This restriction can be explained, but this is not part of the lecture.}, it follows from the product rule that
\begin{align*}
  0
  &=
  \delta S[\phi]\\
  &= \int d^{4}x\left[
  - \tfrac{1}{2} \del^{\mu} \delta\phi \del_{\mu} \phi -  \tfrac{1}{2}\del^{\mu} \phi \del_{\mu}\delta \phi - m^{2} \phi \delta \phi
  \right]
\end{align*}

Doing integration by parts in the first two terms leaves us with vanishing boundary terms (because $\delta \phi(x) \to 0$ as $\abs{x} \to 0$), so we are left with the condition
\begin{align*}
  0 &= \int d^{4}x \left(
    \del^{\mu}\del_{\mu} \phi - m^{2} \phi
  \right)
  d \phi
\end{align*}
Since $d \phi$ is arbitrary, this is true if and only if
\begin{align*}
  (\del^{2} - m^{2})\phi = 0
\end{align*}
this equation is called the \textbf{Klein-Gordon} equation.

A simple solution to the Klein-Gordon equation is a plane wave
\begin{align*}
  \exp\left[
    i \vec{k \cdot \vec{x} \pm i \omega t}
  \right]
  \quad \text{for} \quad \vec{k} \in \R^{3},
  \quad
  \omega = \sqrt{k^{2} + m^{2}}
\end{align*}

Define
\begin{align*}
  a(\vec{k}) := i \int d^{3}x e^{-i \vec{k} \cdot \vec{x}}
\end{align*}

Then
\begin{align*}
  \phi(\vec{x},t) = \int d \tilde{k} \left[
    a(\vec{k})^{ikx} + a^{\ast}(\vec{k}) e^{-ikx}
  \right]
  \quad \text{where} \quad 
  d \tilde{k} := \frac{d^{3} k}{(2 \pi)^{3} 2 \omega}
\end{align*}
the factor $\omega := \sqrt{m^{2} +k^{2}}$ is necessary in order to have lorentz-invariance.
