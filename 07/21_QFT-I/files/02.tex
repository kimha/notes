\subsection{Parallels to earlier frameworks}
A \textbf{classical field} is an assignment (of a scalar, vector, 2-tensor etc.) to every point in spacetime.

A familiar example comes from electrodynamics. The assignment
\begin{align*}
  F: x^{\rho} \mapsto (\tensor{F}{_\mu_\nu}(x)) = -\begin{pmatrix}
  0 & E_1 & E_2 & E_3\\
  -E_1 & 0 & B_3 & -B_2\\
   -E_2& -B_3 & 0 & B_1\\
   -E_3& B_2  & -B_1 & 0
  \end{pmatrix}(x)
\end{align*}
describes the electromagnetic field, where the constrains are given by the maxwell equations
\begin{align*}
  dF = 0, \quad \text{and} \quad d \ast F = 0
\end{align*}

There is an alternatve formulation, which instead of having a field which is a $2$-form, we use a covector field
\begin{align*}
  A: (x^{\mu}) \mapsto (A_{\mu}(x)) = \begin{pmatrix}
  -\varphi\\
  A_1\\
  A_2\\
  A_3
  \end{pmatrix}
  (x)
\end{align*}
and only place the constraint $d \cdot dA = 0$ on $A$.
To recover the other formalisation, we just take $F = dA$.


Note that in this formulation, there is never any talk about particles and they will only emerge when we quantize the field.

In Electrodynamics, we have a potential $A$, that satisfies the \textbf{Maxwell equation}
\begin{align*}
  d \ast dA = 0
\end{align*}
which, when quantized gives us a massless spin-$1$ particle (the photon).

the photon (a massless spin-1 particle) is an excitation of the electromagnetic field.
The general idea is that any fundamental particle (electrons, neutrino) is an \emph{excitation} of a corresponding field.


We will later define the Dirac spinor field $\psi$, which satisfies the \textbf{Dirac equation}
\begin{align*}
  (i \slashed{\del} -m )\psi(x) = 0
\end{align*}
which, wenn quantized gives us a spin-$\tfrac{1}{2}$ particle with mass (the electron).

Lastly, we can consider a scalar field $\phi$, which satisfies the \textbf{Klein-Gordon} equation
\begin{align*}
  (-\del^{2} - m^{2}) \phi(x) = 0
\end{align*}
which, when quantized gives us the Higgs boson.


\begin{dfn}[]\label{dfn:quantum-field}
A \textbf{quantum field} is an assignment of an operator-valued tensor to each point in spacetime.
\begin{align*}
  (\hat{\phi}): \R^{4} \to \mathcal{A}, \quad (x^{\mu}) \mapsto \hat{\phi}(x)
\end{align*}
where $\mathcal{A}$ is some space of operators on a Hilbert space
\end{dfn}
The strucure on the space $\mathcal{A}$ of operators is usually specified in terms of commutation relations.
Contrast this with the finite-dimensional case, where we look at matrices and the commutation relations are something we can calculate with the matrix indices.
In the infinite-dimensional case, writing out ``all inifinite rows'' is unhandy, and we are only interested in the commutation relations.

For example, one might write
\begin{align*}
[\phi(\vec{x},t), \dot{\phi}(\vec{x}',t)] = i \delta(x - x')\id
\end{align*}
where $\dot{\phi}$ is defined as
\begin{align*}
  \lim_{\epsilon \to 0} \frac{1}{\epsilon}[\phi(x,t+\epsilon) - \phi(\hat{x},t)]
\end{align*}

We usually work in the Heisenberg picture, where the equation of motion is
\begin{align*}
  \frac{\del }{\del t}\phi(x) = \frac{i}{\hbar} [H, \phi(x)]
\end{align*}
, where $H$ is a suitable Hamiltonian.
In this picture, the operator evolves in time via
\begin{align*}
  \phi(x,t) = e^{\frac{i}{\hbar}H t} \phi(x,0) e^{-\frac{i}{\hbar}Ht}
\end{align*}

\begin{center}
``I have to identify you as a being, which should be no problem for you.
- R. Renner 2022-09-26''
\end{center}


\section{Section 1 of Srednicki}
Consider as an example a non-relativistic quantum field theory $(x \mapsto a(x))$, where $a(x)$ is a (not-necessarily hermitian) operator.
We now specify the commutation relation. Using the notation $a(\vec{x}) = a(0,\vec{x})$, we set
\begin{align*}
  [a(\vec{x}),a(\vec{x}')] = 0
  ,\quad
  [a^{\dagger}(\vec{x}),a^{\dagger}(\vec{x}')] = 0
  ,\quad
  [a(\vec{x}), a^{\dagger}(\vec{x}')] = \delta(\vec{x} - \vec{x}') \id
\end{align*}
and accompany this with the hamilton operator
\begin{align*}
  H^{\text{QFT}} = \int d^{3}x a^{\dagger}(\vec{x}) \left(
    - \frac{\hbar^{2}}{2m} \nabla^{2} + U(\vec{x})
  \right)
  a(\vec{x})
  +
  \int d^{3}x d^{3}y V(\vec{x}-\vec{y}) a^{\dagger}(\vec{x}) a^{\dagger}(\vec{y}) a(\vec{y})a(\vec{x})
\end{align*}

And then define the vacuume state $\ket{0}$ by
\begin{align*}
  a(\vec{x}) \ket{0} = 0, \quad \forall \vec{x}
\end{align*}
the time evolution of the field is then given by
\begin{align*}
  a(x) = a(\vec{x},t) = e^{\frac{i}{\hbar}H t} a(\vec{x}) e^{- \frac{i}{\hbar}Ht}
\end{align*}

\textbf{Claim:} There is an equivalence to non-relativistic quantum mechanics.
In other words, all non-relativistic $n$-particle situations can be reduced to a quantum field theory.

Indeed, let $\psi_s^{\text{QM}}(\vec{x}_{1}, \ldots, \vec{x}_{n},t)$ be an $n$-particle wave function.

We can describe this as the quantum field 
\begin{align*}
  \ket{\psi,t}_s^{\text{QFT}}
  := \int d^{3}x_{1}, \ldots, d^{3}x_{n}
  \psi(\vec{x}_{1}, \ldots, \vec{x}_{n},t) a^{\dagger}(\vec{x}_1) \cdots a^{\dagger}(\vec{x}_) \ket{0}
\end{align*}
and use the Hamiltonian
\begin{align*}
H^{\text{QFT}}  
:=
\sum_{j=1}^{n} \left(
  - \frac{\hbar^{2}}{2m} \nabla_j^{2} + U(\vec{x}_j)
\right)
+ \sum_{j=1}^{n}\sum_{k=1}^{j-1}V(\vec{x}_j - \vec{x}_k)
\end{align*}

and the equivalence of the two theories is stated in the following theorem:
\begin{thm}[]
The $n$-particle wave function satisfies the Schrödinger equation if and only if the quantum-field theory version satisfies the qft analogous Schrödinger equation, i.e.
\begin{align*}
  i \hbar \frac{\del }{\del t} \psi_s^{\text{QM}} = H^{\text{QM}} \psi_s^{\text{QM}}
  \iff
  i \hbar \frac{\del }{\del t} \ket{\psi,t}_s^{\text{QFT}}
  = H^{\text{QFT}}
  \ket{\psi,t}_s^{\text{QFT}}
\end{align*}
\end{thm}
