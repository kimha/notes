\subsection{Comparison of Quantum Mechanics and Quantum field theory}
TODO: Merge with previous chapter, separate from Srednicki chapters.

In quantum mechanics, we described the state as an element in a Hilbert space.
For example, for an $n$-particle system, the state $\psi^{\text{QM}}$ is described as an element in ${L^{2}(\R^{3})}^{\otimes n}$.

To describe the $n$-particle system as a quantum field, one would write it by integrating all the excitations on the ground state $\ket{0}$ over the entire space:
\begin{align*}
  \ket{\psi,t}^{\text{QFT}} = \int d \vec{x}_1 \cdots d \vec{x}_n \psi(\vec{x}_{1}, \ldots, \vec{x}_{n},t) a^{\dagger}(\vec{x}_1) \cdots a^{\dagger}(\vec{x}_n) \ket{0}
\end{align*}
which lives in the space $\mathcal{H}^{\otimes \R^{3}}$which is constructed as follows:
\begin{itemize}
  \item For each point in space, let $\mathcal{H}$ be the hilbert space of a particle living in that point.
  \item Tensor all these spaces together. 
\end{itemize}


This might seem uneconomical, as we have ``more seats than students'', but this is a choice we have to make because as mentionned earlier, the number of particles is not invariant under Lorentz transformations.

Moreover, the fact that particles are a priori indistinguishable is more pronounced in the field theoretical description.
In ${L^{2}(\R^{3})}^{\otimes n}$, the states are not symmetric under exchange of particles

Note that general states in the quantum field theoretic description are \emph{not} of the form above.


\section{Srednicki Chapter 2 on Lorentz invariance}
\begin{table}[h]
\centering
\begin{tabular}{ll}
  3 vector & $\vec{x} = (x_i) = \begin{pmatrix}
  x_1\\
  x_2\\
  x_3
  \end{pmatrix}
  $
  \\
    4 vector & $x = (x^{\mu}) = \begin{pmatrix}
    x_0\\
    x_1\\
    x_2\\
    x_3
    \end{pmatrix}
    $
    \\
    4 covector
             &
             $z = (z_{\mu}) = \begin{pmatrix}
             z_0 & z_1 & z_2 & z_3
             \end{pmatrix}
             $
             \\
    metric tensor 
             &
             $g = (g_{\mu \nu}) = \begin{pmatrix}
             g_{00} & \cdots & g_{03}\\
             \vdots &  & \vdots\\
             g_{30} & \cdots & g_{33}
             \end{pmatrix}
             $
             \\
    inverse metric tensor
             &
             $(g^{\mu \nu}) = \begin{pmatrix}
             g^{00} & \cdots & g^{03}\\
             \vdots &  & \vdots\\
             g^{30} & \vdots & g^{33}
             \end{pmatrix}
             $
\end{tabular}
\caption{Comparison of notation for vectors, covectors and tensors}
\end{table}

The inverse metric tensor is defined such that
\begin{align*}
  g_{\mu \nu} g^{\nu \rho} = \delta_{\mu}^{\rho}
\end{align*}
where we use the Einstein summation convetion, where repeated indices are summed over.
(For example, one would write)
$
  (A \vec{x})^i = \tensor{A}{^{i}_{j}} x^{j}
$

\textbf{Raising and lowering indices:} Given $(x^{\mu})$, and $(z_{\mu})$, we define
\begin{align*}
  x_{\mu} := g_{\mu \nu} x^{\nu}
  \quad \text{and} \quad 
  z^{\mu} := g^{\mu \nu} z_{\nu}
\end{align*}

Given reference frames of Alice and Bob, we can consider some spacetime event $p$ and ask how the description of $p$ will differ, depending on if we ask Alice or Bob.

Alice will in her coordinate system use coordinates $x = x(p)$ to describe this point.
Bob will use coordinates $\overline{x} = \overline{x}(p)$.
In special relativity, there exists a Lorentz transformation $\tensor{\Lambda}{^\mu_\nu}$ such that for all spacetime events $p$
\begin{align*}
  \overline{x}^{\mu}(p) = \tensor{\Lambda}{^\mu_\nu} x^{\nu}(p)
\end{align*}
we say that four vectors (such as $x$) transform \textbf{contravariantly}.
Now, if Alice and Bob instead talk about a linear form $\omega$, they will use coordinates $z = z(\omega)$, and $\overline{z} = \overline{z}(\omega)$, it will transform
\begin{align*}
  \overline{z}_{\mu} = \tensor{\left(\Lambda^{-1}\right)}{^\mu_\nu} z_{\nu}
\end{align*}
which we call a \textbf{covariant} transformation.
From he transformation rule, it follows that
\begin{align*}
  x^{\mu} z_{\mu} = \overline{x}^{\mu}\overline{z}_{\mu}
\end{align*}
which means that a scalar is invariant under coordinate transforms.

Now, consider the worldline of some particle.
At each point on the worldline, it will have some tangent vector.
The \emph{coordinates} of the tangent vector can be shown to transform contravariantly, i.e.\ are vectors in the physical sense.
The coordinates of gradients of functions can be shown to transform covariantly, i.e.\ are covectors.

To describe both tangent vectors and gradients, we need derivatives.
We define the derivative
\begin{align*}
  (\del_{\mu}) := \begin{pmatrix}
  \del_0 & \del_1 & \del_2 & \del_3
  \end{pmatrix}
  :=
  \begin{pmatrix}
  \frac{\del }{\del x^{0}} & \frac{\del }{\del x^{1}} & \frac{\del }{\del x^{2}} & \frac{\del }{\del x^{3}}
  \end{pmatrix}
\end{align*}
By setting $(\overline{\del}_{\mu}) = \begin{pmatrix}
\frac{\del }{\del \overline{x}^{0}} & \frac{\del }{\del \overline{x}^{1}} & \frac{\del }{\del \overline{x}}^{2} & \frac{\del }{\del \overline{x}^{3}}
\end{pmatrix}$
, one can show that it transforms covariantly, i.e. $\overline{\del}_{\mu} = \tensor{(\Lambda^{-1})}{^\mu_\nu} \del_{\nu}$.
Hence it's called the \textbf{covariant derivative}.

Now let $v$ be a direction and $f: \R^{4} \to \R$ a function on. Then consider
\begin{align*}
  (v^{\mu} \del_{\mu}) f
  = v^{0} \frac{\del f}{\del x^{0}}
  + v^{1} \frac{\del f}{\del x^{1}}
  + v^{2} \frac{\del f}{\del x^{2}}
  + v^{3} \frac{\del f}{\del x^{3}}
\end{align*}
physically, this corresponds to the change in $f$ one gets be travelling along $v$.
This value should be independent on the choice of coordinatesystem. Indeed, by the transformation rule, one finds that
\begin{align*}
  \overline{v}^{\mu}\overline{\del}_{\mu} = v^{\mu} \del_{\mu}
\end{align*}

In order to get a nice theory, we want Alice and Bob to use the same units, which is to say that they agree on what a meter and what a second is.
Restricting to orthonormal spatial coordinate systems, one obtains the metric tensor
\begin{align*}
  (g_{\mu \nu}) = \begin{pmatrix}
  -1 &  &  & \\
   & 1 &  & \\
   &  & 1 & \\
   &  &  & 1
  \end{pmatrix}
\end{align*}

We now obtain a way to define the length of a vector.
\begin{align*}
  v^{2} := v^{\mu} g_{\mu \nu}v^{\nu} = v^{\mu} v_{\mu} = -(v^{0})^{2} + \sum_{i=1}^{3} (v^{i})^{2}
\end{align*}

Likewise, we can determine the square of a covector
\begin{align*}
  \del^{2} := \del_{\mu} g^{\mu \nu} \del_{\nu} = \del_{\mu} \del^{\mu} = -\left(\frac{\del }{\del x^{0}}\right)^{2} + \sum_{i=1}^{3} \left(\frac{\del }{\del x^{i}}\right)^{2}
\end{align*}
which turns out to be the d'Alembert operator $\square$.

This should not be confused with the application of a linear form $\del_{\mu}$ onto a vector $v^{\mu}$ (as in the calculation before).
The difference is that in the case $v^{2}$, we had to turn one vector into a covector by lowering the index, form which the $g_{\mu \nu}$ term comes.

The general rule for transformations of the metric is
\begin{align*}
  \overline{g}_{\mu \nu} = \tensor{(\Lambda^{-1})}{^{\rho}_{\mu}} \tensor{(\Lambda^{-1})}{^{\sigma}_{\nu}} g_{\rho \sigma}
\end{align*}
which in the case of special relativity (where we work in flat spacetime) has the result $\overline{g}_{\mu \nu} = g_{\mu \nu}$.

In the general case, the transformations that preserve the metric, are precisely those that satisfy
\begin{align}
  \label{eq:lorentz-transformation}
  g_{\rho \sigma} = \tensor{\Lambda}{^{\mu}_{\rho}} \tensor{\Lambda}{^{\nu}_{\sigma}} g_{\mu \nu}
\end{align}
the $\Lambda$ that satisfy this relation are called \textbf{Lorentz transformations}.

In QM, we looked at the infinitesimal generators of the grop of rotations $O(3)$ which lead us to study the Lie algebra $\mathfrak{su}(2)$ (among others).

It turns out that the collection of Lorentz transformations also forms a group, called the \textbf{Lorentz Group} $O(1,3)$ (in SRT).

An infinitesimal Lorentz transformation $I + \epsilon \del \omega$, can be written as
\begin{align*}
  \tensor{\Lambda}{^\mu_\nu} = \tensor{\delta}{^{\mu}_{\nu}}
  +
  \epsilon \tensor{\delta \omega}{^\mu_\nu}
\end{align*}
In $0$-th order, this satisfies~\ref{eq:lorentz-transformation}.
In first order, equation~\ref{eq:lorentz-transformation} requires
\begin{align*}
  0 &= g_{\mu \nu} \epsilon \tensor{\delta}{^{\mu}_{\rho}} \tensor{\delta}{^{\nu}_{\sigma}}
  + g_{\mu \nu} \tensor{\delta}{^{\mu}_{\rho}} \epsilon \tensor{\delta \omega}{^{\nu}_{\sigma}}
  \\
  \iff
  0 &= \delta \tensor{\omega}{_{\sigma \rho}}+ \delta \tensor{\omega}{_{\rho \sigma}}
\end{align*}
which is the case if and only if $\omega$ is antisymmetric.

Explicitly, the infinitesimal rotating Lorentz transformations are those of the form
\begin{align*}
  \delta \omega_{ij} = - \epsilon_{ijk} \hat{n}_k \delta \theta
\end{align*}
where $\hat{n}_k$ is the rotation axis and $\theta$ the rotation angle.

The infinitesimal Lorentz boosts are those of the form
\begin{align*}
  \delta \omega_{i0} = \hat{n}_i \delta \eta
\end{align*}
, where $\hat{n}_i$ describes the direction of the boost and $\eta \in \R$ is the rapidity.


