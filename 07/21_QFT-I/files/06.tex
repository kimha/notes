We should regard the canonical quantization as a procedure that starts with a classical field and then guess on how a consistent quantum field theory should look like.
Consistent in the sense that in the classical limit $\hbar \to 0$, one should recover the original field theory.

However, from a purely axiomatic viewpoint, one does not need to construct a quantum field theory \emph{from} a classical field theory.

\begin{table}[h]
\centering
\begin{tabular}{l|l}
  Classical Field
  &
  Quantum Field
  \\
  \hline
  $x \mapsto  \phi(x)$
  &
  $x \mapsto  \phi(x)$ (operator)
  \\
  classical reference frames
  &
  classical reference frames
  \\
  \parbox{5cm}{
  $\vec{x} \mapsto \phi(\vec{x})$, $\vec{x} \mapsto  \pi(\vec{x})$\\
  or more generally, distributions over the phase space $\Gamma$.
}
  &
  density operators $\rho$ on $\mathcal{H}$.
  \\
  generators of transformations $g = \vec{L},\vec{P},H$
  &
  $G = \vec{L},\vec{P},H$
  \\
  $\frac{d }{d \lambda}f_{\lambda} = \left\{
    f_{\lambda},g
  \right\}
  $
  &
  $\frac{d }{d \lambda}F_{\lambda} = \tfrac{i}{\hbar}\left[
    G,F_{\lambda}
  \right]$
\end{tabular}
\caption{Comparison of properties of classical and quantum field theories.}
\end{table}

In the classical field theory, a particle initially has four degrees of freedom (spacetime). 
The Lagrangian $\mathcal{L}$ determines the equations of motion, which, given some initial starting position (in spacetime) reduces the degrees of freedom to one, resulting in the particle's world line.

The classical reference frames are related by the Lorentz transformations $\Lambda,a$.

In quantum field theory, we have to deal with the fact that not all information is available at the same time.



We also assume that the reference frames are classical. This means that we assume that the coordinates behave as if the observer is a clasical object.
In the case that we have very small objects acting as reference frames (i.e.\ using very small rulers to measure distances) the quantum effects of those are not taken into account.
As a trade-off, we may keep using the classical transformation rules.




In classical field theory, the field $x \mapsto \phi(x)$ contains the same amount of information as the spatial part $\vec{x} \mapsto \vec{\phi}(\vec{x})$ together with its conjugate momentum $\vec{x} \mapsto  \pi(\vec{x}) = \frac{\del }{\del \dot{\phi}(x)}\mathcal{L}$.




The canonical tranformations on $\Gamma$ are those that preseve the symplectic form.
The unitary transformations are precisely those transformations on $\mathcal{H}$ that preserve the inner product.


In the classical case, consider the generators of those transformations $g = \vec{L},\vec{P},H$.
For a continuous transformation $f_{\lambda}$ parmetrized by $\lambda$, an infinitesimal transformation will move around all point in the phase space a bit.
This will define a flow on the phase space $\Gamma$ given by the Poisson bracket
\begin{align*}
  \frac{d }{d \lambda} f_{\lambda} = \left\{
    f_{\lambda},g
  \right\}
\end{align*}

In the quantum field theory, we have earlier found the generator $\tensor{M}{^\mu^\nu}$, from which we defined $\vec{L}$, for instance.
We have the generators $G = \vec{L},\vec{P},H$ and obtain the relation
\begin{align*}
  \frac{d }{d \lambda}F_{\lambda} = \tfrac{i}{\hbar} [G,F_{\lambda}]
\end{align*}
in the special case $G = H$, we obtain the Schrödinger equation
\begin{align*}
  F_{\lambda = t} =
  e^{\tfrac{i}{\hbar}Ht}
  F_{t = 0}
  e^{-\tfrac{i}{\hbar}Ht}
\end{align*}

This motivates the canonical quantization
\begin{align*}
  [A,B] = i \hbar \left\{
    A,B
  \right\}
\end{align*}


One thing that should worry us that the field $x \mapsto  \phi(x)$ is Lorentz invariance.
The compression state $\vec{x} \mapsto \phi(\vec{x}), \vec{x} \mapsto  \pi(\vec{x})$ depends on the time slice, which is observer dependent.

Now assume we start with the field $x \mapsto \phi(x)$, quantize it to get a quantum field theory.
Then we first look at the compression state, do a change of reference frame and then quantize it.
Do we get the same quantum field theory?

Recall that we chose the Lagrangian
\begin{align*}
  \mathcal{L} 
  \\
  &= - \tfrac{1}{2} \del^{\mu} \phi \del_{\mu} \phi - \tfrac{1}{2} m^{2} \phi^{2} + \Omega_0\\
  &= \tfrac{1}{2} \dot{\phi}^{2} - \tfrac{1}{2}(\nabla \phi)^{2} - \tfrac{1}{2} m^{2} \phi^{2} + \Omega_0
  \\
  \implies
  \pi(\vec{x})
  &= \frac{\del }{\del \dot{\phi}(\vec{x})}\mathcal{L} = \dot{\phi}(\vec{x})
\end{align*}
The \emph{Hamiltonian density} is generalized to
\begin{align*}
  \mathcal{H}
  &= \dot{\phi}(\vec{x}) \pi(\vec{x}) - \mathcal{L}(\vec{x})
  \\
  \implies \mathcal{H}[\phi,\pi]
  &= \tfrac{1}{2} \pi^{2} + \tfrac{1}{2} (\nabla \phi)^{2} + \tfrac{1}{2} m^{2} \phi^{2} - \Omega_0
\end{align*}


%In QM, we defined the Hilbert space in which most of our calculations happened.
%In QFT, 

We will now write a recipe on how to do canonical quantization:
\begin{enumerate}
  \item For each canonical coordinate $\vec{x}$, associate operators $\phi(\vec{x}), \pi(\vec{x})$ such that
    \begin{align*}
      [\phi(\vec{x}), \phi(\vec{x}')] 
      &= 0\\
      [\pi(\vec{x}), \pi(\vec{x}')]
      &= 0\\
      [\phi(\vec{x}), \pi(\vec{x}')]
      &= i \hbar \delta(\vec{x} - \vec{x}')
    \end{align*} 
  \item Take the completion of these the operators into a $C^{\ast}$-algebra $\mathcal{A}$. 
  \item Regard symmetries on the field theory as representation on $\mathcal{A}$.
    For example, for an infinitesimal translation by $\delta a$, then the representation on the algebra will be a transformation
    \begin{align*}
      W \mapsto  T(\delta a)^{-1}W T(\delta a)
    \end{align*}
    this will again imply other commutation relations. For example, we obtained the relation
    \begin{align*}
      \dot{\phi}(x) = \tfrac{i}{\hbar} [ H, \phi(x)]
    \end{align*}
    , where $H$ is the operator version of the classical operator of time translations.

    This allows us to express $\phi(x)$ for arbitrary $x = (t,\vec{x})$ in terms of $(t=0,\vec{x})$.
  \item Construct a hilbert space representation of $\mathcal{A}$.
    The idea is to express $H$ in terms of operators $a(\vec{k}), a^{\dagger}(\vec{k})$ such that Hamiltonian as the form
    \begin{align*}
      H = \int d \tilde{k} f(\vec{k}) a^{\dagger}\vec{k}) a(\vec{k}) + \ldots
    \end{align*}
    and such that the operators are raising and lowering operators in the sense that they must satisfy the commutator relations
    \begin{align*}
      [a(\vec{k}), a(\vec{k}')] 
      &= 0\\
      [a^{\dagger}(\vec{k}), a^{\dagger}(\vec{k}')] 
      &= 0\\
      [a(\vec{k}), a^{\dagger}(\vec{k}')]
      &= (2 \pi)^{3} 2 \omega \delta(\vec{k}-\vec{k}')
      \quad \text{for} \quad \omega = \sqrt{k^{2} + m^{2}}
    \end{align*}
    One such example would be
    \begin{align*}
      a(\vec{k}) = i \int d^{3} \vec{x} e^{- i \vec{k} \cdot \vec{x}} \del_0^{\leftrightarrow} \phi(\vec{x})
    \end{align*}

    Then, we construct $\mathcal{H}$ be defining the vacuume state $\ket{0}$ via $a(\vec{k})\ket{0} = 0$ and defining the number state (basis) via
    \begin{align*}
      \ket{k_1,\ldots,k_m,\ldots}
      = \text{const.\ }
      a^{\dagger}(\vec{k}_1) \cdots a^{\dagger}(\vec{k}_m) \cdots \ket{0}
    \end{align*}
    And then we consider its completion.

\end{enumerate}

There is another mathematical approach, called the GNS (Gelfand-Naimark-Segal) construction 

%So unlike QM, where most of the work is done in the Hilbert space, the fundamental object of study is the $C^{\ast}$-algebra $\mathcal{A}$.

\section{The LSZ reduction formula}
For the creation operator
\begin{align*}
  a^{\dagger}(\vec{k}) := - i \int d^{3} \vec{x} e^{i \vec{k} \cdot \vec{x}} \stackrel{\leftrightarrow}{\del_0} \phi(x)
\end{align*}
let $\ket{\vec{k}}:= a^{\dagger}(\vec{k})\ket{0}$ be the state with one particle with momentum $k$.

Of course, we can call $\ket{\vec{k}}$ a ``particle with momentum $\vec{k}$'', but that doesn't mean that it will \emph{behave} like such a particle.

This chapter will help us see that it does indeed behave that way.
Since the vacuume state $\ket{0}$ has unit norm, one finds that
\begin{align*}
  \braket{\vec{k}|\vec{k}'} = (2 \pi)^{3} 2 \omega d(\vec{k} - \vec{k}')
  \quad \text{for} \quad 
  \omega = \sqrt{\vec{k}^{2} + m^{2}}
\end{align*}

Since $a^{\dagger}(\vec{k})$ is not necessarily time dependent, we define the initial state $\ket{i}$ to be
\begin{align*}
  \ket{i} := \lim_{t \to - \infty} a^{\dagger}_1(t) a^{\dagger}_2(t) \ket{0}
\end{align*}

