\subsection{Path integrals in QM}
In the LSZ formula, we would like to compute the terms to modell an interacting quantum field theory.
\begin{align*}
  \braket{f|i}
  =
  i^{n+n'}
  \int d^{4}x_1 e^{ik_1 x_1} ( -\del_1^{2} + mh^{2})
  \ldots
  \int d^{4}x_1 e^{ik_1 x_1} ( -\del_1^{2} + mh^{2})
  \ldots
  \times
  \underbrace{
  \braket{0|T \phi(x_1) \cdots \phi(x_1') \cdots|0}
}_{\text{This is what we want to calculate}}
\end{align*}

Our starting point is the path integral formula
\begin{align}
  \braket{q'',t''|q',t'} 
  =
  \int \mathcal{D}_q \mathcal{D}_p
  \exp
  \left[
    i \int_{t'}^{t''}
    dt \left(
      p(t) \dot{q}(t) - H(p(t),q(t))
    \right)
  \right]
  \label{eq:path-integral-formula}
\end{align}
which, when adding operators into the mix becomes
\begin{align*}
  \braket{q'',t''|T Q(t_1) \cdots P(t_n) \cdots|q',t'}
  =
  \int \mathcal{D}_q \mathcal{D}_p
  q(t_1) \cdots p(t_n) \cdots
  \exp
  \left[
    i \int_{t'}^{t''}
    dt p \dot{q} - H
  \right]
\end{align*}

One of the tricks we will use is that of the \textbf{functional derivative}.

Recall that for $L(x_{1}, \ldots, x_{n})$ a function in $n$ variables, one can define the partial derivative $\frac{\del }{\del x_i}L(x_{1}, \ldots, x_{n})$.
For example
\begin{align*}
  L(x_{1}, \ldots, x_{n}) = x_j \implies \frac{\del }{\del x_i}L = \delta_{ij}
\end{align*}

To do this for functions $L[f]$ defined on a continuous set of variables (i.e.\ $f: \R \to \R$, where each $f(t)$ is to be viewed as a variable which $L$ depends on), we define the functional derivative $\frac{\delta}{\delta f(t)}L$ such that one has
\begin{align*}
  L[f] := f(t') \implies
  \frac{\delta}{\delta f(t)}L[f] = \delta(t -t')
\end{align*}
This functional derivative will then satisfy the chain rule: For $g$ a composable function, one has
\begin{align*}
  \frac{\delta}{\delta f(t)} g(L[f]) = g'(t) \cdot \frac{\delta}{\delta f(t)}L[f]
\end{align*}

An important example will be
\begin{align*}
  \frac{\delta }{\delta f(t_1)}
  \underbrace{\exp}_{g} \left[
    \int_{t'}^{t''} dt f(t) q(t)
  \right]
  &=
  \underbrace{\exp \left[
    \ldots
  \right]}_{g'(L)}
  \cdot \frac{\delta }{\delta f(t_1)}
  \int_{t'}^{t''} dt f(t) q(t)
  \\
  &= \exp[\ldots]
  \int_{t'}^{t''} dt q(t) \delta(t_1-t)
  \\
  &= \exp[\ldots] q(t_1)
\end{align*}
The reason this is useful becomes clear when we want to adjust the Hamiltonian by adding additional forces $f,h$,
\begin{align*}
  H(p,q) \mapsto  H(p,q) - f(t) q(t) - h(t) p(t)
\end{align*}
the path integral formula~\ref{eq:path-integral-formula} will change accordingly:
\begin{align*}
  \frac{1}{i} \frac{\delta }{\delta f(t_1)}
  \cdots
  \frac{1}{i} \frac{\delta }{\delta f(t_n)}
  \cdots
  \braket{q'',t''|q',t'}_{f,n}
  =
  \int \mathcal{D}_p \mathcal{D}_q q(t_1)\cdots p(_n)
  \exp \left[
    i \int_{t'}^{t''} dt \left(
      p \dot{q} - H + fq + hp
    \right)
  \right]
\end{align*}
and when adding operators, we get
\begin{align*}
  \braket{q'',t''|TQ(t_1) \cdots P(t_n) |q',t'}_{f,n}
  =
  \frac{1}{i} \frac{\delta }{\delta f(t_1)}
  \cdots
  \frac{1}{i} \frac{\delta }{\delta f(t_n)}
  \cdots
  \braket{q'',t''|q',t'}_{f,h}|_{f,h = 0}
\end{align*}
So now instead of having to deal with additional operators, we only have to deal with new states (and some force factors).

Note that now, the new Hamiltonian $H = fq - hp$ might be time dependent.


Now we want to compute the ground state correlation functions.
For this, we will denote by $H$ the original Hamiltonian without the forces $f,h$, and we will write $H_{f,h}$ for the Hamiltonian with non-zero $f,h$.
In order for the coming mathematical steps to work out, we will also assume that we only consider functions $f,h$ with finite support, in particular $f,h \stackrel{t \to  \pm \infty}{\to}0$.

The states that we are interested in are the stable ground states $\ket{0,-\infty}, \ket{0,+ \infty}$.
In the Heisenberg picture, we denote by $\ket{q,t}$ the state at time $0$, such that at time $t$, the system evolves to the state $\ket{q}$. In the case of a time-independent hamiltonian, this will be $\ket{q,t} = e^{iHt}\ket{q}$.

In particular, for $\ket{0}$ the ground state of the original Hamiltonian $H$, one has
\begin{align*}
  \ket{0,t}_{f,h}
  &=
  \exp \left[
    i \int_0^{t}dt' H_{f,h}(t')
  \right]
  \ket{0}
  \\
  &= \exp[\ldots]
  \int dq' \ket{q'} \Psi_0(q')
  \\
  &=
  \int dq' \ket{q',t}_{f,h}
  \Psi_0(q')
\end{align*}
, where $\Psi_0(q') = \braket{q'|0}$ is the ground state wave function at $q'$.


In the ground state as both final and initial state, we take the limit $t' \to -\infty$ and $t'' \to \infty$:
\begin{align*}
  \braket{0,\infty|0,-\infty}_{f,h}
  =
  \lim_{\stackrel{t'' \to \infty}{t' \to -\infty}}
  \braket{0,t''|0,t'}_{f,h}
\end{align*}
which we then can insert into the previous formula.
The problem with this approach is that we don't yet know the ground state wave function.

To work around this, we employ the following trick. We replace the Hamiltonian with $H \mapsto (1 - i \epsilon)H$, take the limit over $t'$ and after that take the limit $\epsilon \to 0$.

\begin{center}
Disclaimer: I don't fully understand this part.
(see Srednicki Page 61 for details)
\end{center}
\begin{align*}
  \lim_{t' \to -\infty} \braket{q',t'}_{f,h} 
  &= \lim_{t \to - \infty} \exp
  \left[
    i (1 - i \epsilon) \int dt H_{f,h}(t)
  \right]
  \ket{q'}
  \\
  &= \lim_{t' \to -\infty}
  \exp \left[
    i \int dt H_{f,h}(t)
  \right]
  \exp \left[
    \epsilon \int dt H_{f,h}(t)
  \right]
  \ket{q'}
\end{align*}
For $\ket{n}$ an Eigenvector of $H$ with eigenvalue $E_n$, we get
\begin{align*}
  \lim_{t' \to -\infty} \braket{q',t'}_{f,h} 
  &= \lim_{t' \to -\infty}
  \exp [\ldots]
  e^{\epsilon H t'} \ket{q'}
  \\
  &= \lim_{t' \to -\infty}
  \exp [\ldots]
  e^{\epsilon H t'} 
  \sum_{n=0}^{\infty} \bra{n} \ket{n} \ket{q'}
  \\
  &= \lim_{t' \to -\infty}
  \exp [\ldots]
  \sum_{n=0}^{\infty} \braket{n|q'} e^{\epsilon E_n t'} \ket{n}
  \\
  &= \lim_{t' \to -\infty}
  \exp [\ldots]
  \left(
    \braket{0|q'} \ket{0}
    +
    \sum_{n=1}^{\infty} \braket{n|q'} e^{\epsilon E_n t'} \ket{n}
  \right)
\end{align*}
We can assume that $E_0 = 0$ by shifting $H$ by a constant, if necessary.
So for all $n > 0$, the exponential term with $t'$ in it will go to $0$ as $t' \to -\infty$, so all non-ground state terms will vanish and we are left with
\begin{align*}
  \lim_{t' \to -\infty} \braket{q',t'}_{f,h} 
  &=
  \braket{0|q'} \lim_{t' \to - \infty} \ket{0,t'}_{f,h}
  \\
  &= \braket{0|q'} \ket{0}_{f,h}
\end{align*}
analogously one finds
\begin{align*}
  \lim_{t'' \to \infty} \bra{q'',t''} = \braket{q''|0} \bra{0}_{f,h}
\end{align*}
Combining these two results together, one ends up with
\begin{align*}
  \braket{0|0}_{f,h}
  = \text{Const.}
  \lim_{\stackrel{t' \to - \infty}{t'' \to \infty}}
  \braket{q'',t''|q',t'}_{f,h}
\end{align*}
The normalization constant can be calculated to be $\frac{1}{\braket{0|q'} \braket{0|q''}}$.

With the path integral formula, one obtains
\begin{align*}
  \braket{0|0}_{f,h}
  =
  \int \mathcal{D}_p \mathcal{D}_q
  \exp
  \left[
    i \int_{- \infty}^{\infty}
    dt (p \dot{q} - (1 - i \epsilon)(H - fq - hp))
  \right]
\end{align*}
since $f$ and $h$ only have finite support and $t \to  \pm \infty$, we have
\begin{align*}
  \int_{- \infty}^{\infty} (1 - i \epsilon)(fq + hp) \to  \int_{-\infty}^{\infty} dt fq + hp
\end{align*}
so the ground state will satisfy
\begin{align*}
  \braket{0|0}_{f,h}
  =
  \int \mathcal{D}_p \mathcal{D}_q \exp \left[
    i \int _{-\infty}^{\infty}
    (p \dot{q} - (1 - i \epsilon) H + fq + hp)
  \right]
\end{align*}
